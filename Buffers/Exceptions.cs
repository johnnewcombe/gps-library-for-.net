using System;

namespace Waymex.Buffers
{
	/// <summary>
	/// Base Exception Class.
	/// </summary>
#if(PUBLIC)
	public class ExceptionBase : System.ApplicationException
#else
	internal class ExceptionBase : System.ApplicationException
#endif
	{
		private string m_AppSource = "";

		/// <summary>
		/// GISSystem Exception.
		/// </summary>
		public ExceptionBase(string Message,Exception e):base(Message, e)
		{
		}
		/// <summary>
		/// GPSSystem Exception.
		/// </summary>
		public ExceptionBase(string Message):base(Message)
		{
		}
		// We only want this method exposed to code
		// in the same assembly. However, we might need to 
		// change the scope if this class were in another
		// assembly.
		internal void LogError()
		{
			System.Diagnostics.EventLog e;
			e = new System.Diagnostics.EventLog("Application");
			e.Source = this.AppSource;
			e.WriteEntry(this.Message, System.Diagnostics.EventLogEntryType.Error);
			e.Dispose();
		}

		/// <summary>
		/// Exposed in order that derived classe can overide.
		/// </summary>
		public virtual string AppSource
		{
			get
			{
				return m_AppSource;
			}
		}																	
	}
         /// <summary>
    /// BufferException Exception.
    /// </summary>
#if(PUBLIC)
   public class BufferException : ExceptionBase
#else
   internal class BufferException : ExceptionBase
#endif
    {
        /// <summary>
        /// BufferException Exception contructor.
        /// </summary>
        public BufferException(string Message) : base(Message) { }
        /// <summary>
        /// BufferException Exception contructor.
        /// </summary>
        public BufferException(string Message, Exception e) : base(Message, e) { }
    }

}
