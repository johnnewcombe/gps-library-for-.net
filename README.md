# Project Description #

The GPS Library is a single .Net 4.0 Assembly designed to provide a simple object based interface to both the Garmin and Magellan range of GPS devices. In addition the GPS Library will support NMEA devices.

The GPS Library was originally sold as the Waymex GPS Library for .Net. Waymex IT Ltd no longer exists and the rights to this software now lie with me, the original author.

__Please not that the code here has been forked by Nerzhul and is available as a NuGet package [here](https://www.nuget.org/packages/GPS.Net/).__

The code was originally written in VB6, ported to .Net 1.1 and then converted from VB.net to C#. Please bear in mind that all of this happened before Generics, Linq, Lambdas and all the other features we know and love today. In addition, at the time it was written, Patterns and Practices were not what they are now. The code is not representative of my current work, so please be kind. Having said all of that, the product has performed well for many customers over many years.

Enjoy!


__Note:__
Some portions of the code were supplied by US Trail Maps in an informal code exchange agreement. All copyright notices have been left intact.