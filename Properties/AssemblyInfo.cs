using System;
using System.Security.Permissions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

//---------------------------------------------------------------------------------------
//  DESCRIPTION:
//
//  TODO:
//
//          The ACK\NAK handling in the ReceiveData and Transmit Data could
//          do with being enhanced to allow for re-sends etc.
//
//HISTORY:
//---------------------------------------------------------------------------------------
//  Date        Ver         Intls   Comment
//---------------------------------------------------------------------------------------
//  30.09.2002  2.0.0       JLN     Version 2 implemented in VB.Net based.
//                                  Combase (Abstract) and GPSSerial classes added. Fix for
//                                  D150 and D450 implemented.
//  25.11.2002  2.0.1100    JLN     Added Trial Version Conditional compilation stuff.
//  09.11.2002  2.1.0       JLN     Switched to using SerialBase (Commbase).
//                                  Left the thread sync stuff out.
//  2.12.2002	2.2.0.n		JLN     Updated SerialBase commbase 1.2. Added Dispose methods
//									to GPSInterface and Link to allow explicit
//                                  release of serial port.
//  19.01.2003  2.2.1.n     JLN     Waypoint D104 and D105 fixed.
//  13.03.2003  2.2.2.n     JLN     Waypoint D109 fixed.
//  13.04.2003  2.2.3.n     JLN     Fixed A201 with multiple routes.
//  01.06.2003  2.2.4.n     JLN     PortTimeout Property Added.
//  25.06.2003  2.2.5.n     JLN     Fixed error in CreateWaypoint Array for D151, D152,
//                                  D154, D155 where the fields were being converted from
//                                  Single rather than String.
//                                  Added explicit type conversion for altitude.
//--------------------------------------------------------------------------------------
//  06.06.2003  2.3.1.n		JLN     Split from VB.Net GPS Library.Net branch and
//									converted to C#.
//  03.07.2003  2.3.2.n     JLN     Added default attribute settings for D108 and D109.
//                                  Removed full stops from all error messages.
//									Added soap stuff, removed caching and created objects
//									for position and product info.
//	01.11.2004	2.5.0.n		JLN		C# Version released.
//	28.01.2004	2.5.1.n		JLN		GetDate function fixed to return correct Month
//									and hour.
//	23.02.2004	2.6.0.n		JLN		Shape File Support Added. And full stops put back on
//									error messages. 
//	21.03.2004	2.6.1.n		JLN		Utm Shape File support added.
//	10.08.2004	2.6.2.n		JLN		Removed call that passed the Time to the
//									DegreesToSemicircles function. Caused an overflow
//									error.
//	18.08.2004	2.6.3.n		JLN		Added new protocols as per Feb 2003 spec including
//									Lap and Flightbook stuff.
//	10.11.2004	2.6.4.n		JLN		Fixed forerunner Tracklog Bug in CreateTrackpoint.
//	24.09.2004	2.6.5.n		JLN		Overloaded Set and Get Tracks to handle multiple
//									segments.
//	01.01.2005	2.7.0.n		JLN		Added USB Transport. Made Link internal and 
//									added PortOpen/PortClose methods to Device class.
//									Modified SaveToShape for Waypoints to put the X,Y
//									values into the DB rather than Lat Long.
//	21.02.2005	2.7.1.n		JLN		Added Bulk In suppport for 276 type devices.
//	04.05.2005	2.7.2.n		JLN		Enhanced code for Opening USB Port.
//									Added support for D303.
//									Added developer notes to help file.
//	27.06.2005	2.7.3.n		JLN		Added CharactersetRules property. Improved USB open
//									routines for GetProductData.
//	06.07.2005	2.7.4.n		JLN		Used CharacterSet Rules when creating Shape
//									Files.
//	06.07.2005	2.7.5.n		JLN		Added thread sleep to CloseUSB function. Added 
//									InvariantCulture to the creation of ShapeFiles.
//	21.09.2005	2.7.6.n		JLN		Fixed bug that stopped Bulk In working in some
//									devices.
//	21.09.2005	2.7.7.n		JLN		Overriden event delegate in GPSSerial class no
//									longer throws receive thread exceptions. The exception
//									is now stored and is Thrown on the calling thread. This
//									change fixes the un-trappable Framing Error that can occur
//									when using Garmin protocol to try and access an NMEA
//									device.
//	20.11.2005	2.7.8.n		JLN		Fixed bug causing incorrect offset value in index file of
//									ESRI Shape file when multiple parts are are created.
//	28.11.2005	2.7.9.n		JLN		Implemented various potential issues, many thanks to Benji
//									Yeager of Acuit Development.
//--------------------------------------------------------------------------------------
//	22.09.2005	3.0.0.n		JLN		Split from Release 2 with namespace changes and
//									NMEA functionality added.
//	13.04.2006	3.0.1.n		JLN		Default Attribute Value for D110 Waypoints changed
//                                  to 128 (80h). Attribute property now overrides default.
//	11.05.2006	3.0.2.n		JLN		Maintenance Release USB code improved.
//	13.05.2006	3.0.3.n		JLN		Trial option added to Personal Edition.
//	25.05.2006	3.0.4.n		JLN		Reworked MagellanDevice Port open to allow for
//									serial resources not being released.
//	30.05.2006	3.0.5.n		JLN		Added code to fix progress value issue on magellan
//									devices that don't report waypoint count.
//	01.06.2006	3.0.6.n		JLN		Added 30 day trial option and updated error 
//									message text.
//	24.07.2006	3.0.7.n		JLN		Updated protocol D303 and added protocol D304.
//									Fixed GGA sentence discrepencies.
//									Added Symbol/Icon enumeration support.
//	24.08.2006	3.1.0.n		JLN		MegallenDevice updated to raise "Device Not Responding"
//                                  application error. Port Open validation removed (port 
//                                  enumerated). Port Enumerator increased to 32 ports.
//                                  Usb Routines rewritten.
//  09.10.2006  3.1.1.n     JLN     Added fixed bug in Tracelog that prevented tracing in 
//                                  some scenarios. Added new UsbCode (Benji Teagers version)
//  19.10.2006  3.1.2       JLN     Reverted to original Usb code after beta testing due
//                                  to timeout errors. Code left in place. UsbTransport updated
//                                  to allow easy switch between old usb and new usb using
//                                  compiler switch (USB30).
//                                  Fixed D108 which was preventing the upload of waypoints.
//  31.10.2006 3.1.3        JLN     Removed throw from PVTData thread.
//  12.11.2006 3.1.4        JLN     Added support for D1001 and D1011 Lap types.
//  29.11.2006 3.1.5        JLN     Fixed PID issues associated with GetLap functions.
//	12.12.2006 3.1.6		JLN		Added the GetUnitId method.
//	21.12.2006 3.1.7		JLN		GetTracklogs changed to return empty collection where
//									no tracklog exists. Previously threw an exception.
//  31.03.2007 3.1.8        JLN     Interop.ManagedMethods class added to marshal and handle all
//                                  P/Invoke calls. Now works with Vista.
//  19.04.2007 3.1.9        JLN     D1015 Lap protocol added.
//--------------------------------------------------------------------------------------
//  12.11.2007 3.2.0        JLN     Split from .Net 1.1 version. KML Support Added.
//  19.12.2007 3.2.1        JLN     Refactoring of Gis.Convert and Gis.Transform, exposing 
//                                  datum and projection conversion and transformamtion methods.
//                                  Addition of OSTN02 methods and British National Grid.
//  14.01.2008 3.2.2        JLN     Fix to GetTracklogs to correctly populate Track headers
//                                  on multiple track logs for devices with non D310 protocols.
//  14.07.2008 3.2.3        JLN     Assembly signed.
//  04.11.2008 3.2.4        JLN     Check for a blank UTC time in FormatTime base method added 
//                                  changes in 3.2.4 removed.
//  16.12.2008 3.2.031216   JLN     Versioning changed to standard Major, Minor, Build (Date), release.
//                                  using AssemblyInfoAutoUpdate utility.
//  14.12.2009 3.2.041214   JLN     Fix to Sentence.FormatTime, now handles empty strings better.
//--------------------------------------------------------------------------------------
//Version information for an assembly consists of the following four values:
//
//      Major Version    'incompatible changes
//      Minor Version    'incompatible changes  e.g. method parameter functions
//      Build Number     'compatible changes e.g. fixes new functions
//      Revision         '
//
//You can specify all the values or you can default the Build and Revision Numbers 
//by using the '*' as shown below:

//**** REMEMBER ****
// if a minor version change is done then Waymex.GpsLibrary.Setup and the constants (PRODUCT_VERSION) in deviceBase and NMEADevice classes needs to be changed also

[assembly: AssemblyVersion("3.2.000127.80")]


[assembly: AssemblyTitle("Waymex.GpsLibrary.dll")]
[assembly: AssemblyDescription("GPS Library for .Net. Please Note that this is NOT a Garmin or Magellan product. Garmin or Magellan are not in anyway responsible for this library. All trademarks and Copyrights have been respected.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Waymex IT Ltd")]

[assembly: AssemblyProduct("GPS Library for .Net (.Net 4.0)")]

//#if(TRIAL)
//#else
//#endif


[assembly: AssemblyCopyright("Waymex IT Ltd 2004, 2005, 2006, 2007, 2008, 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
//      1. Delay sign the assembly during development. This is done by embedding
//      two custom attributes into your assembly. For C#, you would include the 
//      following lines into AssemblyInfo.cs:
//      [assembly:AssemblyKeyFileAttribute("keyfile.snk")]
//      [assembly:AssemblyDelaySignAttribute(true)]
//
//      Where keyfile.snk is the name of the file containing your public key.
//
//      2. Use the strong name tool (sn.exe) that comes with the .NET Framework
//      to turn off the strong name verification while you are testing your assembly:
//
//      sn -Vr TestAsm.exe
//
//      3. Obfuscate the delay signed assembly using Dotfuscator.
//
//      4. After running through Dotfuscator turn on the verification for the
//      obfuscated assembly using sn.exe. This unregisters the dotfuscated assembly
//      for verification skipping:
//
//      sn -Vu TestAsm.exe
//
//      5. Now complete the signing process of the dotfuscated assembly:
//
//      sn -R TestAsm.exe keyfile.snk
//
//      Where keyfile.snk is the name of the file containing your private key. 

//[assembly: AssemblyKeyFile("C:\\Working\\Waymex\\StrongNameKey\\sgKey.snk")]
//[assembly: AssemblyDelaySignAttribute(true)]
//[assembly: AssemblyKeyName("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

//security
[assembly: SecurityPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
//[assembly: PermissionSet(SecurityAction.RequestOptional, Name = "Nothing")]
//[assembly: IsolatedStorageFilePermission(SecurityAction.RequestMinimum, UserQuota = 1048576)]
//[assembly: SecurityPermission(SecurityAction.RequestRefuse, UnmanagedCode = false)]
//[assembly: FileIOPermission(SecurityAction.RequestOptional, Unrestricted = true)]
