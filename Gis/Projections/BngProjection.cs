using System;
using System.Collections.Generic;
using System.Text;

namespace Waymex.Gis.Projections
{
    /// <summary>
    /// This immutable class, derived from Projection, represents the British National Grid projection and is provided for convienience.
    /// </summary>
    public class BngProjection : Projection
    {
        private const double SCALE_FACTOR = 0.9996012717;
        private const double E = 400000;
        private const double TO_LAT = 49;
        private const double TO_LONG = -2;
        private const double N_NH = -100000;

        /// <summary>
        /// Constructor.
        /// </summary>
        public BngProjection()
            : base(SCALE_FACTOR, E, N_NH, TO_LAT, TO_LONG)
        {
        }
    }
}
