using System;
using System.Collections.Generic;
using System.Text;

namespace Waymex.Gis.Coordinates
{
    /// <summary>
    /// This immutable class, derived from EastingNorthingCoordinates, provides a convenient means 
    /// to represent OSGB36 eastings and northings.
    /// </summary>
    /// <remarks>
    /// Eastings and northings are represented in British National Grid and Height is specifgied
    /// in meters based on the geoid datum returned by the RegionGeoidDatum property.
    /// </remarks>
    public class Osgb36Cordinates : EastingNorthingCoordinates
    {
        Osgb36GeoidDatum _datum = Osgb36GeoidDatum.OutsideModelBoundary;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="easting"></param>
        /// <param name="northing"></param>
        /// <param name="height"></param>
        /// <param name="datum"></param>
        public Osgb36Cordinates(double easting, double northing, double height, Osgb36GeoidDatum datum)
            : base(easting, northing, height)
        {
            _datum = datum;
        }
        /// <summary>
        /// Returns the Local Geoid datum in use. other property values should be 
        /// considered in valid if this property is set to OutsideModelBoundary.
        /// </summary>
        public Osgb36GeoidDatum RegionGeoidDatum
        {
            get { return _datum; }
        }
    }
}
