using System;

namespace Waymex.Gis.Ellipsoids
{

    /// <summary>
    /// This immutable class, derived from coordinates, represents an Airy1930 ellipsoid and is provided for convienience.
    /// </summary>
    public class Airy1830Ellipsoid : Ellipsoid
	{
		private const double  C_SEMI_MAJOR_AXIS          = 6377563.396;             //a
		private const double  C_SEMI_MINOR_AXIS          = 6356256.910;              //b


        /// <summary>
        /// Constructor.
        /// </summary>
        public Airy1830Ellipsoid()
            : base(C_SEMI_MAJOR_AXIS, C_SEMI_MINOR_AXIS)
        {
        }

	}
}

