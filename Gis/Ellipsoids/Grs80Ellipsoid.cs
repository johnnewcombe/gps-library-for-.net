using System;

namespace Waymex.Gis.Ellipsoids
{
    /// <summary>
    /// This immutable class, derived from coordinates, represents an GRS80 ellipsoid and is provided for convienience.
    /// </summary>
    public class Grs80Ellipsoid : Ellipsoid
    {
        //WGS constants
        private const double C_SEMI_MAJOR_AXIS = 6378137;             //a
        private const double C_SEMI_MINOR_AXIS = 6356752.314;         //b

        /// <summary>
        /// Consructor.
        /// </summary>
        public Grs80Ellipsoid()
            : base(C_SEMI_MAJOR_AXIS, C_SEMI_MINOR_AXIS)
        {
        }

    }
}

