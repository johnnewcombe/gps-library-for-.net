using System;

namespace Waymex.Gis
{
	/// <summary>
	/// Base Exception Class.
	/// </summary>
#if(PUBLIC)
	public class ExceptionBase : System.ApplicationException
#else
	internal class ExceptionBase : System.ApplicationException
#endif
	{
		private string m_AppSource = "";
			
		/// <summary>
        /// Base Exception Class Constructor.
		/// </summary>
		public ExceptionBase(string Message,Exception e):base(Message, e)
		{
		}
		/// <summary>
        /// Base Exception Class Constructor.
		/// </summary>
		public ExceptionBase(string Message):base(Message)
		{
		}
		// We only want this method exposed to code
		// in the same assembly. However, we might need to 
		// change the scope if this class were in another
		// assembly.
		internal void LogError()
		{
			System.Diagnostics.EventLog e;
			e = new System.Diagnostics.EventLog("Application");
			e.Source = this.AppSource;
			e.WriteEntry(this.Message, System.Diagnostics.EventLogEntryType.Error);
			e.Dispose();
		}

		/// <summary>
		/// Exposed in order that derived classe can overide.
		/// </summary>
		public virtual string AppSource
		{
			get
			{
				return m_AppSource;
			}
		}																	
	}
	/// <summary>
	/// Attrribute Exception Class.
	/// </summary>
#if(PUBLIC)
	public class AttrributeException : ExceptionBase
#else
	internal class AttrributeException : ExceptionBase
#endif
	{
		/// <summary>
        /// Attrribute Exception Class Constructor.
		/// </summary>
		public  AttrributeException(string Message) : base(Message){}
		/// <summary>
        /// Attrribute Exception Class Constructor.
        /// </summary>
		public  AttrributeException(string Message, Exception e) : base(Message, e){}
	}
	/// <summary>
	/// Shape Exception Class.
	/// </summary>
#if(PUBLIC)
	public class ShapeException : ExceptionBase
#else
	internal class ShapeException : ExceptionBase
#endif
	{
		/// <summary>
        /// Shape Exception Class Constructor.
        /// </summary>
		public  ShapeException(string Message) : base(Message){}
		/// <summary>
        /// Shape Exception Class Constructor.
        /// </summary>
		public  ShapeException(string Message, Exception e) : base(Message, e){}
	}
	/// <summary>
    /// Shape Type Exception Class.
    /// </summary>
#if(PUBLIC)
	public class ShapeTypeException : ExceptionBase
#else
	internal class ShapeTypeException : ExceptionBase
#endif
	{
		/// <summary>
        /// Shape Type Exception Class Constructor.
        /// </summary>
		public  ShapeTypeException(string Message) : base(Message){}
		/// <summary>
        /// Shape Type Exception Class Constructor.
        /// </summary>
		public  ShapeTypeException(string Message, Exception e) : base(Message, e){}
	}

}
