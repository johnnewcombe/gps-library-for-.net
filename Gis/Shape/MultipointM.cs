using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for MultipointM.
	/// </summary>
#if(PUBLIC)
	public class MultiPointM : Waymex.Gis.Shape.ShapeBase
#else
	internal class MultiPointM : Waymex.Gis.Shape.ShapeBase
#endif
	{
		private double[]	m_dblBox = new double[4]; 		// Bounding Box
		private int			m_intNumPoints = 0; 			// Number of Points
		private Point[]		m_objPoints = null;				// The Points in the Set [NumPoints]
		private double[]	m_dblMRange = new double[2];	// Bounding Measure Range
		private double[]	MArray = null;					// Measures [NumPoints]

		public MultiPointM()
		{
			m_enuType = ShapeBase.ShapeType.ShapeTypeMultiPointM;
		}
	}
}
