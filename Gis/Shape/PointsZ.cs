using System;

namespace Waymex.Gis.Shape
{
#if(PUBLIC)
	public class PointsZ : System.Collections.IEnumerable
#else
	internal class PointsZ : System.Collections.IEnumerable
#endif
	{
					
		private System.Collections.ArrayList m_colPoints; 

		public PointsZ() : base()
		{
			m_colPoints = new System.Collections.ArrayList();
		}

		internal void Add(Waymex.Gis.Shape.PointZ Point)
		{
			try
			{
				m_colPoints.Add(Point);
			}
			catch (Exception e)
			{
				throw new ShapeException(e.Message, e);
			}
		}

		public int Count
		{
			get
			{
				try
				{
					return m_colPoints.Count;
				}

				catch (Exception e)
				{
					throw new ShapeException(e.Message, e);
				}
			}
		}

		public PointZ this[int Index]
		{
			get
			{
				return (PointZ)m_colPoints[Index];
			}

		}

		/// <summary>
		/// Returns an Enumerator for the Collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colPoints.GetEnumerator();
		}
	}
}
