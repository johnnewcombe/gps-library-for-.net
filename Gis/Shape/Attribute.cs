using System;
using System.Globalization;

namespace Waymex.Gis.Shape
{
#if(PUBLIC)
	public class Attribute
#else
	internal class Attribute
#endif
	{
		
		private string 		m_strName = "";
		private string		m_strValue = "";
		private DataType	m_enumType;
		
		public enum DataType
		{
			Integer = 1,
			Single = 2, 
			Double = 3,
			String = 4
		}

		public Attribute(string Name, int Value)
		{
			Populate(Name,Value.ToString(CultureInfo.InvariantCulture),DataType.Integer);
		}
		public Attribute(string Name, double Value)
		{
			Populate(Name,Value.ToString(CultureInfo.InvariantCulture),DataType.Double);
		}
		public Attribute(string Name, float Value)
		{
			Populate(Name,Value.ToString(CultureInfo.InvariantCulture),DataType.Single);
		}
		public Attribute(string Name, string Value)
		{
			Populate(Name,Value.ToString(CultureInfo.InvariantCulture),DataType.String);
		}
		private void Populate(string Name, string Value, DataType Type)
		{
			m_strName = Name;
			m_strValue = Value;
			m_enumType = Type;
		}
		public string Name
		{
			get
			{
				return m_strName;
			}
		}
		public DataType Type
		{
			get
			{
				return m_enumType;
			}
		}
		public override string ToString()
		{
				return m_strValue;
		}
	}
}