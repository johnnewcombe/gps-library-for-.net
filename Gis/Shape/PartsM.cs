using System;

namespace Waymex.Gis.Shape
{
#if(PUBLIC)
	public class PartsM : System.Collections.IEnumerable
#else
	internal class PartsM : System.Collections.IEnumerable
#endif
	{
					
		private System.Collections.ArrayList m_colParts; 

		public PartsM() : base()
		{
			m_colParts = new System.Collections.ArrayList();
		}

		internal void Add(Waymex.Gis.Shape.PointsM Points)
		{
			try
			{
				m_colParts.Add(Points);
			}
			catch (Exception e)
			{
				throw new ShapeException(e.Message, e);
			}
		}

		public int Count
		{
			get
			{
				try
				{
					return m_colParts.Count;
				}

				catch (Exception e)
				{
					throw new ShapeException(e.Message, e);
				}
			}
		}

		public PointsM this[int Index]
		{
			get
			{
				return (PointsM)m_colParts[Index];
			}

		}

		/// <summary>
		/// Returns an Enumerator for the Collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colParts.GetEnumerator();
		}
		
	}

}
