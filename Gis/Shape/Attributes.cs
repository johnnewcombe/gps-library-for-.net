using System;

namespace Waymex.Gis.Shape
{
#if(PUBLIC)
	public class Attributes : System.Collections.IEnumerable
#else
	internal class Attributes : System.Collections.IEnumerable
#endif
	{
					
		private System.Collections.ArrayList m_colAttributes;
		

		public Attributes() : base()
		{
			m_colAttributes = new System.Collections.ArrayList();
		}

		public void Append(Attribute Attribute)
		{
			m_colAttributes.Add(Attribute);
		}
		public int Count
		{
			get
			{
				try
				{
					return m_colAttributes.Count;
				}

				catch (Exception e)
				{
					throw new ShapeException(e.Message, e);
				}
			}
		}

		public Attribute this[int Index]
		{
			get
			{
				return (Attribute)m_colAttributes[Index];
			}

		}

		/// <summary>
		/// Returns an Enumerator for the Collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colAttributes.GetEnumerator();
		}
		
	}

}
