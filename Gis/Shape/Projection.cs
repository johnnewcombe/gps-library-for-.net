using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Transform enumeraton, this is used to specify the coordinate system to be used
	/// when saving GPS objects to Shape files.
	/// </summary>
    public enum Projection
	{
		/// <summary>
		/// Standard WGS84 Latitude and Longtitude.
		/// </summary>
		Geographic = 0,
		/// <summary>
		/// Universal Transverse Mercator based on the WGS84 Ellipsoid. 
		/// </summary>
		Utm = 1
	}
}