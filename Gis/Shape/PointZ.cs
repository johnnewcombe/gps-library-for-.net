using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for PointZ.
	/// </summary>
#if(PUBLIC)
	public class PointZ : Waymex.Gis.Shape.ShapeBase
#else
	internal class PointZ : Waymex.Gis.Shape.ShapeBase
#endif
	{
		private double m_dblX = 0;			// X coordinate
		private double m_dblY = 0;			// Y coordinate
		private double m_dblM = NO_DATA;	// Measure
		private double m_dblZ = 0;			// Z coordinate


		public PointZ()
		{
			 m_enuType = ShapeBase.ShapeType.ShapeTypePointZ;
		}

		internal byte[] GetPointBinary()
		{
			byte[] bytData = null;

			//returns the point data e.g. only the X and Y data
			//used by other types that consume the point class e.g. PolyLine etc
			//ToByte cannot be used as it contains additional data e.g. the type ID.
			bytData = ByteConcat(bytData,ConvertToByte(m_dblX, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblY, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblM, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblZ, ByteOrder.ByteOrderLittleEndian));

			return bytData;
		}
		internal override byte[] ToByte()
		{
			byte[] bytData = null;

			bytData = ByteConcat(bytData,ConvertToByte((int)m_enuType, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblX, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblY, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblM, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblZ, ByteOrder.ByteOrderLittleEndian));

			return bytData;
		}

	}
}
