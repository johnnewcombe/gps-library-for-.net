using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for NullShape.
	/// </summary>
#if(PUBLIC)
	public class NullShape : Waymex.Gis.Shape.ShapeBase
#else
	internal class NullShape : Waymex.Gis.Shape.ShapeBase
#endif
	{

		public NullShape()
		{
			m_enuType = ShapeBase.ShapeType.ShapeTypeNull;
		}


		internal override byte[] ToByte()
		{
			byte[] bytData = null;

			bytData = ByteConcat(bytData,ConvertToByte((int)m_enuType, ByteOrder.ByteOrderLittleEndian));

			return bytData;

		}

	}
}
