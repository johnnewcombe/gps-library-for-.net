using System;

namespace Waymex.Gis.Shape
{
#if(PUBLIC)
	public class Points : System.Collections.IEnumerable
#else
	internal class Points : System.Collections.IEnumerable
#endif
	{
					
		private System.Collections.ArrayList m_colPoints; 

		public Points() : base()
		{
			m_colPoints = new System.Collections.ArrayList();
		}

		internal void Add(Waymex.Gis.Shape.Point Point)
		{
			try
			{
				m_colPoints.Add(Point);
			}
			catch (Exception e)
			{
				throw new ShapeException(e.Message, e);
			}
		}

		public int Count
		{
			get
			{
				try
				{
					return m_colPoints.Count;
				}

				catch (Exception e)
				{
					throw new ShapeException(e.Message, e);
				}
			}
		}

		public Point this[int Index]
		{
			get
			{
				return (Point)m_colPoints[Index];
			}

		}

		/// <summary>
		/// Returns an Enumerator for the Collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colPoints.GetEnumerator();
		}
		
	}

}
