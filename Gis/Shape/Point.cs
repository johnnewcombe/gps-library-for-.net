using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for GISAttribute.
	/// </summary>
#if(PUBLIC)
	public class Point : Waymex.Gis.Shape.ShapeBase
#else
	internal class Point : Waymex.Gis.Shape.ShapeBase
#endif
	{

		private double m_dblX = 0;
		private double m_dblY = 0;

		public Point()
		{
			m_enuType = ShapeBase.ShapeType.ShapeTypePoint;
		}

		public double ValueX
		{
			get
			{
				return m_dblX;
			}
			set
			{
				m_dblX = value;
			}
		}
		public double ValueY
		{
			get
			{
				return m_dblY;
			}
			set
			{
				m_dblY = value;
			}
		}

		internal byte[] GetPointBinary()
		{
			byte[] bytData = null;

			//returns the point data e.g. only the X and Y data
			//used by other types that consume the point class e.g. PolyLine etc
			//ToByte cannot be used as it contains additional data e.g. the type ID.
			bytData = ByteConcat(bytData,ConvertToByte(m_dblX, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblY, ByteOrder.ByteOrderLittleEndian));

			return bytData;
		}

		internal override byte[] ToByte()
		{
			byte[] bytData = null;

			//type is included when the shape file is full of point shapes
			bytData = ByteConcat(bytData,ConvertToByte((int)m_enuType, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblX, ByteOrder.ByteOrderLittleEndian));
			bytData = ByteConcat(bytData,ConvertToByte(m_dblY, ByteOrder.ByteOrderLittleEndian));

			return bytData;
		}

	}
}
