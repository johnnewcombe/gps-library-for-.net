using System;

namespace Waymex.Gis.Shape
{
#if(PUBLIC)
	public class PartsZ : System.Collections.IEnumerable
#else
	internal class PartsZ : System.Collections.IEnumerable
#endif
	{
					
		private System.Collections.ArrayList m_colParts; 

		public PartsZ() : base()
		{
			m_colParts = new System.Collections.ArrayList();
		}

		internal void Add(Waymex.Gis.Shape.PointsZ Points)
		{
			try
			{
				m_colParts.Add(Points);
			}
			catch (Exception e)
			{
				throw new ShapeException(e.Message, e);
			}
		}

		public int Count
		{
			get
			{
				try
				{
					return m_colParts.Count;
				}

				catch (Exception e)
				{
					throw new ShapeException(e.Message, e);
				}
			}
		}

		public PointsZ this[int Index]
		{
			get
			{
				return (PointsZ)m_colParts[Index];
			}

		}

		/// <summary>
		/// Returns an Enumerator for the Collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colParts.GetEnumerator();
		}
		
	}

}
