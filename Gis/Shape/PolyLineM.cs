using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for PolyLineM.
	/// </summary>
#if(PUBLIC)
	public class PolyLineM : Waymex.Gis.Shape.ShapeBase
#else
	internal class PolyLineM : Waymex.Gis.Shape.ShapeBase
#endif
	{
		private double[]	m_dblBox = new double[4];		// Bounding Box
		private int			m_intNumParts = 0;				// Number of Parts
		private int			m_intNumPoints = 0;				// Total Number of Points
		private int[]		m_intParts = null;				// Index to First Point in Part [NumParts]
		private Point[]		m_objPoints = null;				// Points for All Parts [NumPoints]
		private double[]	m_dblMRange = new double[2];	// Bounding Measure Range
		private double[]	m_dblMArray = null;				// Measures for All Points [NumPoints]

		public PolyLineM()
		{
			m_enuType = ShapeBase.ShapeType.ShapeTypePolyLineM;
		}
	}
}
