using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for Multipoint.
	/// </summary>
#if(PUBLIC)
	public class MultiPoint : Waymex.Gis.Shape.ShapeBase
#else
	internal class MultiPoint : Waymex.Gis.Shape.ShapeBase
#endif
	{
		
		private double	m_dblMinX = 0;
		private double	m_dblMinY = 0;
		private double	m_dblMaxX = 0;
		private double	m_dblMaxY = 0;
		private Points	m_colPoints = null; 

		public MultiPoint()
		{
			m_enuType = ShapeBase.ShapeType.ShapeTypeMultiPoint;
		}


		public bool AddPoint(Waymex.Gis.Shape.Point Point)
		{
			// update min and max values
			if (m_dblMaxX < Point.ValueX)
				m_dblMaxX = Point.ValueX;
			
			if (m_dblMaxY < Point.ValueY)
				m_dblMaxY = Point.ValueY;

			if (m_dblMinX > Point.ValueX)
				m_dblMinX = Point.ValueX;
			
			if (m_dblMinX > Point.ValueX)
				m_dblMinX = Point.ValueX;

			m_colPoints.Add(Point);

			return true;
		}

		public double MinValueX
		{
			get
			{
				return m_dblMinX;
			}
		}
		public double MinValueY
		{
			get
			{
				return m_dblMinY;
			}
		}
		public double MaxValueX
		{
			get
			{
				return m_dblMaxX;
			}
		}
		public double MaxValueY
		{
			get
			{
				return m_dblMaxY;
			}
		}

	}
}
