using System;

namespace Waymex.Gis.Shape
{
	///<summary>
	///</summary>
#if(PUBLIC)
	public class Parts : System.Collections.IEnumerable
#else
	internal class Parts : System.Collections.IEnumerable
#endif
	{			
		private System.Collections.ArrayList m_colParts; 

		public Parts() : base()
		{
			m_colParts = new System.Collections.ArrayList();
		}

		internal void Add(Waymex.Gis.Shape.Points Points)
		{
			try
			{
				m_colParts.Add(Points);
			}
			catch (Exception e)
			{
				throw new ShapeException(e.Message, e);
			}
		}

		public int Count
		{
			get
			{
				try
				{
					return m_colParts.Count;
				}

				catch (Exception e)
				{
					throw new ShapeException(e.Message, e);
				}
			}
		}

		public Points this[int Index]
		{
			get
			{
				return (Points)m_colParts[Index];
			}

		}

		/// <summary>
		/// Returns an Enumerator for the Collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colParts.GetEnumerator();
		}
		
	}

}
