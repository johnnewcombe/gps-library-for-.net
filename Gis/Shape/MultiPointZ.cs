using System;

namespace Waymex.Gis.Shape
{
	/// <summary>
	/// Summary description for MultiPointZ.
	/// </summary>
#if(PUBLIC)
	public class MultiPointZ : Waymex.Gis.Shape.ShapeBase
#else
	internal class MultiPointZ : Waymex.Gis.Shape.ShapeBase
#endif
	{
		private double[]	m_dblBox = new double[4];		// Bounding Box
		private int			m_intNumPoints = 0;				// Number of Points
		private Point[]		m_objPoints = null;				// The Points in the Set [NumPoints]
		private double[]	m_dblZRange = new double[2];	// Bounding Z Range
		private double[]	m_dblZArray = null;				// Z Values [NumPoints]
		private double[]	m_dblMRange = new double[2];	// Bounding Measure Range
		private double[]	m_dblMArray = null;				// Measures [NumPoints]

		public MultiPointZ()
		{
			m_enuType = ShapeBase.ShapeType.ShapeTypeMultiPointZ;
		}
	}
}
