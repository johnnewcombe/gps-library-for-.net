using System;
using System.ComponentModel;
using System.Text;
using System.Runtime.InteropServices;

namespace Waymex.Interop
{
    /// <summary>
    /// This class defines platform specific method declarations. These methods should not be
    /// used directly, the higher level functions should be used instead e.g. Waymex.Gps.Interop.
    /// </summary>
    internal sealed class NativeMethods
    {
        //private constructor
        private NativeMethods() { }

        //Constants for errors:
        internal const Int32 NO_ERROR = 0;

        internal const Int32 ERROR_FILE_NOT_FOUND = 2;
        internal const Int32 ERROR_INVALID_NAME = 123;
        internal const Int32 ERROR_ACCESS_DENIED = 5;
        internal const Int32 ERROR_INVALID_HANDLE = 6;
        internal const Int32 ERROR_IO_PENDING = 997;
        internal const Int32 ERROR_IO_INCOMPLETE = 996;   //Overlapped I/O event is not in a signaled state.
        internal const Int32 ERROR_NOACCESS = 998;
        internal const Int32 ERROR_HANDLE_EOF = 38;

        internal const UInt32 DIGCF_DEFAULT = 0x1;   //only valid with DIGCF_DEVICEINTERFACE
        internal const UInt32 DIGCF_PRESENT = 0x2;
        internal const UInt32 DIGCF_ALLCLASSES = 0x4;
        internal const UInt32 DIGCF_PROFILE = 0x8;
        internal const UInt32 DIGCF_DEVICEINTERFACE = 0x10;

        internal const UInt32 FILE_ANY_ACCESS = 0;
        internal const UInt32 FILE_DEVICE_UNKNOWN = 0x22;
        internal const UInt32 FILE_READ_ACCESS = 0x1;
        internal const UInt32 FILE_WRITE_ACCESS = 0x2;
        internal const UInt32 FILE_SHARE_READ = 0x1;
        internal const UInt32 FILE_SHARE_WRITE = 0x2;
        internal const UInt32 FILE_FLAG_OVERLAPPED = 0x40000000;
        internal const UInt32 FILE_ATTRIBUTE_NORMAL = 0x80;

        internal const UInt32 GENERIC_READ = 0x80000000;
        internal const UInt32 GENERIC_WRITE = 0x40000000;

        internal const UInt32 CREATE_ALWAYS = 2;
        internal const UInt32 OPEN_ALWAYS = 4;

        internal const UInt32 FORMAT_MESSAGE_FROM_SYSTEM = 0x1000;
        internal const Int32 INVALID_HANDLE_VALUE = -1;

        internal const UInt32 OPEN_EXISTING = 3;


        //structures
        /// <summary>
        ///  typedef struct _OVERLAPPED { 
        ///  ULONG_PTR  Internal; 
        ///  ULONG_PTR  InternalHigh; 
        ///  DWORD  Offset; 
        ///  DWORD  OffsetHigh; 
        ///  HANDLE hEvent; 
        ///  } OVERLAPPED;
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct OVERLAPPED
        {
            internal UIntPtr Internal;
            internal UIntPtr InternalHigh;
            internal UInt32 Offset;
            internal UInt32 OffsetHigh;
            internal IntPtr hEvent;


        }

        /// <summary>
        ///  typedef struct _SP_DEVINFO_DATA {
        ///  DWORD cbSize;  
        ///  GUID ClassGuid;
        ///  DWORD DevInst;
        ///  ULONG_PTR Reserved;
        ///  } SP_DEVINFO_DATA,  *PSP_DEVINFO_DATA;
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct SP_DEVINFO_DATA
        {
            internal UInt32 cbsize;
            internal Guid ClassGuid;
            internal UInt32 DevInst;
            internal IntPtr Reserved;


        }

        /// <summary>
        ///  typedef struct _SP_DEVICE_INTERFACE_DATA {
        ///  DWORD cbSize;
        ///  GUID  InterfaceClassGuid;
        ///  DWORD Flags;
        ///  ULONG_PTR Reserved;
        ///  } SP_DEVICE_INTERFACE_DATA, *PSP_DEVICE_INTERFACE_DATA;
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct DEVICE_INTERFACE_DATA
        {
            internal UInt32 cbsize;
            internal Guid InterfaceClassGuid;
            internal UInt32 Flags;
            internal IntPtr ReservedPtr;


        }

        /// <summary>
        ///  typedef struct _SP_DEVICE_INTERFACE_DETAIL_DATA {
        ///  DWORD  cbSize;
        ///  TCHAR  DevicePath[ANYSIZE_ARRAY];
        ///  } SP_DEVICE_INTERFACE_DETAIL_DATA, *PSP_DEVICE_INTERFACE_DETAIL_DATA;
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct DEVICE_INTERFACE_DETAIL_DATA
        {
            internal uint cbsize;
            //dummy variable to get the struct size right
            internal short strDevicePath; //pointer to a null terminated string

        }

        /// <summary>
        ///  typedef struct _SECURITY_ATTRIBUTES {
        ///  DWORD nLength;
        ///  LPVOID lpSecurityDescriptor;
        ///  BOOL bInheritHandle;
        ///  } SECURITY_ATTRIBUTES, *PSECURITY_ATTRIBUTES;
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct SECURITY_ATTRIBUTES
        {
            internal Int32 nLength;
            internal Int32 lpSecurityDescriptor;
            internal Int32 bInheritHandle;


        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct DEVICEIODATA
        {
            internal long Data1; //dummy 64 byte block for the DeviceIoControl api call
            internal long Data2;
            internal long Data3;
            internal long Data4;
            internal long Data5;
            internal long Data6;
            internal long Data7;
            internal long Data8;
        }

        /// <summary>
        ///  HANDLE CreateEvent(
        ///  LPSECURITY_ATTRIBUTES lpEventAttributes, // SD
        ///  BOOL bManualReset,                       // reset type
        ///  BOOL bInitialState,                      // initial state
        ///  LPCTSTR lpName                           // object name
        ///  );
        /// </summary>
        /// <param name="lpEventAttributes"></param>
        /// <param name="bManualReset"></param>
        /// <param name="bInitialState"></param>
        /// <param name="lpName"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        internal unsafe static extern IntPtr CreateEvent(
            IntPtr lpEventAttributes,
            int bManualReset,
            int bInitialState,
            string lpName


            );
        /// <summary>
        ///  HANDLE CreateFile(
        ///  LPCTSTR lpFileName,                         // file name
        ///  DWORD dwDesiredAccess,                      // access mode
        ///  DWORD dwShareMode,                          // share mode
        ///  LPSECURITY_ATTRIBUTES lpSecurityAttributes, // SD
        ///  DWORD dwCreationDisposition,                // how to create
        ///  DWORD dwFlagsAndAttributes,                 // file attributes
        ///  HANDLE hTemplateFile                        // handle to template file
        ///  );
        /// </summary>
        /// <param name="lpFileName"></param>
        /// <param name="dwDesiredAccess"></param>
        /// <param name="dwShareMode"></param>
        /// <param name="lpSecurityAttributes"></param>
        /// <param name="dwCreationDisposition"></param>
        /// <param name="dwFlagsAndAttributes"></param>
        /// <param name="hTemplateFile"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal unsafe static extern IntPtr CreateFile(
            String lpFileName,
            UInt32 dwDesiredAccess,
            UInt32 dwShareMode,
            IntPtr lpSecurityAttributes,
            UInt32 dwCreationDisposition,
            UInt32 dwFlagsAndAttributes,
            IntPtr hTemplateFile

            );
        /// <summary>
        ///  BOOL SetupDiDestroyDeviceInfoList(
        ///  HDEVINFO DeviceInfoSet
        ///  );
        /// </summary>
        /// <param name="DeviceInfoSet"></param>
        /// <returns></returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool SetupDiDestroyDeviceInfoList(
            IntPtr DeviceInfoSet

            );
        /// <summary>
        ///  BOOL SetupDiGetDeviceInterfaceDetail(
        ///  HDEVINFO DeviceInfoSet,
        ///  PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
        ///  PSP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData, 
        ///  DWORD DeviceInterfaceDetailDataSize,
        ///  PDWORD RequiredSize, 
        ///  PSP_DEVINFO_DATA DeviceInfoData 
        ///  );
        /// </summary>
        /// <param name="DeviceInfoSet"></param>
        /// <param name="DeviceInterfaceData"></param>
        /// <param name="DeviceInterfaceDetailData"></param>
        /// <param name="DeviceInterfaceDetailDataSize"></param>
        /// <param name="RequiredSize"></param>
        /// <param name="DeviceInfoData"></param>
        /// <returns></returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool SetupDiGetDeviceInterfaceDetail(
            IntPtr DeviceInfoSet,
            ref DEVICE_INTERFACE_DATA DeviceInterfaceData,
            IntPtr DeviceInterfaceDetailData, //done like this as parameter is optional
            //ref DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData,
            int DeviceInterfaceDetailDataSize,
            ref int RequiredSize,
            IntPtr DeviceInfoData
            );


        /// <summary>
        ///  BOOL SetupDiEnumDeviceInterfaces(
        ///  HDEVINFO DeviceInfoSet,
        ///  PSP_DEVINFO_DATA DeviceInfoData, 
        ///  CONST GUID * InterfaceClassGuid,
        ///  DWORD MemberIndex,
        ///  PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData
        ///  );
        /// </summary>
        /// <param name="DeviceInfoSet"></param>
        /// <param name="DeviceInfoData"></param>
        /// <param name="InterfaceClassGuid"></param>
        /// <param name="MemberIndex"></param>
        /// <param name="DeviceInterfaceData"></param>
        /// <returns></returns>
        [DllImport("Setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool SetupDiEnumDeviceInterfaces(
            IntPtr DeviceInfoSet,
            //ref SP_DEVINFO_DATA DeviceInfoData,
            IntPtr DeviceInfoData, //done like this as the parameter is optional
            ref Guid InterfaceClassGuid,
            int MemberIndex,
            ref DEVICE_INTERFACE_DATA DeviceInterfaceData
            );


        /// <summary>
        ///  HDEVINFO SetupDiGetClassDevs(
        ///  CONST GUID * ClassGuid, 
        ///  PCTSTR Enumerator, 
        ///  HWND hwndParent, 
        ///  DWORD Flags
        ///  );
        /// </summary>
        /// <param name="ClassGuid"></param>
        /// <param name="Enumerator"></param>
        /// <param name="hwndParent"></param>
        /// <param name="Flags"></param>
        /// <returns></returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal unsafe static extern IntPtr SetupDiGetClassDevs(
            ref Guid ClassGuid,
            string Enumerator,
            IntPtr hwndParent,
            uint Flags


            );
        /// <summary>
        ///  BOOL CloseHandle(
        ///  HANDLE hObject   // handle to object
        ///  );
        /// </summary>
        /// <param name="hObject"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool CloseHandle(
            IntPtr hObject


            );


        /// <summary>
        ///  BOOL ResetEvent(
        ///  HANDLE hEvent   // handle to event
        ///  );
        /// </summary>
        /// <param name="hEvent"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool ResetEvent(
            IntPtr hEvent


            );
        /// <summary>
        ///  BOOL WriteFile(
        ///  HANDLE hFile,                    // handle to file
        ///  LPCVOID lpBuffer,                // data buffer
        ///  DWORD nNumberOfBytesToWrite,     // number of bytes to write
        ///  LPDWORD lpNumberOfBytesWritten,  // number of bytes written
        ///  LPOVERLAPPED lpOverlapped        // overlapped buffer
        ///  );
        /// </summary>
        /// <param name="hFile"></param>
        /// <param name="lpBuffer"></param>
        /// <param name="nNumberOfBytesToWrite"></param>
        /// <param name="lpNumberOfBytesWritten"></param>
        /// <param name="lpOverlapped"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool WriteFile(
            IntPtr hFile,
            IntPtr lpBuffer,
            UInt32 nNumberOfBytesToWrite,
            ref UInt32 lpNumberOfBytesWritten,
            ref OVERLAPPED lpOverlapped
            //NativeOverlapped* lpOverlapped

            );

        /// <summary>
        ///  BOOL ReadFile(
        ///  HANDLE hFile,                // handle to file<br/>
        ///  LPVOID lpBuffer,             // data buffer<br/>
        ///  DWORD nNumberOfBytesToRead,  // number of bytes to read<br/>
        ///  LPDWORD lpNumberOfBytesRead, // number of bytes read<br/>
        ///  LPOVERLAPPED lpOverlapped    // overlapped buffer<br/>
        ///  );
        /// </summary>
        /// <param name="hFile"></param>
        /// <param name="lpBuffer"></param>
        /// <param name="nNumberOfBytesToRead"></param>
        /// <param name="lpNumberOfBytesRead"></param>
        /// <param name="lpOverlapped"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool ReadFile(
            IntPtr hFile,
            IntPtr lpBuffer,
            UInt32 nNumberOfBytesToRead,
            ref UInt32 lpNumberOfBytesRead,
            ref OVERLAPPED lpOverlapped
            //NativeOverlapped* lpOverlapped

        );

        /// <summary>
        ///  BOOL DeviceIoControl(
        ///  HANDLE hDevice,              // handle to device
        ///  DWORD dwIoControlCode,       // operation
        ///  LPVOID lpInBuffer,           // input data buffer
        ///  DWORD nInBufferSize,         // size of input data buffer
        ///  LPVOID lpOutBuffer,          // output data buffer
        ///  DWORD nOutBufferSize,        // size of output data buffer
        ///  LPDWORD lpBytesReturned,     // byte count
        ///  LPOVERLAPPED lpOverlapped    // overlapped information
        ///  );
        /// </summary>
        /// <param name="hDevice"></param>
        /// <param name="dwIoControlCode"></param>
        /// <param name="lpInBuffer"></param>
        /// <param name="nInBufferSize"></param>
        /// <param name="lpOutBuffer"></param>
        /// <param name="nOutBufferSize"></param>
        /// <param name="lpBytesReturned"></param>
        /// <param name="lpOverlapped"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool DeviceIoControl(
            IntPtr hDevice,
            UInt32 dwIoControlCode,
            IntPtr lpInBuffer,
            UInt32 nInBufferSize,
            IntPtr lpOutBuffer,
            UInt32 nOutBufferSize,
            ref UInt32 lpBytesReturned,
            ref OVERLAPPED lpOverlapped
            //NativeOverlapped* lpOverlapped

            );

        /// <summary>
        ///  BOOL GetOverlappedResult(
        ///  HANDLE hFile,                       // handle to file, pipe, or device
        ///  LPOVERLAPPED lpOverlapped,          // overlapped structure
        ///  LPDWORD lpNumberOfBytesTransferred, // bytes transferred
        ///  BOOL bWait                          // wait option
        ///  );
        /// </summary>
        /// <param name="hFile"></param>
        /// <param name="lpOverlapped"></param>
        /// <param name="lpNumberOfBytesTransferred"></param>
        /// <param name="bWait"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool GetOverlappedResult(
            IntPtr hFile,
            ref OVERLAPPED lpOverlapped,
            ref UInt32 lpNumberOfBytesTransferred,
            [MarshalAs(UnmanagedType.Bool)] bool bWait


            );

        /// <summary>
        ///  BOOL CancelIo(
        ///  HANDLE hFile  // handle to file
        ///  );
        /// </summary>
        /// <param name="hFile"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal unsafe static extern bool CancelIo(
            IntPtr hFile
            );

    }

}

//using System;
//using System.ComponentModel;
//using System.Collections.Generic;
//using System.Text;
//using System.Runtime.InteropServices;

//namespace Waymex.Interop
//{
//    /// <summary>
//    /// This class defines platform specific method declarations. These methods should not be
//    /// used directly, the higher level functions should be used instead e.g. Waymex.Gps.Interop.
//    /// </summary>
//    internal sealed class NativeMethods
//    {
//        //private constructor
//        private NativeMethods() { }

//        //Constants for errors:
//        internal const Int32 NO_ERROR = 0;

//        internal const Int32 ERROR_FILE_NOT_FOUND = 2;
//        internal const Int32 ERROR_INVALID_NAME = 123;
//        internal const Int32 ERROR_ACCESS_DENIED = 5;
//        internal const Int32 ERROR_INVALID_HANDLE = 6;
//        internal const Int32 ERROR_IO_PENDING = 997;
//        internal const Int32 ERROR_IO_INCOMPLETE = 996;   //Overlapped I/O event is not in a signaled state.
//        internal const Int32 ERROR_NOACCESS = 998;
//        internal const Int32 ERROR_HANDLE_EOF = 38;
//        internal const Int32 ERROR_GEN_FAILURE = 31;

//        internal const UInt32 DIGCF_DEFAULT = 0x1;   //only valid with DIGCF_DEVICEINTERFACE
//        internal const UInt32 DIGCF_PRESENT = 0x2;
//        internal const UInt32 DIGCF_ALLCLASSES = 0x4;
//        internal const UInt32 DIGCF_PROFILE = 0x8;
//        internal const UInt32 DIGCF_DEVICEINTERFACE = 0x10;

//        internal const UInt32 FILE_ANY_ACCESS = 0;
//        internal const UInt32 FILE_DEVICE_UNKNOWN = 0x22;
//        internal const UInt32 FILE_READ_ACCESS = 0x1;
//        internal const UInt32 FILE_WRITE_ACCESS = 0x2;
//        internal const UInt32 FILE_SHARE_READ = 0x1;
//        internal const UInt32 FILE_SHARE_WRITE = 0x2;
//        internal const UInt32 FILE_FLAG_OVERLAPPED = 0x40000000;
//        internal const UInt32 FILE_ATTRIBUTE_NORMAL = 0x80;

//        internal const UInt32 GENERIC_READ = 0x80000000;
//        internal const UInt32 GENERIC_WRITE = 0x40000000;

//        internal const UInt32 CREATE_ALWAYS = 2;
//        internal const UInt32 OPEN_ALWAYS = 4;

//        internal const UInt32 FORMAT_MESSAGE_FROM_SYSTEM = 0x1000;
//        internal const Int32 INVALID_HANDLE_VALUE = -1;

//        internal const UInt32 OPEN_EXISTING = 3;


//        //structures
//        /// <summary>
//        ///  typedef struct _OVERLAPPED { 
//        ///  ULONG_PTR  Internal; 
//        ///  ULONG_PTR  InternalHigh; 
//        ///  DWORD  Offset; 
//        ///  DWORD  OffsetHigh; 
//        ///  HANDLE hEvent; 
//        ///  } OVERLAPPED;

//        /// </summary>
//        [StructLayout(LayoutKind.Sequential)]
//        internal struct OVERLAPPED
//        {
//            internal UIntPtr Internal;
//            internal UIntPtr InternalHigh;
//            internal UInt32 Offset;
//            internal UInt32 OffsetHigh;
//            internal IntPtr hEvent;


//        }

//        /// <summary>
//        ///  typedef struct _SP_DEVINFO_DATA {
//        ///  DWORD cbSize;  
//        ///  GUID ClassGuid;
//        ///  DWORD DevInst;
//        ///  ULONG_PTR Reserved;
//        ///  } SP_DEVINFO_DATA,  *PSP_DEVINFO_DATA;
//        /// </summary>
//        [StructLayout(LayoutKind.Sequential)]
//        internal struct SP_DEVINFO_DATA
//        {
//            internal UInt32 cbsize;
//            internal Guid ClassGuid;
//            internal UInt32 DevInst;
//            internal IntPtr Reserved;


//        }

//        /// <summary>
//        ///  typedef struct _SP_DEVICE_INTERFACE_DATA {
//        ///  DWORD cbSize;
//        ///  GUID  InterfaceClassGuid;
//        ///  DWORD Flags;
//        ///  ULONG_PTR Reserved;
//        ///  } SP_DEVICE_INTERFACE_DATA, *PSP_DEVICE_INTERFACE_DATA;
//        /// </summary>
//        [StructLayout(LayoutKind.Sequential)]
//        internal struct DEVICE_INTERFACE_DATA
//        {
//            internal UInt32 cbsize;
//            internal Guid InterfaceClassGuid;
//            internal UInt32 Flags;
//            internal IntPtr ReservedPtr;


//        }

//        /// <summary>
//        ///  typedef struct _SP_DEVICE_INTERFACE_DETAIL_DATA {
//        ///  DWORD  cbSize;
//        ///  TCHAR  DevicePath[ANYSIZE_ARRAY];
//        ///  } SP_DEVICE_INTERFACE_DETAIL_DATA, *PSP_DEVICE_INTERFACE_DETAIL_DATA;
//        /// </summary>
//        [StructLayout(LayoutKind.Sequential, Pack = 1)]
//        internal struct DEVICE_INTERFACE_DETAIL_DATA
//        {
//            internal uint cbsize;
//            //dummy variable to get the struct size right
//            internal short strDevicePath; //pointer to a null terminated string

//        }

//        /// <summary>
//        ///  typedef struct _SECURITY_ATTRIBUTES {
//        ///  DWORD nLength;
//        ///  LPVOID lpSecurityDescriptor;
//        ///  BOOL bInheritHandle;
//        //} SECURITY_ATTRIBUTES, *PSECURITY_ATTRIBUTES;
//        /// </summary>
//        [StructLayout(LayoutKind.Sequential, Pack = 1)]
//        internal struct SECURITY_ATTRIBUTES
//        {
//            internal Int32 nLength;
//            internal Int32 lpSecurityDescriptor;
//            internal Int32 bInheritHandle;


//        }
//        [StructLayout(LayoutKind.Sequential, Pack = 1)]
//        internal struct DEVICEIODATA
//        {
//            internal long Data1; //dummy 64 byte block for the DeviceIoControl api call
//            internal long Data2;
//            internal long Data3;
//            internal long Data4;
//            internal long Data5;
//            internal long Data6;
//            internal long Data7;
//            internal long Data8;
//        }

//        /// <summary>
//        ///  HANDLE CreateEvent(
//        ///  LPSECURITY_ATTRIBUTES lpEventAttributes, // SD
//        ///  BOOL bManualReset,                       // reset type
//        ///  BOOL bInitialState,                      // initial state
//        ///  LPCTSTR lpName                           // object name
//        ///  );
//        /// </summary>
//        /// <param name="lpEventAttributes"></param>
//        /// <param name="bManualReset"></param>
//        /// <param name="bInitialState"></param>
//        /// <param name="lpName"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", SetLastError = true)]
//        internal unsafe static extern IntPtr CreateEvent(
//            IntPtr lpEventAttributes,
//            int bManualReset,
//            int bInitialState,
//            string lpName


//            );
//        /// <summary>
//        ///  HANDLE CreateFile(
//        ///  LPCTSTR lpFileName,                         // file name
//        ///  DWORD dwDesiredAccess,                      // access mode
//        ///  DWORD dwShareMode,                          // share mode
//        ///  LPSECURITY_ATTRIBUTES lpSecurityAttributes, // SD
//        ///  DWORD dwCreationDisposition,                // how to create
//        ///  DWORD dwFlagsAndAttributes,                 // file attributes
//        ///  HANDLE hTemplateFile                        // handle to template file
//        ///  );
//        /// </summary>
//        /// <param name="lpFileName"></param>
//        /// <param name="dwDesiredAccess"></param>
//        /// <param name="dwShareMode"></param>
//        /// <param name="lpSecurityAttributes"></param>
//        /// <param name="dwCreationDisposition"></param>
//        /// <param name="dwFlagsAndAttributes"></param>
//        /// <param name="hTemplateFile"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        internal unsafe static extern IntPtr CreateFile(
//            String lpFileName,
//            UInt32 dwDesiredAccess,
//            UInt32 dwShareMode,
//            IntPtr lpSecurityAttributes,
//            UInt32 dwCreationDisposition,
//            UInt32 dwFlagsAndAttributes,
//            IntPtr hTemplateFile

//            );
//        /// <summary>
//        ///  BOOL SetupDiDestroyDeviceInfoList(
//        ///  HDEVINFO DeviceInfoSet
//        ///  );

//        /// </summary>
//        /// <param name="DeviceInfoSet"></param>
//        /// <returns></returns>
//        [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool SetupDiDestroyDeviceInfoList(
//            IntPtr DeviceInfoSet

//            );
//        /// <summary>
//        ///  BOOL SetupDiGetDeviceInterfaceDetail(
//        ///  HDEVINFO DeviceInfoSet,
//        ///  PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
//        ///  PSP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData, 
//        ///  DWORD DeviceInterfaceDetailDataSize,
//        ///  PDWORD RequiredSize, 
//        ///  PSP_DEVINFO_DATA DeviceInfoData 
//        ///  );
//        /// </summary>
//        /// <param name="DeviceInfoSet"></param>
//        /// <param name="DeviceInterfaceData"></param>
//        /// <param name="DeviceInterfaceDetailData"></param>
//        /// <param name="DeviceInterfaceDetailDataSize"></param>
//        /// <param name="RequiredSize"></param>
//        /// <param name="DeviceInfoData"></param>
//        /// <returns></returns>
//        [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool SetupDiGetDeviceInterfaceDetail(
//            IntPtr DeviceInfoSet,
//            ref DEVICE_INTERFACE_DATA DeviceInterfaceData,
//            IntPtr DeviceInterfaceDetailData, //done like this as parameter is optional
//            //ref DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData,
//            int DeviceInterfaceDetailDataSize,
//            ref int RequiredSize,
//            IntPtr DeviceInfoData
//            );


//        /// <summary>
//        ///  BOOL SetupDiEnumDeviceInterfaces(
//        ///  HDEVINFO DeviceInfoSet,
//        ///  PSP_DEVINFO_DATA DeviceInfoData, 
//        ///  CONST GUID * InterfaceClassGuid,
//        ///  DWORD MemberIndex,
//        ///  PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData
//        ///  );
//        /// </summary>
//        /// <param name="DeviceInfoSet"></param>
//        /// <param name="DeviceInfoData"></param>
//        /// <param name="InterfaceClassGuid"></param>
//        /// <param name="MemberIndex"></param>
//        /// <param name="DeviceInterfaceData"></param>
//        /// <returns></returns>
//        [DllImport("Setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool SetupDiEnumDeviceInterfaces(
//            IntPtr DeviceInfoSet,
//            //ref SP_DEVINFO_DATA DeviceInfoData,
//            IntPtr DeviceInfoData, //done like this as the parameter is optional
//            ref Guid InterfaceClassGuid,
//            int MemberIndex,
//            ref DEVICE_INTERFACE_DATA DeviceInterfaceData
//            );


//        /// <summary>
//        ///  HDEVINFO SetupDiGetClassDevs(
//        ///  CONST GUID * ClassGuid, 
//        ///  PCTSTR Enumerator, 
//        ///  HWND hwndParent, 
//        ///  DWORD Flags
//        ///  );
//        /// </summary>
//        /// <param name="ClassGuid"></param>
//        /// <param name="Enumerator"></param>
//        /// <param name="hwndParent"></param>
//        /// <param name="Flags"></param>
//        /// <returns></returns>
//        [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        internal unsafe static extern IntPtr SetupDiGetClassDevs(
//            ref Guid ClassGuid,
//            string Enumerator,
//            IntPtr hwndParent,
//            uint Flags


//            );
//        /// <summary>
//        ///  BOOL CloseHandle(
//        ///  HANDLE hObject   // handle to object
//        ///  );
//        /// </summary>
//        /// <param name="hObject"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool CloseHandle(
//            IntPtr hObject


//            );


//        /// <summary>
//        //  BOOL ResetEvent(
//        //  HANDLE hEvent   // handle to event
//        //  );
//        /// </summary>
//        /// <param name="hEvent"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool ResetEvent(
//            IntPtr hEvent


//            );
//        /// <summary>
//        ///  BOOL WriteFile(
//        ///  HANDLE hFile,                    // handle to file
//        ///  LPCVOID lpBuffer,                // data buffer
//        ///  DWORD nNumberOfBytesToWrite,     // number of bytes to write
//        ///  LPDWORD lpNumberOfBytesWritten,  // number of bytes written
//        ///  LPOVERLAPPED lpOverlapped        // overlapped buffer
//        ///  );
//        /// </summary>
//        /// <param name="hFile"></param>
//        /// <param name="lpBuffer"></param>
//        /// <param name="nNumberOfBytesToWrite"></param>
//        /// <param name="lpNumberOfBytesWritten"></param>
//        /// <param name="lpOverlapped"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool WriteFile(
//            IntPtr hFile,
//            IntPtr lpBuffer,
//            UInt32 nNumberOfBytesToWrite,
//            ref UInt32 lpNumberOfBytesWritten,
//            ref OVERLAPPED lpOverlapped
//            //NativeOverlapped* lpOverlapped

//            );

//        /// <summary>
//        ///  BOOL ReadFile(
//        ///  HANDLE hFile,                // handle to file<br/>
//        ///  LPVOID lpBuffer,             // data buffer<br/>
//        ///  DWORD nNumberOfBytesToRead,  // number of bytes to read<br/>
//        ///  LPDWORD lpNumberOfBytesRead, // number of bytes read<br/>
//        ///  LPOVERLAPPED lpOverlapped    // overlapped buffer<br/>
//        ///  );
//        /// </summary>
//        /// <param name="hFile"></param>
//        /// <param name="lpBuffer"></param>
//        /// <param name="nNumberOfBytesToRead"></param>
//        /// <param name="lpNumberOfBytesRead"></param>
//        /// <param name="lpOverlapped"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool ReadFile(
//            IntPtr hFile,
//            IntPtr lpBuffer,
//            UInt32 nNumberOfBytesToRead,
//            ref UInt32 lpNumberOfBytesRead,
//            ref OVERLAPPED lpOverlapped
//            //NativeOverlapped* lpOverlapped

//        );

//        /// <summary>
//        ///  BOOL DeviceIoControl(
//        ///  HANDLE hDevice,              // handle to device
//        ///  DWORD dwIoControlCode,       // operation
//        ///  LPVOID lpInBuffer,           // input data buffer
//        ///  DWORD nInBufferSize,         // size of input data buffer
//        ///  LPVOID lpOutBuffer,          // output data buffer
//        ///  DWORD nOutBufferSize,        // size of output data buffer
//        ///  LPDWORD lpBytesReturned,     // byte count
//        ///  LPOVERLAPPED lpOverlapped    // overlapped information
//        ///  );
//        /// </summary>
//        /// <param name="hDevice"></param>
//        /// <param name="dwIoControlCode"></param>
//        /// <param name="lpInBuffer"></param>
//        /// <param name="nInBufferSize"></param>
//        /// <param name="lpOutBuffer"></param>
//        /// <param name="nOutBufferSize"></param>
//        /// <param name="lpBytesReturned"></param>
//        /// <param name="lpOverlapped"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool DeviceIoControl(
//            IntPtr hDevice,
//            UInt32 dwIoControlCode,
//            IntPtr lpInBuffer,
//            UInt32 nInBufferSize,
//            IntPtr lpOutBuffer,
//            UInt32 nOutBufferSize,
//            ref UInt32 lpBytesReturned,
//            ref OVERLAPPED lpOverlapped
//            //NativeOverlapped* lpOverlapped

//            );

//        /// <summary>
//        ///  BOOL GetOverlappedResult(
//        ///  HANDLE hFile,                       // handle to file, pipe, or device
//        ///  LPOVERLAPPED lpOverlapped,          // overlapped structure
//        ///  LPDWORD lpNumberOfBytesTransferred, // bytes transferred
//        ///  BOOL bWait                          // wait option
//        ///  );
//        /// </summary>
//        /// <param name="hFile"></param>
//        /// <param name="lpOverlapped"></param>
//        /// <param name="lpNumberOfBytesTransferred"></param>
//        /// <param name="bWait"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool GetOverlappedResult(
//            IntPtr hFile,
//            ref OVERLAPPED lpOverlapped,
//            ref UInt32 lpNumberOfBytesTransferred,
//            [MarshalAs(UnmanagedType.Bool)] bool bWait


//            );

//        /// <summary>
//        ///  BOOL CancelIo(
//        ///  HANDLE hFile  // handle to file
//        ///  );
//        /// </summary>
//        /// <param name="hFile"></param>
//        /// <returns></returns>
//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]
//        internal unsafe static extern bool CancelIo(
//            IntPtr hFile
//            );

//    }

//}

