﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="UTF-8" />
  <xsl:param name="lineColor" select="FF0000FF" />
  <xsl:param name="polyColor" select="FF00FF00" />
  <xsl:param name="lineWidth" select="4" />

  <xsl:template match="gpswaypoints">
    <kml xmlns="http://earth.google.com/kml/2.1">
      <Document>

        <!-- Creates Placemarks from Waypoints -->
        <xsl:for-each select ="gpswaypoint">
          <Placemark>
            <name>
              <xsl:value-of select="identifier"/>
            </name>
            <description>
              <xsl:value-of select="comment"/>
            </description>
            <Point>
              <coordinates>
                <xsl:value-of select="longitude"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="latitude"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="altitude"/>
              </coordinates>
            </Point>
          </Placemark>
        </xsl:for-each>
      </Document>
    </kml>
  </xsl:template>

  <xsl:template match="/gpsroutes">
    <!-- Creates Paths from Routes -->
    <kml xmlns="http://earth.google.com/kml/2.1">
      <Document>
        <xsl:for-each select ="/gpsroutes/gpsroute">
          <name />
          <description />
          <Placemark>
            <name>
              <xsl:value-of select="identifier"/>
            </name>
            <description>
              <xsl:value-of select="comment"/>
            </description>
            <Style ID="linePolyColour">
              <LineStyle>
                <color>
                  <xsl:value-of select="$lineColor"/>
                </color>
                <width>
                  <xsl:value-of select="$lineWidth"/>
                </width>
              </LineStyle>
              <PolyStyle>
                <color>
                  <xsl:value-of select="$polyColor"/>
                </color>
              </PolyStyle>
            </Style>
            <LineString>
              <extrude>1</extrude>
              <tesselate>1</tesselate>
              <coordinates>
                <xsl:for-each select ="gpswaypoints/gpswaypoint">
                  <xsl:value-of select="longitude"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="latitude"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="altitude"/>
                  <xsl:text>&#32;</xsl:text>
                </xsl:for-each>
              </coordinates>
            </LineString>
          </Placemark>
        </xsl:for-each>
      </Document>
    </kml>
  </xsl:template>

  <xsl:template match="/gpstracklogs">
    <kml xmlns="http://earth.google.com/kml/2.1">
      <!-- Creates Paths from Tracklogs -->
      <Document>
        <name />
        <description />
        <xsl:for-each select ="/gpstracklogs/gpstracklog">
          <Placemark>
            <name>
              <xsl:value-of select="identifier"/>
            </name>
            <description />
            <Style ID="linePolyColour">
              <LineStyle>
                <color>
                  <xsl:value-of select="$lineColor"/>
                </color>
                <width>
                  <xsl:value-of select="$lineWidth"/>
                </width>
              </LineStyle>
              <PolyStyle>
                <color>
                  <xsl:value-of select="$polyColor"/>
                </color>
              </PolyStyle>
            </Style>
            <LineString>
              <extrude>1</extrude>
              <tesselate>1</tesselate>
              <coordinates>
                <xsl:for-each select ="gpstrackpoints/gpstrackpoint">
                  <xsl:value-of select="longitude"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="latitude"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="altitude"/>
                  <xsl:text>&#32;</xsl:text>

                </xsl:for-each>
              </coordinates>
            </LineString>
          </Placemark>
        </xsl:for-each>
      </Document>
    </kml>
  </xsl:template>


</xsl:stylesheet>

