using System;
using System.Xml;
using System.Text;
using System.Globalization;


namespace Waymex.Gps
{
	/// <summary>
	/// This object forms part of the RouteCollection object which is returned 
	/// by the GetRoutes method of the Device object. The object represents 
	/// a single route stored in the connected GPS device.
	/// </summary>
	/// <remarks>
    /// <para><b>Magellan Devices</b></para>
    /// <para>The following Properties are used by the Magellan devices.</para>
    /// <list type="table">
    /// <item><term>Number</term><description>Int16</description></item>
    /// <item><term>Identifier</term><description>String</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Garmin Devices</b></para>
    /// <para>
	/// When the Host requests the GPS to send routes, the GPS will send every route stored in its
	/// database. When the Host sends routes to the GPS, the Host may selectively transfer any
	/// number of routes.
	/// </para>
	/// <para>
	/// Some GPS devices contain an internal database of waypoint information; for example, most
	/// aviation products have an internal database of aviation waypoints, and others may have an
	/// internal database of land waypoints. When routes are being transferred from the Host to one
	/// of these units, the GPS will attempt to match the incoming route waypoints with waypoints in
	/// its internal database. The GPS inspects the 'Class' member of the incoming route waypoint
	/// and if it indicates a non-user waypoint, the GPS searches its internal database using either
	/// the 'Identifier' and 'Country' members or the 'Sub Class' member. If a match is found, the
	/// waypoint from the internal database is used for the route; otherwise, a new user waypoint is
	/// created and used.
	/// </para>
	/// <para>
	/// In most cases not all of the Properties of the Route object are used. The Properties
	/// actually used are determined by the protocol employed by the connected GPS Device.
	/// Below is a list of which Properties are supported by Protocol. Please note that even though a
	/// Property is identified as being supported does not mean that it will actually be used/populated.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D200</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Number</term><description>Int16</description></item>
	/// <item><term>Waypoints</term><description>Waypoints</description></item>
	/// </list>
	/// <para>
	/// The 'Number' Property must be unique for each route.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D201</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Number</term><description>Int16</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Waypoints</term><description>Waypoints</description></item>
	/// </list>
	/// <para>
	/// Some GPS devices require a unique 'Comment' Property value for each route, 
	/// and other GPS devices do not. Unfortunately there is no way to determine 
	/// which method is used for a specific GPS device, therefore it is recommended 
	/// that a unique value is used in order to support the widest range of units.
	/// </para>
	/// <para>
	/// The 'Number' Property must be unique for each route.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D202</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Waypoints</term><description>Waypoints</description></item>
	/// </list>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D210</b>
	/// </para>
	/// <para>
	/// This protocol is a Route Link Protocol and if implemented by a GPS device has 
	/// the effect of populating additional properties of the Waypoint objects 
	/// returned by the �Waypoints� Property of the Route object. (Please note 
	/// that this is not supported in the GPS ActiveX Library prior to Version 2 
	/// and in the GPS Library for .Net prior to version 2.3).
	/// </para>
	/// <para>
	/// The Properties of the Waypoint object affected are shown below:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>RouteLinkIdentifier</term><description>String</description></item>
	/// <item><term>RouteLinkClass</term><description>Int16</description></item>
	/// <item><term>RouteLinkSubclass</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'RouteLinkClass' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Line</description></item>
	/// <item><term>1</term><description>Link</description></item>
	/// <item><term>2</term><description>Net</description></item>
	/// <item><term>3</term><description>Direct</description></item>
	/// <item><term>255</term><description>Snap</description></item>
	/// </list>
	/// <para>
	/// The 'RouteLinkIdentifier' Property has a maximum length of 50 characters.
	/// If the 'RouteLinkClass' Property is set to the value for 'Direct' or 'Snap'
	/// then 'RouteLinkSubclass' should be set to 
	/// "00 00 00 00 00 00 FF FF FF FF FF FF FF FF FF FF FF FF".
	/// </para>
	/// </remarks>
	public class Route : DataObjectBase
	{
		private int m_hashCode = -1;

        private short m_number = 0;
        private string m_identifier = "";
        private string m_comment = "";
        private WaypointCollection m_waypoints;

		/// <summary>
		/// Returns/Sets the Route Number. Not all GPS devices supply a route number.
		/// </summary>
        public short Number
        {
            get
            {
                return m_number;
            }
            set
            {
                m_number = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Route Identifier. Not all GPS devices supply a Route Identifier.
		/// </summary>
        public string Identifier
        {
            get
            {
                return m_identifier;
            }
            set
            {
                m_identifier = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Comment for the Route.
		/// </summary>
        public string Comment
        {
            get
            {
                return m_comment;
            }
            set
            {
                m_comment = value;
            }
        }
		/// <summary>
		/// Returns a WaypointCollection object. 
		/// </summary>
		public WaypointCollection Waypoints
        {
            get
            {
                return m_waypoints;
            }
            set
            {
                m_waypoints = value;
            }
        }
	
		private const string XML_ROOT = "gpsroute";
		private const string XML_NUMBER = "number";
		private const string XML_IDENTIFIER = "identifier";
		private const string XML_COMMENT = "comment";
		private const string XML_WAYPOINTS = "gpswaypoints";
		private const string XML_WAYPOINT = "gpswaypoint";

		/// <summary>
		/// Constructor.
		/// </summary>
		public Route()
		{
			//MyBase.New();
			Waypoints = new WaypointCollection();
		}
		/// <summary>
		/// Returns an XML representation of the object.
		/// </summary>
		public string ToXml()
		{
			XmlDocument objXMLDOM = new XmlDocument();

			XmlElement objRootNode = null;
			XmlElement objChildNode = null;
			string strXML = "";
			StringBuilder strbldXML = new StringBuilder("");
			
			try
			{
				//create the root node
				objRootNode = objXMLDOM.CreateElement(XML_ROOT);
			
				//append the root node to the document
				objXMLDOM.AppendChild(objRootNode);

				//create the child nodes
				AddXMLNode(objRootNode, XML_NUMBER, Number.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_IDENTIFIER, Identifier, true);
				AddXMLNode(objRootNode, XML_COMMENT, Comment, true);
				
				objChildNode = AddXMLNode(objRootNode, XML_WAYPOINTS, "", false);
        
				//get the waypoints XML
				foreach(Waypoint objWaypoint in Waypoints)
				{
					strbldXML.Append(objWaypoint.ToXml());
				}
	
				strXML = strbldXML.ToString();

				objChildNode.InnerXml = strXML; 
				
				//append the root node of the wpt dom to our root node
				objRootNode.AppendChild(objChildNode);

				//objWptDom = null;
			
			}
            catch (NullReferenceException e)
            {
				throw new XmlException(e.Message, e);
			}

			return objXMLDOM.OuterXml;
		}

		/// <summary>
		/// This method populates the object from XML.
		/// </summary>
        public void XmlLoad(string xml)
		{
			string strXPathRoot = "";
			XmlDocument objDOM = new XmlDocument();
			XmlNode objNode = null;

			try
			{
                objDOM.LoadXml(xml);

				if(objDOM.FirstChild.Name == XML_ROOT)
				{
					strXPathRoot = string.Concat("//" , XML_ROOT , "/");

					//need to ignore type conversion errors and missing xml elements
					Number = ReadXMLNodeAsShort(objDOM,string.Concat(strXPathRoot , XML_IDENTIFIER));
					Identifier = ReadXMLNodeAsString(objDOM,string.Concat(strXPathRoot , XML_IDENTIFIER));
					Comment = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_COMMENT));
					
					try
					{
						objNode = objDOM.SelectSingleNode(string.Concat(strXPathRoot, XML_WAYPOINTS));
						foreach(XmlNode objChildNode in objNode.ChildNodes)
						{
							switch(objChildNode.Name)
							{
								case XML_WAYPOINT:

									Waypoint objWaypoint = new Waypoint();
									objWaypoint.XmlLoad(objChildNode.OuterXml);
									Waypoints.Add(objWaypoint);
									break;
							
							}

						}
					}
					catch(NullReferenceException ex)
					{
					}
								
				}
			}
			catch(Exception e)
			{
				throw new XmlException(e.Message ,e);
			}

		}
        /// <summary>
        /// Overridden method. Returns true of the values of each
        /// of the properties are equal in value.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>boolean</returns>
        public override bool Equals(object obj)
		{
			try
			{
				//check the type first
				if(obj.GetType() != this.GetType() )
					return false;

				//cast it
				Route objRoute = (Route)obj;

				//TODO: [jn] removed as comment not transferred with magellan
				//if( objRoute.Comment.ToString(CultureInfo.InvariantCulture) != Comment.ToString(CultureInfo.InvariantCulture) ) return false; 
				if( objRoute.Identifier.ToString(CultureInfo.InvariantCulture)  != Identifier.ToString(CultureInfo.InvariantCulture) ) return false;
				if( objRoute.Number != Number ) return false;

				if( !Waypoints.Equals(objRoute.Waypoints, true) ) return false;

			}
			catch
			{
				throw;
			}
			finally
			{
			}
			//if we get here all must be the same.
			return true;
		}
        /// <summary>
        /// Overridden function. Retrieves a value that indicates the hash code value for the object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{

			//this value will be used for the overriden GetHashCode function and should
			//ensure that two object that are the same return the same Hash Code.
			//the hash code has to be imutable to is stored in a member variable.
			if( m_hashCode <= 0 )
				m_hashCode = Identifier.GetHashCode() ^ Comment.GetHashCode();

			return m_hashCode;
		}

	}
}
