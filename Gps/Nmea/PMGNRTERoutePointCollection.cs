using System;
using System.Collections.Generic;
using System.Text;

namespace Waymex.Gps.Nmea
{
    /// <summary>
    /// A collection of Route Points.
    /// </summary>
    public class PmgnRteRoutePointCollection : System.Collections.IEnumerable
    {

        private System.Collections.ArrayList m_routePoints = new System.Collections.ArrayList();

        /// <summary>
        /// Contructor for the PmgnRteRoutePointCollection class.
        /// </summary>
        public PmgnRteRoutePointCollection()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        /// <summary>
        /// This method is the enumerator for the collection.
        /// </summary>
        public System.Collections.IEnumerator GetEnumerator()
        {
            return m_routePoints.GetEnumerator();
        }
        /// <summary>
        /// This method adds a RoutePoint object to the collection. 
        /// </summary>
        internal void Add(PmgnRteRoutePoint routePoint)
        {
            m_routePoints.Add(routePoint);
        }
        /// <summary>
        /// Returns the number of RoutePoint objects in the GsvSatelliteCollection. 
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return m_routePoints.Count;
        }
        /// <summary>
        /// Can be used to refer to a member of the PmgnRteRoutePointCollection by ordinal reference.
        /// This Property is the default indexer in C#.
        /// </summary>
        public PmgnRteRoutePoint this[int index]
        {
            get
            {
                return (PmgnRteRoutePoint)m_routePoints[index];
            }
        }

    }
}
