using System;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// PGRME NMEA propriertry sentence class.
	/// </summary>
	public class SentencePgRme : Sentence
	{
		private double m_dblEstimatedHPE = 0.0;
		private double m_dblEstimatedVPE = 0.0;
		private double m_dblOverallSphericalEPE = 0.0;

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePgRme(string sentence)
            : base(sentence)
		{
            Populate(sentence);
		}
		private void Populate(string strSentence)
		{
			if(SentenceItemExists(1)) m_dblEstimatedHPE = FormatNumber(m_astrSentence[1]);
			if(SentenceItemExists(3)) m_dblEstimatedVPE = FormatNumber(m_astrSentence[3]);
			if(SentenceItemExists(5)) m_dblOverallSphericalEPE = FormatNumber(m_astrSentence[5]);        
		}
		///<summary>
		///Returns the estimated Horizontal position error (HPE) in meters.
		///</summary>
		public double EstimatedHpe
		{
			get
			{
				return m_dblEstimatedHPE;
			}
		}
		///<summary>
		///Returns the estimated vertical position error (VPE) in meters.
		///</summary>
		public double EstimatedVpe
		{
			get
			{
				return m_dblEstimatedVPE;
			}
		}
		///<summary>
		///Returns the estimated overall position error (EPE) in meters.
		///</summary>
		public double OverallSphericalEpe
		{
			get
			{
				return m_dblOverallSphericalEPE;
			}
		}
	}
}
