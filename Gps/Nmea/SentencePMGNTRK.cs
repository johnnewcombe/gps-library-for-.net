using System;
using System.Globalization;
using System.Text;


namespace Waymex.Gps.Nmea
{
    /// <summary>
    /// PMGNTRK NMEA propriertry sentence class.
    /// </summary>
    public class SentencePmgnTrk : Sentence
    {
        private const string SENTENCE_TYPE = "PMGNTRK";

        double m_latitude = 0.0;
        double m_longitude = 0.0;
        double m_altitude = 0.0;
        string m_altUnits = "";
        string m_name = "";
        DateTime m_time;
        DateTime m_date;
        bool m_fix = false;
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        public SentencePmgnTrk()
            : base()
		{
		}
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePmgnTrk(string sentence)
            : base(sentence)
        {
            try
            {
                //check the type to ensure that we can continue
                if (this.TypeId.ToUpper(CultureInfo.InvariantCulture) == SENTENCE_TYPE)
                {
                    Populate(sentence);
                }
                else
                {
                    throw new InvalidSentence(C_MSG_001);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private void Populate(string p_strSentence)
        {
            string time = "";
            string date = "";
            string fix = "";

            //add sentence specifics here
            if (SentenceItemExists(1) && SentenceItemExists(2)) m_latitude = FormatPosition(m_astrSentence[1] + m_astrSentence[2]);
            if (SentenceItemExists(3) && SentenceItemExists(4)) m_longitude = FormatPosition(m_astrSentence[3] + m_astrSentence[4]);
            if (SentenceItemExists(5)) m_altitude = FormatNumber(m_astrSentence[5]);
            if (SentenceItemExists(6)) m_altUnits = m_astrSentence[6];
            if (SentenceItemExists(7)) m_time = Convert.ToDateTime(FormatTime(m_astrSentence[7]), CultureInfo.InvariantCulture);
            if (SentenceItemExists(8)) fix = m_astrSentence[8];
            if (SentenceItemExists(9)) m_name = m_astrSentence[9];
            if (SentenceItemExists(10)) m_date = Convert.ToDateTime(FormatDate(m_astrSentence[10]), CultureInfo.InvariantCulture);

            switch (fix.ToUpper(CultureInfo.InvariantCulture))
            {
                case "A":
                    m_fix = false;
                    break;
                case "V":
                    m_fix = true;
                    break;
            }


            if (SentenceItemExists(7)) date = m_astrSentence[8];

        }
        /// <summary>
        /// Gets/Sets the Track  Name.
        /// </summary>
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }
        /// <summary>
        /// Gets/Sets the Latitude of the track point.
        /// </summary>
        public double Latitude
        {
            get
            {
                return m_latitude;
            }
            set
            {
                m_latitude = value;
            }
        }
        /// <summary>
        /// Gets/Sets the Longitude of the track point.
        /// </summary>
        public double Longitude
        {
            get
            {
                return m_longitude;
            }
            set
            {
                m_longitude = value;
            }
        }
        /// <summary>
        /// Gets/Sets the Altitude of the track point.
        /// </summary>
        public double Altitude
        {
            get
            {
                return m_altitude;
            }
            set
            {
                m_altitude = value;
            }
        }
        /// <summary>
        /// Gets/Sets the units of measurement for the Altitude of the track point.
        /// </summary>
        public string AltitudeUnits
        {
            get
            {
                return m_altUnits;
            }
            set
            {
                m_altUnits = value;
            }
        }
        /// <summary>
        /// Gets/Sets the Date (UTC) of the track point.
        /// </summary>
        public DateTime UtcDate
        {
            get
            {
                return m_date;
            }
            set
            {
                m_date = value;
            }
        }
        /// <summary>
        /// Gets/Sets the Time (UTC) of the track point.
        /// </summary>
        public DateTime UtcTime
        {
            get
            {
                return m_time;
            }
            set
            {
                m_time = value;
            }
        }
        /// <summary>
        /// Gets/Sets the Fix value for the track point.
        /// </summary>
        public bool Fix
        {
            get
            {
                return m_fix;
            }
            set
            {
                m_fix = value;
            }
        }
        /// <summary>
        /// Returns the sentence as a correctlt formatted NMEA sentence with checksum.
        /// </summary>
        /// <returns>NMEA sentence.</returns>
        public override string ToString()
        {
            //This message is to be used to transmit Track information (basically a list of previous position fixes) which
            //is often displayed on the plotter or map screen of the unit. The first field in this message is the Latitude,
            //followed by N or S. The next field is the Longitude followed by E or W. The next field is the altitude
            //followed by �F� for feet or �M� for meters. The next field is the UTC time of the fix. The next field
            //consists of a status letter of �A� to indicated that the data is valid, or �V� to indicate that the data is not
            //valid. The last character field is the name of the track, for those units that support named tracks. The last
            //field contains the UTC date of the fix. Note that this field is (and its preceding comma) is only produced by
            //the unit when the command PMGNCMD,TRACK,2 is given. It is not present when a simple command of
            //PMGNCMD,TRACK is issued.
            //NOTE: The Latitude and Longitude Fields are shown as having two decimal places. As many additional
            //decimal places may be added as long as the total length of the message does not exceed 82 bytes.
            //$PMGNTRK,llll.ll,a,yyyyy.yy,a,xxxxx,a,hhmmss.ss,A,c----
            //c,ddmmyy*hh<CR><LF>
            try
            {
                //need to see if sentence was passed in constructor
                if (m_strSentence.Length == 0)
                {
                    //not passed in so create from properties
                    StringBuilder sbSentence = new StringBuilder();
                    sbSentence.Append("$");
                    sbSentence.Append(SENTENCE_TYPE);
                    sbSentence.Append(",");
                    sbSentence.Append(FormatPosition(Latitude, Longitude));
                    sbSentence.Append(",");
                    sbSentence.Append(Altitude.ToString(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(AltitudeUnits.ToUpper(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(UtcTime.ToString("hhmmss", CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    if (Fix)
                    {
                        sbSentence.Append("A");
                    }
                    else
                    {
                        sbSentence.Append("V");
                    }
                    sbSentence.Append(",");
                    sbSentence.Append(Name.ToUpper(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(UtcDate.ToString("ddmmyy", CultureInfo.InvariantCulture));
                    sbSentence.Append("*");

                    //calculate checksum
                    long checksum = CalculateChecksum(sbSentence.ToString());

                    string chkSum = checksum.ToString("X");
                    if (chkSum.Length == 1)
                        sbSentence.Append("0");
                    sbSentence.Append(chkSum);

                    return sbSentence.ToString();
                }
                else
                {
                    //passed in to simply return
                    return m_strSentence;
                }
            }
            catch (Exception ex)
            {
                throw new CreateSentenceException(ex.Message, ex);
            }
        }
    }
}

