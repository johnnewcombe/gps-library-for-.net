using System;
using System.Globalization;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// PSLIB NMEA propriertry sentence class.
	/// </summary>
	public class SentencePsLib : Sentence
	{
		private double m_dblFrequency = 0.0;
		private double m_dblBitRate = 0.0;
		private PsLibRequestType m_utRequestType;

		/// <summary>
		/// Enumeration describing the PSLIB request type.
		/// </summary>
		public enum PsLibRequestType
		{
			/// <summary>
			/// Unknown.
			/// </summary>
			Unknown = 0,
			/// <summary>
			/// Status.
			/// </summary>
			Status = 1,
			/// <summary>
			/// Configuration.
			/// </summary>
			Configuration = 2,
			/// <summary>
			/// Tuning message.
			/// </summary>
			TuningMessage = 3
		}

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePsLib(string sentence)
            : base(sentence)
		{
			Populate(sentence);
		}
		private void Populate(string strSentence)
		{

			if(SentenceItemExists(1)) m_dblFrequency = FormatNumber(m_astrSentence[1]);
			if(SentenceItemExists(2)) m_dblBitRate = FormatNumber(m_astrSentence[2]);
			if(SentenceItemExists(3))
			{
				switch(m_astrSentence[3].ToUpper(CultureInfo.InvariantCulture))
				{
					case "J":
						m_utRequestType = PsLibRequestType.Status;
						break;
					case "K":
						m_utRequestType = PsLibRequestType.Configuration;
						break;
					case "":
						m_utRequestType = PsLibRequestType.TuningMessage;
						break;
					default:
						m_utRequestType = PsLibRequestType.Unknown;
						break;
				}
			}
			else
			{
				m_utRequestType = PsLibRequestType.Unknown;
			}
		}

		///<summary>
		///Returns the frequency in Kilohertz (KHz).
		///</summary>
		public double Frequency
		{
			get
			{
				return m_dblFrequency;
			}
		}

		///<summary>
		///Returns bit rate in bits per second (Bps).
		///</summary>
		public double BitRate
		{
			get
			{
				return m_dblBitRate;
			}
		}

		///<summary>
		///Indicates the request type. See RequestType.
		///</summary>
		public PsLibRequestType RequestType
		{
			get
			{
				return m_utRequestType;
			}
		}
	}
}
