using System;
using System.Collections.Generic;
using System.Text;

namespace Waymex.Gps.Nmea
{
    /// <summary>
    /// This object is a Route Point object that forms part of the
    /// PmgnRte sentence.
    /// </summary>
	public class PmgnRteRoutePoint
	{
        private string m_routePointName = "";
        /// <summary>
        /// Gets/Sets the Route point Name.
        /// </summary>
        public string Name
        {
            get
            {
                return m_routePointName;
            }
            set
            {
                m_routePointName = value;
            }
        }
	}
}
