using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
	/// <summary>
    /// PMGNVER NMEA propriertry sentence class.
    /// </summary>
	public class SentencePmgnVer : Sentence
	{
		private const string SENTENCE_TYPE = "PMGNVER";

		private int m_pid = 0;
		private string m_sid = "";
		private string m_description = "";
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePmgnVer(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == SENTENCE_TYPE)
				{
                    Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
				throw e;
			}
		}
		private void Populate(string p_strSentence)
		{
				//add sentence specifics here
            if (SentenceItemExists(1)) m_pid = Convert.ToInt32(FormatNumber(m_astrSentence[1]), CultureInfo.InvariantCulture);
				if(SentenceItemExists(2)) m_sid = m_astrSentence[2];
				if(SentenceItemExists(3)) m_description = m_astrSentence[3];
		}
        /// <summary>
        /// Gets the Product ID of the connected device.
        /// </summary>
		public int ProductId
		{
			get
			{
				return m_pid;
			}
		}
        /// <summary>
        /// Gets the Software version in use.
        /// </summary>
        public string SoftwareVersion
		{
			get
			{
				return m_sid;
			}
		}
        /// <summary>
        /// Gets the Product Description of the connected device.
        /// </summary>
		public string ProductDescription
		{
			get
			{
				return m_description;
			}
		}

	}
}
