using System;

namespace Waymex.Gps.Nmea
{
	/// <summary>
    /// GSV Nmea sentence class.
    /// </summary>

    public class GsaSatellite
	{
	private int m_intSatellitePrn = 0;
		
		/// <summary>
		/// Returns the satellite PRN number.
		/// </summary>
		public int SatellitePrn
		{
			get
			{
				return m_intSatellitePrn;
			}
			set
			{
				m_intSatellitePrn = value;
			}
		}
	}
}
