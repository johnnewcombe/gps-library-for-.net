using System;
using System.Globalization;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// PGRMZ NMEA propriertry sentence class.
	/// </summary>
	public class SentencePgRmz : Sentence
	{
		private double m_dblAltitude = 0.0;
		private PgRmzPositionFix m_utPositionFix;

		/// <summary>
		/// Enumerator describing the PGRMZ position fix.
		/// </summary>
		public enum PgRmzPositionFix
		{
			/// <summary>
			/// Unknown.
			/// </summary>
			DimensionsUnknown = 0,
			/// <summary>
			/// User altitude.
			/// </summary>
			UserAltitude = 1,
			/// <summary>
			/// GPS altitude.
			/// </summary>
			GpsAltitude = 2
		}

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePgRmz(string sentence)
            : base(sentence)
		{
			Populate(sentence);
		}
		private void Populate(string strSentence)
		{
			m_dblAltitude = FormatNumber(m_astrSentence[1]);
            
			//astrSentence(2) is the units for above
			if(SentenceItemExists(3))
			{
				switch(m_astrSentence[3].ToUpper(CultureInfo.InvariantCulture))
				{
					case "2":
						m_utPositionFix = PgRmzPositionFix.UserAltitude;
						break;
					case "3":
						m_utPositionFix = PgRmzPositionFix.GpsAltitude;
						break;
					default:
						m_utPositionFix = PgRmzPositionFix.DimensionsUnknown;
						break;
				}
			}
			else
			{
				m_utPositionFix = PgRmzPositionFix.DimensionsUnknown;
			}
		}
		///<summary>
		///Returns the Altitude in feet.
		///</summary>
		public double Altitude
		{
			get
			{
				return m_dblAltitude;
			}
		}

		///<summary>
		///Returns the Position Fix.
		///</summary>
		public PgRmzPositionFix PositionFix
		{
			get
			{
				return m_utPositionFix;
			}
		}
	}
}
