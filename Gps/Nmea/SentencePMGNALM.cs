using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
    /// <summary>
    /// PMGNALM NMEA propriertry sentence class.
    /// </summary>
    public class SentencePmgnAlm : Sentence
    {
        private const string SENTENCE_TYPE = "PMGNALM";

        private int m_totalMessages = 0;
        private int m_messageNumber = 0;
        private int m_prnNumber = 0;
        private int m_weekNumber = 0;
        private int m_svHealth = 0;
        private int m_eccentricity = 0;
        private int m_refTime = 0;
        private int m_inclinationAngle = 0;
        private int m_rightAscentionRate = 0;
        private int m_rootSemiMajorAxis = 0;
        private int m_perigree = 0;
        private int m_longitudeOfAscensionNode = 0;
        private int m_meanAnomaly = 0;
        private int m_clockF0 = 0;
        private int m_clockF1 = 0;
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePmgnAlm(string sentence)
            : base(sentence)
        {
            try
            {
                //check the type to ensure that we can continue
                if (this.TypeId.ToUpper(CultureInfo.InvariantCulture) == SENTENCE_TYPE)
                {
                    Populate(sentence);
                }
                else
                {
                    throw new InvalidSentence(C_MSG_001);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        private void Populate(string p_strSentence)
        {
            if (SentenceItemExists(1)) m_totalMessages = Convert.ToInt32(FormatNumber(m_astrSentence[1]), CultureInfo.InvariantCulture);
            if (SentenceItemExists(2)) m_messageNumber = Convert.ToInt32(FormatNumber(m_astrSentence[2]), CultureInfo.InvariantCulture);
            if (SentenceItemExists(3)) m_prnNumber = Convert.ToInt32(FormatNumber(m_astrSentence[3]), CultureInfo.InvariantCulture);
            if (SentenceItemExists(4)) m_weekNumber = Convert.ToInt32(FormatNumber(m_astrSentence[4]), CultureInfo.InvariantCulture);

            //all these are hex, so convert to integer
            if (SentenceItemExists(5)) m_svHealth = Convert.ToInt32(m_astrSentence[5], 16);
            if (SentenceItemExists(6)) m_eccentricity = Convert.ToInt32(m_astrSentence[6], 16);
            if (SentenceItemExists(7)) m_refTime = Convert.ToInt32(m_astrSentence[7], 16);
            if (SentenceItemExists(8)) m_inclinationAngle = Convert.ToInt32(m_astrSentence[8], 16);
            if (SentenceItemExists(9)) m_rightAscentionRate = Convert.ToInt32(m_astrSentence[9], 16);
            if (SentenceItemExists(10)) m_rootSemiMajorAxis = Convert.ToInt32(m_astrSentence[10], 16);
            if (SentenceItemExists(11)) m_perigree = Convert.ToInt32(m_astrSentence[11], 16);
            if (SentenceItemExists(12)) m_longitudeOfAscensionNode = Convert.ToInt32(m_astrSentence[12], 16);
            if (SentenceItemExists(13)) m_meanAnomaly = Convert.ToInt32(m_astrSentence[13], 16);
            if (SentenceItemExists(14)) m_clockF0 = Convert.ToInt32(m_astrSentence[14], 16);
            if (SentenceItemExists(15)) m_clockF1 = Convert.ToInt32(m_astrSentence[15], 16);

        }
        /// <summary>
        /// Gets the total number of messages.
        /// </summary>
        public int TotalMessages
        {
            get
            {
                return m_totalMessages;
            }
        }
        /// <summary>
        /// Gets the Message Number.
        /// </summary>
        public int MessageNumber
        {
            get
            {
                return m_messageNumber;
            }
        }
        /// <summary>
        /// Gets the Satellite PRN Number.
        /// </summary>
        public int SatelliteId
        {
            get
            {
                return m_prnNumber;
            }
        }
        /// <summary>
        /// Gets the GPS Week Number. Date and time in GPS is computed
        /// as number of weeks from 6 January 1980 plus the number of 
        /// Seconds into the week.
        /// </summary>
        public int WeekNumber
        {
            get
            {
                return m_weekNumber;
            }
        }
        /// <summary>
        /// Gets the satellite vehicles Health.
        /// </summary>
        public int Health
        {
            get
            {
                return m_svHealth;
            }
        }
        /// <summary>
        /// Gets the Eccentricity.
        /// </summary>
        public int Eccentricity
        {
            get
            {
                return m_eccentricity;
            }
        }
        /// <summary>
        /// Gets the Almanac Reference Time.
        /// </summary>
        public int ReferenceTime
        {
            get
            {
                return m_refTime;
            }
        }
        /// <summary>
        /// Gets the Inclination Angle.
        /// </summary>
        public int InclinationAngle
        {
            get
            {
                return m_inclinationAngle;
            }
        }
        /// <summary>
        /// Gets the Rate of Right Ascension.
        /// </summary>
        public int RightAscensionRate
        {
            get
            {
                return m_rightAscentionRate;
            }
        }
        /// <summary>
        /// Gets the Root of Semi-Major Axis.
        /// </summary>
        public int RootOfSemiMajorAxis
        {
            get
            {
                return m_rootSemiMajorAxis;
            }
        }
        /// <summary>
        /// Gets the Argument of Perigee.
        /// </summary>
        public int Perigee
        {
            get
            {
                return m_perigree;
            }
        }
        /// <summary>
        /// Gets the  Longitude of Ascension Node.
        /// </summary>
        public int LongitudeOfAscensionNode
        {
            get
            {
                return m_longitudeOfAscensionNode;
            }
        }
        /// <summary>
        /// Gets the Mean Anomaly
        /// </summary>
        public int MeanAnomaly
        {
            get
            {
                return m_meanAnomaly;
            }
        }
        /// <summary>
        /// Gets the F0 Clock Parameter.
        /// </summary>
        public int ClockCorrectionF0
        {
            get
            {
                return m_clockF0;
            }
        }
        /// <summary>
        /// Gets the F1 Clock Parameter.
        /// </summary>
        public int ClockCorrectionF1
        {
            get
            {
                return m_clockF1;
            }
        }

    }
}
