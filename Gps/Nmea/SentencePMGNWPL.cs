using System;
using System.Globalization;
using System.Text;


namespace Waymex.Gps.Nmea
{
	/// <summary>
    /// PMGNWPL NMEA propriertry sentence class.
    /// </summary>
	public class SentencePmgnWpl : Sentence
	{
		private const string SENTENCE_TYPE = "PMGNWPL";

		private double m_latitude = 0.0;
		private double m_longitude = 0.0;
		private double m_altitude = 0.0;
		private string m_altUnits = "";
		private string m_name = "";
		private string m_comment = "";
		private string m_icon = "";
		private int m_type = 0;

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        public SentencePmgnWpl()
            : base()
		{
		}
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePmgnWpl(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == SENTENCE_TYPE)
				{
					Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
				throw e;
			}
		}
		private void Populate(string p_strSentence)
		{
				if(SentenceItemExists(1) && SentenceItemExists(2)) m_latitude = FormatPosition(m_astrSentence[1] + m_astrSentence[2]);
				if(SentenceItemExists(3) && SentenceItemExists(4)) m_longitude = FormatPosition(m_astrSentence[3] + m_astrSentence[4]);
				if(SentenceItemExists(5)) m_altitude = FormatNumber(m_astrSentence[5]);
				if(SentenceItemExists(6)) m_altUnits = m_astrSentence[6];
				if(SentenceItemExists(7)) m_name = m_astrSentence[7];
				if(SentenceItemExists(8)) m_comment = m_astrSentence[8];
				if(SentenceItemExists(9)) m_icon = m_astrSentence[9];
				if(SentenceItemExists(10)) m_type = (int)FormatNumber(m_astrSentence[10]);

		}
        /// <summary>
        /// Gets/Sets the Latitude of the Waypoint.
        /// </summary>
		public double Latitude
		{
			get
			{
				return m_latitude;
			}
			set
			{
				m_latitude = value;
			}
		}
        /// <summary>
        /// Gets/Sets the Longitude of the Waypoint.
        /// </summary>
        public double Longitude
		{
			get
			{
				return m_longitude;
			}
			set
			{
				m_longitude = value;
			}
		}
        /// <summary>
        /// Gets/Sets the Altitude of the Waypoint.
        /// </summary>
        public double Altitude
		{
			get
			{
				return m_altitude;
			}
			set
			{
				m_altitude = value;
			}
		}
        /// <summary>
        /// Gets/Sets the units of measure for the Altitude.
        /// </summary>
        public string AltitudeUnits
		{
			get
			{
				return m_altUnits;
			}
			set
			{
				m_altUnits = value;
			}
		}

        /// <summary>
        /// Gets/Sets the name of the Waypoint.
        /// </summary>
        public string Name
		{
			get
			{
				return m_name;
			}
			set
			{
				m_name = value;
			}
		}
        /// <summary>
        /// Gets/Sets the Comment associated with the Waypoint.
        /// </summary>
        public string Comment
		{
			get
			{
				return m_comment;
			}
			set
			{
				m_comment = value;
			}
		}
        /// <summary>
        /// Gets/Sets the Icon value for the Waypoint.
        /// </summary>

		public string Icon
		{
			get
			{
				return m_icon;
			}
			set
			{
				m_icon = value;
			}
		}
        /// <summary>
        /// Gets/Sets the Waypoint type.
        /// </summary>

		public int Type
		{
			get
			{
				return m_type;
			}
			set
			{
				m_type = value;
			}
		}
        /// <summary>
        /// Returns the sentence as a correctlt formatted NMEA sentence with checksum.
        /// </summary>
        /// <returns>NMEA sentence.</returns>
        public override string ToString()
		{
            try
            {
                //need to see if sentence was passed in constructor
                if (m_strSentence.Length == 0)
                {
                    //not passed in so create from properties
                    StringBuilder sbSentence = new StringBuilder();
                    sbSentence.Append("$");
                    sbSentence.Append(SENTENCE_TYPE);
                    sbSentence.Append(",");
                    sbSentence.Append(FormatPosition(Latitude, Longitude));
                    sbSentence.Append(",");
                    sbSentence.Append(Altitude.ToString(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(AltitudeUnits.ToUpper(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(Name.ToUpper(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(Comment.ToUpper(CultureInfo.InvariantCulture));
                    sbSentence.Append(",");
                    sbSentence.Append(Icon);
                    sbSentence.Append(",");
                    sbSentence.Append(Type.ToString("00", CultureInfo.InvariantCulture));
                    sbSentence.Append("*");

                    //calculate checksum
                    long checksum = CalculateChecksum(sbSentence.ToString());

                    string chkSum = checksum.ToString("X");
                    if (chkSum.Length == 1)
                        sbSentence.Append("0");
                    sbSentence.Append(chkSum);

                    //string temp = sbSentence.ToString(CultureInfo.InvariantCulture);

                    return sbSentence.ToString();
                }
                else
                {
                    //passed in to simply return
                    return m_strSentence;
                }
            }
            catch (Exception ex)
            {
                throw new CreateSentenceException(ex.Message, ex);
            }
		}
	}
}
