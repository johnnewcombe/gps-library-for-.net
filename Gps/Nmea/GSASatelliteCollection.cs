using System;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// GsaSatelliteCollection class.
	/// </summary>
	public class GsaSatelliteCollection: System.Collections.IEnumerable
	{
		private System.Collections.ArrayList m_colGsaSatellites = new System.Collections.ArrayList();

		/// <summary>
		/// Constructor for the GsaSatelliteCollection class.
		/// </summary>
        public GsaSatelliteCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		/// <summary>
		/// This method is the enumerator for the GsaSatelliteCollection collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colGsaSatellites.GetEnumerator();
		}

		
		internal void Add(GsaSatellite Satellite)
		{
			m_colGsaSatellites.Add(Satellite);
		}
	
		/// <summary>
		/// Returns the number of GsaSatellite objects in the GsaSatelliteCollection collection. 
		/// </summary>
		public int Count()
		{
			return m_colGsaSatellites.Count;
		}

		//default indexer property
		/// <summary>
		/// Can be used to refer to a member of the GsaSatelliteCollection collection by ordinal reference.
		/// This Property is the default indexer in C#.
		/// </summary>
		public GsaSatellite this[int index]
		{
			get
			{
				return (GsaSatellite)m_colGsaSatellites[index];
			}
		}
	}
}
