using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// WPL NMEA sentence class.
	/// </summary>
	public class SentenceWpl : Sentence
	{
		private const string C_SENTENCE_TYPE = "WPL";

		private double m_dblLatitude = 0.0;
		private double m_dblLongitude = 0.0;
		private string m_strWaypointName = "";

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentenceWpl(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == C_SENTENCE_TYPE)
				{
					Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
                throw new InvalidSentence(e.Message, e);
			}
		}

		private void Populate(string p_strSentence)
		{
			//add sentence specifics here
			if(SentenceItemExists(2)) m_dblLatitude = FormatPosition(m_astrSentence[1] + m_astrSentence[2]);
			if(SentenceItemExists(4)) m_dblLongitude = FormatPosition(m_astrSentence[3] + m_astrSentence[4]);
			if(SentenceItemExists(5)) m_strWaypointName = m_astrSentence[5];
		}

		///<summary>
		///Returns the Latitude in Degrees. Negative values indicate positions West.
		///</summary>
		public double Latitude
		{
			get
			{
				return m_dblLatitude;
			}
		}

		///<summary>
		///Returns the Longitude in Degrees. Negative values indicate positions South.
		///</summary>
		public double Longitude
		{
			get
			{
				return m_dblLongitude;
			}
		}

		///<summary>
		///Returns the Waypoint name.
		///</summary>
		public string WaypointName
		{
			get
			{
				return m_strWaypointName;
			}
		}

	}
}
