using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
	/// <summary>
    /// PMGNCMD NMEA propriertry sentence class.
    /// </summary>
	public class SentencePmgnCmd : Sentence
	{
		private const string SENTENCE_TYPE = "PMGNCMD";

		private string m_command = "";
		private string m_parameter1 = "";
		private string m_parameter2 = "";
		private string m_parameter3 = "";
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePmgnCmd(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == SENTENCE_TYPE)
				{
					Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
				throw e;
			}
		}
		private void Populate(string p_strSentence)
		{
				//add sentence specifics here
				if(SentenceItemExists(1)) m_command = m_astrSentence[1];
				if(SentenceItemExists(2)) m_parameter1 = m_astrSentence[2];
				if(SentenceItemExists(3)) m_parameter2 = m_astrSentence[3];
				if(SentenceItemExists(3)) m_parameter3 = m_astrSentence[4];
		}
        /// <summary>
        /// Gets the Magellan Command.
        /// </summary>
		public string Command
		{
			get
			{
				return m_command;
			}
		}
        /// <summary>
        /// Gets the first parameter for the command.
        /// </summary>
		public string Parameter1
		{
			get
			{
				return m_parameter1;
			}
		}
        /// <summary>
        /// Gets the second parameter for the command.
        /// </summary>
        public string Parameter2
		{
			get
			{
				return m_parameter2;
			}
		}
        /// <summary>
        /// Gets the third parameter for the command.
        /// </summary>
        public string Parameter3
		{
			get
			{
				return m_parameter3;
			}
		}

	}
}
