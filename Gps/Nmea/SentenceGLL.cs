using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// GLL NMEA sentence class.
	/// </summary>
	public class SentenceGll : Sentence
	{
		private const string C_SENTENCE_TYPE = "GLL";

		private double m_dblLatitude = 0.0;
		private double m_dblLongitude = 0.0;
        private System.DateTime m_dtUTC = Convert.ToDateTime("01 Jan 0001", CultureInfo.InvariantCulture);
		private bool m_blnDataValid = false;
		

		/// <summary>
		/// Constructor for the SentenceGll class.
		/// </summary>
        public SentenceGll(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == C_SENTENCE_TYPE)
				{
                    Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
                throw new InvalidSentence(e.Message, e);
			}
		}
		private void Populate(string p_strSentence)
		{
			
			if(SentenceItemExists(2)) m_dblLatitude = FormatPosition(m_astrSentence[1] + m_astrSentence[2]);
			if(SentenceItemExists(4)) m_dblLongitude = FormatPosition(m_astrSentence[3] + m_astrSentence[4]);
            if(SentenceItemExists(5)) m_dtUTC = Convert.ToDateTime(FormatTime(m_astrSentence[1]), CultureInfo.InvariantCulture);

			if(SentenceItemExists(6)) 
			{
				if(m_astrSentence[6].ToUpper(CultureInfo.InvariantCulture) == "A") //A = OK, V = Invalid
				{
					m_blnDataValid = true;
				}
				else
				{
					m_blnDataValid = false;
				}
			}
			else
			{
				//no explicit data valid field so assume its OK
				m_blnDataValid = true;
			}
		}

		///<summary>
		///Returns the Latitude in Degrees. Negative values indicate positions West.
		///</summary>
		public double Latitude
		{
			get
			{
				return m_dblLatitude;
			}
			set
			{
				m_dblLatitude = value;
			}
		}

		///<summary>
		///Returns the Longitude in Degrees. Negative values indicate positions South.
		///</summary>
		public double Longitude
		{
			get
			{
				return m_dblLongitude;
			}
			set
			{
				m_dblLongitude = value;
			}
		}

		///<summary>
		///Returns the the time (UTC).
		///</summary>
		public System.DateTime Utc
		{
			get
			{
				return m_dtUTC;
			}
			set
			{
				m_dtUTC = value;
			}
		}

		///<summary>
		///Returns the status of the data.
		///</summary>
		public bool DataValid
		{
			get
			{
				return m_blnDataValid;
			}
			set
			{
				m_blnDataValid = value;
			}
		}
	}
}
