using System;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// GSV satellite class.
	/// </summary>
	public class GsvSatellite
	{
		/// <summary>
		/// Constructor for the GsvSatellite class.
		/// </summary>
        public GsvSatellite()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private int m_intSatellitePrn = 0;
		private int m_intSatelliteElevation = 0;
		private int m_intSatelliteAzimuth = 0;
		private int m_intSignalToNoiseRatio = 0;
		/// <summary>
		/// Returns the satellite PRN number.
		/// </summary>
		public int SatellitePrn
		{
			get
			{
				return m_intSatellitePrn;
			}
			set
			{
				m_intSatellitePrn = value;
			}
		}
		/// <summary>
		/// Returns the satellite elevation (0-90 Degrees). 
		/// </summary>
		public int SatelliteElevation
		{
			get
			{
				return m_intSatelliteElevation;
			}
			set
			{
				m_intSatelliteElevation = value;
			}
		}
		/// <summary>
		/// Returns the satellite azimuth (0-359 Degrees True). 
		/// </summary>
		public int SatelliteAzimuth
		{
			get
			{
				return m_intSatelliteAzimuth;
			}
			set
			{
				m_intSatelliteAzimuth = value;
			}
		}
		/// <summary>
		/// Returns the signal to noise ratio (0-99 dB). 
		/// </summary>
		public int SignalToNoiseRatio
		{
			get
			{
				return m_intSignalToNoiseRatio;
			}
			set
			{
				m_intSignalToNoiseRatio = value;
			}
		}
	}
}
