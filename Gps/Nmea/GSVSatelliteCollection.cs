using System;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// GsvSatelliteCollection collection class.
	/// </summary>
    public class GsvSatelliteCollection : System.Collections.IEnumerable
	{
		/// <summary>
		/// Contructor for the GsvSatelliteCollection class.
		/// </summary>
        public GsvSatelliteCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private System.Collections.ArrayList m_colGsvSatellites = new System.Collections.ArrayList();

		/// <summary>
		/// This method is the enumerator for the GsvSatelliteCollection collection.
		/// </summary>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_colGsvSatellites.GetEnumerator();
		}

		internal void Add(GsvSatellite Satellite)
		{
			m_colGsvSatellites.Add(Satellite);
		}
		/// <summary>
		/// Returns the number of GsvSatellite objects in the GsvSatelliteCollection collection. 
		/// </summary>
		/// <returns></returns>
		public int Count()
		{
			return m_colGsvSatellites.Count;
		}

		//default indexer property
		/// <summary>
		/// Can be used to refer to a member of the GsaSatelliteCollection collection by ordinal reference.
		/// This Property is the default indexer in C#.
		/// </summary>
		public GsvSatellite this[int index]
		{
			get
			{
				return (GsvSatellite)m_colGsvSatellites[index];
			}
		}

	}
}
