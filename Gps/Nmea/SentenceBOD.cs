using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// BOD NMEA sentence class.
	/// </summary>
	public class SentenceBod : Sentence
	{
		private const string C_SENTENCE_TYPE = "BOD";

		private double m_dblBearingDegreesTrue = 0.0;
		private double m_dblBearingDegreesMagnetic = 0.0;
		private string m_strToWaypoint = "";
		private string m_strFromWaypoint = "";

		/// <summary>
		/// Constructer for the SentenceBod class.
		/// </summary>
        public SentenceBod(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == C_SENTENCE_TYPE)
				{
					Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
				throw e;
			}
		}
		private void Populate(string p_strSentence)
		{
			//add sentence specifics here
			if(SentenceItemExists(1)) m_dblBearingDegreesTrue = FormatNumber(m_astrSentence[1]);
			if(SentenceItemExists(3)) m_dblBearingDegreesMagnetic = FormatNumber(m_astrSentence[3]);
			if(SentenceItemExists(5)) m_strToWaypoint = m_astrSentence[5];
			if(SentenceItemExists(6)) m_strFromWaypoint = m_astrSentence[6];
		}

		///<summary>
		///Returns the bearing in degrees True.
		///</summary>
		public double BearingDegreesTrue
		{
			get
			{
				return m_dblBearingDegreesTrue;
			}
			set
			{
				m_dblBearingDegreesTrue = value;
			}
		}

		///<summary>
		///Returns the bearing in Degrees Magnetic.
		///</summary>
		public double BearingDegreesMagnetic
		{
			get
			{
				return m_dblBearingDegreesMagnetic;
			}
			set
			{
				m_dblBearingDegreesMagnetic = value;
			}
		}

		///<summary>
		///Returns the name of the destination Waypoint.
		///</summary>
		public string ToWaypoint
		{
			get
			{
				return m_strToWaypoint;
			}
			set
			{
				m_strToWaypoint = value;
			}
		}

		///<summary>
		///Returns the name of the originating Waypoint.
		///</summary>
		public string FromWaypoint
		{
			get
			{
				return m_strFromWaypoint;
			}
			set
			{
				m_strFromWaypoint = value;
			}
		}

	}
}
