using System;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// PGRMM NMEA propriertry sentence class.
	/// </summary>
	public class SentencePgRmm : Sentence
	{
		private string m_strActiveDatum = "";

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        /// <param name="sentence">NMEA sentence.</param>
        public SentencePgRmm(string sentence)
            : base(sentence)
		{
			Populate(sentence);
		}
		private void Populate(string strSentence)
		{
			//add sentence specifics here
			if(SentenceItemExists(1)) m_strActiveDatum = m_astrSentence[1];
		}
		///<summary>
		///Returns the currently active Datum.
		///</summary>
		public string ActiveDatum
		{
			get
			{
				return m_strActiveDatum;
			}
		}
	}
}
