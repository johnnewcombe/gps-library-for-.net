using System;
using System.Globalization;


namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// GSV NMEA sentence class.
	/// </summary>
	public class SentenceGsv : Sentence
	{
		private const string C_SENTENCE_TYPE = "GSV";

		private int m_intTotalGSVSentences = 0;
		private int m_intCurrentGSVSentence = 0;
		private int m_intSatellitesInView = 0;
		private GsvSatelliteCollection m_colSatellites = new GsvSatelliteCollection();

		/// <summary>
		/// Constructor for the SentenceGsv class.
		/// </summary>
        public SentenceGsv(string sentence)
            : base(sentence)
		{
			try
			{
				//check the type to ensure that we can continue
				if(this.TypeId.ToUpper(CultureInfo.InvariantCulture) == C_SENTENCE_TYPE)
				{
                    Populate(sentence);
				}
				else
				{
					throw new InvalidSentence(C_MSG_001);
				}
			}
			catch(Exception e)
			{
                throw new InvalidSentence(e.Message, e);
			}
		}

		private void Populate(string p_strSentence)
		{
			//add sentence specifics here
			if(SentenceItemExists(1))
			{
				if(IsNumeric(m_astrSentence[1]))
				{
                    m_intTotalGSVSentences = Convert.ToInt32(m_astrSentence[1], CultureInfo.InvariantCulture);
				}
			}
			if(SentenceItemExists(2))
			{
				if(IsNumeric(m_astrSentence[2]))
				{
                    m_intCurrentGSVSentence = Convert.ToInt32(m_astrSentence[2], CultureInfo.InvariantCulture);
				}
			}
			if(SentenceItemExists(3))
			{
				if(IsNumeric(m_astrSentence[3]))
				{
                    m_intSatellitesInView = Convert.ToInt32(m_astrSentence[3], CultureInfo.InvariantCulture);
				}
			}           
			for(int f = 4; f < m_astrSentence.Length;f = f + 4)//maximum of 12 satellites
			{                
				//dont add an object to the collection if the Satellite PRN Number is not valid
				if(IsNumeric(m_astrSentence[f]))
				{
					GsvSatellite objSatellite = new GsvSatellite();
                    objSatellite.SatellitePrn = Convert.ToInt32(m_astrSentence[f], CultureInfo.InvariantCulture);
					
					if(IsNumeric(m_astrSentence[f + 1]))
                        objSatellite.SatelliteElevation = Convert.ToInt32(m_astrSentence[f + 1], CultureInfo.InvariantCulture);

					if(IsNumeric(m_astrSentence[f + 2]))
                        objSatellite.SatelliteAzimuth = Convert.ToInt32(m_astrSentence[f + 2], CultureInfo.InvariantCulture);

					if(IsNumeric(m_astrSentence[f + 3]))
                        objSatellite.SignalToNoiseRatio = Convert.ToInt32(m_astrSentence[f + 3], CultureInfo.InvariantCulture);
					
					m_colSatellites.Add(objSatellite);
					objSatellite = null;
				}			
			}		
		}

		///<summary>
		///Returns the total number of GSV sentences to be transmitted.
		///</summary>
		public int TotalGsvSentences
		{
			get
			{
				return m_intTotalGSVSentences;
			}
			set
			{
				m_intTotalGSVSentences = value;
			}
		}

		///<summary>
		///Returns the number of the current GSV sentence.
		///</summary>
		public int CurrentGsvSentence
		{
			get
			{
				return m_intCurrentGSVSentence;
			}
			set
			{
				m_intCurrentGSVSentence = value;
			}
		}

		///<summary>
		///Returns the total number of satellites in view.
		///</summary>
		public int SatellitesInView
		{
			get
			{
				return m_intSatellitesInView;
			}
			set
			{
				m_intSatellitesInView = value;
			}
		}

		///<summary>
		///Returns a GsvSatelliteCollection collection of GsvSatellite objects, each GsaSatellite object represents a satellite in view.
		///</summary>
		public GsvSatelliteCollection Satellites
		{
			get
			{
				return m_colSatellites;
			}
			set
			{
				m_colSatellites = value;
			}
		}
	}
}
