using System;
using System.Text;
using System.Globalization;
using Waymex.Gps;

namespace Waymex.Gps.Nmea
{
	/// <summary>
	/// Base classs for all NMEA sentence classes.
	/// </summary>
	public class Sentence
	{
		internal string m_strSentence = "";//Both
		private string m_strTalkerID = "";//Non Proprietary
		private string m_strTalkerDescription = ""; //Non Proprietary
		private string m_strTypeId = "";//Both
		private string m_strTypeDescription = "";//Non Proprietary
		private long m_lngChecksum = 0;//Both
		private bool m_blnProprietary = false;//Both
		private string m_strManufacturerID = ""; //Proprietary
		private string m_strManufacturerDescription = ""; //Proprietary

		//these constants definitions are repeated in other NMEA Sentence classes and NOT specified as protected
		//this is bt design
		internal const string  C_MSG_001 = "An instance of this object cannot be created with the Sentence passed in the constructor as it is of the wrong type.";
		internal const string  C_MSG_002 = "The checksum in the sentence is incorrect.";
		internal const string  C_MSG_003 = "This property is not available with proprietary sentence objects.";
		internal const string  C_MSG_004 = "This property is not available with non-proprietary sentence objects.";
		internal const string  C_MSG_005 = "The Sentence is invalid.";

		//set here so that  constructors from derived classes have access to the elements of the sentence
		//and dont have to decompose it for themselves

		/// <summary>
		/// This member array is populated by the base class and contains each element of the NMEA Sentence
		/// stored as a string.
		/// </summary>
		internal string[] m_astrSentence = null;
        /// <summary>
        ///Constructor for the Sentence base class. This class is the base class for all other Sentence classes.
        /// </summary>
        public Sentence()
		{
		}
		/// <summary>
		///Constructor for the Sentence base class. This class is the base class for all other Sentence classes.
		/// </summary>
		public Sentence(string sentence)
		{
			Populate(sentence);
		}
		/// <summary>
		///This base class method is used to convert a string representation of a number in to a double. 
		///This function handles locale differences e.g. those that use a comma as a decimel point.
		///Note that NMEA always uses a full stop.
		/// </summary>
		internal double FormatNumber(string Number)
		{

			double dblResult = 0.0;
			
			int intPos = 0;
			int intPlaces = 0;
			string strTemp = "";
			double dblTemp = 0.0;
			try
			{
				if(IsNumeric(Number))
				{
					//find the decimel point
					intPos = Number.IndexOf(".");
				        
					//everything left of the comma are whole numbers
					if(intPos > 0)
					{
						//dblResult = CDbl(Left$(strNumber, intPos - 1))
                        dblResult = Convert.ToDouble(Number.Substring(0, intPos), CultureInfo.InvariantCulture);
					
						//calculate the number of decimel places
						intPlaces = Number.Length  - (intPos + 1);
					
						//add the decimel component
						strTemp = Number.Substring(Number.Length - intPlaces,intPlaces);// / (10 ^ intPlaces);
						
						//if decimel component is empty string then return 0
						if(strTemp.Length == 0)
						{
							dblTemp = 0;
						}
						else
						{
                            dblTemp = Convert.ToDouble(strTemp, CultureInfo.InvariantCulture);
							dblTemp = dblTemp / (Math.Pow(10.0, (double)intPlaces));
						}

						dblResult = dblResult + dblTemp; //+ (()/(10 ^ intPlaces));
					}
					else
					{
						//no decimel point so we can just convert
                        dblResult = Convert.ToDouble(Number, CultureInfo.InvariantCulture);
					}
				}
				else
				{
					dblResult = 0;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return dblResult;
		}

//		internal string FormatString(float Number)
//		{
//			return FormatString((double)Number);
//		}
//		internal string FormatString(double Number)
//			{
//				//sorts out any locale specifics
//			string result = "";
//
//			result = Number.ToString(CultureInfo.InvariantCulture);
//			result = Number.ToString("###.0000", CultureInfo.InvariantCulture);
//			return result;
//
//		}
		internal bool SentenceItemExists(int intParameterNumber)
		{
			// parameter number is the array index
			if(m_astrSentence.Length >= intParameterNumber + 1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Returns True if the NMEA string value passed, is numeric.
		/// </summary>
		/// <param name="Number"></param>
		/// <returns></returns>
		internal bool IsNumeric(string Number)
		{
			char[] achNumber = null;
			achNumber = Number.Trim().ToCharArray();
			bool blnDecimelFound = false;
			//bool blnNegative = false;

			try
			{
				if (achNumber.Length > 0)
				{

					for(int f = 0; f < achNumber.Length;f++)
					{
						if((achNumber[f] <= 57 && achNumber[f] >=48) || (achNumber[f] == 46))
						{
							//no more than one decimel point allowed
							if(achNumber[f] == 46)
							{
								if(blnDecimelFound)
								{
									//cant have more than one decimel point
									return false;
								}
								else
								{
									blnDecimelFound = true;
								}
							}
						}
						else if(achNumber[f] == 45 && f == 0)
						{
							//blnNegative = true;
						}
						else
						{
							//a non number has been found
							return false;
						}
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			//return false;
		}

		private long HexToLong(string p_strHex)
		{
			try
			{
				return Convert.ToInt64(p_strHex.Trim() ,16);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		///<summary>
		///Indicates whether the the NMEA Sentence is a standard NMEA Sentence or a Proprietary Sentence.
		///Propriety Sentences are used by some manufacturers to provide additional data.
		///</summary>
		public bool IsProprietary
		{
			get
			{
				return m_blnProprietary;
			}
		}
		/// <summary>
		/// This method  converts the latitude and longitude values (expressed as signed degrees) into a
		/// string suitable to be inserted into an NMEA sentence. The NMEA format is used i.e. 3815.5,N 
		/// means 38 degrees 15 minutes and 30 seconds, 11805.25 meants 118 degrees, 5 minutes, 15 seconds.
		/// See NMEA documentation for WPL sentence.
		/// </summary>
		/// <param name="Latitude">Latitude part of the position.</param>
		/// <param name="Longitude">Longitude part of the position.</param>
		/// <returns></returns>
		internal string FormatPosition(double Latitude, double Longitude)
		{
			StringBuilder position = new StringBuilder();

			string easting = "";
			string northing = "";
			string latitude = "";
			string longitude = "";

			//sort out the northing and easting
			if( Latitude >= 0 )
			{
				northing = "N";
			}
			else
			{
				northing = "S";
				Latitude = Latitude * -1;
			}
			if( Longitude >= 0 )
			{
				easting = "E";
			}
			else
			{
				easting = "W";
				Longitude = Longitude * -1;
			}

			//first the latitude
			latitude = FormatLatitude(Latitude);
			longitude = FormatLongitude(Longitude);
		
			//build the output so far
			position.Append(latitude);
			position.Append(",");
			position.Append(northing);
			position.Append(",");
			position.Append(longitude);
			position.Append(",");
			position.Append(easting);
						
			return position.ToString();
		}
		/// <summary>
		/// This function formats a latitude (expressed as signed degrees) as per the NMEA 
		/// format suitable to be inserted into an NMEA sentence. The NMEA format is used i.e. 3815.5,N 
		/// means 38 degrees 15 minutes and 30 seconds, 11805.25 meants 118 degrees, 5 minutes, 15 seconds.
		/// See NMEA documentation for WPL sentence.
		/// </summary>
		/// <param name="Position"></param>
		/// <returns></returns>
		private string FormatLatitude(double Position)
		{
			StringBuilder sbPosition = new StringBuilder();

			double minutes = 0.0;
			double whole = 0.0;
			double fraction = 0.0;

			int degrees = 0;
			
			//get the decimel and whole number parts, the whole number is the degrees the fraction is minutes and seconds
			fraction = Position % 1;
			whole = Position - fraction;
			degrees = (int)whole;

			//next stage
			minutes = fraction * 60;

			sbPosition.Append(degrees.ToString("00"));
			sbPosition.Append(minutes.ToString("00.000"));

			return sbPosition.ToString();


		}
		/// <summary>
		/// This function formats a longitude (expressed as signed degrees) as per the NMEA 
		/// format suitable to be inserted into an NMEA sentence. The NMEA format is used i.e. 3815.5,N 
		/// means 38 degrees 15 minutes and 30 seconds, 11805.25 meants 118 degrees, 5 minutes, 15 seconds.
		/// See NMEA documentation for WPL sentence.
		/// </summary>
		/// <param name="Position"></param>
		/// <returns></returns>
		private string FormatLongitude(double Position)
		{
			StringBuilder sbPosition = new StringBuilder();

			double minutes = 0.0;
			double whole = 0.0;
			double fraction = 0.0;

			int degrees = 0;
			
			//get the decimel and whole number parts, the whole number is the degrees the fraction is minutes and seconds
			fraction = Position % 1;
			whole = Position - fraction;
			degrees = (int)whole;

			//next stage
			minutes = fraction * 60;

			sbPosition.Append(degrees.ToString("000"));
			sbPosition.Append(minutes.ToString("00.000"));

			return sbPosition.ToString();


		}
		/// <summary>
		/// This method returns a double based on an NMEA positional values. Values can include the N, E, W, S modifiers
		/// e.g. 4917.24N. The modifiers S and E will cause the return value to be negative.
		/// </summary>
		internal double FormatPosition(string Position)
		{
			string strDegrees = "";
			string strMinutes = "";
			double dblDegrees = 0.0;
			double dblMinutes = 0.0;
			string strDirection = "";
			int intLength = 0;
			double dblPosition = 0.0;
			int intPointPos = 0;

			try
			{
				intLength = Position.Length;
				if(intLength > 0)
				{
					strDirection = Position.Substring(intLength - 1,1).ToUpper(CultureInfo.InvariantCulture);
				
					//if the last char is not a number chck for direction
					if ((strDirection.CompareTo("N") == 0) || (strDirection.CompareTo("S") == 0) || (strDirection.CompareTo("E") == 0) || (strDirection.CompareTo("W") == 0))
					{
						Position = Position.Substring(0,intLength-1);
					}
					intPointPos = Position.IndexOf(".");

					strDegrees = Position.Substring(0, intPointPos - 2);
					strMinutes = Position.Substring(intPointPos - 2, Position.Length  - (intPointPos - 2));
				
					//if the current locale uses a comma as the decimel place then
					//the decimel point in strMinutes will need to be changed to a comma
					//or the Convert.ToDouble doesn't work correctly so we use the FormatNumber function instead
					if(IsNumeric(strDegrees))
					{
						dblDegrees = FormatNumber(strDegrees);
					}
					if(IsNumeric(strMinutes))
					{
						dblMinutes = FormatNumber(strMinutes);
					}
				
					//convert the minutes to degrees (a minute is 1/60th of a degree)
					dblPosition = (dblDegrees + (dblMinutes / 60));
				   
					if(strDirection.CompareTo("S") == 0 || strDirection.CompareTo("W") == 0)
					{
						return dblPosition * -1;
					}
					else
					{
						return dblPosition;
					}
				}    
				else
				{
					return 0.0;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		///<summary>
		///Returns the checksum value of the NMEA Sentence. Not all NMEA Sentences include a checksum
		///in these cases the value of this property will be 0. If the checksum in the contained within
		///the NMEA Sentence is invalid for the Sentence then the value will be set to -1.
		///</summary>
		public long Checksum
		{
			get
			{
					return m_lngChecksum;
			}
		}

		///<summary>
		///Returns the NMEA Sentence as a string in its original format with validated checksum.
		///</summary>
		public override string ToString()
		{
				return m_strSentence;
		}
		
		/// <summary>
		/// This method, available to derived classes to format NMEA date stings into strings
		/// suitable for use in Date/Time functions.
		/// </summary>
		internal string FormatDate(string Date)
		{
			string strFormattedDate = "";
			string strYear = "";
			string strMonth = "";
			int intMonth = 0;
			string strDay = "";
			string[] astrMonth = new string[12]{"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

			try
			{
				if(IsNumeric(Date))
				{
					//need to take account of system locales
					strDay = Date.Substring(0, 2);
					strYear = "20" + Date.Substring(4, 2);
                    intMonth = Convert.ToInt32(Date.Substring(2, 2), CultureInfo.InvariantCulture);
				
					if((intMonth >0 && intMonth <13))
					{
				
						strMonth = astrMonth[intMonth - 1];
				
						strFormattedDate = strDay + " " + strMonth + " " + strYear;
						//strFormattedDate = p_strDate.Substring(0, 2) + "/" + p_strDate.Substring(2, 2) + "/20" + p_strDate.Substring(4, 2);
					}
					else
					{
						strFormattedDate = "01 Jan 0001";
					}
				}
				else
				{
					strFormattedDate = "01 Jan 0001";
				}
			}
			catch( FormatException ex)
			{
				strFormattedDate = "01 Jan 0001";
			}
			catch( OverflowException  ex)
            {
                strFormattedDate = "01 Jan 0001";
            }

			return strFormattedDate;

		}

		/// <summary>
		/// This method, available to derived classes to format NMEA time stings into strings
		/// suitable for use in Date/Time functions.
		/// </summary>
		internal string FormatTime(string Time)
		{
			try
			{
                string strFormattedTime = "00:00:00";// DateTime.MinValue.ToShortTimeString();
                if (Time != String.Empty)
                {
                    strFormattedTime = Time.Substring(0, 2) + ":" + Time.Substring(2, 2) + ":" + Time.Substring(4, 2);
                    strFormattedTime = strFormattedTime.Substring(0, 8);
                }
                return strFormattedTime;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private bool ValidateChecksum(string Sentence)
		{
			string strChecksum = "";
			int intPos = 0;
			bool blnValidateChecksum = false;
			long lngCalcCheckSum = 0;

			try
			{
				intPos = Sentence.IndexOf("*", 0);

				//if the asterist is present it shouldnt be the last character
				if (intPos >= 0 && intPos != Sentence.Length - 1)
				{
					
					//get the checksum from the sentence
					strChecksum = Sentence.Substring(intPos + 1, 2);
					
					lngCalcCheckSum = CalculateChecksum(Sentence);

					//compare
					if(HexToLong(strChecksum) == lngCalcCheckSum)
					{
						//set the checksum property
						m_lngChecksum = lngCalcCheckSum;
						blnValidateChecksum = true;
					}
					else
					{
						m_lngChecksum = -1;
						blnValidateChecksum = false;
					}				
				}    
				else
				{
					//could be an empty sentence or a sentence with no checksum
					if(Sentence.Length > 0)
					{
						//if no checksum on sentence then add it

						//check for the asterisk
						if(Sentence.IndexOf("*", 0) < 0)
						{
							Sentence = Sentence + "*";
						}
						
						m_lngChecksum = CalculateChecksum(Sentence);
						m_strSentence = m_strSentence + m_lngChecksum.ToString("X", CultureInfo.InvariantCulture);

						blnValidateChecksum = true;
					}
					else
					{
						blnValidateChecksum = false;
					}				
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
			return blnValidateChecksum;
		}

		internal long CalculateChecksum(string Sentence)
		{
			char[] bytTemp = null;
			long lngCalcCheckSum = -1;
			int intPos = 0;
			string strTemp;

			try
			{
				intPos = Sentence.IndexOf("*", 0);

				// need to get the string upto the *
				strTemp = Sentence.Substring(1,intPos);

				//calculate checksum
				bytTemp = strTemp.ToCharArray();

				//re-calculate the checksum and compare
				if(bytTemp.Length >= 2)
				{
					lngCalcCheckSum = bytTemp[0];
					for(int f = 1;f <= (bytTemp.Length - 2);f++)
					{
						lngCalcCheckSum = lngCalcCheckSum ^ bytTemp[f];
					}
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return lngCalcCheckSum;
		}

		private string SentenceTrim(string Sentence)
		{
			string strReturn = "";
			int intPos = 0;

			try
			{
				intPos = Sentence.IndexOf("*", 0);
			
				if (intPos > 0)
				{
					strReturn = Sentence.Substring(0, intPos);
				}
				else
				{
					strReturn = Sentence;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return strReturn;
		}

		private void Populate(string p_strSentence)
		{

			//Debug.WriteLine(p_strSentence);

			if (!ValidateSentence(p_strSentence))
			{
				throw new InvalidSentence(C_MSG_005);
			}

			//set the member var so that the property is populated
			m_strSentence = p_strSentence;

			try
			{
				if(ValidateChecksum(p_strSentence))
				{
					//remove the checksum stuff
					p_strSentence = SentenceTrim(p_strSentence);
					char[] achSeparator = ",".ToCharArray();

					//astrSentence = Split(strSentence, ",", , vbTextCompare)
					m_astrSentence = p_strSentence.Split(achSeparator);

				    //determine the type
					if(m_astrSentence[0].Substring(1, 1).ToUpper(CultureInfo.InvariantCulture) == "P")
					{
						m_blnProprietary = true;
					}
					else
					{
						m_blnProprietary = false;
					}

					if(m_blnProprietary)
					{
						m_strTypeId = m_astrSentence[0].Substring(1, m_astrSentence[0].Length - 1);
						m_strTypeDescription = GetTypeDescription(m_strTypeId);
						m_strManufacturerID = m_astrSentence[0].Substring(2, 3);
						m_strManufacturerDescription = GetManufacturerDescription(m_strManufacturerID);
					}
					else
					{
						m_strTalkerID = m_astrSentence[0].Substring(1, 2);
						m_strTalkerDescription = GetTalkerDescription(m_strTalkerID);
						m_strTypeId = m_astrSentence[0].Substring(3, 3);
						m_strTypeDescription = GetTypeDescription(m_strTypeId);
					}
				}
				else
				{
					//checksum error or empty string
					if(p_strSentence.Length  > 0)
					{
						throw new ChecksumException(C_MSG_002);
					}
					else
					{
						//370             'empty string passed
					}			
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			        
			//m_blnProprietary = false;
		}

		private bool ValidateSentence(string p_strSentence)
		{
			bool blnResult = false;
			try
			{
				if(p_strSentence.Length > 3 && p_strSentence.Substring(0,1) == "$")
				{
					blnResult = true;
				}
				else
				{
					blnResult = false;
					}
			}
			catch(Exception ex)
			{
				throw ex;
			}

			return blnResult;

		}
		///<summary>
		///Returns the Talker ID of the NMEA Sentence. This indicates the type of NMEA device
		///that has created the Sentence. For example a GPS device would typically have a
		///Talker ID of 'GP'.
		///</summary>
		public string TalkerId
		{
            get
            {
                if (m_blnProprietary)
                {
                    throw new NotSupportedException(C_MSG_003);
                }
                return m_strTalkerID;

            }		
		}

		///<summary>
		///Returns the Manufacturer ID for Proprietary sentences e.g. 'GRM' would be returned for
		///Garmins Proprietary sentences. The property throws an NMEANotSupported Exception with
		///non Proprietary-sentences.
		///</summary>
		public string ManufacturerId
		{
            get
            {
                if (!m_blnProprietary)
                {
                    throw new NotSupportedException(C_MSG_004);
                }
                return m_strManufacturerID;

            }
		}

		///<summary>
		///Returns the Manufacturer Description for Proprietary Sentences e.g. 'Garmin International Inc.'.
		///The property throws an NMEANotSupported Exception with with non-Proprietary Sentences.
		///</summary>
		public string ManufacturerDescription
		{
            get
            {
                if (!m_blnProprietary)
                {
                    throw new NotSupportedException(C_MSG_004);
                }
                return m_strManufacturerDescription;
            }		
		}

		///<summary>
		///Returns the Type ID of the NMEA Sentence. This property indicates the NMEA Sentence type.
		///For example 'RMC', 'GLL' etc. Proprietary sentence objects return the full type/manufacurer ID
		///e.g. PSLIB, PGRMA etc.
		///</summary>
		public string TypeId
		{
			get
			{
				return m_strTypeId;
			}
		}
		///<summary>
		///Returns the Type Description of the NMEA Sentence. For example if the NMEA Sentence had a
		///Type ID of 'GLL' then this property would return 'Geographic Position - Latitude/Longitude'
		///(See the NMEA Specifications for further details).
		///Proprietary sentence objects return the NMEANotSupported Exception.
		///</summary>
		public string TypeDescription
		{
            get
            {
                if (m_blnProprietary)
                {
                    throw new NotSupportedException(C_MSG_003);
                }
                return m_strTypeDescription;
            }
		}
		///<summary>
		///Returns the Talker Description of the NMEA Sentence. For example a GPS device that had
		///a Talker ID of 'GP' would have a Talker Description of 'Global Positioning System (GPS)'.
		///</summary>
		public string TalkerDescription
		{
            get
            {
                if (m_blnProprietary)
                {
                    throw new NotSupportedException(C_MSG_003);
                }
                return m_strTalkerDescription;
            }
		}


		private string GetManufacturerDescription(string p_strManufacturerID)
		{
			string strManufacturer = "";

			try
			{
				switch(p_strManufacturerID.ToUpper(CultureInfo.InvariantCulture))
				{
					case "AAR":
						strManufacturer = "Asian American Resources";
						break;
					case "ACE":
						strManufacturer = "Auto-Comm Engineering Corporation";
						break;
					case "ACR":
						strManufacturer = "ACR Electronics, Inc.";
						break;
					case "ACS":
						strManufacturer = "Arco Solar, Inc.";
						break;
					case "ACT":
						strManufacturer = "Advanced Control Technology";
						break;
					case "AGI":
						strManufacturer = "Airguide Instrument Company";
						break;
					case "AHA":
						strManufacturer = "Autohelm of America";
						break;
					case "AIP":
						strManufacturer = "Aiphone Corporation";
						break;
					case "ALD":
						strManufacturer = "Alden Electronics, Inc.";
						break;
					case "AMR":
						strManufacturer = "AMR Systems";
						break;
					case "AMT":
						strManufacturer = "Airmar Technology";
						break;
					case "ANS":
						strManufacturer = "Antenna Specialists";
						break;
					case "ANX":
						strManufacturer = "Analytyx Electronic Systems";
						break;
					case "ANZ":
						strManufacturer = "Anschutz of America";
						break;
					case "APC":
						strManufacturer = "Apelco";
						break;
					case "APN":
						strManufacturer = "American Pioneer, Inc.";
						break;
					case "APX":
						strManufacturer = "Amperex, Inc.";
						break;
					case "AQC":
						strManufacturer = "Aqua-Chem, Inc.";
						break;
					case "AQD":
						strManufacturer = "Aquadynamics, Inc.";
						break;
					case "AQM":
						strManufacturer = "Aqua Meter Instrument Company";
						break;
					case "ASP":
						strManufacturer = "American Solar Power";
						break;
					case "ATE":
						strManufacturer = "Aetna Engineering";
						break;
					case "ATM":
						strManufacturer = "Atlantic Marketing Company, Inc.";
						break;
					case "ATR":
						strManufacturer = "Airtron";
						break;
					case "ATV":
						strManufacturer = "Activation, Inc.";
						break;
					case "AVN":
						strManufacturer = "Advanced Navigation, Inc.";
						break;
					case "AWA":
						strManufacturer = "Awa New Zealand, Limited";
						break;
					case "BBL":
						strManufacturer = "BBL Industries, Inc.";
						break;
					case "BBR":
						strManufacturer = "BBR And Associates";
						break;
					case "BDV":
						strManufacturer = "Brisson Development, Inc.";
						break;
					case "BEC":
						strManufacturer = "Boat Electric Company";
						break;
					case "BGS":
						strManufacturer = "Barringer Geoservice";
						break;
					case "BGT":
						strManufacturer = "Brookes and Gatehouse, Inc.";
						break;
					case "BHE":
						strManufacturer = "BH Electronics";
						break;
					case "BHR":
						strManufacturer = "Bahr Technologies, Inc.";
						break;
					case "BLB":
						strManufacturer = "Bay Laboratories";
						break;
					case "BMC":
						strManufacturer = "BMC";
						break;
					case "BME":
						strManufacturer = "Bartel Marine Electronics";
						break;
					case "BNI":
						strManufacturer = "Neil Brown Instrument Systems";
						break;
					case "BNS":
						strManufacturer = "Bowditch Navigation Systems";
						break;
					case "BRM":
						strManufacturer = "Mel Barr Company";
						break;
					case "BRY":
						strManufacturer = "Byrd Industries";
						break;
					case "BTH":
						strManufacturer = "Benthos, Inc.";
						break;
					case "BTK":
						strManufacturer = "Baltek Corporation";
						break;
					case "BTS":
						strManufacturer = "Boat Sentry, Inc.";
						break;
					case "BXA":
						strManufacturer = "Bendix-Avalex, Inc.";
						break;
					case "CAT":
						strManufacturer = "Catel";
						break;
					case "CBN":
						strManufacturer = "Cybernet Marine Products";
						break;
					case "CCA":
						strManufacturer = "Copal Corporation of America";
						break;
					case "CCC":
						strManufacturer = "Coastal Communications Company";
						break;
					case "CCL":
						strManufacturer = "Coastal Climate Company";
						break;
					case "CCM":
						strManufacturer = "Coastal Communications";
						break;
					case "CDC":
						strManufacturer = "Cordic Company";
						break;
					case "CEC":
						strManufacturer = "Ceco Communications, Inc.";
						break;
					case "CHI":
						strManufacturer = "Charles Industries, Limited";
						break;
					case "CKM":
						strManufacturer = "Cinkel Marine Electronics Industries";
						break;
					case "CMA":
						strManufacturer = "Societe Nouvelle D//Equiment du Calvados";
						break;
					case "CMC":
						strManufacturer = "Coe Manufacturing Company";
						break;
					case "CME":
						strManufacturer = "Cushman Electronics, Inc.";
						break;
					case "CMP":
						strManufacturer = "C-Map, s.r.l.";
						break;
					case "CMS":
						strManufacturer = "Coastal Marine Sales Company";
						break;
					case "CMV":
						strManufacturer = "CourseMaster USA, Inc.";
						break;
					case "CNV":
						strManufacturer = "Coastal Navigator";
						break;
					case "CNX":
						strManufacturer = "Cynex Manufactoring Company";
						break;
					case "CPL":
						strManufacturer = "Computrol, Inc.";
						break;
					case "CPN":
						strManufacturer = "Compunav";
						break;
					case "CPS":
						strManufacturer = "Columbus Positioning, Inc.";
						break;
					case "CPT":
						strManufacturer = "CPT, Inc.";
						break;
					case "CRE":
						strManufacturer = "Crystal Electronics, Limited";
						break;
					case "CRO":
						strManufacturer = "The Caro Group";
						break;
					case "CRY":
						strManufacturer = "Crystek Crystals Corporation";
						break;
					case "CSM":
						strManufacturer = "Comsat Maritime Services";
						break;
					case "CST":
						strManufacturer = "Cast, Inc.";
						break;
					case "CSV":
						strManufacturer = "Combined Services";
						break;
					case "CTA":
						strManufacturer = "Current Alternatives";
						break;
					case "CTB":
						strManufacturer = "Cetec Benmar";
						break;
					case "CTC":
						strManufacturer = "Cell-tech Communications";
						break;
					case "CTE":
						strManufacturer = "Castle Electronics";
						break;
					case "CTL":
						strManufacturer = "C - Tech, Limited";
						break;
					case "CNI":
						strManufacturer = "Continental Instruments"; // mybe wrong !?
						break;
					case "CWD":
						strManufacturer = "Cubic Western Data";
						break;
					case "CWV":
						strManufacturer = "Celwave R.F., Inc.";
						break;
					case "CYZ":
						strManufacturer = "cYz, Incorporated";
						break;
					case "DCC":
						strManufacturer = "Dolphin Components Corporation";
						break;
					case "DEB":
						strManufacturer = "Debeg Gmbh";
						break;
					case "DFI":
						strManufacturer = "Defender Industries, Inc.";
						break;
					case "DGC":
						strManufacturer = "Digicourse, Inc.";
						break;
					case "DME":
						strManufacturer = "Digital Marine Electronics Corporation";
						break;
					case "DMI":
						strManufacturer = "Datamarine International, Inc.";
						break;
					case "DNS":
						strManufacturer = "Dornier System Gmbh";
						break;
					case "DNT":
						strManufacturer = "Del Norte Technology, Inc.";
						break;
					case "DPS":
						strManufacturer = "Danaplus, Inc.";
						break;
					case "DRL":
						strManufacturer = "R.L. Drake Company";
						break;
					case "DSC":
						strManufacturer = "Dynascan Corporation";
						break;
					case "DYN":
						strManufacturer = "Dynamote Corporation";
						break;
					case "DYT":
						strManufacturer = "Dytek Laboratories, Inc.";
						break;
					case "EBC":
						strManufacturer = "Emergency Beacon, Corporation";
						break;
					case "ECT":
						strManufacturer = "Echotec, Inc.";
						break;
					case "EEV":
						strManufacturer = "EEV, Inc.";
						break;
					case "EFC":
						strManufacturer = "Efcom Communication Systems";
						break;
					case "ELD":
						strManufacturer = "Electronic Devices, Inc.";
						break;
					case "EMC":
						strManufacturer = "Electric Motion Company";
						break;
					case "EMS":
						strManufacturer = "Electro Marine Systems, Inc.";
						break;
					case "ENA":
						strManufacturer = "Energy Analysts, Inc.";
						break;
					case "ENC":
						strManufacturer = "Encron, Inc.";
						break;
					case "EPM":
						strManufacturer = "Epsco Marine";
						break;
					case "EPT":
						strManufacturer = "Eastprint, Inc.";
						break;
					case "ERC":
						strManufacturer = "The Ericsson Corporation";
						break;
					case "ESA":
						strManufacturer = "European Space Agency";
						break;
					case "FDN":
						strManufacturer = "Fluiddyne";
						break;
					case "FHE":
						strManufacturer = "Fish Hawk Electronics";
						break;
					case "FJN":
						strManufacturer = "Jon Fluke Company";
						break;
					case "FMM":
						strManufacturer = "First Mate Marine Autopilots";
						break;
					case "FNT":
						strManufacturer = "Franklin Net and Twine, Limited";
						break;
					case "FRC":
						strManufacturer = "The Fredericks Company";
						break;
					case "FTG":
						strManufacturer = "T.G. Faria Corporation";
						break;
					case "FUJ":
						strManufacturer = "Fujitsu Ten Corporation of America";
						break;
					case "FEC":
						strManufacturer = "Furuno Electric Company"; // maybe wrong !?
						break;
					case "FUR":
						strManufacturer = "Furuno, USA Inc.";
						break;
					case "GAM":
						strManufacturer = "GRE America, Inc.";
						break;
					case "GCA":
						strManufacturer = "Gulf Cellular Associates";
						break;
					case "GES":
						strManufacturer = "Geostar Corporation";
						break;
					case "GFC":
						strManufacturer = "Graphic Controls, Corporation";
						break;
					case "GIS":
						strManufacturer = "Galax Integrated Systems";
						break;
					case "GPI":
						strManufacturer = "Global Positioning Instrument Corporation";
						break;
					case "GRM":
						strManufacturer = "Garmin Corporation";
						break;
					case "GSC":
						strManufacturer = "Gold Star Company, Limited";
						break;
					case "GTO":
						strManufacturer = "Gro Electronics";
						break;
					case "GVE":
						strManufacturer = "Guest Corporation";
						break;
					case "GVT":
						strManufacturer = "Great Valley Technology";
						break;
					case "HAL":
						strManufacturer = "HAL Communications Corporation";
						break;
					case "HAR":
						strManufacturer = "Harris Corporation";
						break;
					case "HIG":
						strManufacturer = "Hy - Gain";
						break;
					case "HIT":
						strManufacturer = "Hi - Tec";
						break;
					case "HPK":
						strManufacturer = "Hewlett - Packard";
						break;
					case "HRC":
						strManufacturer = "Harco Manufacturing Company";
						break;
					case "HRT":
						strManufacturer = "Hart Systems, Inc.";
						break;
					case "HTI":
						strManufacturer = "Heart Interface, Inc.";
						break;
					case "HUL":
						strManufacturer = "Hull Electronics Company";
						break;
					case "HWM":
						strManufacturer = "Honeywell Marine Systems";
						break;
					case "ICO":
						strManufacturer = "Icom of America, Inc.";
						break;
					case "IFD":
						strManufacturer = "International Fishing Devices";
						break;
					case "IFI":
						strManufacturer = "Instruments for Industry";
						break;
					case "IME":
						strManufacturer = "Imperial Marine Equipment";
						break;
					case "IMI":
						strManufacturer = "I.M.I.";
						break;
					case "IMM":
						strManufacturer = "ITT MacKay Marine";
						break;
					case "IMP":
						strManufacturer = "Impulse Manufacturing, Inc.";
						break;
					case "IMT":
						strManufacturer = "International Marketing and Trading, Inc.";
						break;
					case "INM":
						strManufacturer = "Inmar Electronic and Sales, Inc.";
						break;
					case "INT":
						strManufacturer = "Intech, Inc.";
						break;
					case "IRT":
						strManufacturer = "Intera Technologies, Limited";
						break;
					case "IST":
						strManufacturer = "Innerspace Technology, Inc.";
						break;
					case "ITM":
						strManufacturer = "Intermarine Electronics, Inc.";
						break;
					case "ITR":
						strManufacturer = "Itera, Limited";
						break;
					case "JAN":
						strManufacturer = "Jan Crystals";
						break;
					case "JFR":
						strManufacturer = "Ray Jefferson";
						break;
					case "JMT":
						strManufacturer = "Japan Marine Telecommunications";
						break;
					case "JRC":
						strManufacturer = "Japan Radio Company, Inc.";
						break;
					case "JRI":
						strManufacturer = "J-R Industries, Inc.";
						break;
					case "JTC":
						strManufacturer = "J-Tech Associates, Inc.";
						break;
					case "JTR":
						strManufacturer = "Jotron Radiosearch, Limited";
						break;
					case "KBE":
						strManufacturer = "KB Electronics, Limited";
						break;
					case "KBM":
						strManufacturer = "Kennebec Marine Company";
						break;
					case "KLA":
						strManufacturer = "Klein Associates, Inc.";
						break;
					case "KMR":
						strManufacturer = "King Marine Radio Corporation";
						break;
					case "KNG":
						strManufacturer = "King Radio Corporation";
						break;
					case "KOD":
						strManufacturer = "Koden Electronics Company, Limited";
						break;
					case "KRP":
						strManufacturer = "Krupp International, Inc.";
						break;
					case "KVH":
						strManufacturer = "KVH Company";
						break;
					case "KYI":
						strManufacturer = "Kyocera International, Inc.";
						break;
					case "LAT":
						strManufacturer = "Latitude Corporation";
						break;
					case "LEC":
						strManufacturer = "Lorain Electronics Corporation";
						break;
					case "LMM":
						strManufacturer = "Lamarche Manufacturing Company";
						break;
					case "LRD":
						strManufacturer = "Lorad";
						break;
					case "LSE":
						strManufacturer = "Littlemore Scientific Engineering";
						break;
					case "LSP":
						strManufacturer = "Laser Plot, Inc.";
						break;
					case "LTF":
						strManufacturer = "Littlefuse, Inc.";
						break;
					case "LWR":
						strManufacturer = "Lowrance Electronics Corportation";
						break;
					case "MCL":
						strManufacturer = "Micrologic, Inc.";
						break;
					case "MDL":
						strManufacturer = "Medallion Instruments, Inc.";
						break;
					case "MEC":
						strManufacturer = "Marine Engine Center, Inc.";
						break;
					case "MEG":
						strManufacturer = "Maritec Engineering GmbH";
						break;
					case "MFR":
						strManufacturer = "Modern Products, Limited";
						break;
					case "MFW":
						strManufacturer = "Frank W. Murphy Manufacturing";
						break;
					case "MGN":
						strManufacturer = "Magellan";
						break;
					case "MGS":
						strManufacturer = "MG Electronic Sales Corporation";
						break;
					case "MIE":
						strManufacturer = "Mieco, Inc.";
						break;
					case "MIM":
						strManufacturer = "Marconi International Marine Company";
						break;
					case "MLE":
						strManufacturer = "Martha Lake Electronics";
						break;
					case "MLN":
						strManufacturer = "Matlin Company";
						break;
					case "MLP":
						strManufacturer = "Marlin Products";
						break;
					case "MLT":
						strManufacturer = "Miller Technologies";
						break;
					case "MMB":
						strManufacturer = "Marsh-McBirney, Inc.";
						break;
					case "MME":
						strManufacturer = "Marks Marine Engineering";
						break;
					case "MMP":
						strManufacturer = "Metal Marine Pilot, Inc.";
						break;
					case "MMS":
						strManufacturer = "Mars Marine Systems";
						break;
					case "MNI":
						strManufacturer = "Micro-Now Instrument Company";
						break;
					case "MNT":
						strManufacturer = "Marine Technology";
						break;
					case "MNX":
						strManufacturer = "Marinex";
						break;
					case "MOT":
						strManufacturer = "Motorola Communications and Electronics";
						break;
					case "MPN":
						strManufacturer = "Memphis Net and Twine Company, Inc.";
						break;
					case "MQS":
						strManufacturer = "Marquis Industries, Inc.";
						break;
					case "MRC":
						strManufacturer = "Marinecomp, Inc.";
						break;
					case "MRE":
						strManufacturer = "Morad Electronics Corporation";
						break;
					case "MRP":
						strManufacturer = "Mooring Products of New England";
						break;
					case "MRR":
						strManufacturer = "II Morrow, Inc.";
						break;
					case "MRS":
						strManufacturer = "Marine Radio Service";
						break;
					case "MSB":
						strManufacturer = "Mitsubishi Electric Company, Limited";
						break;
					case "MSE":
						strManufacturer = "Master Electronics";
						break;
					case "MSM":
						strManufacturer = "Master Mariner, Inc.";
						break;
					case "MST":
						strManufacturer = "Mesotech Systems, Limited";
						break;
					case "MTA":
						strManufacturer = "Marine Technical Associates";
						break;
					case "MTG":
						strManufacturer = "Narine Technical Assistance Group";
						break;
					case "MTK":
						strManufacturer = "Martech, Inc.";
						break;
					case "MTR":
						strManufacturer = "Mitre Corporation, Inc.";
						break;
					case "MTS":
						strManufacturer = "Mets, Inc.";
						break;
					case "MUR":
						strManufacturer = "Murata Erie North America";
						break;
					case "MVX":
						strManufacturer = "Magnavox Advanced Products and Systems Company";
						break;
					case "MXX":
						strManufacturer = "Maxxima Marine";
						break;
					case "MES":
						strManufacturer = "Marine Electronics Service, Inc.";
						break;
					case "NAT":
						strManufacturer = "Nautech, Limited";
						break;
					case "NEF":
						strManufacturer = "New England Fishing Gear, Inc.";
						break;
					case "NMR":
						strManufacturer = "Newmar";
						break;
					case "NGS":
						strManufacturer = "Navigation Sciences, Inc.";
						break;
					case "NOM":
						strManufacturer = "Nav-Com, Inc.";
						break;
					case "NOV":
						strManufacturer = "NovAtel Communications, Limited";
						break;
					case "NSM":
						strManufacturer = "Northstar Marine";
						break;
					case "NTK":
						strManufacturer = "Novatech Designs, Limited";
						break;
					case "NVC":
						strManufacturer = "Navico";
						break;
					case "NVS":
						strManufacturer = "Navstar";
						break;
					case "NVO":
						strManufacturer = "Navionics, s.p.a.";
						break;
					case "OAR":
						strManufacturer = "O.A.R.Corporation";
						break;
					case "ODE":
						strManufacturer = "Ocean Data Equipment Corporation";
						break;
					case "ODN":
						strManufacturer = "Odin Electronics, Inc.";
						break;
					case "OIN":
						strManufacturer = "Ocean instruments, Inc.";
						break;
					case "OKI":
						strManufacturer = "Oki Electronic Industry Company";
						break;
					case "OLY":
						strManufacturer = "Navstar Limited (Polytechnic Electronics)";
						break;
					case "OMN":
						strManufacturer = "Omnetics";
						break;
					case "ORE":
						strManufacturer = "Ocean Research";
						break;
					case "OTK":
						strManufacturer = "Ocean Technology";
						break;
					case "PCE":
						strManufacturer = "Pace";
						break;
					case "PDM":
						strManufacturer = "Prodelco Marine Systems";
						break;
					case "PLA":
						strManufacturer = "Plath, C. Division of Litton";
						break;
					case "PLI":
						strManufacturer = "Pilot Instruments";
						break;
					case "PMI":
						strManufacturer = "Pernicka Marine Products";
						break;
					case "PMP":
						strManufacturer = "Pacific Marine Products";
						break;
					case "PRK":
						strManufacturer = "Perko, Inc.";
						break;
					case "PSM":
						strManufacturer = "Pearce - Simpson";
						break;
					case "PTC":
						strManufacturer = "Petro - Com";
						break;
					case "PTG":
						strManufacturer = "P.T.I./Guest";
						break;
					case "PTH":
						strManufacturer = "Pathcom, Inc.";
						break;
					case "RAC":
						strManufacturer = "Racal Marine, Inc.";
						break;
					case "RAE":
						strManufacturer = "RCA Astro-Electronics";
						break;
					case "RAY":
						strManufacturer = "Raytheon Marine Company";
						break;
					case "RCA":
						strManufacturer = "RCA Service Company";
						break;
					case "RCH":
						strManufacturer = "Roach Engineering";
						break;
					case "RCI":
						strManufacturer = "Rochester Instruments, Inc.";
						break;
					case "RDI":
						strManufacturer = "Radar Devices";
						break;
					case "RDM":
						strManufacturer = "Ray-Dar Manufacturing Company";
						break;
					case "REC":
						strManufacturer = "Ross Engineering Company";
						break;
					case "RFP":
						strManufacturer = "Rolfite Products, Inc.";
						break;
					case "RGC":
						strManufacturer = "RCS Global Communications, Inc.";
						break;
					case "RGY":
						strManufacturer = "Regency Electronics, Inc.";
						break;
					case "RMR":
						strManufacturer = "RCA Missile and Surface Radar";
						break;
					case "RSL":
						strManufacturer = "Ross Laboratories, Inc.";
						break;
					case "RSM":
						strManufacturer = "Robertson - Shipmate, USA";
						break;
					case "RWI":
						strManufacturer = "Rockwell International";
						break;
					case "RME":
						strManufacturer = "Racal Marine Electronics";
						break;
					case "RTN":
						strManufacturer = "Robertson Tritech Nyaskaien A/S";
						break;
					case "SAI":
						strManufacturer = "SAIT, Inc.";
						break;
					case "SBR":
						strManufacturer = "Sea-Bird electronics, Inc.";
						break;
					case "SCR":
						strManufacturer = "Signalcrafters, Inc.";
						break;
					case "SEA":
						strManufacturer = "Sea";
						break;
					case "SEC":
						strManufacturer = "Sercel Electronics of Canada";
						break;
					case "SEP":
						strManufacturer = "Steel and Engine Products, Inc.";
						break;
					case "SFN":
						strManufacturer = "Seafarer Navigation International, Limited";
						break;
					case "SGC":
						strManufacturer = "SGC, Inc.";
						break;
					case "SIG":
						strManufacturer = "Signet, Inc.";
						break;
					case "SIM":
						strManufacturer = "Simrad,Inc.";
						break;
					case "SKA":
						strManufacturer = "Skantek Corporation";
						break;
					case "SKP":
						strManufacturer = "Skipper Electronics A/S";
						break;
					case "SME":
						strManufacturer = "Shakespeare Marine Electronics";
						break;
					case "SMF":
						strManufacturer = "Seattle Marine and Fishing Supply Company";
						break;
					case "SML":
						strManufacturer = "Simerl Instruments";
						break;
					case "SMI":
						strManufacturer = "Sperry Marine, Inc.";
						break;
					case "SNV":
						strManufacturer = "Starnav Corporation";
						break;
					case "SOM":
						strManufacturer = "Sound Marine Electronics, Inc.";
						break;
					case "SOV":
						strManufacturer = "Sell Overseas America";
						break;
					case "SPL":
						strManufacturer = "Spelmar";
						break;
					case "SPT":
						strManufacturer = "Sound Powered Telephone";
						break;
					case "SRD":
						strManufacturer = "SRD Labs";
						break;
					case "SRS":
						strManufacturer = "Scientific Radio Systems, Inc.";
						break;
					case "SRT":
						strManufacturer = "Standard Radio and Telefon AB";
						break;
					case "SSI":
						strManufacturer = "Sea Scout Industries";
						break;
					case "STC":
						strManufacturer = "Standard Communications";
						break;
					case "STI":
						strManufacturer = "Sea-Temp Instrument Corporation";
						break;
					case "STM":
						strManufacturer = "Si-Tex Marine Electronics";
						break;
					case "SVY":
						strManufacturer = "Savoy Electronics";
						break;
					case "SWI":
						strManufacturer = "Swoffer Marine Instruments, Inc.";
						break;
						//				case "SRS":
						//					strManufacturer = "Shipmate, Rauff & Sorensen, A / S";
						//					break;
					case "TBB":
						strManufacturer = "Thompson Brothers Boat Manufacturing Company";
						break;
					case "TCN":
						strManufacturer = "Trade Commission of Norway (THE)";
						break;
					case "TDL":
						strManufacturer = "Tideland Signal";
						break;
					case "THR":
						strManufacturer = "Thrane and Thrane A/A";
						break;
					case "TLS":
						strManufacturer = "Telesystems";
						break;
					case "TMT":
						strManufacturer = "Tamtech, Limited";
						break;
					case "TNL":
						strManufacturer = "Trimble Navigation";
						break;
					case "TRC":
						strManufacturer = "Tracor, Inc.";
						break;
					case "TSI":
						strManufacturer = "Techsonic Industries, Inc.";
						break;
					case "TTK":
						strManufacturer = "Talon Technology Corporation";
						break;
					case "TTS":
						strManufacturer = "Transtector Systems";
						break;
					case "TWC":
						strManufacturer = "Transworld Communications, Inc.";
						break;
					case "TXI":
						strManufacturer = "Texas Instruments, Inc.";
						break;
					case "UME":
						strManufacturer = "Umec";
						break;
					case "UNI":
						strManufacturer = "Uniden Corporation of America";
						break;
					case "UNP":
						strManufacturer = "Unipas, Inc.";
						break;
					case "UNF":
						strManufacturer = "Uniforce Electronics Company";
						break;
					case "VAN":
						strManufacturer = "Vanner, Inc.";
						break;
					case "VAR":
						strManufacturer = "Varian Eimac Associates";
						break;
					case "VCM":
						strManufacturer = "Videocom";
						break;
					case "VEX":
						strManufacturer = "Vexillar";
						break;
					case "VIS":
						strManufacturer = "Vessel Information Systems, Inc.";
						break;
					case "VMR":
						strManufacturer = "Vast Marketing Corporation";
						break;
					case "WAL":
						strManufacturer = "Walport USA";
						break;
					case "WBG":
						strManufacturer = "Westberg Manufacturing, Inc.";
						break;
					case "WEC":
						strManufacturer = "Westinghouse electric Corporation";
						break;
					case "WHA":
						strManufacturer = "W-H Autopilots";
						break;
					case "WMM":
						strManufacturer = "Wait Manufacturing and Marine Sales Company";
						break;
					case "WMR":
						strManufacturer = "Wesmar Electronics";
						break;
					case "WNG":
						strManufacturer = "Winegard Company";
						break;
					case "WSE":
						strManufacturer = "Wilson Electronics Corporation";
						break;
					case "WTC":
						strManufacturer = "Watercom";
						break;
					case "WST":
						strManufacturer = "West Electronics Limited";
						break;
					case "YAS":
						strManufacturer = "Yaesu Electronics";
						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return strManufacturer;
		}

		private string GetTalkerDescription(string p_strTalkerID)
		{

			string strGetTalkerDescription = "";

			try
			{
				switch(p_strTalkerID.ToUpper(CultureInfo.InvariantCulture))
				{
					case "AG":
						strGetTalkerDescription = "Autopilot - General";
						break;
					case "AP":
						strGetTalkerDescription = "Autopilot - Magnetic";
						break;
					case "CC":
						strGetTalkerDescription = "Commputer - Programmed Calculator (outdated)";
						break;
					case "CD":
						strGetTalkerDescription = "Communications - Digital Selective Calling (DSC)";
						break;
					case "CM":
						strGetTalkerDescription = "Computer - Memory Data (outdated)";
						break;
					case "CS":
						strGetTalkerDescription = "Communications - Satellite";
						break;
					case "CT":
						strGetTalkerDescription = "Communications - Radio - Telephone(MF / HF)";
						break;
					case "CV":
						strGetTalkerDescription = "Communications - Radio - Telephone(VHF)";
						break;
					case "CX":
						strGetTalkerDescription = "Communications - Scanning Receiver";
						break;
					case "DE":
						strGetTalkerDescription = "DECCA Navigation";
						break;
					case "DF":
						strGetTalkerDescription = "Direction Finder";
						break;
					case "EC":
						strGetTalkerDescription = "Electronic Chart Display & Information System (ECDIS)";
						break;
					case "EP":
						strGetTalkerDescription = "Emergency Position Indicating Beacon (EPIRB)";
						break;
					case "ER":
						strGetTalkerDescription = "Engine Room Monitoring Systems";
						break;
					case "GP":
						strGetTalkerDescription = "Global Positioning System (GPS)";
						break;
					case "HC":
						strGetTalkerDescription = "Heading - Magnetic Compass";
						break;
					case "HE":
						strGetTalkerDescription = "Heading - North Seeking Gyro";
						break;
					case "HN":
						strGetTalkerDescription = "Heading - Non North Seeking Gyro";
						break;
					case "II":
						strGetTalkerDescription = "Integrated Instrumentation";
						break;
					case "IN":
						strGetTalkerDescription = "Integrated Navigation";
						break;
					case "LA":
						strGetTalkerDescription = "Lorean A";
						break;
					case "LC":
						strGetTalkerDescription = "Lorean C";
						break;
					case "MP":
						strGetTalkerDescription = "Microwave Positioning System (outdated)";
						break;
					case "OM":
						strGetTalkerDescription = "OMEGA Navigation System";
						break;
					case "OS":
						strGetTalkerDescription = "Distress Alarm System (outdated)";
						break;
					case "RA":
						strGetTalkerDescription = "RADAR and/or ARPA";
						break;
					case "SD":
						strGetTalkerDescription = "Sounder, Depth";
						break;
					case "SN":
						strGetTalkerDescription = "Electronic Positioning System, other/general";
						break;
					case "SS":
						strGetTalkerDescription = "Sounder, Scanning";
						break;
					case "TI":
						strGetTalkerDescription = "Turn Rate Indicator";
						break;
					case "TR":
						strGetTalkerDescription = "TRANSIT Navigation System";
						break;
					case "VD":
						strGetTalkerDescription = "Velocity Sensor, Doppler, other/general";
						break;
					case "DM":
						strGetTalkerDescription = "Velocity Sensor, Speed Log, Water, Magnetic";
						break;
					case "VW":
						strGetTalkerDescription = "Velocity Sensor, Speed Log, Water, Mechanical";
						break;
					case "WI":
						strGetTalkerDescription = "Weather Instruments";
						break;
					case "YC":
						strGetTalkerDescription = "Transducer - Temperature(outdated)";
						break;
					case "YD":
						strGetTalkerDescription = "Transducer - Displacement, Angular Or Linear(outdated)";
						break;
					case "YF":
						strGetTalkerDescription = "Transducer - Frequency(outdated)";
						break;
					case "YL":
						strGetTalkerDescription = "Transducer - Level(outdated)";
						break;
					case "YP":
						strGetTalkerDescription = "Transducer - Pressure(outdated)";
						break;
					case "YR":
						strGetTalkerDescription = "Transducer - Flow Rate (outdated)";
						break;
					case "YT":
						strGetTalkerDescription = "Transducer - Tachometer(outdated)";
						break;
					case "YV":
						strGetTalkerDescription = "Transducer - Volume(outdated)";
						break;
					case "YX":
						strGetTalkerDescription = "Transducer";
						break;
					case "ZA":
						strGetTalkerDescription = "Timekeeper - Atomic Clock";
						break;
					case "ZC":
						strGetTalkerDescription = "Timekeeper - Chronometer";
						break;
					case "ZQ":
						strGetTalkerDescription = "Timekeeper - Quartz";
						break;
					case "ZV":
						strGetTalkerDescription = "Timekeeper - Radio Update, WWV or WWVH";
						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return strGetTalkerDescription;
		}	
		
		private string GetTypeDescription(string p_strTypeId)
		{
			string strGetTypeDescription = "";

			try
			{
				switch(p_strTypeId.ToUpper(CultureInfo.InvariantCulture))
				{
					case "AAM":
						strGetTypeDescription = "Waypoint Arrival Alarm";
						break;
					case "ALM":
						strGetTypeDescription = "GPS Almanac Data";
						break;
					case "APA":
						strGetTypeDescription = "Autopilot Sentence \"A\"";
						break;
					case "APB":
						strGetTypeDescription = "Autopilot Sentence \"B\"";
						break;
					case "ASD":
						strGetTypeDescription = "Autopilot System Data";
						break;
					case "BOD":
						strGetTypeDescription = "Bearing - Waypoint To Waypoint";
						break;
					case "BWC":
						strGetTypeDescription = "Bearing And Distance To Waypoint";
						break;
					case "BWR":
						strGetTypeDescription = "Bearing and Distance to Waypoint - Rhumb Line";
						break;
					case "BWW":
						strGetTypeDescription = "Bearing - Waypoint To Waypoint";
						break;
					case "DBK":
						strGetTypeDescription = "Depth Below Keel";
						break;
					case "DBS":
						strGetTypeDescription = "Depth Below Surface";
						break;
					case "DBT":
						strGetTypeDescription = "Depth below transducer";
						break;
					case "DCN":
						strGetTypeDescription = "Decca Position";
						break;
					case "DPT":
						strGetTypeDescription = "Heading - Deviation & Variation";
						break;
					case "FSI":
						strGetTypeDescription = "Frequency Set Information";
						break;
					case "GGA":
						strGetTypeDescription = "Global Positioning System Fix Data";
						break;
					case "GLC":
						strGetTypeDescription = "Geographic Position, Loran-C";
						break;
					case "GLL":
						strGetTypeDescription = "Geographic Position - Latitude/Longitude";
						break;
					case "GSA":
						strGetTypeDescription = "GPS DOP and active satellites";
						break;
					case "GSV":
						strGetTypeDescription = "Satellites in view";
						break;
					case "GTD":
						strGetTypeDescription = "Geographic Location in Time Differences";
						break;
					case "GXA":
						strGetTypeDescription = "TRANSIT Position - Latitude/Longitude";
						break;
					case "HDG":
						strGetTypeDescription = "Heading - Deviation & Variation";
						break;
					case "HDM":
						strGetTypeDescription = "Heading - Magnetic";
						break;
					case "HDT":
						strGetTypeDescription = "Heading - True";
						break;
					case "HSC":
						strGetTypeDescription = "Heading Steering Command";
						break;
					case "LCD":
						strGetTypeDescription = "Loran-C Signal Data";
						break;
					case "MTW":
						strGetTypeDescription = "Water Temperature";
						break;
					case "MWV":
						strGetTypeDescription = "Wind Speed and Angle";
						break;
					case "OLN":
						strGetTypeDescription = "Omega Lane Numbers";
						break;
					case "OSD":
						strGetTypeDescription = "Own Ship Data";
						break;
					case "R00":
						strGetTypeDescription = "Waypoints in active route";
						break;
					case "RMA":
						strGetTypeDescription = "Recommended Minimum Navigation Information";
						break;
					case "RMB":
						strGetTypeDescription = "Recommended Minimum Navigation Information";
						break;
					case "RMC":
						strGetTypeDescription = "Recommended Minimum Navigation Information";
						break;
					case "ROT":
						strGetTypeDescription = "Rate Of Turn";
						break;
					case "RPM":
						strGetTypeDescription = "Revolutions";
						break;
					case "RSA":
						strGetTypeDescription = "Rudder Sensor Angle";
						break;
					case "RSD":
						strGetTypeDescription = "RADAR System Data";
						break;
					case "RTE":
						strGetTypeDescription = "Routes";
						break;
					case "SFI":
						strGetTypeDescription = "Scanning Frequency Information";
						break;
					case "STN":
						strGetTypeDescription = "Multiple Data ID";
						break;
					case "TRF":
						strGetTypeDescription = "TRANSIT Fix Data";
						break;
					case "TTM":
						strGetTypeDescription = "Tracked Target Message";
						break;
					case "VBW":
						strGetTypeDescription = "Dual Ground/Water Speed";
						break;
					case "VDR":
						strGetTypeDescription = "Set and Drift";
						break;
					case "VHW":
						strGetTypeDescription = "Water speed and heading";
						break;
					case "VLW":
						strGetTypeDescription = "Distance Traveled through Water";
						break;
					case "VPW":
						strGetTypeDescription = "Speed - Measured Parallel to Wind";
						break;
					case "VTG":
						strGetTypeDescription = "Track made good and Ground speed";
						break;
					case "VWR":
						strGetTypeDescription = "Relative Wind Speed and Angle";
						break;
					case "WCV":
						strGetTypeDescription = "Waypoint Closure Velocity";
						break;
					case "WNC":
						strGetTypeDescription = "Distance - Waypoint To Waypoint";
						break;
					case "WPL":
						strGetTypeDescription = "Waypoint Location";
						break;
					case "XDR":
						strGetTypeDescription = "Cross Track Error - Dead Reckoning";
						break;
					case "XTE":
						strGetTypeDescription = "Cross-Track Error, Measured";
						break;
					case "XTR":
						strGetTypeDescription = "Cross Track Error - Dead Reckoning";
						break;
					case "ZDA":
						strGetTypeDescription = "Time & Date";
						break;
					case "ZFO":
						strGetTypeDescription = "UTC & Time from origin Waypoint";
						break;
					case "ZTG":
						strGetTypeDescription = "UTC & Time to Destination Waypoint";
						break;
					    
						//the following types are known to exist but no data details are known
					case "DSC":
						strGetTypeDescription = "Digital Selective Calling Information";
						break;
					case "DSE":
						strGetTypeDescription = "Extended DSC";
						break;
					case "DSI":
						strGetTypeDescription = "DSC Transponder Initiate";
						break;
					case "DSR":
						strGetTypeDescription = "DSC Transponder Response";
						break;
					case "DTM":
						strGetTypeDescription = "Datum Reference";
						break;
					case "GBS":
						strGetTypeDescription = "GPS Satellite Fault Detection";
						break;
					case "GRS":
						strGetTypeDescription = "GPS Range Residuals";
						break;
					case "GST":
						strGetTypeDescription = "GPS Pseudorange Noise Statistics";
						break;
					case "MSK":
						strGetTypeDescription = "MSK Receiver Interface";
						break;
					case "MSS":
						strGetTypeDescription = "MSK Receiver Signal Status";
						break;
					case "MWD":
						strGetTypeDescription = "Wind Direction & Speed";
						break;
					case "TLL":
						strGetTypeDescription = "Target Latitude and Longitude";
						break;
					case "WDC":
						strGetTypeDescription = "Distance to Waypoint - Great Circle";
						break;
					case "WDR":
						strGetTypeDescription = "Distance to Waypoint - Rhumb Line";
						break;
					case "ZDL":
						strGetTypeDescription = "Time and Distance to Variable Point";
						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return strGetTypeDescription;
		}
		/// <summary>
		/// Returns the Datum as a string based on the passed Datum ID.
		/// </summary>
		internal string GetDatum(int DatumID)
		{
			string strDatum = "";

			try
			{
				switch(DatumID)
				{
					case 0:
						strDatum = "ADINDAN"; // Ethiopia, Mali, Senegal, Sudan
						break;
					case 1:
						strDatum = "AFGOOYE"; // Somalia
						break;
					case 2:
						strDatum = "AIN EL ABD 1970"; // Bahrain Island, Saudi Arabia
						break;
					case 3:
						strDatum = "ANNA 1 ASTRO 1965"; // Cocos Island
						break;
					case 4:
						strDatum = "ARC 1950"; // Botswana, Lesotho, Malawi, Swaziland, Zaire, Zambia, Zimbabwe
						break;
					case 5:
						strDatum = "ARC 1960"; // Kenya, Tanzania
						break;
					case 6:
						strDatum = "ASCENSION ISLAND 1958"; // Ascension Island
						break;
					case 7:
						strDatum = "ASTRO BEACON \"E\""; // Iwo Jima Island
						break;
					case 8:
						strDatum = "AUSTRALIAN GEODETIC 1966"; // Australia, Tasmania Island
						break;
					case 9:
						strDatum = "AUSTRALIAN GEODETIC 1984"; // Australia, Tasmania Island
						break;
					case 10:
						strDatum = "ASTRO DOS 71/4"; // St. Helena Island
						break;
					case 11:
						strDatum = "ASTRONOMIC STATION 1952"; // Marcus Island
						break;
					case 12:
						strDatum = "ASTRO B4 SOROL ATOLL"; // Tern Island
						break;
					case 13:
						strDatum = "BELLEVUE (IGN)"; // Efate and Erromango Islands
						break;
					case 14:
						strDatum = "BERMUDA 1957"; // Bermuda Islands
						break;
					case 15:
						strDatum = "BOGOTA OBSERVATORY"; // Colombia
						break;
					case 16:
						strDatum = "CAMPO INCHAUSPE"; // Argentina
						break;
					case 17:
						strDatum = "CANTON ASTRO 1966"; // Phoenix Islands
						break;
					case 18:
						strDatum = "CAPE CANAVERAL"; // Florida, Bahama Islands
						break;
					case 19:
						strDatum = "CAPE"; // South Africa
						break;
					case 20:
						strDatum = "CARTHAGE"; // Tunisia
						break;
					case 21:
						strDatum = "CHATHAM 1971"; // Chatham Island (New Zealand)
						break;
					case 22:
						strDatum = "CHUA ASTRO"; // Paraguay
						break;
					case 23:
						strDatum = "CORREGO ALEGRE"; // Brazil
						break;
					case 24:
						strDatum = "DJAKARTA (BATAVIA)"; // Sumatra Island (Indonesia)
						break;
					case 25:
						strDatum = "DOS 1968"; // Gizo Island (New Georgia Islands)
						break;
					case 26:
						strDatum = "EASTER ISLAND 1967"; // Easter Island
						break;
					case 27:
						strDatum = "EUROPEAN 1950"; // Austria, Belgium, Denmark, Finland, France, Germany, Gibraltar,Greece, Italy, Luxembourg, Netherlands, Norway, Portugal, Spain, Sweden, Swit-zerland
						break;
					case 28:
						strDatum = "EUROPEAN 1979"; // Austria, Finland, Netherlands, Norway, Spain, Sweden, Swit-zerland
						break;
					case 29:
						strDatum = "FINLAND HAYFORD 1910"; // Finland
						break;
					case 30:
						strDatum = "GANDAJIKA BASE"; // Republic of Maldives
						break;
					case 31:
						strDatum = "GEODETIC DATUM 1949"; // New Zealand
						break;
					case 32:
						strDatum = "ORDNANCE SURVEY OF GREAT BRITAIN 1936"; // England, Isle of Man, Scotland,Shetland Islands, Wales
						break;
					case 33:
						strDatum = "GUAM 1963"; // Guam Island
						break;
					case 34:
						strDatum = "GUX 1 ASTRO"; // Guadalcanal Island
						break;
					case 35:
						strDatum = "HJORSEY 1955"; // Iceland
						break;
					case 36:
						strDatum = "HONG KONG 1963"; // Hong Kong
						break;
					case 37:
						strDatum = "INDIAN"; // Bangladesh, India, Nepal
						break;
					case 38:
						strDatum = "INDIAN"; // Thailand, Vietnam
						break;
					case 39:
						strDatum = "IRELAND 1965"; // Ireland
						break;
					case 40:
						strDatum = "ISTS O73 ASTRO 1969"; // Diego Garcia
						break;
					case 41:
						strDatum = "JOHNSTON ISLAND 1961"; // Johnston Island
						break;
					case 42:
						strDatum = "KANDAWALA"; // Sri Lanka
						break;
					case 43:
						strDatum = "KERGUELEN ISLAND"; // Kerguelen Island
						break;
					case 44:
						strDatum = "KERTAU 1948"; // West Malaysia, Singapore
						break;
					case 45:
						strDatum = "L.C. 5 ASTRO"; // Cayman Brac Island
						break;
					case 46:
						strDatum = "LIBERIA 1964"; // Liberia47 LUZON"; // Mindanao Island
						break;
					case 48:
						strDatum = "LUZON"; // Phillippines (excluding Mindanao Island)
						break;
					case 49:
						strDatum = "MAHE 1971"; // Mahe Island
						break;
					case 50:
						strDatum = "MARCO ASTRO"; // Salvage Islands
						break;
					case 51:
						strDatum = "MASSAWA"; // Eritrea (Ethiopia)
						break;
					case 52:
						strDatum = "MERCHICH"; // Morocco
						break;
					case 53:
						strDatum = "MIDWAY ASTRO 1961"; // Midway Island
						break;
					case 54:
						strDatum = "MINNA"; // Nigeria
						break;
					case 55:
						strDatum = "NORTH AMERICAN 1927"; // Alaska
						break;
					case 56:
						strDatum = "NORTH AMERICAN 1927"; // Bahamas (excluding San Salvador Island)
						break;
					case 57:
						strDatum = "NORTH AMERICAN 1927"; // Central America (Belize, Costa Rica, El Salvador,Guatemala, Honduras, Nicaragua)
						break;
					case 58:
						strDatum = "NORTH AMERICAN 1927"; // Canal Zone
						break;
					case 59:
						strDatum = "NORTH AMERICAN 1927"; // Canada (including Newfoundland Island)
						break;
					case 60:
						strDatum = "NORTH AMERICAN 1927"; // Caribbean (Barbados, Caicos Islands, Cuba, Domini-can Republic, Grand Cayman, Jamaica, Leeward Islands, Turks Islands)
						break;
					case 61:
						strDatum = "NORTH AMERICAN 1927"; // Mean Value (CONUS)
						break;
					case 62:
						strDatum = "NORTH AMERICAN 1927"; // Cuba
						break;
					case 63:
						strDatum = "NORTH AMERICAN 1927"; // Greenland (Hayes Peninsula)
						break;
					case 64:
						strDatum = "NORTH AMERICAN 1927"; // Mexico
						break;
					case 65:
						strDatum = "NORTH AMERICAN 1927"; // San Salvador Island
						break;
					case 66:
						strDatum = "NORTH AMERICAN 1983"; // Alaska, Canada, Central America, CONUS, Mexico
						break;
					case 67:
						strDatum = "NAPARIMA , BWI"; // Trinidad and Tobago
						break;
					case 68:
						strDatum = "NAHRWAN"; // Masirah Island (Oman)
						break;
					case 69:
						strDatum = "NAHRWAN"; // Saudi Arabia
						break;
					case 70:
						strDatum = "NAHRWAN"; // United Arab Emirates
						break;
					case 71:
						strDatum = "OBSERVATORIO 1966"; // Corvo and Flores Islands (Azores)
						break;
					case 72:
						strDatum = "OLD EGYPTIAN"; // Egypt
						break;
					case 73:
						strDatum = "OLD HAWAIIAN"; // Mean Value
						break;
					case 74:
						strDatum = "OMAN"; // Oman
						break;
					case 75:
						strDatum = "PICO DE LAS NIEVES"; // Canary Islands
						break;
					case 76:
						strDatum = "PITCAIRN ASTRO 1967"; // Pitcairn Island
						break;
					case 77:
						strDatum = "PUERTO RICO"; // Puerto Rico, Virgin Islands
						break;
					case 78:
						strDatum = "QATAR NATIONAL"; // Qatar
						break;
					case 79:
						strDatum = "QORNOQ"; // South Greenland
						break;
					case 80:
						strDatum = "REUNION"; // Mascarene Island
						break;
					case 81:
						strDatum = "ROME 1940"; // Sardinia Island
						break;
					case 82:
						strDatum = "RT 90"; // Sweden
						break;
					case 83:
						strDatum = "PROVISIONAL SOUTH AMERICAN 1956"; // Bolivia, Chile, Colombia, Ecuador,Guyana, Peru, Venezuela
						break;
					case 84:
						strDatum = "SOUTH AMERICAN 1969"; // Argentina, Bolivia, Brazil, Chile, Colombia, Ecuador,Guyana, Paraguay, Peru, Venezuela, Trinidad and Tobago
						break;
					case 85:
						strDatum = "SOUTH ASIA"; // Singapore
						break;
					case 86:
						strDatum = "PROVISIONAL SOUTH CHILEAN 1963"; // South Chile
						break;
					case 87:
						strDatum = "SANTO (DOS)"; // Espirito Santo Island
						break;
					case 88:
						strDatum = "SAO BRAZ"; // Sao Miguel, Santa Maria Islands (Azores)
						break;
					case 89:
						strDatum = "SAPPER HILL 1943"; // East Falkland Island
						break;
					case 90:
						strDatum = "SCHWARZECK"; // Namibia
						break;
					case 91:
						strDatum = "SOUTHEAST BASE"; // Porto Santo and Madeira Islands
						break;
					case 92:
						strDatum = "SOUTHWEST BASE"; // Faial, Graciosa, Pico, Sao Jorge, and Terceira Islands(Azores)
						break;
					case 93:
						strDatum = "TIMBALAI 1948"; // Brunei and East Malaysia (Sarawak and Sabah)
						break;
					case 94:
						strDatum = "TOKYO"; // Japan, Korea, Okinawa
						break;
					case 95:
						strDatum = "TRISTAN ASTRO 1968"; // Tristan da Cunha
						break;
					case 96:
						strDatum = "User defined earth datum";
						break;
					case 97:
						strDatum = "VITI LEVU 1916"; // Viti Levu Island (Fiji Islands)
						break;
					case 98:
						strDatum = "WAKE-ENIWETOK 1960"; // Marshall Islands
						break;
					case 99:
						strDatum = "WORLD GEODETIC SYSTEM 1972";
						break;
					case 100:
						strDatum = "WORLD GEODETIC SYSTEM 1984";
						break;
					case 101:
						strDatum = "ZANDERIJ"; // Surinam
						break;
					case 102:
						strDatum = "CH -1903"; // Switzerland
						break;
					case 103:
						strDatum = "Hu"; // Tzu"; // Shan
						break;
					case 104:
						strDatum = "Indonesia 74";
						break;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return strDatum;
		}	
	}

}
