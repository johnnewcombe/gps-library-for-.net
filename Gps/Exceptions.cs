using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;

namespace Waymex.Gps
{
	/// <summary>
	/// Base Exception Class.
	/// </summary>
#if(PUBLIC)
	public abstract class ExceptionBase : System.ApplicationException
#else
	internal abstract class ExceptionBase : System.ApplicationException
#endif
	{
		private string m_appSource;

		/// <summary>
		/// Base Exception Class.
		/// </summary>
		public ExceptionBase(string Message,Exception e):base(Message, e)
		{
			m_appSource = "";
		}
		/// <summary>
		/// Base Exception Class.
		/// </summary>
		public ExceptionBase(string Message):base(Message)
		{
			m_appSource = "";
		}
		// We only want this method exposed to code
		// in the same assembly. However, we might need to 
		// change the scope if this class were in another
		// assembly.
		internal void LogError()
		{
			System.Diagnostics.EventLog e;
			e = new System.Diagnostics.EventLog("Application");
			e.Source = this.AppSource;
			e.WriteEntry(this.Message, System.Diagnostics.EventLogEntryType.Error);
			e.Dispose();
		}

		/// <summary>
		/// Exposed in order that derived classe can overide.
		/// </summary>
		public virtual string AppSource
		{
			get
			{
				return m_appSource;
			}
		}																	
	}
    /// <summary>
    /// DataException.
    /// </summary>
#if(PUBLIC)
    public class InvalidDataException : ExceptionBase
#else
    internal class InvalidDataException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public InvalidDataException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public InvalidDataException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// HttpTransferException.
    /// </summary>
#if(PUBLIC)
    public class HttpTransferException : ExceptionBase
#else
    internal class HttpTransferException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public HttpTransferException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public HttpTransferException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// GPS Waypoint Transfer Exception.
    /// </summary>
#if(PUBLIC)
    public class DataTransferException : ExceptionBase
#else
    internal class DataTransferException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DataTransferException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public DataTransferException(string Message, Exception e) : base(Message, e) { }
    }

    /// <summary>
    /// GPS DLE Exception.
    /// </summary>
#if(PUBLIC)
    public class DleException : ExceptionBase
#else
    internal class DleException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DleException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public DleException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// GPS Checksum Exception.
    /// </summary>
#if(PUBLIC)
    public class ChecksumException : ExceptionBase
#else
    internal class ChecksumException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public ChecksumException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public ChecksumException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// GPS Packet Size Exception.
    /// </summary>
#if(PUBLIC)
    public class PacketSizeException : ExceptionBase
#else
    internal class PacketSizeException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public PacketSizeException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public PacketSizeException(string Message, Exception e) : base(Message, e) { }
    }

    /// <summary>
    /// GPS Port Exception.
    /// </summary>
#if(PUBLIC)
    public class PortException : ExceptionBase
#else
    internal class PortException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public PortException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public PortException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// GPS Link Time Out Exception.
    /// </summary>
#if(PUBLIC)
    public class LinkTimeOutException : ExceptionBase
#else
    internal class LinkTimeOutException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public LinkTimeOutException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor.
        /// </summary>
        public LinkTimeOutException(string Message, Exception e) : base(Message, e) { }
    }

    /// <summary>
    /// GPS USB Transport Exception.
    /// </summary>
#if(PUBLIC)
    public class USBTransportException : ExceptionBase
#else
    internal class USBTransportException : ExceptionBase
#endif
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public USBTransportException(string Message) : base(Message) { }
        /// <summary>
        /// Constructor
        /// </summary>
        public USBTransportException(string Message, Exception e) : base(Message, e) { }

        /// <summary>
        /// Constructor for re-raising exceptions from receive thread
        /// </summary>
        /// <param name="e">Inner exception raised on receive thread</param>
        internal USBTransportException(Exception e) : base("Receive Thread Exception", e) { }
    }
    /// <summary>
    /// NMEA Invalid Sentence Exception.
    /// </summary>
#if(PUBLIC)
    public class InvalidSentence : ExceptionBase
#else
    internal class InvalidSentence : ExceptionBase
#endif
    {
        /// <summary>
        /// NMEA Invalid Sentence Exception contructor.
        /// </summary>
        public InvalidSentence(string Message) : base(Message) { }
        /// <summary>
        /// NMEA Invalid Sentence Exception contructor.
        /// </summary>
        public InvalidSentence(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// CreateWaypointException Class.
    /// </summary>
#if(PUBLIC)
    public class CreateWaypointException : ExceptionBase
#else
    internal class CreateWaypointException : ExceptionBase
#endif
    {
        /// <summary>
        /// CreateWaypointException Class Constructor.
        /// </summary>
        public CreateWaypointException(string Message) : base(Message) { }
        /// <summary>
        /// CreateWaypointException Class Constructor.
        /// </summary>
        public CreateWaypointException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// InvalidFileName Exception.
    /// </summary>
#if(PUBLIC)
    public class InvalidFileName : ExceptionBase
#else
    internal class InvalidFileName : ExceptionBase
#endif
    {
        /// <summary>
        /// InvalidFileName Exception Constructor.
        /// </summary>
        public InvalidFileName(string Message) : base(Message) { }
        /// <summary>
        /// InvalidFileName Exception Constructor.
        /// </summary>
        public InvalidFileName(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// CreateSentenceException Conversion Exception.
    /// </summary>
#if(PUBLIC)
    public class CreateSentenceException : ExceptionBase
#else
    internal class CreateSentenceException : ExceptionBase
#endif
    {
        /// <summary>
        /// CreateSentenceException Exception contructor.
        /// </summary>
        public CreateSentenceException(string Message) : base(Message) { }
        /// <summary>
        /// CreateSentenceException Exception contructor.
        /// </summary>
        public CreateSentenceException(string Message, Exception e) : base(Message, e) { }
    }
     /// <summary>
    /// CaptureFileException Exception.
    /// </summary>
#if(PUBLIC)
    public class CaptureFileException : ExceptionBase
#else
    internal class CaptureFileException : ExceptionBase
#endif
    {
        /// <summary>
        /// CaptureFileException Exception contructor.
        /// </summary>
        public CaptureFileException(string Message) : base(Message) { }
        /// <summary>
        /// CaptureFileException Exception contructor.
        /// </summary>
        public CaptureFileException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// UsbLinkException Exception.
    /// </summary>
#if(PUBLIC)
    public class UsbLinkException : ExceptionBase
#else
    internal class UsbLinkException : ExceptionBase
#endif
    {
        /// <summary>
        /// UsbLinkException Exception contructor.
        /// </summary>
        public UsbLinkException(string Message) : base(Message) { }
        /// <summary>
        /// UsbLinkException Exception contructor.
        /// </summary>
        public UsbLinkException(string Message, Exception e) : base(Message, e) { }
    }
    /// <summary>
    /// UsbLinkTimeoutException Exception.
    /// </summary>
#if(PUBLIC)
    public class UsbLinkTimeoutException : ExceptionBase
#else
    internal class UsbLinkTimeoutException : ExceptionBase
#endif
    {
        /// <summary>
        /// UsbLinkTimeoutException Exception contructor.
        /// </summary>
        public UsbLinkTimeoutException(string Message) : base(Message) { }
        /// <summary>
        /// UsbLinkTimeoutException Exception contructor.
        /// </summary>
        public UsbLinkTimeoutException(string Message, Exception e) : base(Message, e) { }
    }
   
}





