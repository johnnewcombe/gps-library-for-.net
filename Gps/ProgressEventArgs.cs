using System;

namespace Waymex.Gps
{
    /// <summary>
    /// ProgressEventArgs is the class containing event data. 
    /// </summary>
    public class ProgressEventArgs :EventArgs
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public ProgressEventArgs(int progress)
		{
			//TODO: expand to include the parsed parameters of the PVT
            m_progress = progress;
		}
		private int m_progress;

		/// <summary>
		/// Progress value as within the range 0 - 100 representing the percentage complete.
		/// </summary>
        public int Progress
        {
            get
            {
                return m_progress;
            }
        }
	}
}
