using System;
using System.Text;

namespace Waymex.Gps
{
    /// <summary>
    /// Enumerator for Symbols (Icons) includes all Garmin and Magellan Symbol Sets.
    /// </summary>
    public enum SymbolId
    {
        /// <summary>
        /// white anchor symbol
        /// </summary>
        Anchor = 0, /*  */
        /// <summary>
        /// white bell symbol
        /// </summary>
        Bell = 1, /*  */
        /// <summary>
        /// green diamond symbol
        /// </summary>
        DiamondGreen = 2, /*  */
        /// <summary>
        /// red diamond symbol
        /// </summary>
        DiamondRed = 3, /*  */
        /// <summary>
        /// diver down flag 1
        /// </summary>
        DiverDown1 = 4, /*  */
        /// <summary>
        /// diver down flag 2
        /// </summary>
        DiverDown2 = 5, /*  */
        /// <summary>
        /// white dollar symbol
        /// </summary>
        Dollar = 6, /*  */
        /// <summary>
        /// white fish symbol
        /// </summary>
        Fish = 7, /*  */
        /// <summary>
        /// white fuel symbol
        /// </summary>
        Fuel = 8, /*  */
        /// <summary>
        /// white horn symbol
        /// </summary>
        Horn = 9, /*  */
        /// <summary>
        /// white house symbol
        /// </summary>
        House = 10, /*  */
        /// <summary>
        /// white knife and fork symbol
        /// </summary>
        Knife = 11, /*  */
        /// <summary>
        /// white light symbol
        /// </summary>
        Light = 12, /*  */
        /// <summary>
        /// white mug symbol
        /// </summary>
        Mug = 13, /*  */
        /// <summary>
        /// white skull and crossbones symbol
        /// </summary>
        Skull = 14, /* */
        /// <summary>
        /// green square symbol
        /// </summary>
        SquareGreen = 15, /*  */
        /// <summary>
        /// red square symbol
        /// </summary>
        SquareRed = 16, /*  */
        /// <summary>
        /// white buoy waypoint symbol
        /// </summary>
        WhiteBuoy = 17, /*  */
        /// <summary>
        /// waypoint dot
        /// </summary>
        WaypointDot = 18, /*  */
        /// <summary>
        /// white wreck symbol
        /// </summary>
        Wreck = 19, /*  */
        /// <summary>
        /// null symbol (transparent)
        /// </summary>
        NullTransparent = 20, /*  */
        /// <summary>
        /// man overboard symbol
        /// </summary>
        Mob = 21, /*  */
        /// <summary>
        /// amber map buoy symbol
        /// </summary>
        BuoyAmber = 22, /*  */
        /// <summary>
        /// black map buoy symbol
        /// </summary>
        BuoyBlack = 23, /*  */
        /// <summary>
        /// blue map buoy symbol
        /// </summary>
        BuoyBlue = 24, /*  */
        /// <summary>
        /// green map buoy symbol
        /// </summary>
        BuoyGreen = 25, /*  */
        /// <summary>
        /// green/red map buoy symbol
        /// </summary>
        BuoyGreenRed = 26, /*  */
        /// <summary>
        /// green/white map buoy symbol
        /// </summary>
        BuoyGreenWhite = 27, /*  */
        /// <summary>
        /// orange map buoy symbol
        /// </summary>
        BuoyOrange = 28, /*  */
        /// <summary>
        /// red map buoy symbol
        /// </summary>
        BuoyRed = 29, /*  */
        /// <summary>
        /// red/green map buoy symbol
        /// </summary>
        BuoyRedGreen = 30, /*  */
        /// <summary>
        /// red/white map buoy symbol
        /// </summary>
        BuoyRedWhite = 31, /*  */
        /// <summary>
        /// violet map buoy symbol
        /// </summary>
        BuoyViolet = 32, /*  */
        /// <summary>
        /// white map buoy symbol
        /// </summary>
        BuoyWhite = 33, /*  */
        /// <summary>
        /// white/green map buoy symbol
        /// </summary>
        BuoyWhiteGreen = 34, /*  */
        /// <summary>
        /// white/red map buoy symbol
        /// </summary>
        BuoyWhiteRed = 35, /*  */
        /// <summary>
        /// white dot symbol
        /// </summary>
        Dot = 36, /*  */
        /// <summary>
        /// radio beacon symbol
        /// </summary>
        RadioBeacon = 37, /*  */
        /// <summary>
        /// boat ramp symbol
        /// </summary>
        BoatRamp = 150, /*  */
        /// <summary>
        /// campground symbol
        /// </summary>
        Camp = 151, /*  */
        /// <summary>
        /// restrooms symbol
        /// </summary>
        Restrooms = 152, /*  */
        /// <summary>
        /// shower symbol
        /// </summary>
        Showers = 153, /*  */
        /// <summary>
        /// drinking water symbol
        /// </summary>
        DrinkingWater = 154, /*  */
        /// <summary>
        /// telephone symbol
        /// </summary>
        Phone = 155, /*  */
        /// <summary>
        /// first aid symbol
        /// </summary>
        FirstAid = 156, /*  */
        /// <summary>
        /// information symbol
        /// </summary>
        Information = 157, /*  */
        /// <summary>
        /// parking symbol
        /// </summary>
        Parking = 158, /*  */
        /// <summary>
        /// park symbol
        /// </summary>
        Park = 159, /*  */
        /// <summary>
        /// picnic symbol
        /// </summary>
        Picnic = 160, /*  */
        /// <summary>
        /// scenic area symbol
        /// </summary>
        Scenic = 161, /*  */
        /// <summary>
        /// skiing symbol
        /// </summary>
        Skiing = 162, /*  */
        /// <summary>
        /// swimming symbol
        /// </summary>
        Swimming = 163, /*  */
        /// <summary>
        /// dam symbol
        /// </summary>
        Dam = 164, /*  */
        /// <summary>
        /// controlled area symbol
        /// </summary>
        Controlled = 165, /*  */
        /// <summary>
        /// danger symbol
        /// </summary>
        Danger = 166, /*  */
        /// <summary>
        /// restricted area symbol
        /// </summary>
        Restricted = 167, /*  */
        /// <summary>
        /// null symbol
        /// </summary>
        Null = 168, /*  */
        /// <summary>
        /// ball symbol
        /// </summary>
        Ball = 169, /*  */
        /// <summary>
        /// car symbol
        /// </summary>
        Car = 170, /*  */
        /// <summary>
        /// deer symbol
        /// </summary>
        Deer = 171, /*  */
        /// <summary>
        /// shopping cart symbol
        /// </summary>
        ShoppingCart = 172, /*  */
        /// <summary>
        /// lodging symbol
        /// </summary>
        Lodging = 173, /*  */
        /// <summary>
        /// mine symbol
        /// </summary>
        Mine = 174, /*  */
        /// <summary>
        /// trail head symbol
        /// </summary>
        TrailHead = 175, /*  */
        /// <summary>
        /// truck stop symbol
        /// </summary>
        TruckStop = 176, /*  */
        /// <summary>
        /// user exit symbol
        /// </summary>
        UserExit = 177, /*  */
        /// <summary>
        /// flag symbol
        /// </summary>
        Flag = 178, /*  */
        /// <summary>
        /// circle with x in the center
        /// </summary>
        CircleX = 179, /*  */
        /// <summary>
        /// open 24 hours symbol
        /// </summary>
        Open24hr = 180, /*  */
        /// <summary>
        /// U fishing hot spots facility
        /// </summary>
        FishingHotSpotFacility = 181, /*  */
        /// <summary>
        /// bottom conditions
        /// </summary>
        BottomConditions = 182, /*  */
        /// <summary>
        /// tide/current prediction station
        /// </summary>
        TidePredictionStation = 183, /*  */
        /// <summary>
        /// U anchor prohibited symbol
        /// </summary>
        AnchorProhibited = 184, /*  */
        /// <summary>
        /// U beacon symbol
        /// </summary>
        Beacon = 185, /*  */
        /// <summary>
        /// U coast guard symbol
        /// </summary>
        CoastGuard = 186, /*  */
        /// <summary>
        /// U reef symbol
        /// </summary>
        Reef = 187, /*  */
        /// <summary>
        /// U weedbed symbol
        /// </summary>
        WeedBed = 188, /*  */
        /// <summary>
        /// U dropoff symbol
        /// </summary>
        DropOff = 189, /*  */
        /// <summary>
        /// U dock symbol
        /// </summary>
        Dock = 190, /*  */
        /// <summary>
        /// U marina symbol
        /// </summary>
        Marina = 191, /*  */
        /// <summary>
        /// U bait and tackle symbol
        /// </summary>
        BaitTackle = 192, /*  */
        /// <summary>
        /// U stump symbol
        /// </summary>
        Stump = 193, /*  */
        /*---------------------------------------------------------------
        User customizable symbols
        The values from begin_custom to end_custom inclusive are
        reserved for the identification of user customizable symbols.
        ---------------------------------------------------------------
        begin_custom = 7680, /* first user customizable symbol 
        end_custom = 8191, /* last user customizable symbol 
        /*---------------------------------------------------------------
        Land symbols
        ---------------------------------------------------------------*/
        /// <summary>
        /// interstate hwy symbol
        /// </summary>
        InterstateHighway = 8192, /*  */
        /// <summary>
        /// us highway symbol
        /// </summary>
        UsHignway = 8193, /*  */
        /// <summary>
        /// state highway symbol
        /// </summary>
        StateHighway = 8194, /*  */
        /// <summary>
        /// mile marker symbol
        /// </summary>
        MileMarker = 8195, /*  */
        /// <summary>
        /// tracback (feet) symbol
        /// </summary>
        TrackBack = 8196, /*  */
        /// <summary>
        /// golf symbol
        /// </summary>
        Golf = 8197, /*  */
        /// <summary>
        /// small city symbol
        /// </summary>
        SmallCity = 8198, /*  */
        /// <summary>
        /// medium city symbol
        /// </summary>
        MediumCity = 8199, /*  */
        /// <summary>
        /// large city symbol
        /// </summary>
        LargeCity = 8200, /*  */
        /// <summary>
        /// intl freeway highway symbol
        /// </summary>
        Freeway = 8201, /*  */
        /// <summary>
        /// intl national highway symbol
        /// </summary>
        NationalHighway = 8202, /*  */
        /// <summary>
        /// capital city symbol (star)
        /// </summary>
        CapitalCity = 8203, /*  */
        /// <summary>
        /// amusement park symbol
        /// </summary>
        AmusementPark = 8204, /*  */
        /// <summary>
        /// bowling symbol
        /// </summary>
        Bowling = 8205, /*  */
        /// <summary>
        /// car rental symbol
        /// </summary>
        CarRental = 8206, /*  */
        /// <summary>
        /// car repair symbol
        /// </summary>
        CarRepair = 8207, /*  */
        /// <summary>
        /// fast food symbol
        /// </summary>
        FastFood = 8208, /*  */
        /// <summary>
        /// fitness symbol
        /// </summary>
        Fitness = 8209, /*  */
        /// <summary>
        /// movie symbol
        /// </summary>
        Movie = 8210, /*  */
        /// <summary>
        /// museum symbol
        /// </summary>
        Museum = 8211, /*  */
        /// <summary>
        /// pharmacy symbol
        /// </summary>
        Pharmacy = 8212, /*  */
        /// <summary>
        /// pizza symbol
        /// </summary>
        Pizza = 8213, /*  */
        /// <summary>
        /// post office symbol
        /// </summary>
        PostOffice = 8214, /*  */
        /// <summary>
        /// RV park symbol
        /// </summary>
        RvPark = 8215, /*  */
        /// <summary>
        /// school symbol
        /// </summary>
        School = 8216, /*  */
        /// <summary>
        /// stadium symbol
        /// </summary>
        Stadium = 8217, /*  */
        /// <summary>
        /// dept. store symbol
        /// </summary>
        Store = 8218, /*  */
        /// <summary>
        /// zoo symbol
        /// </summary>
        Zoo = 8219, /*  */
        /// <summary>
        /// convenience store symbol
        /// </summary>
        GasPlus = 8220, /*  */
        /// <summary>
        /// live theater symbol
        /// </summary>
        Faces = 8221, /*  */
        /// <summary>
        /// ramp intersection symbol
        /// </summary>
        RampIntersection = 8222, /*  */
        /// <summary>
        /// street intersection symbol
        /// </summary>
        StreetIntersection = 8223, /*  */
        /// <summary>
        /// inspection/weigh station symbol
        /// </summary>
        WeighStation = 8226, /*  */
        /// <summary>
        /// toll booth symbol
        /// </summary>
        TollBooth = 8227, /*  */
        /// <summary>
        /// elevation point symbol
        /// </summary>
        ElevationPoint = 8228, /*  */
        /// <summary>
        /// exit without services symbol
        /// </summary>
        ExitNoService = 8229, /*  */
        /// <summary>
        /// geographic place name, man-made
        /// </summary>
        GeographicPlaceManMade = 8230, /*  */
        /// <summary>
        /// geographic place name, water
        /// </summary>
        GeographicPlaceWater = 8231, /*  */
        /// <summary>
        /// geographic place name, land
        /// </summary>
        GeographicPlaceLand = 8232, /*  */
        /// <summary>
        /// bridge symbol
        /// </summary>
        Bridge = 8233, /*  */
        /// <summary>
        /// building symbol
        /// </summary>
        Building = 8234, /*  */
        /// <summary>
        /// cemetery symbol
        /// </summary>
        Cemetery = 8235, /*  */
        /// <summary>
        /// church symbol
        /// </summary>
        Church = 8236, /*  */
        /// <summary>
        /// civil location symbol
        /// </summary>
        Civil = 8237, /*  */
        /// <summary>
        /// crossing symbol
        /// </summary>
        Crossing = 8238, /*  */
        /// <summary>
        /// historical town symbol
        /// </summary>
        HistoricTown = 8239, /*  */
        /// <summary>
        /// levee symbol
        /// </summary>
        Levee = 8240, /*  */
        /// <summary>
        /// military location symbol
        /// </summary>
        Military = 8241, /*  */
        /// <summary>
        /// oil field symbol
        /// </summary>
        OilField = 8242, /*  */
        /// <summary>
        /// tunnel symbol
        /// </summary>
        Tunnel = 8243, /*  */
        /// <summary>
        /// beach symbol
        /// </summary>
        Beach = 8244, /*  */
        /// <summary>
        /// forest symbol
        /// </summary>
        Forest = 8245, /*  */
        /// <summary>
        /// summit symbol
        /// </summary>
        Summit = 8246, /*  */
        /// <summary>
        /// large ramp intersection symbol
        /// </summary>
        LargeRampIntersection = 8247, /*  */
        /// <summary>
        /// large exit without services smbl
        /// </summary>
        LargeExitNoService = 8248, /*  */
        /// <summary>
        /// police/official badge symbol
        /// </summary>
        Badge = 8249, /*  */
        /// <summary>
        /// gambling/casino symbol
        /// </summary>
        Cards = 8250, /*  */
        /// <summary>
        /// snow skiing symbol
        /// </summary>
        SnowSkiingSymbol = 8251, /*  */
        /// <summary>
        /// ice skating symbol
        /// </summary>
        IceSkating = 8252, /*  */
        /// <summary>
        /// tow truck (wrecker) symbol
        /// </summary>
        Wrecker = 8253, /*  */
        /// <summary>
        /// border crossing (port of entry)
        /// </summary>
        Border = 8254, /*  */
        /// <summary>
        /// geocache location
        /// </summary>
        Geocache = 8255, /*  */
        /// <summary>
        /// found geocache
        /// </summary>
        GeocacheFind = 8256, /*  */
        /// <summary>
        /// Rino contact symbol, "smiley" 
        /// </summary>
        ContactSmiley = 8257, /* */
        /// <summary>
        /// Rino contact symbol, "ball cap"
        /// </summary>
        ContactBallCap = 8258, /*  */
        /// <summary>
        /// Rino contact symbol, "big ear"
        /// </summary>
        ContactBigEars = 8259, /*  */
        /// <summary>
        /// Rino contact symbol, "spike"
        /// </summary>
        ContactSpike = 8260, /*  */
        /// <summary>
        /// Rino contact symbol, "goatee"
        /// </summary>
        ContactGoatee = 8261, /*  */
        /// <summary>
        /// Rino contact symbol, "afro"
        /// </summary>
        ContactAfro = 8262, /*  */
        /// <summary>
        /// Rino contact symbol, "dreads"
        /// </summary>
        ContactDreads = 8263, /*  */
        /// <summary>
        /// Rino contact symbol, "female 1"
        /// </summary>
        ContactFemale1 = 8264, /*  */
        /// <summary>
        /// Rino contact symbol, "female 2"
        /// </summary>
        ContactFemale2 = 8265, /*  */
        /// <summary>
        /// Rino contact symbol, "female 3"
        /// </summary>
        ContactFemale3 = 8266, /*  */
        /// <summary>
        /// Rino contact symbol, "ranger"
        /// </summary>
        ContactRanger = 8267, /*  */
        /// <summary>
        /// Rino contact symbol, "kung fu"
        /// </summary>
        ContactKungFu = 8268, /*  */
        /// <summary>
        /// Rino contact symbol, "sumo"
        /// </summary>
        ContactSumo = 8269, /*  */
        /// <summary>
        /// Rino contact symbol, "pirate"
        /// </summary>
        ContactPirate = 8270, /*  */
        /// <summary>
        /// Rino contact symbol, "biker"
        /// </summary>
        ContactBiker = 8271, /*  */
        /// <summary>
        /// Rino contact symbol, "alien"
        /// </summary>
        ContactAlien = 8272, /*  */
        /// <summary>
        /// Rino contact symbol, "bug"
        /// </summary>
        ContactBug = 8273, /*  */
        /// <summary>
        /// Rino contact symbol, "cat"
        /// </summary>
        ContactCat = 8274, /*  */
        /// <summary>
        /// Rino contact symbol, "dog"
        /// </summary>
        ContactDog = 8275, /*  */
        /// <summary>
        /// Rino contact symbol, "pig"
        /// </summary>
        ContactPig = 8276, /*  */
        /// <summary>
        /// water hydrant symbol
        /// </summary>
        Hydrant = 8282, /*  */
        /// <summary>
        /// blue flag symbol
        /// </summary>
        FlagBlue = 8284, /*  */
        /// <summary>
        /// green flag symbol
        /// </summary>
        FlagGreen = 8285, /*  */
        /// <summary>
        /// red flag symbol
        /// </summary>
        FlagRed = 8286, /*  */
        /// <summary>
        /// blue pin symbol
        /// </summary>
        PinBlue = 8287, /*  */
        /// <summary>
        /// green pin symbol
        /// </summary>
        PinGreen = 8288, /*  */
        /// <summary>
        /// red pin symbol
        /// </summary>
        PinRed = 8289, /*  */
        /// <summary>
        /// blue block symbol
        /// </summary>
        BlockBlue = 8290, /*  */
        /// <summary>
        /// green block symbol
        /// </summary>
        BlockGreen = 8291, /*  */
        /// <summary>
        /// red block symbol
        /// </summary>
        BlockRed = 8292, /*  */
        /// <summary>
        /// bike trail symbol
        /// </summary>
        BikeTrail = 8293, /*  */
        /// <summary>
        /// red circle symbol
        /// </summary>
        CircleRed = 8294, /*  */
        /// <summary>
        /// green circle symbol
        /// </summary>
        CircleGreen = 8295, /*  */
        /// <summary>
        /// blue circle symbol
        /// </summary>
        CircleBlue = 8296, /*  */
        /// <summary>
        /// blue diamond symbol
        /// </summary>
        DiamondBlue = 8299, /*  */
        /// <summary>
        /// red oval symbol
        /// </summary>
        OvalRed = 8300, /*  */
        /// <summary>
        /// green oval symbol
        /// </summary>
        OvalGreen = 8301, /*  */
        /// <summary>
        /// blue oval symbol
        /// </summary>
        OvalBlue = 8302, /*  */
        /// <summary>
        /// red rectangle symbol
        /// </summary>
        RectRed = 8303, /*  */
        /// <summary>
        /// green rectangle symbol
        /// </summary>
        RectGreen = 8304, /*  */
        /// <summary>
        /// blue rectangle symbol
        /// </summary>
        RectBlue = 8305, /*  */
        /// <summary>
        /// blue square symbol
        /// </summary>
        SquareBlue = 8308, /*  */
        /// <summary>
        /// red letter 'A' symbol
        /// </summary>
        LetterRed_A = 8309, /*  */
        /// <summary>
        /// red letter 'B' symbol
        /// </summary>
        LetterRed_B = 8310, /*  */
        /// <summary>
        /// red letter 'C' symbol
        /// </summary>
        LetterRed_C = 8311, /*  */
        /// <summary>
        /// red letter 'D' symbol
        /// </summary>
        LetterRed_D = 8312, /*  */
        /// <summary>
        /// green letter 'A' symbol
        /// </summary>
        LetterGreen_A = 8313, /*  */
        /// <summary>
        /// green letter 'C' symbol
        /// </summary>
        LetterGreen_B = 8314, /*  */
        /// <summary>
        /// green letter 'B' symbol
        /// </summary>
        LetterGreen_C = 8315, /*  */
        /// <summary>
        /// green letter 'D' symbol
        /// </summary>
        LetterGreen_D = 8316, /*  */
        /// <summary>
        /// blue letter 'A' symbol
        /// </summary>
        LetterBlue_A = 8317, /*  */
        /// <summary>
        /// blue letter 'B' symbol
        /// </summary>
        LetterBlue_B = 8318, /*  */
        /// <summary>
        /// blue letter 'C' symbol
        /// </summary>
        LetterBlue_C = 8319, /*  */
        /// <summary>
        /// blue letter 'D' symbol
        /// </summary>
        LetterBlue_D = 8320, /*  */
        /// <summary>
        /// red number '0' symbol
        /// </summary>
        NumberRed_0_ = 8321, /*  */
        /// <summary>
        /// red number '1' symbol
        /// </summary>
        NumberRed_1_ = 8322, /*  */
        /// <summary>
        /// red number '2' symbol
        /// </summary>
        NumberRed_2 = 8323, /*  */
        /// <summary>
        /// red number '3' symbol
        /// </summary>
        NumberRed_3 = 8324, /*  */
        /// <summary>
        /// red number '4' symbol
        /// </summary>
        NumberRed_4 = 8325, /*  */
        /// <summary>
        /// red number '5' symbol
        /// </summary>
        NumberRed_5 = 8326, /*  */
        /// <summary>
        /// red number '6' symbol
        /// </summary>
        NumberRed_6 = 8327, /*  */
        /// <summary>
        /// red number '7' symbol
        /// </summary>
        NumberRed_7 = 8328, /*  */
        /// <summary>
        /// red number '8' symbol
        /// </summary>
        NumberRed_8 = 8329, /*  */
        /// <summary>
        /// red number '9' symbol
        /// </summary>
        NumberRed_9 = 8330, /*  */
        /// <summary>
        /// green number '0' symbol
        /// </summary>
        NumberGreen_0 = 8331, /*  */
        /// <summary>
        /// green number '1' symbol
        /// </summary>
        NumberGreen_1 = 8332, /*  */
        /// <summary>
        /// green number '2' symbol
        /// </summary>
        NumberGreen_2 = 8333, /*  */
        /// <summary>
        /// green number '3' symbol
        /// </summary>
        NumberGreen_3 = 8334, /*  */
        /// <summary>
        /// green number '4' symbol
        /// </summary>
        NumberGreen_4 = 8335, /*  */
        /// <summary>
        /// green number '5' symbol
        /// </summary>
        NumberGreen_5 = 8336, /*  */
        /// <summary>
        /// green number '6' symbol
        /// </summary>
        NumberGreen_6 = 8337, /*  */
        /// <summary>
        /// green number '7' symbol
        /// </summary>
        NumberGreen_7 = 8338, /*  */
        /// <summary>
        /// green number '8' symbol
        /// </summary>
        NumberGreen_8 = 8339, /*  */
        /// <summary>
        /// green number '9' symbol
        /// </summary>
        NumberGreen_9 = 8340, /*  */
        /// <summary>
        /// blue number '0' symbol
        /// </summary>
        NumberBlue_0 = 8341, /*  */
        /// <summary>
        /// blue number '1' symbol
        /// </summary>
        NumberBlue_1 = 8342, /*  */
        /// <summary>
        /// blue number '2' symbol
        /// </summary>
        NumberBlue_2 = 8343, /*  */
        /// <summary>
        /// blue number '3' symbol
        /// </summary>
        NumberBlue_3 = 8344, /*  */
        /// <summary>
        /// blue number '4' symbol
        /// </summary>
        NumberBlue_4 = 8345, /*  */
        /// <summary>
        /// blue number '5' symbol
        /// </summary>
        NumberBlue_5 = 8346, /*  */
        /// <summary>
        /// blue number '6' symbol
        /// </summary>
        NumberBlue_6 = 8347, /*  */
        /// <summary>
        /// blue number '7' symbol
        /// </summary>
        NumberBlue_7 = 8348, /*  */
        /// <summary>
        /// blue number '8' symbol
        /// </summary>
        NumberBlue_8 = 8349, /*  */
        /// <summary>
        /// blue number '9' symbol
        /// </summary>
        NumberBlue_9 = 8350, /*  */
        /// <summary>
        /// blue triangle symbol
        /// </summary>
        TriangleBlue = 8351, /*  */
        /// <summary>
        /// green triangle symbol
        /// </summary>
        TriangleGreen = 8352, /*  */
        /// <summary>
        /// red triangle symbol
        /// </summary>
        TriangleRed = 8353, /*  */
        /// <summary>
        /// asian food symbol
        /// </summary>
        FoodAsian = 8359, /*  */
        /// <summary>
        /// deli symbol
        /// </summary>
        FoodDeli = 8360, /*  */
        /// <summary>
        /// italian food symbol
        /// </summary>
        FoodItalian = 8361, /*  */
        /// <summary>
        /// seafood symbol
        /// </summary>
        FoodSeafood = 8362, /*  */
        /// <summary>
        /// steak symbol
        /// </summary>
        FoodSteak = 8363, /*  */
        /*---------------------------------------------------------------
        Aviation symbols
        ---------------------------------------------------------------*/
        /// <summary>
        /// airport symbol
        /// </summary>
        Airport = 16384, /*  */
        /// <summary>
        /// intersection symbol
        /// </summary>
        Intersection = 16385, /*  */
        /// <summary>
        /// non-directional beacon symbol
        /// </summary>
        Ndb = 16386, /*  */
        /// <summary>
        /// VHF omni-range symbol
        /// </summary>
        Vor = 16387, /*  */
        /// <summary>
        /// heliport symbol
        /// </summary>
        Heliport = 16388, /*  */
        /// <summary>
        /// private field symbol
        /// </summary>
        PrivateField = 16389, /*  */
        /// <summary>
        /// soft field symbol
        /// </summary>
        SoftField = 16390, /*  */
        /// <summary>
        /// tall tower symbol
        /// </summary>
        TallTower = 16391, /*  */
        /// <summary>
        /// short tower symbol
        /// </summary>
        ShortTower = 16392, /*  */
        /// <summary>
        /// glider symbol
        /// </summary>
        Glider = 16393, /*  */
        /// <summary>
        /// ultralight symbol
        /// </summary>
        Ultralight = 16394, /*  */
        /// <summary>
        /// parachute symbol
        /// </summary>
        Parachute = 16395, /*  */
        /// <summary>
        /// VOR/TACAN symbol
        /// </summary>
        VorTac = 16396, /*  */
        /// <summary>
        /// VOR-DME symbol
        /// </summary>
        VorDme = 16397, /*  */
        /// <summary>
        /// first approach fix
        /// </summary>
        Faf = 16398, /*  */
        /// <summary>
        /// localizer outer marker
        /// </summary>
        Lom = 16399, /*  */
        /// <summary>
        /// missed approach point
        /// </summary>
        Map = 16400, /*  */
        /// <summary>
        /// TACAN symbol
        /// </summary>
        Tacan = 16401, /*  */
        /// <summary>
        /// seaplane base
        /// </summary>
        Seaplane = 16402, /*  */

        // additional symbols for D103, D107

        /// <summary>
        /// dot symbol
        /// </summary>
        //Dot = 65536, /*  */
        /// <summary>
        /// house symbol
        /// </summary>
        //House = 65537, /*  */
        /// <summary>
        /// gas symbol
        /// </summary>
        Gas = 65538, /*  */
        /// <summary>
        /// car symbol
        /// </summary>
        //Car = 65539, /*  */
        /// <summary>
        /// fish symbol
        /// </summary>
        //Fish = 65540, /*  */
        /// <summary>
        /// boat symbol
        /// </summary>
        Boat = 65541, /*  */
        /// <summary>
        /// anchor symbol
        /// </summary>
        //Anchor = 65542, /*  */
        /// <summary>
        /// wreck symbol
        /// </summary>
        //Wreck = 65543, /*  */
        /// <summary>
        /// exit symbol
        /// </summary>
        Exit = 65544, /*  */
        /// <summary>
        /// skull symbol
        /// </summary>
        //Skull = 65545, /*  */
        /// <summary>
        /// flag symbol
        /// </summary>
        //Flag = 65546, /*  */
        /// <summary>
        /// camp symbol
        /// </summary>
        //Camp = 65547, /*  */
        /// <summary>
        /// circle with x symbol
        /// </summary>
        //CircleX = 65548, /*  */
        /// <summary>
        /// deer symbol
        /// </summary>
        //Deer = 65549, /*  */
        /// <summary>
        /// first aid symbol
        /// </summary>
        //FirstAid = 65550, /*  */
        /// <summary>
        /// back track symbol
        /// </summary>
        BackTrack = 65551, /*  */



        //Magellan icons

        //Colortrak (19), Tracker (23), GPS 315/320 (24), MAP 410 (25) and Sportrak (37):
        /// <summary>
        /// filled circle
        /// </summary>
        CircleFilled = 16777216,
        /// <summary>
        /// box
        /// </summary>
        Box = 16777217,
        /// <summary>
        /// red buoy
        /// </summary>
        //m_BuoyRed = 16777218,
        /// <summary>
        /// green buoy
        /// </summary>
        //m_BuoyGreen = 16777219,
        /// <summary>
        /// anchor
        /// </summary>
        //m_Anchor = 16777220,
        /// <summary>
        /// rocks
        /// </summary>
        Rocks = 16777221,
        /// <summary>
        /// green daymark
        /// </summary>
        DaymarkGreen = 16777222,
        /// <summary>
        /// red daymark
        /// </summary>
        DaymarkRed = 16777223,
        /// <summary>
        /// bell
        /// </summary>
        //m_Bell = 16777224,
        /// <summary>
        /// danger
        /// </summary>
        //m_Danger = 16777225,
        /// <summary>
        /// diver down
        /// </summary>
        DiverDown = 16777226,
        /// <summary>
        /// fish
        /// </summary>
        //m_Fish = 16777227,
        /// <summary>
        /// house
        /// </summary>
        //m_House = 16777228,
        /// <summary>
        /// mark
        /// </summary>
        Mark = 16777229,
        /// <summary>
        /// car
        /// </summary>
        //m_Car = 16777230,
        /// <summary>
        /// tent
        /// </summary>
        Tent = 16777231,
        /// <summary>
        /// boat
        /// </summary>
        //m_Boat = 16777232,
        /// <summary>
        /// food
        /// </summary>
        Food = 16777233,
        /// <summary>
        /// fuel
        /// </summary>
        //m_Fuel = 16777234,
        /// <summary>
        /// tree
        /// </summary>
        Tree = 16777235,


        //NAV 6000 (18):
        /// <summary>
        /// filled circle
        /// </summary>
        //m_FilledCircle = 16777236,
        /// <summary>
        /// fish
        /// </summary>
        //m_Fish = 16777237,
        /// <summary>
        /// buoy
        /// </summary>
        Buoy = 16777238,
        /// <summary>
        /// light
        /// </summary>
        //m_Light = 16777239,
        /// <summary>
        /// anchor
        /// </summary>
        //m_Anchor = 16777240,
        /// <summary>
        /// flag
        /// </summary>
        //m_Flag = 16777241,
        /// <summary>
        /// red daymark
        /// </summary>
        //m_RedDaymark = 16777242,
        /// <summary>
        /// green daymark
        /// </summary>
        //m_GreenDaymark = 16777243,
        /// <summary>
        /// wreck
        /// </summary>
        //m_Wreck = 16777244,
        /// <summary>
        /// house
        /// </summary>
        //m_House = 16777245,
        /// <summary>
        /// boat
        /// </summary>
        //m_Boat = 16777246,
        /// <summary>
        /// fuel
        /// </summary>
        //m_Fuel = 16777247,
        /// <summary>
        /// danger
        /// </summary>
        //m_Danger = 16777248,
        /// <summary>
        /// diver down
        /// </summary>
        //m_DiverDown = 16777249,
        /// <summary>
        /// food
        /// </summary>
        //m_Food = 16777250,
        /// <summary>
        /// w symbol
        /// </summary>
        W = 16777251,
        /// <summary>
        /// l symbol
        /// </summary>
        L = 16777252,
        /// <summary>
        /// r symbol
        /// </summary>
        R = 16777253,


        //Meridian XL (5):
        /// <summary>
        /// filled circle
        /// </summary>
        //m_filled_circle = 16777254,
        /// <summary>
        /// flag right
        /// </summary>
        FlagRight = 16777255,
        /// <summary>
        /// flag left
        /// </summary>
        FlagLeft = 16777256,
        /// <summary>
        /// diamond
        /// </summary>
        Diamond = 16777257,
        /// <summary>
        /// square
        /// </summary>
        Square = 16777258,
        /// <summary>
        /// filled square
        /// </summary>
        SquareFilled = 16777259,

        /*
        /// <summary>
        /// anchor
        /// </summary>
        m_Anchor = 16777260,
         */

        /// <summary>
        /// banner
        /// </summary>
        Banner = 16777261,

        /*
        /// <summary>
        /// fish
        /// </summary>
        //m_Fish = 16777262,
        */

        /// <summary>
        /// crosshair
        /// </summary>
        Crosshair = 16777263,

        /*
        //Trailblazer XL (7):
        /// <summary>
        /// filled circle
        /// </summary>
        m_FilledCircle = 16777264,
        /// <summary>
        /// flag right
        /// </summary>
        m_FlagRight = 16777265,
        /// <summary>
        /// flag left
        /// </summary>
        m_FlagLeft = 16777266,
        /// <summary>
        /// diamond
        /// </summary>
        m_Diamond = 16777267,
        /// <summary>
        /// square
        /// </summary>
        //m_Square = 16777268,
        /// <summary>
        /// filled square
        /// </summary>
        m_FilledSquare = 16777269,
        /// <summary>
        /// anchor
        /// </summary>
        m_Anchor = 16777270,
        /// <summary>
        /// banner
        /// </summary>
        m_Banner = 16777271,
        /// <summary>
        /// fish
        /// </summary>
        //m_Fish = 16777272,
        /// <summary>
        /// crosshair
        /// </summary>
        m_Crosshair = 16777273,
        /// <summary>
        /// car
        /// </summary>
        m_Car = 16777274,
        /// <summary>
        /// house
        /// </summary>
        m_House = 16777275,
        */

        /// <summary>
        /// wine glass
        /// </summary>
        WineGlass = 16777276,
        /// <summary>
        /// mountain
        /// </summary>
        Mountain = 16777277,
        /// <summary>
        /// sign
        /// </summary>
        Sign = 16777278,
        
        /*
        /// <summary>
        /// tree
        /// </summary>
        //m_Tree = 16777279,
        */

        /// <summary>
        /// water
        /// </summary>
        Water = 16777280,
        /// <summary>
        /// airplane
        /// </summary>
        Airplane = 16777281,
        /// <summary>
        /// deers head
        /// </summary>
        DeersHead = 16777282,


        //MAP 330 (30), ProMark 2 (35), Meridian New (33), Sportrak Map/Pro (36):
        /// <summary>
        /// crossed square
        /// </summary>
        SquareCrossed = 16777283,
        /// <summary>
        /// box
        /// </summary>
        //m_Box = 16777284,
        /// <summary>
        /// house
        /// </summary>
        //m_House = 16777285,
        /// <summary>
        /// aerial
        /// </summary>
        Aerial = 16777286,
        /// <summary>
        /// airport
        /// </summary>
        //m_Airport = 16777287,
        /// <summary>
        /// amusement park
        /// </summary>
        //m_AmusementPark = 16777288,
        /// <summary>
        /// ATM
        /// </summary>
        Atm = 16777289,
        /// <summary>
        /// auto repair
        /// </summary>
        AutoRepair = 16777290,
        /// <summary>
        /// boating
        /// </summary>
        Boating = 16777291,
        /// <summary>
        /// camping
        /// </summary>
        Camping = 16777292,
        /// <summary>
        /// exit ramp
        /// </summary>
        ExitRamp = 16777293,
        /// <summary>
        /// first aid
        /// </summary>
        //m_FirstAid = 16777294,
        /// <summary>
        /// navigation aid
        /// </summary>
        NavAid = 16777295,
        /// <summary>
        /// buoy
        /// </summary>
        //m_Buoy = 16777296,
        /// <summary>
        /// fuel
        /// </summary>
        //m_Fuel = 16777297,
        /// <summary>
        /// garden
        /// </summary>
        Garden = 16777298,
        /// <summary>
        /// golf
        /// </summary>
        //m_Golf = 16777299,
        /// <summary>
        /// hotel
        /// </summary>
        Hotel = 16777300,
        /// <summary>
        /// hunting and fishing
        /// </summary>
        HuntingFishing = 16777301,
        /// <summary>
        /// large city
        /// </summary>
        //m_LargeCity = 16777302,
        /// <summary>
        /// light house
        /// </summary>
        Lighthouse = 16777303,
        /// <summary>
        /// major city
        /// </summary>
        MajorCity = 16777304,
        /// <summary>
        /// marina
        /// </summary>
        //m_Marina = 16777305,
        /// <summary>
        /// medium city
        /// </summary>
        //m_MediumCity = 16777306,
        /// <summary>
        /// museum
        /// </summary>
        //m_Museum = 16777307,
        /// <summary>
        /// obstruction
        /// </summary>
        Obstruction = 16777308,
        /// <summary>
        /// park
        /// </summary>
        //m_Park = 16777309,
        /// <summary>
        /// resort
        /// </summary>
        Resort = 16777310,
        /// <summary>
        /// restaurant
        /// </summary>
        Restaurant = 16777311,
        /// <summary>
        /// rock
        /// </summary>
        Rock = 16777312,
        /// <summary>
        /// scuba diving
        /// </summary>
        Scuba = 16777313,
        /// <summary>
        /// RV service
        /// </summary>
        RvService = 16777314,
        /// <summary>
        /// shooting
        /// </summary>
        Shooting = 16777315,
        /// <summary>
        /// sight seeing
        /// </summary>
        SightSeeing = 16777316,
        /// <summary>
        /// small city
        /// </summary>
        //m_SmallCity = 16777317,
        /// <summary>
        /// sounding
        /// </summary>
        Sounding = 16777318,
        /// <summary>
        /// 
        /// </summary>
        SportsArena = 16777319,
        /// <summary>
        /// tourist information
        /// </summary>
        TouristInfo = 16777320,
        /// <summary>
        /// truck service
        /// </summary>
        TruckService = 16777321,
        /// <summary>
        /// winery
        /// </summary>
        Winery = 16777322,
        /// <summary>
        /// wreck
        /// </summary>
        //m_Wreck = 16777323,
        /// <summary>
        /// zoo
        /// </summary>
        //m_Zoo = 16777324,
        /// <summary>
        /// unknown
        /// </summary>
        Unknown = -1
    }

    //Magellan Symbol Sets
    // Colortrak (19), Tracker (23), GPS 315/320 (24), MAP 410 (25) and Sportrak (37):

    //a filled circle 
    //b box 
    //c red buoy 
    //d green buoy 
    //e anchor 
    //f rocks 
    //g green daymark 
    //h red daymark 
    //i bell 
    //j danger 
    //k diver down 
    //l fish 
    //m house 
    //n mark 
    //o car 
    //p tent 
    //q boat 
    //r food 
    //s fuel 
    //t tree 



    //NAV 6000 (18):

    //Value Icon 
    //a filled circle 
    //b fish 
    //c buoy 
    //d light 
    //e anchor 
    //f flag 
    //g red daymark 
    //h green daymark 
    //i wreck 
    //j house 
    //k boat 
    //l fuel 
    //m danger 
    //n diver down 
    //o food 
    //p w 
    //q l 
    //r r 

    //Meridian XL (5):

    //Value Icon 
    //a filled circle 
    //b flag right 
    //c flag left 
    //d diamond 
    //e square 
    //f filled square 
    //g anchor 
    //h banner 
    //i fish 
    //j crosshair 


    //Trailblazer XL (7):

    //Value Icon 
    //a filled circle 
    //b flag right 
    //c flag left 
    //d diamond 
    //e square 
    //f filled square 
    //g anchor 
    //h banner 
    //i fish 
    //j crosshair 
    //k car 
    //l house 
    //m wine glass 
    //n mountain 
    //o sign 
    //p tree 
    //q water 
    //r airplane 
    //s deer�s head 



    //MAP 330 (30), ProMark 2 (35), Meridian New (33), Sportrak Map/Pro (36):

    //Value Icon 
    //a crossed square 
    //b box 
    //c house 
    //d aerial 
    //e airport 
    //f amusement park 
    //g ATM 
    //h auto repair 
    //i boating 
    //j camping 
    //k exit ramp 
    //l first aid 
    //m nav aid 
    //n buoy 
    //o fuel 
    //p garden 
    //q golf 
    //r hotel 
    //s hunting/fishing 
    //t large city 
    //u lighthouse 
    //v major city 
    //w marina 
    //x medium city 
    //y museum 
    //z obstruction 
    //aa park 
    //ab resort 
    //ac restaurant 
    //ad rock 
    //ae scuba 
    //af RV service 
    //ag shooting 
    //ah sight seeing 
    //ai small city 
    //aj sounding 
    //ak sports arena 
    //al tourist info 
    //am truck service 
    //an winery 
    //ao wreck 
    //ap zoo 

}
