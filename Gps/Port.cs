namespace Waymex.Gps
{
	/// <summary>
	/// Communications Port Enumerator.
	/// </summary>
	public enum Port
	{
		/// <summary>USB Port</summary>
		Usb = 0,
		/// <summary>Serial Port 1</summary>
		Serial01 = 1,
		/// <summary>Serial Port 2</summary>
		Serial02 = 2,
		/// <summary>Serial Port 3</summary>
		Serial03 = 3,
		/// <summary>Serial Port 4</summary>
		Serial04 = 4,
		/// <summary>Serial Port 5</summary>
		Serial05 = 5,
		/// <summary>Serial Port 6</summary>
		Serial06 = 6,
		/// <summary>Serial Port 7</summary>
		Serial07 = 7,
		/// <summary>Serial Port 8</summary>
		Serial08 = 8,
		/// <summary>Serial Port 9</summary>
		Serial09 = 9,
		/// <summary>Serial Port 10</summary>
		Serial10 = 10,
		/// <summary>Serial Port 11</summary>
		Serial11 = 11,
		/// <summary>Serial Port 12</summary>
		Serial12 = 12,
		/// <summary>Serial Port 13</summary>
		Serial13 = 13,
		/// <summary>Serial Port 14</summary>
		Serial14 = 14,
		/// <summary>Serial Port 15</summary>
		Serial15 = 15,
		/// <summary>Serial Port 16</summary>
		Serial16 = 16,
		/// <summary>Serial Port 17</summary>
		Serial17 = 17,
		/// <summary>Serial Port 18</summary>
		Serial18 = 18,
		/// <summary>Serial Port 19</summary>
		Serial19 = 19,
		/// <summary>Serial Port 20</summary>
		Serial20 = 20,
		/// <summary>Serial Port 21</summary>
		Serial21 = 21,
		/// <summary>Serial Port 22</summary>
		Serial22 = 22,
		/// <summary>Serial Port 23</summary>
		Serial23 = 23,
		/// <summary>Serial Port 24</summary>
		Serial24 = 24,
		/// <summary>Serial Port 25</summary>
		Serial25 = 25,
		/// <summary>Serial Port 26</summary>
		Serial26 = 26,
		/// <summary>Serial Port 27</summary>
		Serial27 = 27,
		/// <summary>Serial Port 28</summary>
		Serial28 = 28,
		/// <summary>Serial Port 29</summary>
		Serial29 = 29,
		/// <summary>Serial Port 30</summary>
		Serial30 = 30,
		/// <summary>Serial Port 31</summary>
		Serial31 = 31,
		/// <summary>Serial Port 32</summary>
		Serial32 = 32
	}
    /// <summary>
    /// Baud Rate Enumerator.
    /// </summary>
	public enum BaudRate
	{
        ///// <summary>
        ///// Baud Rate Enumerator, auto detect.
        ///// </summary>
        //BaudAuto = 0,
        /// <summary>
        /// Baud Rate Enumerator, 1200 Baud.
        /// </summary>
		Baud1200 = 1200,
        /// <summary>
        /// Baud Rate Enumerator, 1200 Baud.
        /// </summary>
        Baud4800 = 4800,
        /// <summary>
        /// Baud Rate Enumerator, 4800 Baud.
        /// </summary>
        Baud9600 = 9600,
        /// <summary>
        /// Baud Rate Enumerator, 9600 Baud.
        /// </summary>
        Baud19200 = 19200,
        /// <summary>
        /// Baud Rate Enumerator, 57600 Baud.
        /// </summary>
        Baud57600 = 57600,
        /// <summary>
        /// Baud Rate Enumerator, 115200 Baud.
        /// </summary>
        Baud115200 = 115200
	}

}