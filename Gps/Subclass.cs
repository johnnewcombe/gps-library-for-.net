using System;
using System.Collections;
using System.Text;

namespace Waymex.Gps
{
    //IEnumerable removed as the class has an indexer and does need an enumerater as well
    //also FxCop is happier as I do not want to call it a ByteCollection or SubclassCollection.
    /// <summary>
    /// Subclass data class. This class is used to store data for map dartabase Waypoints.
    /// </summary>
    public class Subclass //: IEnumerable
    {
        private ArrayList dataBytes;
         
        /// <summary>
         /// Default constructor for the Subclass.
         /// </summary>
         public Subclass()
             : base()
         {
             byte[] dataArray = new byte[] { 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
             PopulateClass(dataArray);
         }

        /// <summary>
        /// Internal constructor that accepts a byte array.
        /// </summary>
        /// <param name="dataArray"></param>
        internal Subclass(byte[] dataArray)
            : base()
        {
            PopulateClass(dataArray);
        }
        private void PopulateClass(byte[] dataArray)
        {
            dataBytes = new ArrayList();

            if (dataArray != null)
            {
                foreach (byte dataByte in dataArray)
                {
                    Add(dataByte);
                }
            }
        }
        /// <summary>
        /// This method adds a byte to the end of the Subclass. 
        /// </summary>
        public void Add(byte data)
        {
            try
            {
                dataBytes.Add(data);
            }
            catch (Exception e)
            {
                throw new InvalidDataException(e.Message, e);
            }
        }
        /// <summary>
        /// Returns the number of Subclass bytes. 
        /// </summary>
        public int Length
        {
            get
            {
                try
                {
                    return dataBytes.Count;
                }
                catch (Exception e)
                {
                    throw new InvalidDataException(e.Message, e);
                }
            }
        }

        //default indexer property
        /// <summary>
        /// Can be used to refer to a member of the Subclass by ordinal reference.
        /// This Property is the default indexer in C#.
        /// </summary>
        public byte this[int index]
        {
            get
            {
                return (byte)dataBytes[index];
            }

        }
        /// <summary>
        /// Returns the string as a hexadecimel, space delimited string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string sTemp = "";
            string sMessage = "";

            foreach (byte dataByte in this.ToByteArray())
            {

                sTemp = dataByte.ToString("X"); //hex returns a string representing the passed number

                if (sTemp.Length < 2)
                {
                    sTemp = string.Concat("0", sTemp);
                }

                sMessage = string.Concat(sMessage, " ", sTemp);
            }

            return sMessage.Trim();

        }
        /// <summary>
        /// Returns the SubClass as an array of bytes.
        /// </summary>
        /// <returns>Array of bytes.</returns>
        public byte[] ToByteArray()
        {
            //int length = dataBytes.Count;

            //byte[] result = new byte[length];

            //for (int i = 0; i < length; i++)
            //{
            //    result[i] = (byte)dataBytes[i];

            //}
            //return result;
            return (byte[])dataBytes.ToArray(typeof( byte ));
        }
    }
}
