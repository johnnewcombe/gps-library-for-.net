using System;
using System.Xml;
using System.Globalization;


namespace Waymex.Gps
{
	/// <summary>
	/// This object is typically used to store the current position. 
	/// It is the object returned by the GetPosition method of the 
    /// Device object.
	/// </summary>
    /// <remarks>
    /// <para>Garmin Devices Only.</para>
    /// </remarks>
	public class Position : DataObjectBase
	{
        private double m_latitude;
        private double m_longitude;

		/// <summary>
		/// This property returns the current Latitude in degrees as reported
		/// by the GPS device.
		/// </summary>
		public double Latitude
        {
            get
            {
                return m_latitude;
            }
            set
            {
                m_latitude = value;
            }
        }
		/// <summary>
		/// This property returns the current Longitude in degrees as reported
		/// by the GPS device.
		/// </summary>
		public double Longitude
        {
            get
            {
                return m_longitude;
            }
            set
            {
                m_longitude = value;
            }
        }

		private const string XML_ROOT = "gpswaypoint";
		private const string XML_LATITUDE = "latitude";
		private const string XML_LONGITUDE = "longitude";

		private int m_hashCode = -1;
		
		/// <summary>
		/// Returns an XML representation of the object.
		/// </summary>
		public string ToXml()
		{
			XmlDocument objXMLDOM = new XmlDocument();

			XmlElement objRootNode = null;
			
			try
			{
				//create the root node
				objRootNode = objXMLDOM.CreateElement(XML_ROOT);
			
				//append the root node to the document
				objXMLDOM.AppendChild(objRootNode);

				//create the child nodes
				AddXMLNode(objRootNode, XML_LATITUDE, Latitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_LONGITUDE, Longitude.ToString(CultureInfo.InvariantCulture), false);

			}
			catch(NullReferenceException  e)
			{
				throw new XmlException(e.Message, e);
			}

			return objXMLDOM.OuterXml;
		}
        /// <summary>
        /// Overridden method. Returns true of the values of each
        /// of the properties are equal in value.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>boolean</returns>
        public override bool Equals(object obj)
		{
			try
			{
				//check the type first
				if(obj.GetType() != this.GetType() )
					return false;

				//cast it
				Position objPosition = (Position)obj;

				if( objPosition.Latitude != Latitude ) return false; 
				if( objPosition.Longitude != Longitude ) return false;

			}
			catch
			{
				throw;
			}
			finally
			{
			}
			//if we get here all must be the same.
			return true;
		}
        /// <summary>
        /// Overridden function. Retrieves a value that indicates the hash code value for the object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{

			//this value will be used for the overriden GetHashCode function and should
			//ensure that two object that are the same return the same Hash Code.
			//the hash code has to be imutable to is stored in a member variable.
			if( m_hashCode <= 0 )
				m_hashCode = Latitude.GetHashCode() ^ Longitude.GetHashCode();

			return m_hashCode;
		}
	}
}
