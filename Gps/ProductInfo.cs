using System;
using System.Globalization;

namespace Waymex.Gps
{
	/// <summary>
	/// This object contains details about the connected GPS device.
	/// </summary>
 
	public class ProductInfo : DataObjectBase
	{
		private int m_hashCode = -1;

        private string m_softwareVersion;
        private int m_productID;
        private string m_productDescription;
        private bool m_protocolCapability;
        private string m_protocolTable;
        private string m_productName;
        private int m_symbolSet = -1;   //set defaults as Megellan devices dont use them
        private int m_symbolMax = 0;
        //private long m_unitId = 0;


        ///// <summary>
        ///// Returns a numeric value representing the Unit ID of the connected GPS device. This may not be supported by all devices.
        ///// </summary>
        //public long UnitId
        //{
        //    get
        //    {
        //        return m_unitId;
        //    }
        //    set
        //    {
        //        m_unitId = value;
        //    }
        //}
        /// <summary>
		/// Returns an Integer representing the version of software used in the connected GPS device. 
		/// </summary>
		//public int SoftwareVersion;
		public string SoftwareVersion
        {
            get
            {
                return m_softwareVersion;
            }
            set
            {
                m_softwareVersion = value;
            }
        }
		/// <summary>
		/// Returns an Integer representing the Product ID of the connected GPS device.
		/// </summary>
		public int ProductId
        {
            get
            {
                return m_productID;
            }
            set
            {
                m_productID = value;
            }
        }
		/// <summary>
		/// Returns the product description of the connected GPS device as a String.
		/// </summary>
		public string ProductDescription
        {
            get
            {
                return m_productDescription;
            }
            set
            {
                m_productDescription = value;
            }
        }
		/// <summary>
		/// Returns True if the connected GPS device supports the Protocol Capabilities Protocol.
		/// </summary>
		public bool ProtocolCapability
        {
            get
            {
                return m_protocolCapability;
            }
            set
            {
                m_protocolCapability = value;
            }
        }
		/// <summary>
		/// Returns a CR/LF delimited string containing the protocol information for the connected GPS device. 
		/// </summary>
		public string ProtocolTable
        {
            get
            {
                return m_protocolTable;
            }
            set
            {
                m_protocolTable = value;
            }
        }
		/// <summary>
		/// Returns the Name of the connected GPS device.
		/// </summary>
		public string ProductName
        {
            get
            {
                return m_productName;
            }
            set
            {
                m_productName = value;
            }
        }
        /// <summary>
        /// Returns the symbol set in use for the connected device. Only applicable for Garmin devices.
        /// Other devices will return -1.
        /// </summary>
        public int SymbolSet
        {
            get
            {
                return m_symbolSet;
            }
            set
            {
                m_symbolSet = value;
            }
        }
        ///// <summary>
        ///// Returns the maximum value that can be used for a symbol with this device. Typically
        ///// the value will be either 255 or 65535. 
        ///// </summary>
        ///// <remarks>
        ///// If a value of 255 is indicated and symbol set 1 is in use (see SymbolSet property) then 
        ///// only the first 256 symbols would be available for use with the device. In practice a much
        ///// smaller subset of symbols would typically be used. There is no way to identify the actual
        ///// symbols supported by the device.
        ///// </remarks>
        //public int SymbolMaxValue
        //{
        //    get
        //    {
        //        return m_symbolMax;
        //    }
        //    set
        //    {
        //        m_symbolMax = value;
        //    }
        //}
		/// <summary>
		/// Returns the software version as an integer or zero if unknown.
		/// </summary>
		/// <returns></returns>
		internal int GetSoftwareVersionAsInteger()
		{
			try
			{
                return Convert.ToInt32(SoftwareVersion, CultureInfo.InvariantCulture);
			}
			catch
			{
                throw;
			}
		}
        /// <summary>
        /// Overridden method. Returns true of the values of each
        /// of the properties are equal in value.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>boolean</returns>
        public override bool Equals(object obj)
		{
			try
			{
				//check the type first
				if(obj.GetType() != this.GetType() )
					return false;

				//cast it
				ProductInfo objProductInfo = (ProductInfo)obj;

				if( objProductInfo.ProductDescription != ProductDescription ) return false; 
				if( objProductInfo.ProductId != ProductId ) return false;
				if( objProductInfo.ProductName != ProductName ) return false;
				if( objProductInfo.ProtocolCapability != ProtocolCapability ) return false;
				if( objProductInfo.ProtocolTable != ProtocolTable ) return false;
				if( objProductInfo.SoftwareVersion != SoftwareVersion ) return false;

			}
			catch
			{
                throw;
			}
			finally
			{
			}
			//if we get here all must be the same.
			return true;
		}
        /// <summary>
        /// Overridden function. Retrieves a value that indicates the hash code value for the object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{

			//this value will be used for the overriden GetHashCode function and should
			//ensure that two object that are the same return the same Hash Code.
			//the hash code has to be imutable to is stored in a member variable.
			if( m_hashCode <= 0 )
				m_hashCode = ProductDescription.GetHashCode() ^ ProductName.GetHashCode();

			return m_hashCode;
		}
	}

}


