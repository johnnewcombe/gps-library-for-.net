namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// Link Protocol Enumerator.
	/// </summary>
	public enum LinkProtocol
	{
		/// <summary>0</summary>
		L000 = 0,
		/// <summary>1</summary>
		L001 = 1,
		/// <summary>2</summary>
		L002 = 2
	}
}	