namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// Waypoint type Enumerator.
	/// </summary>
	public enum WaypointType
	{
		/// <summary>0</summary>
		Waypoint = 0,
		/// <summary>1</summary>
		Proximity = 1,
		/// <summary>2</summary>
		Route = 2
	}
}