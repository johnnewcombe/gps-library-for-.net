namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// Device Command ID Enumerator.
	/// </summary>
	internal enum CommandID
	{
		/// <summary>0</summary>
		AbortTransfer = 0,
		/// <summary>1</summary>
		TransferAlm = 1,
		/// <summary>2</summary>
		TransferPos = 2,
		/// <summary>3</summary>
		TransferPrx = 3,
		/// <summary>4</summary>
		TransferRte = 4,
		/// <summary>5</summary>
		TransferTime = 5,
		/// <summary>6</summary>
		TransferTrk = 6,
		/// <summary>7</summary>
		TransferWpt = 7,
		/// <summary>8</summary>
		TurnOffPower = 8,
		/// <summary>9</summary>
		StartPVTData = 9, //49
		/// <summary>10</summary>
		StopPVTData = 10, //50
		/// <summary>11</summary>
		FlightBookTransfer = 11, //92
		/// <summary>12</summary>
		TransferLaps = 12, //117
		/// <summary>13</summary>
		TransferWptCat = 13, //121
		/// <summary>14</summary>
		ReceiverMeasurementOn = 14, //110
		/// <summary>15</summary>
		ReceiverMeasurementOff = 15, //111
        /// <summary>26</summary>
        TransferUnitId = 16, //14
	}
}

