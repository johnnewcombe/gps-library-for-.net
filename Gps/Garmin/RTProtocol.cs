namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// Route Protocol Enumerator.
	/// </summary>
	public enum RTProtocol
	{
		/// <summary>0</summary>
		A200 = 0,
		/// <summary>1</summary>
		A201 = 1
	}
}