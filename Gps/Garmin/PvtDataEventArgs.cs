using System;
using System.Collections.Generic;
using System.Text;

namespace Waymex.Gps.Garmin
{
    /// <summary>
    /// PvtDataEventArgs is the class containing event data. 
    /// </summary>
    public class PvtDataEventArgs : EventArgs
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public PvtDataEventArgs(string pvtData)
        {
            //TODO: expand to include the parsed parameters of the PVT
            m_pvtData = pvtData;
        }
        private string m_pvtData;

        /// <summary>
        /// Returns the PVTData as a semi-colon delimited string.
        /// </summary>
        public string PvtData
        {
            get
            {
                return m_pvtData;
            }
        }
    }
}
