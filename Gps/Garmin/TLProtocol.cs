namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// Tracklog Protocol Enumerator.
	/// </summary>
	public enum TLProtocol
	{
		/// <summary>0</summary>
		A300 = 0,
		/// <summary>1</summary>
		A301 = 1
	}
}