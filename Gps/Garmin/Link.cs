using System;
using Microsoft.VisualBasic;
using System.Globalization;
//using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Waymex.Diagnostics;


namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// This object contains the link layer functionality. 
	/// It is responsible for the assembly and disassembly of packets, 
	/// checksum calculations, DLE stuffing and ACK/NAK control. 
	/// Normally the Link object would only be accessed through 
	/// the Link property of the Interface object. 
	/// The methods and properties shown below are exposed to provide 
	/// direct access to GPS devices in order that additional protocols 
	/// can be added by client software if required.
	/// </summary>
	internal class Link : System.IDisposable
	{
		//---------------------------------------------------------------------------
		//   DESCRIPTION:    This is the Link layer implementation of the Garmin GPS
		//                   Protocol. The Phsical Layer is controlled by the
		//                   SerialPort Class which is, in turn, controlled
		//                   from this component. This class is created and 
		//                   controlled from the application layer (GPSInterface.cls).
		//   HISTORY:
		//                   History Info is shown on the declarations section
		//                   of the apllication layer (GPSInterface).
		//
		//---------------------------------------------------------------------------
		
		//error constants
		
		private const string LOG_LINE = "------------------------------------------------------------------";

		//general errors
		private const string ERR_MSG_1000 = "Port not Initialised";

		//packet errors
		private const string ERR_MSG_2001 = "Bad Checksum byte detected during disassembly";
		private const string ERR_MSG_2002 = "Bad Size byte detected during disassembly";
		private const string ERR_MSG_2003 = "Bad DLE byte detected during disassembly";
		private const string ERR_MSG_2004 = "Packet length received exceeds maximum length permissible";
		private const string ERR_MSG_2005 = "No Acknowledgement received from the GPS device";
		private const string ERR_MSG_2006 = "Receive Timeout";
		private const string ERR_MSG_2007 = "The argument must be greater than zero";
		private const string ERR_MSG_2008 = "The argument length must be greater than zero";

		//constants for the packet
		private const int DLE = 0x10;
		private const int ETX = 0x3;
		private const byte ACK = 6;
		private const byte NAK = 21;
		
		//misc constants
		private const int NOSUPPORT = -1;
		private const double ACK_TIMEOUT = 2.0;
		private const int RDATA_TIMEOUT = 1;

		//Pid Data table Data
		private short[] mPidData = new short[27];

		//used to store error details in error handlers

		private bool mbPortInitialised; //status of port
		private LinkProtocol miProtocol; //link protocol to be used
		private int miPortTimeout = 10; //sets the port timeout, default is 10 seconds

		private SerialLink m_CommPort;
		private bool mblnDisposed; //allows dispose to be called multiple times

		/// <summary>
		/// Constructor.
		/// </summary>
		internal Link()
		{
			SetPidValues(LinkProtocol.L001);
			m_CommPort = new SerialLink();
			m_CommPort.Port = 1;
		}
		//Dispose(disposing As Boolean) executes in two distinct scenarios.
		//If disposing is true, the method has been called directly 
		//or indirectly by a user's code. Managed and unmanaged resources 
		//can be disposed.
		//If disposing equals false, the method has been called by the runtime
		//from inside the finalizer and you should not reference other    
		//objects. Only unmanaged resources can be disposed.

		/// <summary>
		/// Releases all resources and destroys the object.
		/// </summary>
		public virtual void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		/// <summary>
		/// Releases all resources and destroys the object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			//Check to see if Dispose has already been called.
			if (!mblnDisposed)
			{
				//If disposing equals true, dispose all managed 
				//and unmanaged resources.
				if(disposing)
				{
					//Dispose managed resources.
					m_CommPort.Dispose();
					m_CommPort = null;
				}
				//Release unmanaged resources. If disposing is false,
				//only the following code is executed.      
				
				//Note that this is not thread safe.
				//Another thread could start disposing the object
				//after the managed resources are disposed,
				//but before the disposed flag is set to true.
			}

			mblnDisposed = true;
		}
		//		protected override void Finalize()
		//		{
		//			//Simply call Dispose(False).
		//			Dispose(false);
		//			//MyBase.Finalize();
		//		}
		
		private byte[] DisassemblePacket(byte[] Packet)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    Accepts a packet in assembled format and dissasembles
			//                   it. This function handles the size, DLE Stuffing and
			//                   checksum bytes and raises errors as required.
			//
			//   PARAMETERS:
			//                   Packet()    Array of bytes containing the packet.
			//
			//   RETURNS:
			//                   Byte()      Array of bytes containing the dissasembled
			//                               packet. First byte is the Pid remaining
			//                               bytes are the packet data.
			//
			//---------------------------------------------------------------------------

			int f = 0;
			bool DLEFound = false;
			byte[] OutData = null;
			byte[] TempData = null;
			int iChecksum = 0;
			int iSize = 0;
			//bool bBadPacket = false;
			int iElement = 0;
			int iLastByte = 0;

			//transfer the data into the tempdata array removing stuffed DLE's
			try
			{
				if(Packet != null)
				{
					iLastByte = Packet.GetUpperBound(0);

					for(f = 0; f <= iLastByte;f++)
					{

						if(!DLEFound)
						{

							//populate data including first DLE
							TempData = ReDimPreserve(TempData, iElement + 1);
							TempData[iElement] = Packet[f];

							//check for a DLE from byte 2 up to and including the checksum,
							//record the fact if one has been found
							
							if(((f > 1) && (f < iLastByte - 1)) && (Packet[f] == DLE))
								DLEFound = true;

							//this is used to help create the TempData array
							iElement = iElement + 1;
						}
						else
						{
							//reset the DLE found flag ready for the next byte
							DLEFound = false;
							//
							//previous char was a DLE so this should be one also (the stuffed one)
							if(Packet[f] != DLE)
								throw new DleException(ERR_MSG_2003);
						}						
					}
					//transfer the data into the output array leaving the first byte of the array empty
					for(f = 0; f <= TempData.GetUpperBound(0) - 6; f++)
					{
						//populate data
						OutData = ReDimPreserve(OutData, f + 2);
						OutData[f + 1] = TempData[f + 3];
					}

					//add the pid to the first byte of the array
					OutData[0] = TempData[1];

					//we now have two arrays Packet may contain stuffed DLE's so we calculate the checksum
					//from the data in TempData
					//calculate the checksum
					for(f = 1; f <= TempData.GetUpperBound(0) - 3; f++)
					{
						iChecksum = iChecksum + TempData[f];
					}

					//perform the two's complement
					iChecksum = (256 - iChecksum) & 0xFF;

					//Debug.WriteLine(String.Concat("Checksum: ", iChecksum.ToString(CultureInfo.InvariantCulture)));

					//compare it to the byte in the packet
					if(Packet[Packet.GetUpperBound(0) - 2] != iChecksum)
						throw new ChecksumException(string.Concat(ERR_MSG_2001, " (" , Packet[Packet.GetUpperBound(0) - 2].ToString(CultureInfo.InvariantCulture)," received, " , iChecksum.ToString(CultureInfo.InvariantCulture) , " calculated)"));

					//determine the size, length of packet - i.e. no of DATA bytes in OutData
					//(i.e no of bytes in OutData - 1 for the Pid which is the same as UBound(OutData))
					iSize = OutData.GetUpperBound(0);

					//compare it to the byte in the packet
					if(Packet[2] != iSize)
						throw new PacketSizeException(string.Concat(ERR_MSG_2002, " (", Packet[2].ToString(CultureInfo.InvariantCulture), " expected, ", iSize.ToString(CultureInfo.InvariantCulture),  " received)"));
					return OutData;
				}
				else
				{
					return null;
				}			
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private bool IsACK(byte[] Packet, byte ExpectedPid)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    Determines if the packet passed is a ACK packet.
			//
			//
			//
			//   PARAMETERS:
			//                   Packet()        Array of bytes containing the packet.
			//                   Expected Pid    Byte containing the Pid that is expected
			//                                   in the ACK packet. ACK packets contain
			//                                   the Pid of the packet they are in
			//                                   response to.
			//   RETURNS:
			//                   Boolean         True if packet is an ACK, false if not.
			//
			//---------------------------------------------------------------------------

			try
			{
				if(Packet != null)
				{
					if(Packet[0] == ACK)
					{
						//compare the data bytes with the expected pid
						if(Packet[1] == ExpectedPid)
							return true;
					}				
				}			
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return false;
		}

		private bool IsNAK(byte[] Packet, byte ExpectedPid)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    Determines if the packet passed is a NAK packet.
			//
			//
			//
			//   PARAMETERS:
			//                   Packet()        Array of bytes containing the packet.
			//                   Expected Pid    Byte containing the Pid that is expected
			//                                   in the NAK packet. NAK packets contain
			//                                   the Pid of the packet they are in
			//                                   response to.
			//   RETURNS:
			//                   Boolean         True if packet is an NAK, false if not.
			//
			//---------------------------------------------------------------------------

			//to be re-written not required in current version
			return false;
		}

		private ProcessId LookupPid(byte bPid) 
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    This function converts a Pid to the enumerated
			//                   equivelent. Internally (i.e. when passing Pid's between
			//                   application layer and link layer Pids are represented as
			//                   enumerated integer. This is then converted to the actual
			//                   Pid value in the link layer depending upon the protocol
			//                   property. This function does the reverse by returning
			//                   the integer value of the Pid based on the actual Pid
			//                   value passed and the protocol curently set.
			//
			//   PARAMETERS:
			//                   bPid        Byte containing actual Pid
			//
			//   RETURNS:
			//                   ProcessId   Enumerated integer representing the internal
			//                               protocol independent Pid value. If the
			//                               Pid cannot be found, ProcessId.Null
			//								 is returned
			//
			//---------------------------------------------------------------------------

			//process array should always be populated with valid data
			int f = 0;
			//short iPid = 0;
			
			ProcessId nResult = ProcessId.Null;

			try
			{
				//run through module level array looking for first occurence of actual Pid
				for(f = 0; f <= mPidData.GetUpperBound(0);f++)
				{
                    if (bPid == 26)
                    {
                    }

					if(mPidData[f] == (short)bPid)
					{
						nResult = (ProcessId)f;
						break;
					}
					else
					{
						nResult = ProcessId.Null;
					}				
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return nResult;
		}
		internal void ClearBuffer()
		{
			m_CommPort.ClearBuffer();
		}
		/// <summary>
		/// This method receives an application layer packet as a GpsPidData data type. 
		/// When the GPS device is accessed via the interface object, this method will be 
		/// invoked automatically as required. It is exposed to allow direct access to 
		/// the connected GPS device so that additional application protocols can be 
		/// implemented by client applications. See the Garmin GPS Specification for 
		/// further explanation of Process ID�s etc.
		/// </summary>
		internal GpsPidData ReceiveData()
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    This is the main Receive function called from the
			//                   application layer (GPSInterface). This function
			//                   handles ACK and NAK responses to the GPS.
			//
			//
			//   PARAMETERS:
			//                   <none>
			//
			//   RETURNS:
			//                   PidData     User type consisting of Process ID and Byte
			//                               data. Note, The Pid returned is the
			//                               Enumerated Pid value. In other words the
			//                               Pid is NOT protocol dependant.
			//
			//---------------------------------------------------------------------------
			//
			byte[] Packet;
			double dblStart = 0;
			bool bTimeOut = false;
			short f = 0;
			//string sPacket = "";
			//string sData = "";
			GpsPidData pidReturn;

			//initialise the return value
			pidReturn.Pid = ProcessId.Null;
			pidReturn.ByteData = null;

			try
			{
				//set the return value to null
				pidReturn.Pid = ProcessId.Null;

				//record the start time
				dblStart = Timer();

				while(!bTimeOut)
				{
					System.Windows.Forms.Application.DoEvents();

					//get a packet if it is available
					Packet = m_CommPort.ReceivePacket();

					//Debug.WriteLine("Packet Length Received = " + Packet.Length);

					if(Packet != null && Packet.Length > 0)
					{
						//create the log message to log after the loop has ended
						TraceLog.WriteMessage(string.Concat("Receive Packet  : ", ByteToHex(Packet)), TraceLevel.Verbose, true);
						//
						//dissasemble the packet
						Packet = DisassemblePacket(Packet);
						//
						//create the log message to log after the loop has ended
						//sData = string.Concat("Receive Data    : ", WMX.Utils.ByteToHex(Packet));

						//if we find a packet then stop looking
						if(Packet.Length > 0)
						{
							TraceLog.WriteMessage(string.Concat("Receive Data    : ", ByteToHex(Packet), " (", ByteToAsc(Packet), ")"), TraceLevel.Verbose, true);

							//if the packet is an ACK or NAK then ignore. We dont use the IsACK and
							//IsNAK functions as they are designed to check for specific types of
							//ACKs and NAKs
							if(Packet[0] != ACK && Packet[0] != NAK)
							{
								//Debug.WriteLine("ReceiveData(): Pid = " & CStr(Packet(0)))

								//transfer the received data to the return data type
								//this transfers the protocol independant pid back rather than the actual
								//protocol dependant one.
								pidReturn.Pid = LookupPid(Packet[0]);
								for(f = 1; f <= Packet.GetUpperBound(0);f++)
								{
									pidReturn.ByteData = ReDimPreserve(pidReturn.ByteData, Packet.GetUpperBound(0));
									pidReturn.ByteData[f - 1] = Packet[f];
								}

								//send Ack
								SendAck(Packet[0]);

								//all done, so exit with the data
								return pidReturn; //could be a PidData.Null
							}
							else
							{
								//Debug.WriteLine("ReceiveDatata() Ack or Nak Received: Pid = " & CStr(Packet(0)))
							}						
						}
						else
						{
							//Debug.WriteLine(String.Concat("Receive Data(): Packet received from GPS Serial but no data after dissasembly: ", ByteToHex(Packet)))
						}					
					}
					else
					{
						//Debug.WriteLine(String.Concat("Receive Data(): No Packet received from GPS Serial: ", ByteToHex(Packet)))
					}

					//check timeout to see if we have been waiting too long
					if(Timer() > (dblStart + miPortTimeout))
					{
						bTimeOut = true;
					}
					//Debug.WriteLine("              Start = " & CStr(dblStart))
					//Debug.WriteLine("             Finish = " & CStr(dblStart + miPortTimeout))
					//Debug.WriteLine("Current Timer Value = " & CStr(Microsoft.VisualBasic.Timer()))
				}
				//
				//added here rather than within the loop for simplicity
				if(bTimeOut)
					throw new LinkTimeOutException(ERR_MSG_2006);
			}
			catch(Exception ex)
			{
				throw ex;
			}

			//set pid data to null
			pidReturn.Pid = ProcessId.Null;
			pidReturn.ByteData = null;

			return pidReturn;
		}
		
		private void SendAck(byte Pid)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    This Sub Procedure controls the sending of an ACK to the
			//                   GPS. It is typically called when the ReceiveData
			//                   function is succesfull in retrieving and diassembling
			//                   a packet. The function prepared a PidData variable and
			//                   calls the Transmitdata function.
			//
			//   PARAMETERS:
			//                   Pid     Byte that represents the Pid of the packet that
			//                           this ACK is to acknowledge
			//
			//   RETURNS:
			//                   <none>
			// 
			//---------------------------------------------------------------------------

			GpsPidData PD;
			short iPid = 0;
			byte[] bData = new byte[2];

			try
			{
			
				iPid = (short)(Pid);

				PD.Pid = ProcessId.AckByte;
				PD.ByteData = ShortToByte(iPid);
				TransmitData(PD, false);
			}
			catch(Exception ex)
			{
				throw ex;
			}		
		}
		private void SetPidValues(LinkProtocol Protocol)
		{
			//---------------------------------------------------------------------------
			//  DESCRIPTION:	This populates the Process ID array (mPidData). This
			//                  is used throughout the module to translate the enumerated
			//                  Pid value passed as arguments to the actual value required
			//                  depending on the protocol selected. This function
			//                  is called when this class initialises to set default
			//                  values.
			//            
			//	PARAMETERS:
			//                  Protocol    Enumerated Integer representing L000,
			//                              L001 or L002.
			//    
			//  RETURNS:
			//                  <None>
			// 
			//---------------------------------------------------------------------------
			//
			
			try
			{
				//set the common L000/1/2 protocol values
				mPidData[0] = 6; //AckByte
				mPidData[1] = 21; //NAK_Byte
				mPidData[2] = 253; //ProtocolArray
				mPidData[3] = 254; //ProductRequest
				mPidData[4] = 255; //ProductData

                //set the rest to -1 (NOSUPPORT)
                for (int f = 5; f <= mPidData.Length - 1; f++)
				{
					mPidData[f] = NOSUPPORT;
				}
				
				//determine the additional protocol values to use
				switch(Protocol)
				{
						//
					case LinkProtocol.L001://basic values plus these
					{
						
						mPidData[5] = 10; //CommandData
						mPidData[6] = 12; //Xfer_Cmplt
						mPidData[7] = 14; //Date_Time
						mPidData[8] = 17; //PositionData
						mPidData[9] = 19; //ProximityWaypointData
						mPidData[10] = 27; //Records
						mPidData[11] = 29; //RouteHeader
						mPidData[12] = 30; //Rte_WaypointData
						mPidData[13] = 31; //AlmanacData
						mPidData[14] = 34; //TrackData
						mPidData[15] = 35; //WaypointData
						mPidData[16] = 51; //PvtData
						mPidData[17] = 98; //RouteLinkData
						mPidData[18] = 99; //TrackHeader
						mPidData[19] = 134; //FlghtBook data
                        mPidData[20] = 149; //Lap data
                        mPidData[21] = 152; //Waypoint Category
                        mPidData[22] = 52; //Receiver Measurement
                        mPidData[23] = 114; //Satellite data
						mPidData[24] = 48; //Baud Request Data
						mPidData[25] = 49; //Baud Accept Data
                        mPidData[26] = 38; //Unit ID (undocumented)


						break;
					}
					case LinkProtocol.L002: //basic values plus these
					{	
						mPidData[5] = 11; //CommandData
						mPidData[6] = 12; //Xfer_Cmplt
						mPidData[7] = 20; //Date_Time
						mPidData[8] = 24; //PositionData
						mPidData[9] = NOSUPPORT; //ProximityWaypointData (not supported)
						mPidData[10] = 35; //Records
						mPidData[11] = 37; //RouteHeader
						mPidData[12] = 39; //Rte_WaypointData
						mPidData[13] = 4; //AlmanacData
						mPidData[15] = 43; //WaypointData
						mPidData[16] = NOSUPPORT; //(not supported)
						mPidData[17] = NOSUPPORT; //(not supported)
						mPidData[18] = NOSUPPORT; //(not supported)
						mPidData[19] = NOSUPPORT; //(not supported)
						mPidData[20] = NOSUPPORT; //(not supported)
						mPidData[21] = NOSUPPORT; //(not supported)
						mPidData[22] = NOSUPPORT; //(not supported)
						mPidData[23] = NOSUPPORT; //(not supported)
						mPidData[24] = NOSUPPORT; //(not supported)
                        mPidData[25] = NOSUPPORT; //(not supported)
                        mPidData[26] = 38; //Unit ID (undocumented)
						
						break;
					}
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
		}
		/// <summary>
		/// This method transmits an application layer packet as a GpsPidData data type. 
		/// When the GPS device is accessed via the interface object, this method will be 
		/// invoked automatically as required. It is exposed to allow direct access to 
		/// the connected GPS device so that additional application protocols can be 
		/// implemented by client applications. See the Garmin GPS Specification for 
		/// further explanation of Process ID�s etc.
		/// </summary>
		internal bool TransmitData(GpsPidData PData)
		{
			return TransmitData(PData,true);
		}
		/// <summary>
		/// This method transmits an application layer packet as a GpsPidData data type. 
		/// When the GPS device is accessed via the interface object, this method will be 
		/// invoked automatically as required. It is exposed to allow direct access to 
		/// the connected GPS device so that additional application protocols can be 
		/// implemented by client applications. See the Garmin GPS Specification for 
		/// further explanation of Process ID�s etc.
		/// </summary>
		internal bool TransmitData(GpsPidData PData , bool bACKRequired)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    This is the main entry point for the application layer
			//                   (GPSInterface) and handles the transmission of data to
			//                   the GPS. Data that expects a response from the GPS
			//                   (ACK) is handled here.
			//
			//   PARAMETERS:
			//                   PData           User defined type consisting of a
			//                                   process ID (Pid) and some byte data.
			//                   bAckRequired    Optional Boolean default = true to
			//                                   indicate to the function that an ACK
			//                                   is expected.
			//   RETURNS:
			//                   Boolean         True if ACK received, false if no ACK
			//                                   received. If the bAckRequired parameter
			//                                   was passed as false the function always
			//                                   returns true.
			//
			//---------------------------------------------------------------------------

			bool blnResult = false;
			byte[] XmitData = null;
			byte[] Response = null;
			byte[] TempData = null;
			bool bTimeOut = false;
			double dblStart = 0.0;
			//int lArrayLength = 0;

			try
			{
				//changing integer Pid to byte value depending upon Protocol Property
				//xmit eack packet in the collection in turn
				//this will need to be modifed to deal with re-sending errored packets
				
				//if(PData.ByteData == null)

				         
				
				if(!mbPortInitialised)
					throw new PortException(ERR_MSG_1000);

				//combine the pid with the data
				//check the amount of data
				if(PData.ByteData != null && PData.ByteData.Length > 0)
				{

					//make room for the passed data
					XmitData = ReDimPreserve(XmitData, PData.ByteData.GetUpperBound(0) + 2);

					//copy the incomming data
					//TODO: implement SystemCopyArray
					//TODO: Think this is more correct, please test!
					//for(int i = XmitData.GetUpperBound(0);i >= 1; i--)
					for(int i = PData.ByteData.GetUpperBound(0);i >= 1; i--)
					{
						XmitData[i] = PData.ByteData[i - 1];
					}
				}
				else
				{
					//empty so just make the array large enough for the Pid
					XmitData = new byte[1];
				}
				//add the appropriate Pid value dependig upon protocol
				XmitData[0] = (byte)mPidData[(int)PData.Pid];
						
				//log the data
				TraceLog.WriteMessage(string.Concat("Transmit Data   : ",ByteToHex(XmitData)," (",ByteToAsc(XmitData),")"), TraceLevel.Verbose, true);

				//assemble the packet
				TempData = AssemblePacket(XmitData);

				//log the data
				TraceLog.WriteMessage(string.Concat("Transmit Packet : ",ByteToHex(TempData)), TraceLevel.Verbose, true);

				//send the data to the serial port
				m_CommPort.SendPacket(TempData);

				//check for ACK/NAK
				if (bACKRequired)
				{
					//record the start time
					dblStart = Timer();
					
					while(!bTimeOut)
					{
						
						//System.Windows.Forms.Application.DoEvents()
						
						TempData = m_CommPort.ReceivePacket();
						Response = DisassemblePacket(TempData);
						
						//check for an ack or nak
						if (IsACK(Response, XmitData[0]))
						{
							//ack received
							//Debug.WriteLine "ACK Received"
							TraceLog.WriteMessage(string.Concat("Receive Packet  : ",ByteToHex(TempData)), TraceLevel.Verbose, true);
							TraceLog.WriteMessage(string.Concat("Receive Data    : ",ByteToHex(Response)), TraceLevel.Verbose, true);
						
							blnResult = true;
							
							//ACK Received so exit
							break;
						}
						else if(IsNAK(Response, XmitData[0]))
						{
							//nak received
							//Debug.WriteLine "NAK Received"
							//
							//retry function may be required here in future versions
							//
						}
						else
						{
							//keep looking
							//check timeout to see if we have been waiting too log
							if(Timer() > (dblStart + ACK_TIMEOUT))
								bTimeOut = true;
						}

						//added here rather than within the loop for simplicity
						if(bTimeOut)
							throw new LinkTimeOutException(ERR_MSG_2005);
					}
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}

			return blnResult;
		}
		
		private byte[] AssemblePacket(byte[] Data)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    Accepts a packet as an array of bytes and assembles it
			//                   adding checksum, DLE stuffing and size bytes as
			//                   appropriate.
			//
			//   PARAMETERS:
			//                   Byte()      Array of bytes containing the packet first
			//                               byte is the Pid remaining bytes are the
			//                               packet data.
			//   RETURNS:
			//                   Packet()    Array of bytes containing the assembled
			//                               packet.
			//
			//---------------------------------------------------------------------------

			byte[] Pkt;				//forms the input data stream
			byte[] OutData;			//forms the output data stream
			int i = 0;				//general purpose counter
			int o = 0;				//general purpose counter offset
			int n = 0;				//index character for array positioning
			int iChecksum = 0;		//holds the calculated checksum
			int iLastByte = 0;		//holds the packet data length
			//string sLogMsg = "";	//used to hold output data as a string for logging
			//string sLogChar = "";	//use to pad the log msg

			try
			{

				//get index of last byte of the data
				iLastByte = Data.GetUpperBound(0);

				//create packet to the size of the message to be sent + the SIZE and CHKSUM bytes
				Pkt = new byte[iLastByte + 3];

				//set up the packet header
				Pkt[0] = Data[0]; //Package ID is first byte, data is rest
				
				//packet length cannot exceed 255 as it is stored in one byte
				if(iLastByte > 254)
					throw new PacketSizeException(ERR_MSG_2004);

				Pkt[1] = (byte)iLastByte; //Size of data
				//the value iLastByte = the index of the last byte of our array
				//(which includes a Pid byte) is the same as the data size required by the
				//protocol
				
				//Pkt(3 - n-4) are the message itself obtained from data(1 - ilastbyte)
				for(i = 1; i <= iLastByte; i++)
				{
					//run through the message adding each byte to the packet
					Pkt[1 + i] = Data[i];
				}
				
				//work out the last element of Pkt()
				n = Pkt.GetUpperBound(0);
				
				//add each byte collected so far
				for(i = 0; i < n; i++)
				{
					iChecksum = iChecksum + Pkt[i];
				}

				//add the checksum to the packet
				//don't bother adding the tail bits this can be done later
				Pkt[n] = (byte)((256 - iChecksum) & 0xFF);
				
				//run through the packet moving the bytes from the pkt array
				//to the data array adding the DLE stuff characters when appropriate
				//these are added if the DLE byte appears in the data
				
				//first character is DLE
				OutData = new byte[2];
				OutData[0] = DLE;
				OutData[1] = Data[0];

				//reset the offset counter
				o = 1;
				
				//bytes up to and including the checksum require DLE stuffing
				for(i = 1; i <= Pkt.GetUpperBound(0);i++)
				{
					//create the new element in the data array
					OutData = ReDimPreserve(OutData,i + o + 1);
					OutData[i + o] = Pkt[i];

					if(Pkt[i] == DLE)
					{
						//increase the offset counter and create another
						//element for the extra DLE
						o = o + 1;
						OutData = ReDimPreserve(OutData, i + o + 1);
						OutData[i + o] = DLE;
					}
				}
				
				//work out the last element of Data() and add the bytes for the tail
				n = OutData.GetUpperBound(0);
				OutData = ReDimPreserve(OutData, n + 3);
				
				//add the last two bytes
				OutData[n + 1] = DLE;
				OutData[n + 2] = ETX;
			}
			catch(Exception ex)
			{
				throw ex;
			}

			//return the data
			return OutData;

		}
		private static byte[] ReDimPreserve(byte[] bytArray,int intNewDimension)
		{
			//re-dimensions a byte array and preserves the contents
			if(intNewDimension <= 0 || intNewDimension == 0)
				throw new ArgumentException(ERR_MSG_2007);

			byte[] bytNewArray = new byte[intNewDimension];

			//copy any existing data
			if(bytArray != null)
				System.Array.Copy(bytArray,0,bytNewArray,0,bytArray.Length);
			
			return bytNewArray;
		}
		private static string[] ReDimPreserveS(string[] strArray, int intNewDimension)
		{
			//re-dimensions a byte array and preserves the contents
			if(intNewDimension <= 0 || intNewDimension == 0)
				throw new ArgumentException(ERR_MSG_2008);

			string[] strNewArray = new string[intNewDimension];

			//copy any existing data
			if(strArray != null)
				System.Array.Copy(strArray,0,strNewArray,0,strArray.Length);
			
			return strNewArray;
		}

		private static byte[] ShortToByte(short iData)
		{
			return ShortToByte(iData, false);
		}
		private static byte[] ShortToByte(short shtData, bool bSingleByte)
		{
			//-------------------------------------------------------------------------
			// ABSTRACT:
			//
			// PARAMETERS:
			//
			// RETURNS:
			//
			// DESCRIPTION:
			//
			//-------------------------------------------------------------------------

			byte[] bytData;

			try
			{
				if(bSingleByte)
				{
					bytData = new byte[1];
					bytData[0] = (byte)(shtData % 256); //modulus (remainder)
				}
				else
				{
					bytData = new byte[2];
					bytData[0] = (byte)(shtData % 256);
					bytData[1] = (byte)((int)shtData / (int)256); //div (integer division)
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return bytData;
		}

		private static string ByteToHex(byte[] ByteData)
		{
			//---------------------------------------------------------------------------
			//	DESCRIPTION:	This function converts an array of bytes to a string
			//					of hex digits representing those bytes.
			//
			//
			//	PARAMETERS:
			//				ByteData()  Array of bytes.
			//
			//	RETURNS:
			//				String      String of hex character pairs representing
			//							each byte in the byte array, separated
			//							with a space.
			//
			//---------------------------------------------------------------------------
		
			string sTemp = "";
			string sMessage = "";

			try
			{
				if(ByteData != null)
				{
					for(int f = 0;f <= ByteData.GetUpperBound(0);f++)
					{
						sTemp = ByteData[f].ToString("X"); //hex returns a string representing the passed number

						if(sTemp.Length < 2)
						{
							sTemp = string.Concat("0", sTemp);
						}
						
						sMessage = string.Concat(sMessage, " ", sTemp);
					}
					
					return sMessage.Trim();
				}
				else
				{
					return "";
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private static string ByteToAsc(byte[] ByteData)
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    This function converts an array of bytes to a string
			//                   of ascii characters representing those bytes.
			//
			//
			//   PARAMETERS:
			//                   ByteData()  Array of bytes.
			//
			//   RETURNS:
			//                   String      String of ascii character representing
			//                               each byte in the byte array, an invalid
			//                               ascii code is represented by a ".".
			//
			//---------------------------------------------------------------------------

			char chrTemp = '.';
			string sMessage = "";

			try
			{
				if(ByteData != null)
				{
					for(int f = 0;f <= ByteData.GetUpperBound(0);f++)
					{
						if((ByteData[f] >= 32) && (ByteData[f] <= 127))
						{
                            chrTemp = Convert.ToChar(ByteData[f], CultureInfo.InvariantCulture);
						}
						else
						{
							chrTemp = '.';
						}
		
						sMessage = string.Concat(sMessage,chrTemp.ToString(CultureInfo.InvariantCulture));

					}
				}			
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
			return sMessage.Trim();

		}

		private static double Timer() 
		{
			DateTime dtTimer;

			dtTimer = DateTime.Now;
			return (double) dtTimer.Ticks % 711573504 / 1e+007;
		}
		/// <summary>
		/// This property sets the port timeout. The value is in seconds and refers to the time
		/// that the com port waits for a response before an exception is raised.
		/// </summary>
		internal int PortTimeout
		{
			set
			{
				miPortTimeout = value;
			}
			get
			{
				return miPortTimeout;
			}
		}
		/// <summary>
		/// This method closes the currently open serial port. If the serial prot is already
		/// closed then a call to this method will have no effect.
		/// </summary>
		internal bool PortClose()
		{
			bool blnResult = false;
			try
			{
				if( m_CommPort.Online )
				{
					m_CommPort.Close();
				}
				blnResult = true;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return blnResult;
		}
		/// <summary>
		/// This method opens the specified serial port in order to establish 
		/// a connection with the connected GPS device.
		/// </summary>
		internal bool PortOpen(int PortNumber, int Speed)
		{
			bool blnResult = false;
			try
			{
				//set default port
                //if(PortNumber < 1 || PortNumber > 16)
                //    PortNumber = 1;

				//initialise the port
				m_CommPort.Port = (short)PortNumber;

                m_CommPort.Open();
				//open the port, try three times as occasionally windows doesn't release
				//it as quickly as it might from the previous close
                //int tries = 0;
                //while(tries < 3)
                //{
                //    try
                //    {
                //        m_CommPort.Open();
                //        break;
                //    }
                //    catch(Exception ex)
                //    {	
                //        Debug.WriteLine("Thread.Sleep(100)");
                //        System.Threading.Thread.Sleep(100);
                //        tries++;
                //    }
                //}

				mbPortInitialised = true; //not sure if this is actually used any more
				blnResult = true;
			}
			catch
			{
				throw;
			}
			
			return blnResult;
		}
		internal LinkProtocol Protocol
		{
			//---------------------------------------------------------------------------
			//   DESCRIPTION:    Sets the module level variable to indicate the required
			//                   link protocol and calls the setPidValues procedure to
			//                   set the actual pid values for the protocol selected.
			//
			//
			//   PARAMETERS:     iProt   Enumerated integer indicating the required link
			//                           level protocol, L000, L001 or L002.
			//
			//
			//   RETURNS:        <none>
			//
			//
			//---------------------------------------------------------------------------

			set
			{
				try
				{
					//save the value for the get property
					miProtocol = value;

					//use a function to set the actual values so that the same function can
					//be called in the initialise event with a default value
					SetPidValues(miProtocol);
				}
				catch(Exception ex)
				{
					throw ex;
				}
			}
		}
	}
}
