//============================================================================
// LinkPacketIds.cs
//
// Copyright (c) 2005 U.S. TrailMaps, LLC
// THIS MATERIAL IS CONFIDENTIAL AND PROPRIETARY TO U.S. TRAILMAPS AND
// MAY NOT BE REPRODUCED, PUBLISHED OR DISCLOSED TO OTHERS WITHOUT COMPANY
// AUTHORIZATION.
//
// This work is derived from Waymex GPS Library for .Net version 2.7.9 source.
// Portions copyright (c) 2004-2005 Waymex IT Ltd
//============================================================================

using System;
using System.Globalization;

namespace Waymex.Gps.Garmin.Usb
{
	/// <summary>
	/// Packet ids for a given link protocol.
	/// </summary>
	internal class LinkPacketIds
	{
		private const byte Unsupported = 0;

		public LinkPacketIds(string linkProtocol)
		{
			switch (linkProtocol)
			{
				case "L000":
					// No additional ids to set
					break;

				case "L001":
					commandData = 10;
					xferCmplt = 12;
					dateTimeData = 14;
					positionData = 17;
					prxWptData = 19;
					records = 27;
					rteHdr = 29;
					rteWptData = 30;
					almanacData = 31;
					trkData = 34;
					wptData = 35;
					pvtData = 51;
					rteLinkData = 98;
					trkHdr = 99;
					flightBookRecord = 134;
					lap = 149;
					wptCat = 152;
					break;

				case "L002":
					almanacData = 4;
					commandData = 11;
					xferCmplt = 12;
					dateTimeData = 20;
					positionData = 24;
					prxWptData = 27;
					records = 35;
					rteHdr = 37;
					rteWptData = 39;
					wptData = 43;
					break;

				default:
                    throw new ApplicationException(String.Format(CultureInfo.CurrentCulture, "Link protocol \"{0}\" is not supported.", linkProtocol));
			}
		}

		private static byte CheckId(byte id)
		{
			if (id == Unsupported)
				throw new ApplicationException("A packet id not supported by this device was attempted to be used.");
			return id;
		}

		// Serial protocol packet ids:
		private byte ackByte = 6;
		public byte AckByte
		{
			get
			{
				return CheckId(ackByte);
			}
		}
		private byte nakByte = 21;
		public byte NakByte
		{
			get
			{
				return CheckId(nakByte);
			}
		}

		// USB protocol layer packet ids:
		private byte dataAvailable = 2;
		public byte DataAvailable
		{
			get
			{
				return CheckId(dataAvailable);
			}
		}
		private byte startSession = 5;
		public byte StartSession
		{
			get
			{
				return CheckId(startSession);
			}
		}
		private byte sessionStarted = 6;
		public byte SessionStarted
		{
			get
			{
				return CheckId(sessionStarted);
			}
		}

		// L000 basic link protocol packet ids:
		private byte protocolArray = 253;
		public byte ProtocolArray
		{
			get
			{
				return CheckId(protocolArray);
			}
		}
		private byte productRqst = 254;
		public byte ProductRqst
		{
			get
			{
				return CheckId(productRqst);
			}
		}
		private byte productData = 255;
		public byte ProductData
		{
			get
			{
				return CheckId(productData);
			}
		}
		private byte extProductData = 248;
		public byte ExtProductData
		{
			get
			{
				return CheckId(extProductData);
			}
		}

		// Other link protocol packet ids:
		private byte commandData = Unsupported;
		public byte CommandData
		{
			get
			{
				return CheckId(commandData);
			}
		}
		private byte xferCmplt = Unsupported;
		public byte XferCmplt
		{
			get
			{
				return CheckId(xferCmplt);
			}
		}
		private byte dateTimeData = Unsupported;
		public byte DateTimeData
		{
			get
			{
				return CheckId(dateTimeData);
			}
		}
		private byte positionData = Unsupported;
		public byte PositionData
		{
			get
			{
				return CheckId(positionData);
			}
		}
		private byte prxWptData = Unsupported;
		public byte PrxWptData
		{
			get
			{
				return CheckId(prxWptData);
			}
		}
		private byte records = Unsupported;
		public byte Records
		{
			get
			{
				return CheckId(records);
			}
		}
		private byte rteHdr = Unsupported;
		public byte RteHdr
		{
			get
			{
				return CheckId(rteHdr);
			}
		}
		private byte rteWptData = Unsupported;
		public byte RteWptData
		{
			get
			{
				return CheckId(rteWptData);
			}
		}
		private byte almanacData = Unsupported;
		public byte AlmanacData
		{
			get
			{
				return CheckId(almanacData);
			}
		}
		private byte trkData = Unsupported;
		public byte TrkData
		{
			get
			{
				return CheckId(trkData);
			}
		}
		private byte wptData = Unsupported;
		public byte WptData
		{
			get
			{
				return CheckId(wptData);
			}
		}
		private byte pvtData = Unsupported;
		public byte PvtData
		{
			get
			{
				return CheckId(pvtData);
			}
		}
		private byte rteLinkData = Unsupported;
		public byte RteLinkData
		{
			get
			{
				return CheckId(rteLinkData);
			}
		}
		private byte trkHdr = Unsupported;
		public byte TrkHdr
		{
			get
			{
				return CheckId(trkHdr);
			}
		}
		private byte flightBookRecord = Unsupported;
		public byte FlightBookRecord
		{
			get
			{
				return CheckId(flightBookRecord);
			}
		}
		private byte lap = Unsupported;
		public byte Lap
		{
			get
			{
				return CheckId(lap);
			}
		}
		private byte wptCat = Unsupported;
		public byte WptCat
		{
			get
			{
				return CheckId(wptCat);
			}
		}
	}
}
