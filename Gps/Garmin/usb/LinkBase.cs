//============================================================================
// Link.cs
//
// Copyright (c) 2005 U.S. TrailMaps, LLC
// THIS MATERIAL IS CONFIDENTIAL AND PROPRIETARY TO U.S. TRAILMAPS AND
// MAY NOT BE REPRODUCED, PUBLISHED OR DISCLOSED TO OTHERS WITHOUT COMPANY
// AUTHORIZATION.
//
// This work is derived from Waymex GPS Library for .Net version 2.7.9 source.
// Portions copyright (c) 2004-2005 Waymex IT Ltd
//============================================================================

using System;

namespace Waymex.Gps.Garmin.Usb
{
	internal abstract class LinkBase : IDisposable
	{
		/// <summary>
		/// The packet ids used for communicating with the device.
		/// </summary>
		/// <remarks>
		/// Until we learn what link protocol the device wants to use, start with the basic link protocol.
		/// </remarks>
		protected LinkPacketIds linkPacketIds = new LinkPacketIds("L000");
		public LinkPacketIds PacketIds
		{
			get
			{
				return linkPacketIds;
			}
		}

		public LinkBase()
		{
		}

		~LinkBase()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
		}

		public void SetLinkProtocol(string linkProtocol)
		{
			this.linkPacketIds = new LinkPacketIds(linkProtocol);
		}

		public abstract void Open(string portName);

		public abstract void Close();

		public virtual void StartSession()
		{
		}

		public virtual void EndSession()
		{
		}

		public abstract long UnitId
		{
			get;
		}

        public abstract bool TransmitData(GpsPidData pidData);
        public abstract void TransmitData(ApplicationPacket packet, bool ackRequired);

		public void TransmitData(ApplicationPacket packet)
		{
			TransmitData(packet, true);
		}

        public abstract GpsPidData ReceiveData();

		public virtual void ClearBuffer()
		{
		}
	}
}
