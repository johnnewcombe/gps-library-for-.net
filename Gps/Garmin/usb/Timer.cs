//============================================================================
// Timer.cs
//
// Copyright (c) 2005 U.S. TrailMaps, LLC
// THIS MATERIAL IS CONFIDENTIAL AND PROPRIETARY TO U.S. TRAILMAPS AND
// MAY NOT BE REPRODUCED, PUBLISHED OR DISCLOSED TO OTHERS WITHOUT COMPANY
// AUTHORIZATION.
//
// This work is derived from Waymex GPS Library for .Net version 2.7.9 source.
// Portions copyright (c) 2004-2005 Waymex IT Ltd
//============================================================================

using System;

namespace Waymex.Gps.Garmin.Usb
{
	internal class Timer
	{
		private long startTicks;

		public Timer()
		{
			Start();
		}

		public void Start()
		{
			startTicks = DateTime.Now.Ticks;
		}

		public double ElapsedSeconds
		{
			get
			{
				long elapsedTicks = DateTime.Now.Ticks - startTicks;
				return ((double)elapsedTicks / 10000000.0);
			}
		}
	}
}
