//============================================================================
// ApplicationPacket.cs
//
// Copyright (c) 2005 U.S. TrailMaps, LLC
// THIS MATERIAL IS CONFIDENTIAL AND PROPRIETARY TO U.S. TRAILMAPS AND
// MAY NOT BE REPRODUCED, PUBLISHED OR DISCLOSED TO OTHERS WITHOUT COMPANY
// AUTHORIZATION.
//
// This work is derived from Waymex GPS Library for .Net version 2.7.9 source.
// Portions copyright (c) 2004-2005 Waymex IT Ltd
//============================================================================

using System;
using System.Globalization;

namespace Waymex.Gps.Garmin.Usb
{
	internal class ApplicationPacket
	{
		public byte PacketId;
		public BytePacket PacketData;

		public ApplicationPacket(byte packetId, BytePacket packetData)
		{
			PacketId = packetId;
			PacketData = packetData;
		}

		public ApplicationPacket(byte packetId) : this(packetId, new BytePacket())
		{
		}

		public ApplicationPacket(byte packetId, byte[] packetData) : this(packetId, new BytePacket(packetData))
		{
		}

		public ApplicationPacket(byte packetId, ushort packetData) : this(packetId, BitConverter.GetBytes(packetData))
		{
		}

		public override string ToString()
		{
			return String.Format(CultureInfo.CurrentCulture, "ApplicationPacket id {0} data {1}", PacketId, PacketData);
		}
	}
}
