namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// GpsPidData Structure.
	/// </summary>
	internal struct GpsPidData
	{
		/// <summary>Process ID</summary>
        internal ProcessId Pid;
		/// <summary>
		/// Byte Data for the Command, see the Garmin Interface Specification for more details.
		/// </summary>
        internal byte[] ByteData;
	}
}