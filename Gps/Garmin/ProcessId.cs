namespace Waymex.Gps.Garmin
{
	//Link protocol Pid Indexes, these are converted to the approriate BYTE values
	//depending upon the protocol selected with the 'Protocol' property.
	/// <summary>
	/// Link Protocol Process ID Enumerator.
	/// </summary>
	public enum ProcessId
	{
		/// <summary>0</summary>
		AckByte = 0,
		/// <summary>1</summary>
		NakByte = 1,
		/// <summary>2</summary>
		ProtocolArray = 2,
		/// <summary>3</summary>
        ProductRequest = 3,
		/// <summary>4</summary>
		ProductData = 4,
		/// <summary>5</summary>
		CommandData = 5,
		/// <summary>6</summary>
		TransferComplete = 6,
		/// <summary>7</summary>
		DateTimeData = 7,
		/// <summary>8</summary>
		PositionData = 8,
		/// <summary>9</summary>
		ProximityWaypointData = 9,
		/// <summary>10</summary>
		Records = 10,
		/// <summary>11</summary>
		RouteHeader = 11,
		/// <summary>12</summary>
		RouteWaypointData = 12,
		/// <summary>13</summary>
		AlmanacData = 13,
		/// <summary>14</summary>
		TrackData = 14,
		/// <summary>15</summary>
		WaypointData = 15,
		/// <summary>16</summary>
		PvtData = 16,
		/// <summary>17</summary>
		RouteLinkData = 17,
		/// <summary>18</summary>
		TrackHeader = 18,
		/// <summary>19</summary>
		FlightBookRecord = 19,
		/// <summary>20</summary>
		LapData = 20,
		/// <summary>21</summary>
		WaypointCategory = 21,
		/// <summary>22</summary>
		ReceiverMeasurement = 22,
		/// <summary>23</summary>
		SatelliteData = 23,
		/// <summary>24</summary>
		BaudRequestData = 24,
		/// <summary>25</summary>
		BaudAcceptData = 25,
        /// <summary>25</summary>
        UnitId = 26,
        /// <summary>255</summary>
		Null = 255
	}
}
	