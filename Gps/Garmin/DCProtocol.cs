namespace Waymex.Gps.Garmin
{
	/// <summary>
	/// Device Command Protocol Enumerator.
	/// </summary>
	public enum DCProtocol
	{
		/// <summary>0</summary>
		A010 = 0,
		/// <summary>1</summary>
		A011 = 1
	}
}