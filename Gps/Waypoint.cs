using System;
using System.Xml;
using System.Globalization;


namespace Waymex.Gps
{
	/// <summary>
	/// This object represents a single Waypoint and forms part of the 
	/// of the WaypointCollection object that is returned by the GetWaypoints 
	/// and GetProximityWaypoints methods of the Device object. 
	/// This object is also returned by the Waypoints Property 
	/// of the Route object.
	/// </summary>
	/// <remarks>
    /// <para>
	/// When receiving data from the Host, most GPS devices will overwrite any existing 
	/// waypoints with the same name stored in the GPS device. For example, if the Host 
	/// sends a waypoint named CBELLS, most GPS devices will overwrite a waypoint named
	/// CBELLS that may be stored in GPS memory.
	/// </para>
	/// <para>
	/// However some GPS devices compare the position of the new waypoint with the position
	/// of the stored waypoint (altitude is ignored). If the positions match, the GPS
	/// will erase the identically named waypoint and replace it with the new one.
	/// If the positions differ, the unit will create a new, unique name for the
	/// incoming waypoint and preserve the existing waypoint under its original name.
	/// </para>
	/// <para>
	/// When the Host requests the GPS to send waypoints, the GPS will send every
	/// waypoint stored in its database. When the Host sends waypoints to the GPS,
	/// the Host may selectively transfer any number of waypoints.
	/// </para>
	/// <para>
	/// When the Host requests the GPS to send proximity waypoints, the GPS will send all proximity
	/// waypoints stored in its database.
	/// </para>
    /// <para>
    /// <b>Symbols</b>
	/// </para>
    /// <para>
    /// Garmin devices use three symbol sets for Waypoint symbols and 
    /// Magellan devices use five symbol sets. 
    /// The set used depends upon the specific device connected. 
    /// The <see cref="ProductInfo.SymbolSet">ProductInfo.SymbolSet</see> property returns the symbol set in 
    /// use by a particular device.
    /// The <see cref="Waypoint.SymbolIdentifier">Waypoint.SymbolIdentifier</see> property allows for an enumerated value that 
    /// works for all symbols across all devices (including Magellan devices).
    /// The Waypoint.Symbol property (Int16) returns the raw symbol value of the waypoint
    /// for garmin devices and the Waypoint.Icon property (String) returns the raw Icon 
    /// value of the waypoint for Magellan devices.
    /// </para>
    /// <para>
    /// When retrieving waypoint data from one device and transferring it to another device,
    /// care must be taken when using the Symbol or Icon properties. For example the raw
    /// symbol value for a 'dot' on a garmin device using symbol set 0 is 0 but a dot for
    /// a device using symbol set 1 is 18. The SymbolIdentifier is a much safer approach.
    /// </para>
    /// <para><b>
    /// Garmin Symbol Sets
    /// </b>
    /// </para>
    /// <para>
    /// Many GPS devices are limited to a much smaller subset of these symbols.  
    /// However, the GPS will ignore any disallowed symbol values that are received 
    /// and instead substitute the value for a generic dot symbol.
    /// </para>
    /// <para><b>Symbols Set 0</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Symbol property)</term><description>Symbol Description</description></listheader>
    /// <item><term>0</term><description>dot</description></item>
    /// <item><term>1</term><description>house</description></item>
    /// <item><term>2</term><description>gas</description></item>
    /// <item><term>3</term><description>car</description></item>
    /// <item><term>4</term><description>fish</description></item>
    /// <item><term>5</term><description>boat</description></item>
    /// <item><term>6</term><description>anchor</description></item>
    /// <item><term>7</term><description>wreck</description></item>
    /// <item><term>8</term><description>exit</description></item>
    /// <item><term>9</term><description>skull</description></item>
    /// <item><term>10</term><description>flag</description></item>
    /// <item><term>11</term><description>camp</description></item>
    /// <item><term>12</term><description>circle with x </description></item>
    /// <item><term>13</term><description>deer</description></item>
    /// <item><term>14</term><description>first aid</description></item>
    /// <item><term>15</term><description>back track</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Symbols Set 1</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Symbol property)</term><description>Symbol Description</description></listheader>
    /// <item><term>0</term><description>white anchor symbol</description></item>
    /// <item><term>1</term><description>white bell symbol</description></item>
    /// <item><term>2</term><description>green diamond symbol</description></item>
    /// <item><term>3</term><description>red diamond symbol</description></item>
    /// <item><term>4</term><description>diver down flag 1</description></item>
    /// <item><term>5</term><description>diver down flag 2</description></item>
    /// <item><term>6</term><description>white dollar symbol</description></item>
    /// <item><term>7</term><description>white fish symbol</description></item>
    /// <item><term>8</term><description>white fuel symbol</description></item>
    /// <item><term>9</term><description>white horn symbol</description></item>
    /// <item><term>10</term><description>white house symbol</description></item>
    /// <item><term>11</term><description>white knife and fork symbol</description></item>
    /// <item><term>12</term><description>white light symbol</description></item>
    /// <item><term>13</term><description>white mug symbol</description></item>
    /// <item><term>14</term><description>white skull and crossbones symbol</description></item>
    /// <item><term>15</term><description>green square symbol</description></item>
    /// <item><term>16</term><description>red square symbol</description></item>
    /// <item><term>17</term><description>white buoy waypoint symbol</description></item>
    /// <item><term>18</term><description>waypoint dot</description></item>
    /// <item><term>19</term><description>white wreck symbol</description></item>
    /// <item><term>20</term><description>null symbol (transparent)</description></item>
    /// <item><term>21</term><description>man overboard symbol</description></item>
    /// <item><term>22</term><description>amber map buoy symbol</description></item>
    /// <item><term>23</term><description>black map buoy symbol</description></item>
    /// <item><term>24</term><description>blue map buoy symbol</description></item>
    /// <item><term>25</term><description>green map buoy symbol</description></item>
    /// <item><term>26</term><description>green/red map buoy symbol</description></item>
    /// <item><term>27</term><description>green/white map buoy symbol</description></item>
    /// <item><term>28</term><description>orange map buoy symbol</description></item>
    /// <item><term>29</term><description>red map buoy symbol</description></item>
    /// <item><term>30</term><description>red/green map buoy symbol</description></item>
    /// <item><term>31</term><description>red/white map buoy symbol</description></item>
    /// <item><term>32</term><description>violet map buoy symbol</description></item>
    /// <item><term>33</term><description>white map buoy symbol</description></item>
    /// <item><term>34</term><description>white/green map buoy symbol</description></item>
    /// <item><term>35</term><description>white/red map buoy symbol</description></item>
    /// <item><term>36</term><description>white dot symbol</description></item>
    /// <item><term>37</term><description>radio beacon symbol</description></item>
    /// <item><term>150</term><description>boat ramp symbol</description></item>
    /// <item><term>151</term><description>campground symbol</description></item>
    /// <item><term>152</term><description>restrooms symbol</description></item>
    /// <item><term>153</term><description>shower symbol</description></item>
    /// <item><term>154</term><description>drinking water symbol</description></item>
    /// <item><term>155</term><description>telephone symbol</description></item>
    /// <item><term>156</term><description>first aid symbol</description></item>
    /// <item><term>157</term><description>information symbol</description></item>
    /// <item><term>158</term><description>parking symbol</description></item>
    /// <item><term>159</term><description>park symbol</description></item>
    /// <item><term>160</term><description>picnic symbol</description></item>
    /// <item><term>161</term><description>scenic area symbol</description></item>
    /// <item><term>162</term><description>skiing symbol</description></item>
    /// <item><term>163</term><description>swimming symbol</description></item>
    /// <item><term>164</term><description>dam symbol</description></item>
    /// <item><term>165</term><description>controlled area symbol</description></item>
    /// <item><term>166</term><description>danger symbol</description></item>
    /// <item><term>167</term><description>restricted area symbol</description></item>
    /// <item><term>168</term><description>null symbol</description></item>
    /// <item><term>169</term><description>ball symbol</description></item>
    /// <item><term>170</term><description>car symbol</description></item>
    /// <item><term>171</term><description>deer symbol</description></item>
    /// <item><term>172</term><description>shopping cart symbol</description></item>
    /// <item><term>173</term><description>lodging symbol</description></item>
    /// <item><term>174</term><description>mine symbol</description></item>
    /// <item><term>175</term><description>trail head symbol</description></item>
    /// <item><term>176</term><description>truck stop symbol</description></item>
    /// <item><term>177</term><description>user exit symbol</description></item>
    /// <item><term>178</term><description>flag symbol</description></item>
    /// <item><term>179</term><description>circle with x in the center</description></item>
    /// <item><term>180</term><description>open 24 hours symbol</description></item>
    /// <item><term>181</term><description>U fishing hot spots facility (tm)</description></item>
    /// <item><term>182</term><description>bottom conditions</description></item>
    /// <item><term>183</term><description>tide/current prediction station</description></item>
    /// <item><term>184</term><description>U anchor prohibited symbol</description></item>
    /// <item><term>185</term><description>U beacon symbol</description></item>
    /// <item><term>186</term><description>U coast guard symbol</description></item>
    /// <item><term>187</term><description>U reef symbol</description></item>
    /// <item><term>188</term><description>U weedbed symbol</description></item>
    /// <item><term>189</term><description>U dropoff symbol</description></item>
    /// <item><term>190</term><description>U dock symbol</description></item>
    /// <item><term>191</term><description>U marina symbol</description></item>
    /// <item><term>192</term><description>U bait and tackle symbol</description></item>
    /// <item><term>193</term><description>U stump symbol</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Symbols Set 2</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Symbol property)</term><description>Symbol Description</description></listheader>
    /// <item><term>0</term><description>white anchor symbol</description></item>
    /// <item><term>1</term><description>white bell symbol</description></item>
    /// <item><term>2</term><description>green diamond symbol</description></item>
    /// <item><term>3</term><description>red diamond symbol</description></item>
    /// <item><term>4</term><description>diver down flag 1</description></item>
    /// <item><term>5</term><description>diver down flag 2</description></item>
    /// <item><term>6</term><description>white dollar symbol</description></item>
    /// <item><term>7</term><description>white fish symbol</description></item>
    /// <item><term>8</term><description>white fuel symbol</description></item>
    /// <item><term>9</term><description>white horn symbol</description></item>
    /// <item><term>10</term><description>white house symbol</description></item>
    /// <item><term>11</term><description>white knife and fork symbol</description></item>
    /// <item><term>12</term><description>white light symbol</description></item>
    /// <item><term>13</term><description>white mug symbol</description></item>
    /// <item><term>14</term><description>white skull and crossbones symbol</description></item>
    /// <item><term>15</term><description>green square symbol</description></item>
    /// <item><term>16</term><description>red square symbol</description></item>
    /// <item><term>17</term><description>white buoy waypoint symbol</description></item>
    /// <item><term>18</term><description>waypoint dot</description></item>
    /// <item><term>19</term><description>white wreck symbol</description></item>
    /// <item><term>20</term><description>null symbol (transparent)</description></item>
    /// <item><term>21</term><description>man overboard symbol</description></item>
    /// <item><term>22</term><description>amber map buoy symbol</description></item>
    /// <item><term>23</term><description>black map buoy symbol</description></item>
    /// <item><term>24</term><description>blue map buoy symbol</description></item>
    /// <item><term>25</term><description>green map buoy symbol</description></item>
    /// <item><term>26</term><description>green/red map buoy symbol</description></item>
    /// <item><term>27</term><description>green/white map buoy symbol</description></item>
    /// <item><term>28</term><description>orange map buoy symbol</description></item>
    /// <item><term>29</term><description>red map buoy symbol</description></item>
    /// <item><term>30</term><description>red/green map buoy symbol</description></item>
    /// <item><term>31</term><description>red/white map buoy symbol</description></item>
    /// <item><term>32</term><description>violet map buoy symbol</description></item>
    /// <item><term>33</term><description>white map buoy symbol</description></item>
    /// <item><term>34</term><description>white/green map buoy symbol</description></item>
    /// <item><term>35</term><description>white/red map buoy symbol</description></item>
    /// <item><term>36</term><description>white dot symbol</description></item>
    /// <item><term>37</term><description>radio beacon symbol</description></item>
    /// <item><term>150</term><description>boat ramp symbol</description></item>
    /// <item><term>151</term><description>campground symbol</description></item>
    /// <item><term>152</term><description>restrooms symbol</description></item>
    /// <item><term>153</term><description>shower symbol</description></item>
    /// <item><term>154</term><description>drinking water symbol</description></item>
    /// <item><term>155</term><description>telephone symbol</description></item>
    /// <item><term>156</term><description>first aid symbol</description></item>
    /// <item><term>157</term><description>information symbol</description></item>
    /// <item><term>158</term><description>parking symbol</description></item>
    /// <item><term>159</term><description>park symbol</description></item>
    /// <item><term>160</term><description>picnic symbol</description></item>
    /// <item><term>161</term><description>scenic area symbol</description></item>
    /// <item><term>162</term><description>skiing symbol</description></item>
    /// <item><term>163</term><description>swimming symbol</description></item>
    /// <item><term>164</term><description>dam symbol</description></item>
    /// <item><term>165</term><description>controlled area symbol</description></item>
    /// <item><term>166</term><description>danger symbol</description></item>
    /// <item><term>167</term><description>restricted area symbol</description></item>
    /// <item><term>168</term><description>null symbol</description></item>
    /// <item><term>169</term><description>ball symbol</description></item>
    /// <item><term>170</term><description>car symbol</description></item>
    /// <item><term>171</term><description>deer symbol</description></item>
    /// <item><term>172</term><description>shopping cart symbol</description></item>
    /// <item><term>173</term><description>lodging symbol</description></item>
    /// <item><term>174</term><description>mine symbol</description></item>
    /// <item><term>175</term><description>trail head symbol</description></item>
    /// <item><term>176</term><description>truck stop symbol</description></item>
    /// <item><term>177</term><description>user exit symbol</description></item>
    /// <item><term>178</term><description>flag symbol</description></item>
    /// <item><term>179</term><description>circle with x in the center</description></item>
    /// <item><term>180</term><description>open 24 hours symbol</description></item>
    /// <item><term>181</term><description>U fishing hot spots facility (tm)</description></item>
    /// <item><term>182</term><description>bottom conditions</description></item>
    /// <item><term>183</term><description>tide/current prediction station</description></item>
    /// <item><term>184</term><description>U anchor prohibited symbol</description></item>
    /// <item><term>185</term><description>U beacon symbol</description></item>
    /// <item><term>186</term><description>U coast guard symbol</description></item>
    /// <item><term>187</term><description>U reef symbol</description></item>
    /// <item><term>188</term><description>U weedbed symbol</description></item>
    /// <item><term>189</term><description>U dropoff symbol</description></item>
    /// <item><term>190</term><description>U dock symbol</description></item>
    /// <item><term>191</term><description>U marina symbol</description></item>
    /// <item><term>192</term><description>U bait and tackle symbol</description></item>
    /// <item><term>193</term><description>U stump symbol</description></item>
    /// <item><term>7680</term><description>first user customisable symbol</description></item>
    /// <item><term>8191</term><description>last user customisable symbol</description></item>
    /// <item><term>8192</term><description>interstate hwy symbol</description></item>
    /// <item><term>8193</term><description>us hwy symbol</description></item>
    /// <item><term>8194</term><description>state hwy symbol</description></item>
    /// <item><term>8195</term><description>mile marker symbol</description></item>
    /// <item><term>8196</term><description>TracBack (feet) symbol</description></item>
    /// <item><term>8197</term><description>golf symbol</description></item>
    /// <item><term>8198</term><description>small city symbol</description></item>
    /// <item><term>8199</term><description>medium city symbol</description></item>
    /// <item><term>8200</term><description>large city symbol</description></item>
    /// <item><term>8201</term><description>intl freeway hwy symbol</description></item>
    /// <item><term>8202</term><description>intl national hwy symbol</description></item>
    /// <item><term>8203</term><description>capitol city symbol (star)</description></item>
    /// <item><term>8204</term><description>amusement park symbol</description></item>
    /// <item><term>8205</term><description>bowling symbol</description></item>
    /// <item><term>8206</term><description>car rental symbol</description></item>
    /// <item><term>8207</term><description>car repair symbol</description></item>
    /// <item><term>8208</term><description>fast food symbol</description></item>
    /// <item><term>8209</term><description>fitness symbol</description></item>
    /// <item><term>8210</term><description>movie symbol</description></item>
    /// <item><term>8211</term><description>museum symbol</description></item>
    /// <item><term>8212</term><description>pharmacy symbol</description></item>
    /// <item><term>8213</term><description>pizza symbol</description></item>
    /// <item><term>8214</term><description>post office symbol</description></item>
    /// <item><term>8215</term><description>RV park symbol</description></item>
    /// <item><term>8216</term><description>school symbol</description></item>
    /// <item><term>8217</term><description>stadium symbol</description></item>
    /// <item><term>8218</term><description>dept. store symbol</description></item>
    /// <item><term>8219</term><description>zoo symbol</description></item>
    /// <item><term>8220</term><description>convenience store symbol</description></item>
    /// <item><term>8221</term><description>live  heatre symbol</description></item>
    /// <item><term>8222</term><description>ramp intersection symbol</description></item>
    /// <item><term>8223</term><description>street intersection symbol</description></item>
    /// <item><term>8226</term><description>inspection/weigh station symbol</description></item>
    /// <item><term>8227</term><description>toll booth symbol</description></item>
    /// <item><term>8228</term><description>elevation point symbol</description></item>
    /// <item><term>8229</term><description>exit without services symbol</description></item>
    /// <item><term>8230</term><description>Geographic place name</description></item>
    /// <item><term>8231</term><description>Geographic place name</description></item>
    /// <item><term>8232</term><description>Geographic place name</description></item>
    /// <item><term>8233</term><description>bridge symbol</description></item>
    /// <item><term>8234</term><description>building symbol</description></item>
    /// <item><term>8235</term><description>cemetery symbol</description></item>
    /// <item><term>8236</term><description>church symbol</description></item>
    /// <item><term>8237</term><description>civil location symbol</description></item>
    /// <item><term>8238</term><description>crossing symbol</description></item>
    /// <item><term>8239</term><description>historical town symbol</description></item>
    /// <item><term>8240</term><description>levee symbol</description></item>
    /// <item><term>8241</term><description>military location symbol</description></item>
    /// <item><term>8242</term><description>oil field symbol</description></item>
    /// <item><term>8243</term><description>tunnel symbol</description></item>
    /// <item><term>8244</term><description>beach symbol</description></item>
    /// <item><term>8245</term><description>forest symbol</description></item>
    /// <item><term>8246</term><description>summit symbol</description></item>
    /// <item><term>8247</term><description>large ramp intersection symbol</description></item>
    /// <item><term>8248</term><description>large exit without services smbl</description></item>
    /// <item><term>8249</term><description>police/official badge symbol</description></item>
    /// <item><term>8250</term><description>gambling/casino symbol</description></item>
    /// <item><term>8251</term><description>snow skiing symbol</description></item>
    /// <item><term>8252</term><description>ice skating symbol</description></item>
    /// <item><term>8253</term><description>tow truck (wrecker) symbol</description></item>
    /// <item><term>8254</term><description>border crossing (port of entry)</description></item>
    /// <item><term>8255</term><description>goecache location symbol</description></item>
    /// <item><term>8256</term><description>found geocache symbol</description></item>
    /// <item><term>8257</term><description>Rhino contact symbol, "smiley"</description></item>
    /// <item><term>8258</term><description>Rhino contact symbol, "ball cap"</description></item>
    /// <item><term>8259</term><description>Rhino contact symbol, "big ear"</description></item>
    /// <item><term>8260</term><description>Rhino contact symbol, "spike"</description></item>
    /// <item><term>8261</term><description>Rhino contact symbol, "goatee"</description></item>
    /// <item><term>8262</term><description>Rhino contact symbol, "afro"</description></item>
    /// <item><term>8263</term><description>Rhino contact symbol, "dreads"</description></item>
    /// <item><term>8264</term><description>Rhino contact symbol, "female 1"</description></item>
    /// <item><term>8265</term><description>Rhino contact symbol, "female 2" </description></item>
    /// <item><term>8266</term><description>Rhino contact symbol, "female 3" </description></item>
    /// <item><term>8267</term><description>Rhino contact symbol, "ranger"</description></item>
    /// <item><term>8268</term><description>Rhino contact symbol, "kung fu"</description></item>
    /// <item><term>8269</term><description>Rhino contact symbol, "sumo"</description></item>
    /// <item><term>8270</term><description>Rhino contact symbol, "pirate"</description></item>
    /// <item><term>8271</term><description>Rhino contact symbol, "biker"</description></item>
    /// <item><term>8272</term><description>Rhino contact symbol, "alien"</description></item>
    /// <item><term>8273</term><description>Rhino contact symbol, "bug"</description></item>
    /// <item><term>8274</term><description>Rhino contact symbol, "cat"</description></item>
    /// <item><term>8275</term><description>Rhino contact symbol, "dog"</description></item>
    /// <item><term>8276</term><description>Rhino contact symbol, "pig"</description></item>
    /// <item><term>8282</term><description>water hydrant symbol</description></item>
    /// <item><term>8284</term><description>blue flag symbol</description></item>
    /// <item><term>8285</term><description>green flag symbol</description></item>
    /// <item><term>8286</term><description>red flag symbol</description></item>
    /// <item><term>8287</term><description>blue pin symbol</description></item>
    /// <item><term>8288</term><description>green pin symbol</description></item>
    /// <item><term>8289</term><description>red pin symbol</description></item>
    /// <item><term>8290</term><description>blue block symbol</description></item>
    /// <item><term>8291</term><description>green block symbol</description></item>
    /// <item><term>8292</term><description>red block symbol</description></item>
    /// <item><term>8293</term><description>bike trail symbol</description></item>
    /// <item><term>8294</term><description>red circle symbol</description></item>
    /// <item><term>8295</term><description>green circle symbol</description></item>
    /// <item><term>8296</term><description>blue circle symbol</description></item>
    /// <item><term>8299</term><description>blue diamond symbol</description></item>
    /// <item><term>8300</term><description>red oval symbol</description></item>
    /// <item><term>8301</term><description>green oval symbol</description></item>
    /// <item><term>8302</term><description>blue oval symbol</description></item>
    /// <item><term>8303</term><description>red rectangle symbol</description></item>
    /// <item><term>8304</term><description>green rectangle symbol</description></item>
    /// <item><term>8305</term><description>blue oval symbol</description></item>
    /// <item><term>8308</term><description>blue square symbol</description></item>
    /// <item><term>8309</term><description>red letter "A"</description></item>
    /// <item><term>8310</term><description>red letter "B"</description></item>
    /// <item><term>8311</term><description>red letter "C"</description></item>
    /// <item><term>8312</term><description>red letter "D"</description></item>
    /// <item><term>8313</term><description>green letter "A"</description></item>
    /// <item><term>8314</term><description>green letter "B"</description></item>
    /// <item><term>8315</term><description>green letter "C"</description></item>
    /// <item><term>8316</term><description>green letter "D"</description></item>
    /// <item><term>8317</term><description>blue letter "A"</description></item>
    /// <item><term>8318</term><description>blue letter "B"</description></item>
    /// <item><term>8319</term><description>blue letter "C"</description></item>
    /// <item><term>8320</term><description>blue letter "D"</description></item>
    /// <item><term>8321</term><description>red number "0"</description></item>
    /// <item><term>8322</term><description>red number "1"</description></item>
    /// <item><term>8323</term><description>red number "2"</description></item>
    /// <item><term>8324</term><description>red number "3"</description></item>
    /// <item><term>8325</term><description>red number "4"</description></item>
    /// <item><term>8326</term><description>red number "5"</description></item>
    /// <item><term>8327</term><description>red number "6"</description></item>
    /// <item><term>8328</term><description>red number "7"</description></item>
    /// <item><term>8329</term><description>red number "8"</description></item>
    /// <item><term>8330</term><description>red number "9"</description></item>
    /// <item><term>8331</term><description>green number "0"</description></item>
    /// <item><term>8332</term><description>green number "1"</description></item>
    /// <item><term>8333</term><description>green number "2"</description></item>
    /// <item><term>8334</term><description>green number "3"</description></item>
    /// <item><term>8335</term><description>green number "4"</description></item>
    /// <item><term>8336</term><description>green number "5"</description></item>
    /// <item><term>8337</term><description>green number "6"</description></item>
    /// <item><term>8338</term><description>green number "7"</description></item>
    /// <item><term>8339</term><description>green number "8"</description></item>
    /// <item><term>8340</term><description>green number "9"</description></item>
    /// <item><term>8341</term><description>blue number "0"</description></item>
    /// <item><term>8342</term><description>blue number "1"</description></item>
    /// <item><term>8343</term><description>blue number "2"</description></item>
    /// <item><term>8344</term><description>blue number "3"</description></item>
    /// <item><term>8345</term><description>blue number "4"</description></item>
    /// <item><term>8346</term><description>blue number "5"</description></item>
    /// <item><term>8347</term><description>blue number "6"</description></item>
    /// <item><term>8348</term><description>blue number "7"</description></item>
    /// <item><term>8349</term><description>blue number "8"</description></item>
    /// <item><term>8350</term><description>blue number "9"</description></item>
    /// <item><term>8351</term><description>blue triangle symbol</description></item>
    /// <item><term>8352</term><description>green triangle symbol</description></item>
    /// <item><term>8353</term><description>red triangle symbol</description></item>
    /// <item><term>8359</term><description>asian food symbol</description></item>
    /// <item><term>8360</term><description>deli symbol</description></item>
    /// <item><term>8361</term><description>italian food symbol</description></item>
    /// <item><term>8362</term><description>seafood symbol</description></item>
    /// <item><term>8363</term><description>steak symbol</description></item>
    /// <item><term>16384</term><description>airport symbol</description></item>
    /// <item><term>16385</term><description>intersection symbol</description></item>
    /// <item><term>16386</term><description>non-directional beacon symbol</description></item>
    /// <item><term>16387</term><description>VHF omni-range symbol</description></item>
    /// <item><term>16388</term><description>heliport symbol</description></item>
    /// <item><term>16389</term><description>private field symbol</description></item>
    /// <item><term>16390</term><description>soft field symbol</description></item>
    /// <item><term>16391</term><description>tall tower symbol</description></item>
    /// <item><term>16392</term><description>short tower symbol</description></item>
    /// <item><term>16393</term><description>glider symbol</description></item>
    /// <item><term>16394</term><description>ultralight symbol</description></item>
    /// <item><term>16395</term><description>parachute symbol</description></item>
    /// <item><term>16396</term><description>VOR/TACAN symbol</description></item>
    /// <item><term>16397</term><description>VOR-DME symbol</description></item>
    /// <item><term>16398</term><description>first approach fix</description></item>
    /// <item><term>16399</term><description>localizer outer marker</description></item>
    /// <item><term>16400</term><description>missed approach point</description></item>
    /// <item><term>16401</term><description>TACAN symbol</description></item>
    /// <item><term>16402</term><description>Seaplane Base</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>
    /// Magellan Symbol Sets (Product Id is shown in brackets)
    /// </b></para>
    /// <para>
    /// This tables below show which Magellan Icons the lowercase identifiers translate to for a specific device,
    /// the product ID, as returned by MagellaDevice.GetProductInfo.ProductId, is shown in brackets:
    /// </para>
    /// <para><b>Symbols Set 0</b></para>
    /// <para><b>Colortrak (19), Tracker (23), GPS 315/320 (24), MAP 410 (25) and Sportrak (37):</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Icon property)</term><description>Symbol Description</description></listheader>
    /// <item><term>a</term><description>filled circle</description></item>
    /// <item><term>b</term><description>box</description></item>
    /// <item><term>c</term><description>red buoy</description></item>
    /// <item><term>d</term><description>green buoy</description></item>
    /// <item><term>e</term><description>anchor</description></item>
    /// <item><term>f</term><description>rocks</description></item>
    /// <item><term>g</term><description>green daymark</description></item>
    /// <item><term>h</term><description>red daymark</description></item>
    /// <item><term>i</term><description>bell</description></item>
    /// <item><term>j</term><description>danger</description></item>
    /// <item><term>k</term><description>diver down</description></item>
    /// <item><term>l</term><description>fish</description></item>
    /// <item><term>m</term><description>house</description></item>
    /// <item><term>n</term><description>mark</description></item>
    /// <item><term>o</term><description>car</description></item>
    /// <item><term>p</term><description>tent</description></item>
    /// <item><term>q</term><description>boat</description></item>
    /// <item><term>r</term><description>food</description></item>
    /// <item><term>s</term><description>fuel</description></item>
    /// <item><term>t</term><description>tree</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Symbols Set 1</b></para>
    /// <para><b>NAV 6000 (18):</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Icon property)</term><description>Symbol Description</description></listheader>
    /// <item><term>a</term><description>filled circle</description></item>
    /// <item><term>b</term><description>fish</description></item>
    /// <item><term>c</term><description>buoy</description></item>
    /// <item><term>d</term><description>light</description></item>
    /// <item><term>e</term><description>anchor</description></item>
    /// <item><term>f</term><description>flag</description></item>
    /// <item><term>g</term><description>red daymark</description></item>
    /// <item><term>h</term><description>green daymark</description></item>
    /// <item><term>i</term><description>wreck</description></item>
    /// <item><term>j</term><description>house</description></item>
    /// <item><term>k</term><description>boat</description></item>
    /// <item><term>l</term><description>fuel</description></item>
    /// <item><term>m</term><description>danger</description></item>
    /// <item><term>n</term><description>diver down</description></item>
    /// <item><term>o</term><description>food</description></item>
    /// <item><term>p</term><description>w</description></item>
    /// <item><term>q</term><description>l</description></item>
    /// <item><term>r</term><description>r</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Symbols Set 2</b></para>
    /// <para><b>Meridian XL (5):</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Icon property)</term><description>Symbol Description</description></listheader>
    /// <item><term>a</term><description>filled circle</description></item>
    /// <item><term>b</term><description>flag right</description></item>
    /// <item><term>c</term><description>flag left</description></item>
    /// <item><term>d</term><description>diamond</description></item>
    /// <item><term>e</term><description>square</description></item>
    /// <item><term>f</term><description>filled square</description></item>
    /// <item><term>g</term><description>anchor</description></item>
    /// <item><term>h</term><description>banner</description></item>
    /// <item><term>i</term><description>fish</description></item>
    /// <item><term>j</term><description>crosshair</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Symbols Set 3</b></para>
    /// <para><b>Trailblazer XL (7):</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Icon property)</term><description>Symbol Description</description></listheader>
    /// <item><term>a</term><description>filled circle</description></item>
    /// <item><term>b</term><description>flag right</description></item>
    /// <item><term>c</term><description>flag left</description></item>
    /// <item><term>d</term><description>diamond</description></item>
    /// <item><term>e</term><description>square</description></item>
    /// <item><term>f</term><description>filled square</description></item>
    /// <item><term>g</term><description>anchor</description></item>
    /// <item><term>h</term><description>banner</description></item>
    /// <item><term>i</term><description>fish</description></item>
    /// <item><term>j</term><description>crosshair</description></item>
    /// <item><term>k</term><description>car</description></item>
    /// <item><term>l</term><description>house</description></item>
    /// <item><term>m</term><description>wine glass</description></item>
    /// <item><term>n</term><description>mountain</description></item>
    /// <item><term>o</term><description>sign</description></item>
    /// <item><term>p</term><description>tree</description></item>
    /// <item><term>q</term><description>water</description></item>
    /// <item><term>r</term><description>airplane</description></item>
    /// <item><term>s</term><description>deer�s head</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Symbols Set 4</b></para>
    /// <para><b>MAP 330 (30), ProMark 2 (35), Meridian New (33), Sportrak Map/Pro (36):</b></para>
    /// <list type="table">
    /// <listheader><term>Raw Value (Icon property)</term><description>Symbol Description</description></listheader>
    /// <item><term>a</term><description>crossed square</description></item>
    /// <item><term>b</term><description>box</description></item>
    /// <item><term>c</term><description>house</description></item>
    /// <item><term>d</term><description>aerial</description></item>
    /// <item><term>e</term><description>airport</description></item>
    /// <item><term>f</term><description>amusement park</description></item>
    /// <item><term>g</term><description>ATM</description></item>
    /// <item><term>h</term><description>auto repair</description></item>
    /// <item><term>i</term><description>boating</description></item>
    /// <item><term>j</term><description>camping</description></item>
    /// <item><term>k</term><description>exit ramp</description></item>
    /// <item><term>l</term><description>first aid</description></item>
    /// <item><term>m</term><description>nav aid</description></item>
    /// <item><term>n</term><description>buoy</description></item>
    /// <item><term>o</term><description>fuel</description></item>
    /// <item><term>p</term><description>garden</description></item>
    /// <item><term>q</term><description>golf</description></item>
    /// <item><term>r</term><description>hotel</description></item>
    /// <item><term>s</term><description>hunting/fishing</description></item>
    /// <item><term>t</term><description>large city</description></item>
    /// <item><term>u</term><description>lighthouse</description></item>
    /// <item><term>v</term><description>major city</description></item>
    /// <item><term>w</term><description>marina</description></item>
    /// <item><term>x</term><description>medium city</description></item>
    /// <item><term>y</term><description>museum</description></item>
    /// <item><term>z</term><description>obstruction</description></item>
    /// <item><term>aa</term><description>park</description></item>
    /// <item><term>ab</term><description>resort</description></item>
    /// <item><term>ac</term><description>restaurant</description></item>
    /// <item><term>ad</term><description>rock</description></item>
    /// <item><term>ae</term><description>scuba</description></item>
    /// <item><term>af</term><description>RV service</description></item>
    /// <item><term>ag</term><description>shooting</description></item>
    /// <item><term>ah</term><description>sight seeing</description></item>
    /// <item><term>ai</term><description>small city</description></item>
    /// <item><term>aj</term><description>sounding</description></item>
    /// <item><term>ak</term><description>sports arena</description></item>
    /// <item><term>al</term><description>tourist info</description></item>
    /// <item><term>am</term><description>truck service</description></item>
    /// <item><term>an</term><description>winery</description></item>
    /// <item><term>ao</term><description>wreck</description></item>
    /// <item><term>ap</term><description>zoo</description></item>
    ///</list>
    /// <br/><br/>
    /// <para><b>Magellan Devices</b></para>
    /// <para>The following Properties are normally used by the Magellan devices. 
    /// However when Waypoints form part of a route some may not be.
    /// </para>
    /// <list type="table">
    /// <item><term>Identifier</term><description>String</description></item>
    /// <item><term>Latitude</term><description>Double</description></item>
    /// <item><term>Longitude</term><description>Double</description></item>
    /// <item><term>Comment</term><description>String</description></item>
    /// <item><term>Icon</term><description>String</description></item>
    /// <item><term>Altitude</term><description>Single</description></item>
    /// </list>
    /// <br/><br/>
    /// <para><b>Garmin Devices</b></para>
    /// <para>
	/// In most cases not all of the Properties of the Waypoint object are used. The Properties
	/// actually used are determined by the protocol employed by the connected GPS Device.
	/// Below is a list of which Properties are supported by Protocol. Please note that even though a
	/// Property is identified as being supported does not mean that it will actually be used/populated.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D100</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// </list>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D101</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
    /// <para>
	/// The 'ProximityDistance' Property is valid only during Proximity Waypoint transfer.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D102</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
	/// <para>
	/// The 'ProximityDistance' Property is valid only during Proximity Waypoint transfer.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D103</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Display</term><description>Int16</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display symbol with waypoint name</description></item>
	/// <item><term>1</term><description>Display symbol by itself</description></item>
	/// <item><term>2</term><description>Display symbol with comment</description></item>
	/// </list>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D104</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term>
	/// <description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Display</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display no symbol</description></item>
	/// <item><term>1</term><description>Display symbol by itself</description></item>
	/// <item><term>2</term><description>Display symbol with waypoint name</description></item>
	/// <item><term>2</term><description>Display symbol with comment</description></item>
	/// </list>
	/// <para>
	/// The 'ProximityDistance' Property is valid only during Proximity Waypoint transfer.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D105</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// </list>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D106</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>Subclass</term><description>String</description></item>
	/// <item><term>Link</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>Zero</term><description>Indicates a user waypoint (Subclass is ignored)</description></item>
	/// <item><term>Non-Zero</term><description>Indicates a non-user waypoint (Subclass must be valid)</description></item>
	/// <item><term>2</term><description>Display symbol with comment</description></item>
	/// </list>
	/// <br/><br/>
	/// <para>
	/// For non-user waypoints (such as a city in the GPS map database), the GPS will provide a
	/// non-zero value in the 'Class' Property, and the 'Subclass' member will contain valid data to
	/// further identify the non-user waypoint. If the Host wishes to transfer this waypoint back to the
	/// GPS (as part of a route), the Host must leave the 'Class' and 'Subclass' Properties
	/// unmodified.
	/// </para>
	/// <para>
	/// For user waypoints, the Host must ensure that the 'Class' Property is zero (default value), the
	/// 'Subclass' Property will be ignored.
	/// </para>
	/// <para>
	/// The 'Link' Property provides a string that indicates the name of the path from the previous
	/// waypoint in the route to this one. An empty string indicates that no particular path is specified.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D107</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Colour</term><description>int</description></item>
	/// <item><term>Display</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display symbol with waypoint name</description></item>
	/// <item><term>1</term><description>Display symbol by itself</description></item>
	/// <item><term>2</term><description>Display symbol with comment</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Colour' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Symbol</description></listheader>
	/// <item><term>0</term><description>Default waypoint colour</description></item>
	/// <item><term>1</term><description>Red</description></item>
	/// <item><term>2</term><description>Green</description></item>
	/// <item><term>3</term><description>Blue</description></item>
	/// </list>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D108</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Colour</term><description>int</description></item>
	/// <item><term>Display</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Depth</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>Subclass</term><description>String</description></item>
	/// <item><term>Attributes</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Address</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// <item><term>Crossroad</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0x00</term><description>User Waypoint</description></item>
	/// <item><term>0x40</term><description>Aviation Airport Waypoint</description></item>
	/// <item><term>0x41</term><description>Aviation Intersection Waypoint</description></item>
	/// <item><term>0x42</term><description>Aviation NDB Waypoint</description></item>
	/// <item><term>0x43</term><description>Aviation VOR Waypoint</description></item>
	/// <item><term>0x44</term><description>Aviation Airport Runway Waypoint</description></item>
	/// <item><term>0x45</term><description>Aviation Airport Intersection Waypoint</description></item>
	/// <item><term>0x46</term><description>Aviation Airport NDB Waypoint</description></item>
	/// <item><term>0x80</term><description>Map Point Waypoint</description></item>
	/// <item><term>0x81</term><description>Map Area Waypoint</description></item>
	/// <item><term>0x82</term><description>Map Intersection Waypoint</description></item>
	/// <item><term>0x83</term><description>Map Address Waypoint</description></item>
	/// <item><term>0x84</term><description>Map Line Waypoint</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Colour' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Symbol</description></listheader>
	/// <item><term>0x00</term><description>Black</description></item>
	/// <item><term>0x01</term><description>Dark Red</description></item>
	/// <item><term>0x02</term><description>Dark Green</description></item>
	/// <item><term>0x03</term><description>Dark Yellow</description></item>
	/// <item><term>0x04</term><description>Dark Blue</description></item>
	/// <item><term>0x05</term><description>Dark Magenta</description></item>
	/// <item><term>0x06</term><description>Dark Cyan</description></item>
	/// <item><term>0x07</term><description>Light Grey</description></item>
	/// <item><term>0x08</term><description>Dark Grey</description></item>
	/// <item><term>0x09</term><description>Red</description></item>
	/// <item><term>0x0A</term><description>Green</description></item>
	/// <item><term>0x0B</term><description>Yellow</description></item>
	/// <item><term>0x0C</term><description>Blue</description></item>
	/// <item><term>0x0D</term><description>Magenta</description></item>
	/// <item><term>0x0E</term><description>Cyan</description></item>
	/// <item><term>0x0F</term><description>White</description></item>
	/// <item><term>0xFF</term><description>Default colour</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display symbol with waypoint name</description></item>
	/// <item><term>1</term><description>Display symbol by itself</description></item>
	/// <item><term>2</term><description>Display symbol with comment</description></item>
	/// </list>
	/// <para>
	/// The 'Attributes' Property should be set to a value of 0x60. This is the default setting for this
	/// Property on versions after 2.30 and does not need to
	/// be explicitly set.
	/// </para>
	/// <para>
	/// The 'Sub Class' Property is used for map waypoints only, and should be set to "00 00 00 00
	/// 00 00 FF FF FF FF FF FF FF FF FF FF FF FF" for other classes of waypoints.
	/// </para>
	/// <para>
	/// The 'Altitude' and 'Depth' Properties may or may not be supported on a given unit. A value of
	/// 1.0e25 in either of these Properties indicates that this parameter is not supported or is
	/// unknown for this waypoint.
	/// </para>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <para>
	/// The 'Comment' Property is used for user waypoints only, and should be an empty string for
	/// other waypoint classes.
	/// </para>
	/// <para>
	/// The 'Facility' and 'City' Properties are used only for aviation waypoints, and should be empty
	/// strings for other waypoint classes.
	/// </para>
	/// <para>
	/// The 'Address' Property is only valid for 'Map Address Waypoint' class waypoints and should
	/// be an empty string for other waypoint classes.
	/// </para>
	/// <para>
	/// The 'Crossroad' member is valid only for 'Map Intersection Waypoint' class waypoints, and
	/// should be an empty string for other waypoint classes.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D109</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Colour</term><description>int</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Depth</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>Subclass</term><description>String</description></item>
	/// <item><term>Attributes</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Address</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// <item><term>Crossroad</term><description>String</description></item>
	/// <item><term>PacketType</term><description>Int16</description></item>
	/// <item><term>TimeENRoute</term><description>int</description></item>
	/// </list>
	/// <para>
	/// All fields are defined the same as D108 except as noted below.
	/// </para>
	/// <para>
	/// The 'PacketType' Property, must be set to 0x01.
	/// </para>
	/// <para>
	/// The 'Colour' Property contains three fields; bits 0-4 specify the colour, bits 5-6 specify the
	/// waypoint display attribute and bit 7 is unused and must be 0. Colour values are as specified
	/// for D108 except that the default value is 0x1f.
	/// </para>
	/// <para>
	/// The 'Attribute' Property must be set to 0x70 for D109. This is the default setting for this
	/// Property on versions after 2.30 and does not need to
	/// be explicitly set.
	/// </para>
	/// <para>
	/// The 'TimeENRoute' is the estimated time in seconds to the next waypoint the default value is
	/// 0xFFFFFFFF.	
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D110</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Colour</term><description>int</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Depth</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>Subclass</term><description>String</description></item>
	/// <item><term>Attributes</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Address</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// <item><term>Crossroad</term><description>String</description></item>
	/// <item><term>PacketType</term><description>Int16</description></item>
	/// <item><term>TimeENRoute</term><description>int</description></item>
	/// <item><term>Temperature</term><description>Single</description></item>
	/// <item><term>Timestamp</term><description>int</description></item>
	/// <item><term>Category</term><description>Int16</description></item>
	/// </list>
	/// <para>
	/// All fields are defined the same as D109 except as noted below.
	/// </para>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0x00</term><description>User Waypoint</description></item>
	/// <item><term>0x40</term><description>Aviation Airport Waypoint</description></item>
	/// <item><term>0x41</term><description>Aviation Intersection Waypoint</description></item>
	/// <item><term>0x42</term><description>Aviation NDB Waypoint</description></item>
	/// <item><term>0x43</term><description>Aviation VOR Waypoint</description></item>
	/// <item><term>0x44</term><description>Aviation Airport Runway Waypoint</description></item>
	/// <item><term>0x45</term><description>Aviation Airport Intersection Waypoint</description></item>
	/// <item><term>0x46</term><description>Aviation Airport NDB Waypoint</description></item>
	/// <item><term>0x80</term><description>Map Point Waypoint</description></item>
	/// <item><term>0x81</term><description>Map Area Waypoint</description></item>
	/// <item><term>0x82</term><description>Map Intersection Waypoint</description></item>
	/// <item><term>0x83</term><description>Map Address Waypoint</description></item>
	/// <item><term>0x84</term><description>Map Line Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Category' Property may not be supported by all units, the default value is 0. 
	/// </para>
	/// <para>
	/// The Temperature Property may not be supported by all units. A value of 1.0e25 in this
	/// field indicates that this parameter is not supported or is unknown for this waypoint.
	/// </para>
	/// <para>
	/// The Timestamp Property may not be supported by all units. A value of 0xFFFFFFFF in this
	/// field indicates that this parameter is not supported or is unknown for this waypoint.
	/// </para>
	/// <para>
	/// The 'Attribute' Property must be set to 0x80. This is the default setting for this
	/// Property and does not need to be explicitly set.
	/// </para>
	/// <para>
	/// The Colour Property contains three fields; bits 0-4 specify the color, bits 5-6 specify
	/// the waypoint display attribute and bit 7 is unused and must be 0. Valid color values are
	/// specified below. If an invalid color value is received, the value will be Black. 
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Symbol</description></listheader>
	/// <item><term>0x00</term><description>Black</description></item>
	/// <item><term>0x01</term><description>Dark Red</description></item>
	/// <item><term>0x02</term><description>Dark Green</description></item>
	/// <item><term>0x03</term><description>Dark Yellow</description></item>
	/// <item><term>0x04</term><description>Dark Blue</description></item>
	/// <item><term>0x05</term><description>Dark Magenta</description></item>
	/// <item><term>0x06</term><description>Dark Cyan</description></item>
	/// <item><term>0x07</term><description>Light Grey</description></item>
	/// <item><term>0x08</term><description>Dark Grey</description></item>
	/// <item><term>0x09</term><description>Red</description></item>
	/// <item><term>0x0A</term><description>Green</description></item>
	/// <item><term>0x0B</term><description>Yellow</description></item>
	/// <item><term>0x0C</term><description>Blue</description></item>
	/// <item><term>0x0D</term><description>Magenta</description></item>
	/// <item><term>0x0E</term><description>Cyan</description></item>
	/// <item><term>0x0F</term><description>White</description></item>
	/// <item><term>0x10</term><description>Transparent</description></item>
	/// </list>
	/// <para>
	/// Valid display attribute values are as shown below. If an invalid display attribute value is received, the value will be Name.
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Symbol</description></listheader>
	/// <item><term>0</term><description>Display symbol with waypoint name (name)</description></item>
	/// <item><term>1</term><description>Display symbol by itself (None)</description></item>
	/// <item><term>2</term><description>DDisplay symbol with comment (Comment)</description></item>
	/// </list>
	///<para>
	/// If a D110 waypoint is received that contains a value in the Latitude Property that is greater
	/// than 2^30 or less than -2^30, then that waypoint will be rejected.
	///</para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D150</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Airport Waypoint</description></item>
	/// <item><term>1</term><description>Intersection Waypoint</description></item>
	/// <item><term>2</term><description>NDB Waypoint</description></item>
	/// <item><term>3</term><description>VOR Waypoint</description></item>
	/// <item><term>4</term><description>User Defined Waypoint</description></item>
	/// <item><term>5</term><description>Airport Runway Threshold Waypoint</description></item>
	/// <item><term>6</term><description>Airport Intersection Waypoint</description></item>
	/// <item><term>7</term><description>Locked Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Locked Waypoint' class code indicates that a route within a GPS contains
	/// an aviation database waypoint that the GPS could not find in its aviation
	/// database (perhaps because the aviation database was updated to a newer version).
	/// The Host should never send this code to the GPS.
	/// </para>
	/// <para>
	/// The 'City', 'State', 'Facility' and 'Country' Properties are invalid when the 
	/// 'Class' Property value is equal to the 'User Defined Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'Altitude' Property is valid only when the 'Class' Property value is equal 
	/// to the 'Airport Waypoint' class code.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D151</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Airport Waypoint</description></item>
	/// <item><term>1</term><description>VOR Waypoint</description></item>
	/// <item><term>2</term><description>User Defined Waypoint</description></item>
	/// <item><term>3</term><description>Locked Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Locked Waypoint' class code indicates that a route within a GPS contains
	/// an aviation database waypoint that the GPS could not find in its aviation
	/// database (perhaps because the aviation database was updated to a newer version).
	/// The Host should never send this code to the GPS.
	/// </para>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <para>
	/// The 'City', 'State', 'Facility' and 'Country' Properties are invalid when the 
	/// 'Class' Property value is equal to the 'User Defined Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'Altitude' Property is valid only when the 'Class' Property value is equal 
	/// to the 'Airport Waypoint' class code.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D152</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Airport Waypoint</description></item>
	/// <item><term>1</term><description>Intersection Waypoint</description></item>
	/// <item><term>2</term><description>NDB Waypoint</description></item>
	/// <item><term>3</term><description>VOR Waypoint</description></item>
	/// <item><term>4</term><description>User Defined Waypoint</description></item>
	/// <item><term>5</term><description>Locked Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Locked Waypoint' class code indicates that a route within a GPS contains
	/// an aviation database waypoint that the GPS could not find in its aviation
	/// database (perhaps because the aviation database was updated to a newer version).
	/// The Host should never send this code to the GPS.
	/// </para>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <para>
	/// The 'City', 'State', 'Facility' and 'Country' Properties are invalid when the 
	/// 'Class' Property value is equal to the 'User Defined Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'Altitude' Property is valid only when the 'Class' Property value is equal 
	/// to the 'Airport Waypoint' class code.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D154</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Airport Waypoint</description></item>
	/// <item><term>1</term><description>Intersection Waypoint</description></item>
	/// <item><term>2</term><description>NDB Waypoint</description></item>
	/// <item><term>3</term><description>VOR Waypoint</description></item>
	/// <item><term>4</term><description>User Defined Waypoint</description></item>
	/// <item><term>5</term><description>Airport Runway Threshold Waypoint</description></item>
	/// <item><term>6</term><description>Airport Intersection Waypoint</description></item>
	/// <item><term>7</term><description>Airport NDB Waypoint</description></item>
	/// <item><term>8</term><description>User Defined Symbol-Only Waypoint</description></item>
	/// <item><term>9</term><description>Locked Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Locked Waypoint' class code indicates that a route within a GPS contains
	/// an aviation database waypoint that the GPS could not find in its aviation
	/// database (perhaps because the aviation database was updated to a newer version).
	/// The Host should never send this code to the GPS.
	/// </para>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <para>
	/// The 'City', 'State', 'Facility' and 'Country' Properties are invalid when the 
	/// 'Class' Property value is equal to the 'User Defined Symbol Only Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'Altitude' Property is valid only when the 'Class' Property value is equal 
	/// to the 'Airport Waypoint' class code.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D155</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Display</term><description>Int16</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display Symbol Only</description></item>
	/// <item><term>3</term><description>Display Symbol with Waypoint Name</description></item>
	/// <item><term>5</term><description>Display Symbol with Comment</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Airport Waypoint</description></item>
	/// <item><term>1</term><description>Intersection Waypoint</description></item>
	/// <item><term>2</term><description>NDB Waypoint</description></item>
	/// <item><term>3</term><description>VOR Waypoint</description></item>
	/// <item><term>4</term><description>User Defined Waypoint</description></item>
	/// <item><term>9</term><description>Locked Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Locked Waypoint' class code indicates that a route within a GPS contains
	/// an aviation database waypoint that the GPS could not find in its aviation
	/// database (perhaps because the aviation database was updated to a newer version).
	/// The Host should never send this code to the GPS.
	/// </para>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <para>
	/// The 'City', 'State', 'Facility' and 'Country' Properties are invalid when the 
	/// 'Class' Property value is equal to the 'User Defined Symbol Only Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'Altitude' Property is valid only when the 'Class' Property value is equal 
	/// to the 'Airport Waypoint' class code.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D400</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D403</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Symbol</term><description>Int16</description></item>
	/// <item><term>Display</term><description>Int16</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Symbol' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Symbol</description></listheader>
	/// <item><term>0</term><description>dot</description></item>
	/// <item><term>1</term><description>house</description></item>
	/// <item><term>2</term><description>gas</description></item>
	/// <item><term>3</term><description>car</description></item>
	/// <item><term>4</term><description>fish</description></item>
	/// <item><term>5</term><description>boat</description></item>
	/// <item><term>6</term><description>anchor</description></item>
	/// <item><term>7</term><description>wreck</description></item>
	/// <item><term>8</term><description>exit</description></item>
	/// <item><term>9</term><description>skull</description></item>
	/// <item><term>10</term><description>flag</description></item>
	/// <item><term>11</term><description>camp</description></item>
	/// <item><term>12</term><description>circle with x </description></item>
	/// <item><term>13</term><description>deer</description></item>
	/// <item><term>14</term><description>first aid</description></item>
	/// <item><term>15</term><description>back track</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display symbol with waypoint name</description></item>
	/// <item><term>1</term><description>Display symbol by itself</description></item>
	/// <item><term>2</term><description>Display symbol with comment</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Display' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Display Symbol with Waypoint Name</description></item>
	/// <item><term>1</term><description>Display Symbol Only</description></item>
	/// <item><term>2</term><description>Display Symbol with Comment</description></item>
	/// </list>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D450</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>Identifier</term><description>String</description></item>
	/// <item><term>Latitude</term><description>Double</description></item>
	/// <item><term>Longitude</term><description>Double</description></item>
	/// <item><term>Comment</term><description>String</description></item>
	/// <item><term>Altitude</term><description>Single</description></item>
	/// <item><term>Class</term><description>Int16</description></item>
	/// <item><term>State</term><description>String</description></item>
	/// <item><term>Country</term><description>String</description></item>
	/// <item><term>City</term><description>String</description></item>
	/// <item><term>Facility</term><description>String</description></item>
	/// <item><term>ProximityDistance</term><description>Single</description></item>
	/// </list>
	/// <para>
	/// The values for the 'Class' Property are defined as follows:
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0</term><description>Airport Waypoint</description></item>
	/// <item><term>1</term><description>Intersection Waypoint</description></item>
	/// <item><term>2</term><description>NDB Waypoint</description></item>
	/// <item><term>3</term><description>VOR Waypoint</description></item>
	/// <item><term>4</term><description>User Defined Waypoint</description></item>
	/// <item><term>5</term><description>Airport Runway Threshold Waypoint</description></item>
	/// <item><term>6</term><description>Airport Intersection Waypoint</description></item>
	/// <item><term>7</term><description>Locked Waypoint</description></item>
	/// </list>
	/// <para>
	/// The 'Locked Waypoint' class code indicates that a route within a GPS contains
	/// an aviation database waypoint that the GPS could not find in its aviation
	/// database (perhaps because the aviation database was updated to a newer version).
	/// The Host should never send this code to the GPS.
	/// </para>
	/// <para>
	/// The 'City', 'State', 'Facility' and 'Country' Properties are invalid when the 
	/// 'Class' Property value is equal to the 'User Defined Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'Altitude' Property is valid only when the 'Class' Property value is equal 
	/// to the 'Airport Waypoint' class code.
	/// </para>
	/// <para>
	/// The 'ProximityDistance' Property is used during Proximity Waypoint transfer, and should be
	/// set to zero in all other cases.
	/// </para>
	/// </remarks>

	public class Waypoint : DataObjectBase
	{
        private const string XML_ROOT = "gpswaypoint";
        private const string XML_IDENTIFIER = "identifier";
        private const string XML_LATITUDE = "latitude";
        private const string XML_LONGITUDE = "longitude";
        private const string XML_COMMENT = "comment";
        private const string XML_SYMBOL = "symbol";
        private const string XML_ICON = "icon";
        private const string XML_COLOUR = "colour";
        private const string XML_DISPLAY = "display";
        private const string XML_PROX_DIST = "proximitydistance";
        private const string XML_PROX_INDEX = "proximityindex";
        private const string XML_ALTITUDE = "altitude";
        private const string XML_DEPTH = "depth";
        private const string XML_CLASS = "class";
        private const string XML_SUB_CLASS = "Subclass";
        private const string XML_ATTRIBUTES = "attributes";
        private const string XML_LINK = "link";
        private const string XML_ROUTE_LINK_CLASS = "routelinkclass";
        private const string XML_ROUTE_LINK_SUB_CLASS = "routelinksubclass";
        private const string XML_ROUTE_LINK_INDENTIFIER = "routelinkidentifier";
        private const string XML_STATE = "state";
        private const string XML_COUNTRY = "country";
        private const string XML_CITY = "city";
        private const string XML_ADDRESS = "address";
        private const string XML_FACILITY = "facility";
        private const string XML_CROSS_ROAD = "crossroad";
        private const string XML_UNUSED_1 = "unused1";
        private const string XML_UNUSED_2 = "unused2";
        private const string XML_PACKETTYPE = "packettype";
        private const string XML_TIME_EN_ROUTE = "timeenroute";
        private const string XML_TEMPERATURE = "temperature";
        private const string XML_TIMESTAMP = "timestamp";
        private const string XML_CATEGORY = "category";

        private const string XML_ROUTE_LINK_SUB_CLASS_BYTE = "routelinksubclassbyte";	//used fro the Subclass and route Subclass child nodes
        private const string XML_SUB_CLASS_BYTE = "subclassbyte";	//used fro the Subclass and route Subclass child nodes
        private const string XML_INDEX = "index";	//used fro the Subclass and route Subclass child nodes
        private const string ERR_MSG_4000 = "This function is not supported in the Personal Edition.";


		//these three are used internally for storing shape file coordinates where a transformation has taken place
		private double m_y = 0;
		private double m_x = 0;
		internal bool Transformed = false;

		private int m_hashCode;
		private double m_latitude = 0;
		private double m_longitude = 0;
        private string m_identifier = "";
        private string m_comment = "";
        private short m_symbol = 0;
        private int m_colour = 0;
        private short m_display = 0;
        private float m_proximityDistance = 0;
        private short m_proximityIndex = 0;
        private float m_altitude = 0;
        private float m_depth = 0;
        private short m_class = 0;
        private short m_attributes = 0;
        private string m_link = "";
        private string m_state = "";
        private string m_country = "";
        private string m_city = "";
        private string m_address = "";
        private string m_facility = "";
        private string m_crossRoad = "";
        private int m_unused1 = 0;
        private int m_unused2 = 0;
        private short m_packetType = 0;
        private int m_timeEnRoute = 0;
        private float m_temperature = 0;
        private short m_category = 0;
        private int m_timeStamp = 0;
        private short m_routeLinkClass = 0;
        private string m_routeLinkIdentifier = "";
        private string m_icon = "";

		private Subclass m_subclass = new Subclass();
        private Subclass m_routeLinkSubclass = new Subclass();

        private SymbolId m_symbolId = SymbolId.Unknown;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Waypoint()
        {
        }

		/// <summary>
		/// Returns/Sets the Identifier for the Waypoint. 
		/// </summary>
        public string Identifier
        {
            get
            {
                return m_identifier;
            }
            set
            {
                m_identifier = value;
            }
        }
		/// <summary>
		/// Returns/Sets Comment for the Waypoint.
		/// </summary>
        public string Comment
        {
            get
            {
                return m_comment;
            }
            set
            {
                m_comment = value;

            }
        }

		/// <summary>
		/// Returns/Sets the raw value representing the symbol. This property is NOT used on Magellan units,
        /// the Icon property (String) is used instead.
		/// </summary>
        /// <remarks>
        /// <para>
        /// Please see the Waypoint Overview for ways to manage symbols and icons for devices.
        /// </para>
        /// </remarks>
        public short Symbol
        {
            get
            {
                return m_symbol;
            }
            set
            {
                m_symbol = value;

            }
        }
        /// <summary>
        /// Returns/Sets the Symbol Identifier
        /// </summary>
        public SymbolId SymbolIdentifier
        {
            get
            {
                return m_symbolId;
            }
            set 
            {
                m_symbolId = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Waypoint Colour.
		/// (See GPSLibrary Developer Notes for further details). 
		/// </summary>
        public int Colour
        {
            get
            {
                return m_colour;
            }
            set
            {
                m_colour = value;
            }
        }

		/// <summary>
		/// Returns/Sets the Waypoint Display.
		/// (See GPSLibrary Developer Notes for further details). 
		/// </summary>
		public short Display
        {
            get
            {
                return m_display;
            }
            set
            {
                m_display = value;
            }
        }
		/// <summary>
		/// Returns/Sets the proximity distance in meters
		/// of the Waypoint. Only valid if the Waypoint is a Proximity Waypoint. 
		/// </summary>
        public float ProximityDistance
        {
            get
            {
                return m_proximityDistance;
            }
            set
            {
                m_proximityDistance = value;
            }
        }
		/// <summary>
		/// Returns/Sets the proximity index of the Waypoint. 
		/// Only valid if the Waypoint is a Proximity Waypoint.
		/// </summary>
		public short ProximityIndex
        {
            get
            {
                return m_proximityIndex;
            }
            set
            {
                m_proximityIndex = value;
            }
        }
		/// <summary>
		/// Returns/Sets the altitude of the Waypoint in meters.
		/// </summary>
		public float Altitude
        {
            get
            {
                return m_altitude;
            }
            set
            {
                m_altitude = value;
            }
        }
		/// <summary>
		/// Return/Setss the depth of the Waypoint in meters.
		/// </summary>
		public float Depth
        {
            get
            {
                return m_depth;
            }
            set
            {
                m_depth = value;
            }
        }
		/// <summary>
		/// Returns/Sets the class 
		/// (See GPSLibrary Developer Notes for further details). 
		/// </summary>
		public short Class
        {
            get
            {
                return m_class;
            }
            set
            {
                m_class = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Waypoint attribute setting. 
		/// </summary>
		public short Attributes
        {
            get
            {
                return m_attributes;
            }
            set
            {
                m_attributes = value;
            }
        }
		/// <summary>
		/// Returns/Sets the path from a previous Waypoint in the route to this one.
		/// </summary>
		public string Link
        {
            get
            {
                return m_link;
            }
            set
            {
                m_link = value;
            }
        }
		/// <summary>
		/// Returns the State part of the Waypoint address.
		/// </summary>
		public string State
        {
            get
            {
                return m_state;
            }
            set
            {
                m_state = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Country part of the Waypoint address.
		/// </summary>
		public string Country
        {
            get
            {
                return m_country;
            }
            set
            {
                m_country = value;
            }
        }
		/// <summary>
		/// Returns/Sets the City part of the Waypoint address.
		/// </summary>
		public string City
        {
            get
            {
                return m_city;
            }
            set
            {
                m_city = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Address of the Waypoint. 
		/// </summary>
		public string Address
        {
            get
            {
                return m_address;
            }
            set
            {
                m_address = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Facility part of the Waypoint address. 
		/// </summary>
		public string Facility
        {
            get
            {
                return m_facility;
            }
            set
            {
                m_facility = value;
            }
        }
		/// <summary>
		/// Returns/Sets the intersection address.
		/// </summary>
		public string Crossroad
        {
            get
            {
                return m_crossRoad;
            }
            set
            {
                m_crossRoad = value;
            }
        }
		/// <summary>
		/// Unused field.
		/// </summary>
		internal int Unused1
        {
            get
            {
                return m_unused1;
            }
            set
            {
                m_unused1 = value;
            }
        }
		/// <summary>
		/// Unused field.
		/// </summary>
		internal int Unused2
        {
            get
            {
                return m_unused2;
            }
            set
            {
                m_unused2 = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Packet type. Used for data type D109 only. 
		/// (See GPSLibrary Developer Notes for further details). 
		/// </summary>
		public short PacketType
        {
            get
            {
                return m_packetType;
            }
            set
            {
                m_packetType = value;
            }
        }
		/// <summary>
		/// Returns/Sets estimated 'time en route' to next waypoint in seconds. 
		/// </summary>
		public int TimeENRoute
        {
            get
            {
                return m_timeEnRoute;
            }
            set
            {
                m_timeEnRoute = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Temperature Property. 
		/// </summary>
		public float Temperature
        {
            get
            {
                return m_temperature;
            }
            set
            {
                m_temperature = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Timestamp Property. 
		/// </summary>
		public short Category
        {
            get
            {
                return m_category;
            }
            set
            {
                m_category = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Category Property.
		/// </summary>
		public int Timestamp
        {
            get
            {
                return m_timeStamp;
            }
            set
            {
                m_timeStamp = value;
            }
        }
		/// <summary>
		/// Returns/Sets the Route Link Class. This Property describes the link that 
		/// this waypoint has with the Waypoint that precedes it. Not used on all GPS devices. 
		/// (See GPSLibrary Developer Notes for further details). 
		/// </summary>
		public short RouteLinkClass //these are populated from the route link data if there is any
        {
            get
            {
                return m_routeLinkClass;
            }
            set
            {
                m_routeLinkClass = value;
            }
        }
        /// <summary>
		/// Returns/Sets the Route Link Identifier. This Property describes the link that 
		/// this waypoint has with the Waypoint that precedes it. Not used on all GPS devices. 
		/// (See GPSLibrary Developer Notes for further details). 
		/// </summary>
		public string RouteLinkIdentifier
        {
            get
            {
                return m_routeLinkIdentifier;
            }
            set
            {
                m_routeLinkIdentifier = value;
            }
        }

//		private string m_strSubClass = "";
//		private string m_strRouteLinkSubClass = "";


//		private const string DEFAULT_SUB_CLASS = "00 00 00 00 00 00 FF FF FF FF FF FF FF FF FF FF FF FF";
//		private const string DEFAULT_SUB_CLASS_STORED = "\x0\x0\x0\x0\x0\x0\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";

		/// <summary>
		/// Returns/Sets the Latitude co-ordinate in degrees. 
		/// </summary>
		public double Latitude
		{
			get
			{
				return m_latitude;
			}
			set
			{
				m_latitude = value;
					m_latitude = m_latitude % 90;
			}
		}

		/// <summary>
		/// Returns/Sets the Longtitude co-ordinate in degrees.
		/// </summary>
		public double Longitude
		{
			get
			{
				return m_longitude;
			}
			set
			{
				m_longitude = value;
				m_longitude = m_longitude % 180;
			}
		}
        /// <summary>
        /// Returns/Sets the X co-ordinate. 
        /// </summary>
        public double X
        {
            get
            {
                return m_x;
            }
            set
            {
                m_x = value;
                Transformed = true;
            }
        }

        /// <summary>
        /// Returns/Sets the Y co-ordinate.
        /// </summary>
        public double Y
        {
            get
            {
                return m_y;
            }
            set
            {
                m_y = value;
                Transformed = true;
            }
        }


        /// <summary>
        /// Returns/Sets the raw value representing the icon. This property is NOT used on Garmin units,
        /// the Symbol property (Int16) is used instead.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Please see the Waypoint Overview for ways to manage symbols and icons for devices.
        /// </para>
        /// </remarks>
        public string Icon
        {
            get
            {
                return m_icon;
            }
            set
            {
                m_icon = value;
            }
        }
		/// <summary>
		/// Returns an XML representation of the object.
		/// </summary>
		public string ToXml()
		{
			XmlDocument objXMLDOM = new XmlDocument();

			XmlElement objRootNode = null;
			XmlElement objChildNode = null;
			XmlElement objByteNode = null;
			
			try
			{
				//create the root node
				objRootNode = objXMLDOM.CreateElement(XML_ROOT);
			
				//append the root node to the document
				objXMLDOM.AppendChild(objRootNode);

				//create the child nodes
				AddXMLNode(objRootNode, XML_IDENTIFIER, Identifier, true);
				AddXMLNode(objRootNode, XML_LATITUDE, Latitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_LONGITUDE, Longitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_COMMENT, Comment, true);
				AddXMLNode(objRootNode, XML_SYMBOL, Symbol.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_ICON, Icon, false);//although its a string it doesn't need a cdata (magellan only)
				AddXMLNode(objRootNode, XML_COLOUR, Colour.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_DISPLAY, Display.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_PROX_DIST, ProximityDistance.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_PROX_INDEX, ProximityIndex.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_ALTITUDE, Altitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_DEPTH, Depth.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_CLASS, Class.ToString(CultureInfo.InvariantCulture), false);

				objChildNode = AddXMLNode(objRootNode, XML_SUB_CLASS, "", false);
				for(int f = 0; f < m_subclass.Length; f++)
				{
					objByteNode = AddXMLNode(objChildNode, XML_SUB_CLASS_BYTE, m_subclass[f].ToString(CultureInfo.InvariantCulture), false);
					objByteNode.SetAttribute(XML_INDEX,f.ToString("00"));

				}

				AddXMLNode(objRootNode, XML_ATTRIBUTES, Attributes.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_LINK, Link, true);
				AddXMLNode(objRootNode, XML_ROUTE_LINK_CLASS, RouteLinkClass.ToString(CultureInfo.InvariantCulture), false);

				objChildNode = AddXMLNode(objRootNode, XML_ROUTE_LINK_SUB_CLASS, "", false);
				for(int f = 0; f < m_routeLinkSubclass.Length; f++)
				{
					objByteNode = AddXMLNode(objChildNode, XML_ROUTE_LINK_SUB_CLASS_BYTE, m_routeLinkSubclass[f].ToString(CultureInfo.InvariantCulture) , false);
					objByteNode.SetAttribute(XML_INDEX,f.ToString("00"));
				}

				AddXMLNode(objRootNode, XML_ROUTE_LINK_INDENTIFIER, RouteLinkIdentifier, true);
				AddXMLNode(objRootNode, XML_STATE, State, true);
				AddXMLNode(objRootNode, XML_COUNTRY, Country, true);
				AddXMLNode(objRootNode, XML_CITY, City, true);
				AddXMLNode(objRootNode, XML_ADDRESS, Address, true);
				AddXMLNode(objRootNode, XML_FACILITY, Facility, true);
				AddXMLNode(objRootNode, XML_CROSS_ROAD, Crossroad, true);
				//AddXMLNode(objRootNode, XML_UNUSED_1, Unused1.ToString(CultureInfo.InvariantCulture), false);
				//AddXMLNode(objRootNode, XML_UNUSED_2, Unused2.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_PACKETTYPE, PacketType.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_TIME_EN_ROUTE, TimeENRoute.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_TEMPERATURE, Temperature.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_TIMESTAMP, Timestamp.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_CATEGORY, Category.ToString(CultureInfo.InvariantCulture), false);
			}
            catch (NullReferenceException e)
            {
				throw new XmlException(e.Message, e);
			}

			return objXMLDOM.OuterXml;
		}

		/// <summary>
		/// This method populates the object from XML.
		/// </summary>
		public void XmlLoad(string xml)
		{
			string strXPathRoot = "";
			XmlDocument objDOM = new XmlDocument();
			XmlNode objNode = null;

			try
			{
				objDOM.LoadXml(xml);

				if(objDOM.FirstChild.Name == XML_ROOT)
				{
					strXPathRoot = string.Concat("//" , XML_ROOT , "/");

					//need to ignore type conversion errors and missing xml elements
					Identifier = ReadXMLNodeAsString(objDOM,string.Concat(strXPathRoot , XML_IDENTIFIER));
					Latitude = ReadXMLNodeAsDouble(objDOM,string.Concat(strXPathRoot, XML_LATITUDE));
					Longitude = ReadXMLNodeAsDouble(objDOM, string.Concat(strXPathRoot, XML_LONGITUDE));
					Comment = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_COMMENT));
					Symbol = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_SYMBOL));
					Icon = ReadXMLNodeAsString(objDOM,string.Concat(strXPathRoot , XML_ICON));
					Colour = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_COLOUR));
					Display = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_DISPLAY));
					ProximityDistance = ReadXMLNodeAsSingle(objDOM, string.Concat(strXPathRoot, XML_PROX_DIST));
					ProximityIndex = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_PROX_INDEX));
					Altitude = ReadXMLNodeAsSingle(objDOM, string.Concat(strXPathRoot, XML_ALTITUDE));
					Depth = ReadXMLNodeAsSingle(objDOM, string.Concat(strXPathRoot, XML_DEPTH));
					Class = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_CLASS));
					
					//need to get the child elements
					//					Subclass = objDOM.SelectSingleNode(string.Concat(strXPathRoot, XML_SUB_CLASS)).InnerText;
					try
					{
						objNode = objDOM.SelectSingleNode(string.Concat(strXPathRoot, XML_SUB_CLASS));
						
						foreach(XmlElement objChildNode in objNode.ChildNodes)
						{
							int intIndex = 0;
						
							//need to evaluate the string returned from get attribute
                            intIndex = Convert.ToInt32(objChildNode.GetAttribute(XML_INDEX), CultureInfo.InvariantCulture);
                            m_subclass.Add(Convert.ToByte(objChildNode.InnerText, CultureInfo.InvariantCulture));
						}
					}
                    catch (NullReferenceException ex)
					{
					}

					Attributes = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_ATTRIBUTES));
					Link = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_LINK));
					RouteLinkClass = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_ROUTE_LINK_CLASS));

					//need to get the child elements
//					RouteLinkSubclass = objDOM.SelectSingleNode(string.Concat(strXPathRoot, XML_ROUTE_LINK_SUB_CLASS)).InnerText;
					try
					{
						objNode = objDOM.SelectSingleNode(string.Concat(strXPathRoot, XML_ROUTE_LINK_SUB_CLASS));
					
						foreach(XmlElement objChildNode in objNode.ChildNodes)
						{
							int intIndex = 0;
						
							//need to evaluate the string returned from get attribute
                            intIndex = Convert.ToInt32(objChildNode.GetAttribute(XML_INDEX), CultureInfo.InvariantCulture);
                            m_routeLinkSubclass.Add(Convert.ToByte(objChildNode.InnerText, CultureInfo.InvariantCulture));
						}
					}
                    catch (NullReferenceException ex)
					{
					}

					RouteLinkIdentifier = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_ROUTE_LINK_INDENTIFIER));
					State = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_STATE));
					Country = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_COUNTRY));
					City = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_CITY));
					Address = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_ADDRESS));
					Facility = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_FACILITY));
					Crossroad = ReadXMLNodeAsString(objDOM, string.Concat(strXPathRoot, XML_CROSS_ROAD));
					//Unused1 = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_UNUSED_1));
					//Unused2 = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_UNUSED_2));
					PacketType = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_PACKETTYPE));
					TimeENRoute = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_TIME_EN_ROUTE));
					Temperature = ReadXMLNodeAsSingle(objDOM, string.Concat(strXPathRoot, XML_TEMPERATURE));
					Timestamp = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_TIMESTAMP));
					Category = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_CATEGORY));

				}
			}
            catch (NullReferenceException e)
            {
				throw new XmlException(e.Message ,e);
			}

		}

//		public override string ToString()
//		{
//			System.Text.StringBuilder sbOutput = new System.Text.StringBuilder();
//
//			sbOutput.AppendFormat("{0}:={1}\n;",XML_IDENTIFIER,Identifier.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_LATITUDE,Latitude.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_LONGITUDE,Longitude.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_COMMENT,Comment.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_SYMBOL,Symbol.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_COLOUR,Colour.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_DISPLAY,Display.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_PROX_DIST,ProximityDistance.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_PROX_INDEX,ProximityIndex.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_ALTITUDE,Altitude.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_DEPTH,Depth.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_CLASS,Class.ToString(CultureInfo.InvariantCulture));
//			
//			sbOutput.AppendFormat("{0}:={1}\n",XML_SUB_CLASS,ByteToHex(Subclass));
//
//			sbOutput.AppendFormat("{0}:={1}\n",XML_ATTRIBUTES,Attributes.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_LINK,Link.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_ROUTE_LINK_CLASS,RouteLinkClass.ToString(CultureInfo.InvariantCulture));
//
//			sbOutput.AppendFormat("{0}:={1}\n",XML_ROUTE_LINK_SUB_CLASS,ByteToHex(RouteLinkSubclass));
//
//			sbOutput.AppendFormat("{0}:={1}\n",XML_ROUTE_LINK_INDENTIFIER,RouteLinkIdentifier.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_STATE,State.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_COUNTRY,Country.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_CITY,City.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_ADDRESS,Address.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_FACILITY,Facility.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_CROSS_ROAD,Crossroad.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_UNUSED_1,Unused1.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_UNUSED_2,Unused2.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_PACKETTYPE,PacketType.ToString(CultureInfo.InvariantCulture));
//			sbOutput.AppendFormat("{0}:={1}\n",XML_TIME_EN_ROUTE,TimeENRoute.ToString(CultureInfo.InvariantCulture));
//
//			return sbOutput.ToString(CultureInfo.InvariantCulture);
//		}
		/// <summary>
		/// Returns/Sets the Waypoint Sub Class. 
		/// </summary>
		public Subclass Subclass
		{
			get
			{
				return m_subclass;
			}
			set
			{
					m_subclass = value;
			}
		}
		/// <summary>
		/// Returns/Sets the Route Link Sub-Class. This Property describes the 
		/// link that this waypoint has with the Waypoint that precedes it. 
		/// Not used on all GPS devices.
		/// </summary>
		public Subclass RouteLinkSubclass
		{
			get
			{
				return m_routeLinkSubclass;
			}
			set
			{
				m_routeLinkSubclass = value;
			}
		}
        /// <summary>
        /// Overridden method. Returns true of the values of each
        /// of the properties are equal in value.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>boolean</returns>
        public override bool Equals(object obj)
		{
			return Equals(obj, false);
		}
        /// <summary>
        /// Overridden method. Returns true of the values of each
        /// of the properties are equal in value. Passing true
        /// to the positionOnly parameter will compare the Trackpoint
        /// in terms of the Latitude and Longitude only.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="positionOnly"></param>
        /// <returns>boolean</returns>
        public bool Equals(object obj, bool positionOnly)
		{
			try
			{
				//check the type first
				if(obj.GetType() != this.GetType() )
					return false;

				//cast it to a waypoint
				Waypoint objWaypoint = (Waypoint)obj;

                if (positionOnly)
				{
					if( objWaypoint.Identifier.ToUpper(CultureInfo.InvariantCulture) != Identifier.ToUpper(CultureInfo.InvariantCulture) ) return false;
					if( objWaypoint.Latitude != Latitude ) return false;
					if( objWaypoint.Longitude != Longitude ) return false;
				}
				else
				{
					if( objWaypoint.Address != Address ) return false; 
					if( objWaypoint.Altitude != Altitude ) return false;
					if( objWaypoint.Attributes != Attributes ) return false;
					if( objWaypoint.Category != Category ) return false;
					if( objWaypoint.City != City ) return false;
					if( objWaypoint.Class != Class ) return false;
					if( objWaypoint.Colour != Colour ) return false;;
					if( objWaypoint.Comment != Comment ) return false;
					if( objWaypoint.Country != Country ) return false;
					if( objWaypoint.Crossroad != Crossroad ) return false;
					if( objWaypoint.Depth != Depth ) return false;
					if( objWaypoint.Display != Display ) return false;
					if( objWaypoint.Facility != Facility ) return false;
					if( objWaypoint.Icon != Icon ) return false;
					if( objWaypoint.Identifier != Identifier ) return false;
					if( objWaypoint.Latitude != Latitude ) return false;
					if( objWaypoint.Link != Link ) return false;
					if( objWaypoint.Longitude != Longitude ) return false;
					if( objWaypoint.PacketType != PacketType ) return false;
					if( objWaypoint.ProximityDistance != ProximityDistance ) return false;
					if( objWaypoint.ProximityIndex != ProximityIndex ) return false;
					if( objWaypoint.RouteLinkClass != RouteLinkClass ) return false;
					if( objWaypoint.RouteLinkIdentifier != RouteLinkIdentifier ) return false;
					if( objWaypoint.State != State ) return false;
					if( objWaypoint.Symbol != Symbol ) return false;
					if( objWaypoint.Temperature != Temperature ) return false;
					if( objWaypoint.TimeENRoute != TimeENRoute ) return false;
					if( objWaypoint.Timestamp != Timestamp ) return false;
                    if( objWaypoint.Icon != Icon) return false;
                    if( objWaypoint.Subclass.ToString() != Subclass.ToString() ) return false;
                    if (objWaypoint.RouteLinkSubclass.ToString() != RouteLinkSubclass.ToString()) return false;

                    ////sub class is a byte array so we need to check each byte
                    //if( objWaypoint.Subclass.Length != Subclass.Length )
                    //    return false;

                    ////we now know thate there is an equal amount of bytes in each
                    //for(int index = 0; index < Subclass.Length; index++ )
                    //{
                    //    if( objWaypoint.Subclass[index] != Subclass[index] )
                    //        return false;
                    //}
				}

			}
			catch
			{
				throw;
			}
			finally
			{
			}
			//if we get here all must be the same.
			return true;
		}
        /// <summary>
        /// Overridden function. Retrieves a value that indicates the hash code value for the object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{

			//this value will be used for the overriden GetHashCode function and should
			//ensure that two object that are the same return the same Hash Code.
			//the hash code has to be imutable to is stored in a member variable.
			if( m_hashCode <= 0 )
				m_hashCode = Latitude.GetHashCode() ^ Longitude.GetHashCode();

			return m_hashCode;
		}

	}
}
