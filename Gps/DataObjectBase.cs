using System;
using System.Xml;
using System.Globalization;

namespace Waymex.Gps
{
	/// <summary>
	/// This is the Base Class for the GPS Data Objects e.g. Waypoint, Route etc.
	/// </summary>
	public abstract class DataObjectBase
	{
		internal DataObjectBase()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		internal double ReadXMLNodeAsDouble(XmlDocument objDocument,string strXpath)
		{
			double dblNodeValue = 0.0;

			try
			{
                dblNodeValue = Convert.ToDouble(ReadXMLNodeAsString(objDocument, strXpath), CultureInfo.InvariantCulture);
			}
			catch(NullReferenceException ex)
			{
				//ignore null errors
			}
			return dblNodeValue;
		}

		internal short ReadXMLNodeAsShort(XmlDocument objDocument,string strXpath)
		{
			short shtNodeValue = 0;

			try
			{
                shtNodeValue = Convert.ToInt16(ReadXMLNodeAsString(objDocument, strXpath), CultureInfo.InvariantCulture);
			}
			catch(NullReferenceException ex)
			{
				//ignore null errors
			}
			return shtNodeValue;
		}

		internal byte ReadXMLNodeAsByte(XmlDocument objDocument,string strXpath)
		{
			byte bytNodeValue = 0;

			try
			{
                bytNodeValue = Convert.ToByte(ReadXMLNodeAsString(objDocument, strXpath), CultureInfo.InvariantCulture);
			}
			catch(NullReferenceException ex)
			{
				//ignore null errors
			}
			return bytNodeValue;
		}

		internal int ReadXMLNodeAsInteger(XmlDocument objDocument,string strXpath)
		{
			int intNodeValue = 0;

			try
			{
                intNodeValue = Convert.ToInt32(ReadXMLNodeAsString(objDocument, strXpath), CultureInfo.InvariantCulture);
			}
			catch(NullReferenceException ex)
			{
				//ignore null errors
			}
			return intNodeValue;
		}

		internal float ReadXMLNodeAsSingle(XmlDocument objDocument,string strXpath)
		{
			float sglNodeValue = 0f;

			try
			{
                sglNodeValue = Convert.ToSingle(ReadXMLNodeAsString(objDocument, strXpath), CultureInfo.InvariantCulture);
			}
            catch (NullReferenceException ex)
			{
				//ignore null errors
			}
			return sglNodeValue;
		}

		internal bool ReadXMLNodeAsBoolean(XmlDocument objDocument,string strXpath)
		{
			bool blnNodeValue = false;

			try
			{
                blnNodeValue = Convert.ToBoolean(ReadXMLNodeAsString(objDocument, strXpath), CultureInfo.InvariantCulture);
			}
			catch(NullReferenceException ex)
			{
				//ignore null errors
			}
			return blnNodeValue;
		}

		internal string ReadXMLNodeAsString(XmlDocument objDocument,string strXpath)
		
		{
			XmlNode objNode = null;
			string strInnerText = "";

			try
			{
				//get the required node
				objNode = objDocument.SelectSingleNode(strXpath);
			
				//if the XPath returned a node then get the text value
				if(objNode != null)
					strInnerText = objNode.InnerText;
			
			}
			catch(NullReferenceException ex)
			{
				//ignore null errors
			}

			//belts and braces
			if(strInnerText == null)
				strInnerText = "";

			return strInnerText;	
		
		}

		internal XmlElement AddXMLNode(XmlElement objParentNode, string strName, string strValue, bool blnCData)
		{    //------------------------------------------------------------------------
			// ABSTRACT:     Adds an XML node as a child to the passed in node
			//
			// PARAMETERS:
			//
			// RETURNS:
			//
			// DESCRIPTION:
			//
			//-------------------------------------------------------------------------
			//

			//XmlDocument objDOM = new XmlDocument();       //used for the factory methods only
			XmlNode objChildNode = null;
			XmlText objText = null;
			XmlCDataSection objCData = null;

			//create new element
			objChildNode = objParentNode.OwnerDocument.CreateElement(strName);

			//dont append any text or cdata sections if there is no value
			if(strValue.Length  > 0)
			{
				//create text of Cdata as appropriate
				if(blnCData)
				{
					objCData = objParentNode.OwnerDocument.CreateCDataSection(strValue);

					//append the Cdata to the node
					objChildNode.AppendChild(objCData);
				}
				else
				{
					objText = objParentNode.OwnerDocument.CreateTextNode(strValue);

					//append the text to the node
					objChildNode.AppendChild(objText);
				}			
			}

			//append the new node as a child of the passed in node
			//objDOM.FirstChild.AppendChild(objChildNode);
			objParentNode.AppendChild(objChildNode);

			objCData = null;
			objText = null;

			return (XmlElement)objChildNode;

		}
		internal static string ByteToHex(byte[] ByteData)
		{
			//---------------------------------------------------------------------------
			//	DESCRIPTION:	This function converts an array of bytes to a string
			//					of hex digits representing those bytes.
			//
			//
			//	PARAMETERS:
			//				ByteData()  Array of bytes.
			//
			//	RETURNS:
			//				String      String of hex character pairs representing
			//							each byte in the byte array, separated
			//							with a space.
			//
			//---------------------------------------------------------------------------
		
			string sTemp = "";
			string sMessage = "";

				if(ByteData != null)
				{
					for(int f = 0; f < ByteData.Length; f++)
					{
						sTemp = ByteData[f].ToString("X"); //hex returns a string representing the passed number

						if(sTemp.Length < 2)
						{
							sTemp = string.Concat("0", sTemp);
						}
						
						sMessage = string.Concat(sMessage, " ", sTemp);
					}
					
					return sMessage.Trim();
				}
				else
				{
					return "";
				}
		}

	}
}
