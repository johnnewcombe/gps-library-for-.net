using System;
using Waymex.Gps.Nmea;

namespace Waymex.Gps
{

    /// <summary>
    /// This class is the event arguments class used to poassargument values in the ReceiveSentence event.
    /// </summary>
    public class ReceiveSentenceEventArgs : EventArgs
    {
        private Sentence m_sentence;

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        public ReceiveSentenceEventArgs(Sentence sentence)
        {
            m_sentence = sentence;
        }
        /// <summary>
        /// Returns the NMEA sentence as a typed object (Sentence.
        /// </summary>
        public Sentence Sentence
        {
            get
            {
                return m_sentence;
            }
        }
    }
}