using System;
using System.Xml;
using System.Globalization;


namespace Waymex.Gps
{

	/// <summary>
	/// This object forms part of the LapCollection object which is returned by 
    /// the GetLaps method of the GarminDevice object.
	/// </summary>
	/// <remarks>
    /// <para>Garmin Devices Only.</para>
    /// <para>
	/// In most cases not all of the Properties of the Lap object are used. The Properties
	/// actually used are determined by the protocol employed by the connected GPS Device.
	/// Below is a list of which Properties are supported by Protocol. Please note that even though a
	/// Property is identified as being supported does not mean that it will actually be used/populated.
	/// </para>
	/// <br/><br/>
	/// <para>
	/// <b>Protocol D906</b>
	/// </para>
	/// <list type="table">
	/// <listheader><term>Property</term><description>Type</description></listheader>
	/// <item><term>StartTime</term><description>Int32</description></item>
	/// <item><term>TotalTime</term><description>Int32</description></item>
	/// <item><term>TotalDistance</term><description>Single</description></item>
	/// <item><term>BeginLatitude</term><description>Double</description></item>
	/// <item><term>EndLatitude</term><description>Double</description></item>
	/// <item><term>BeginLongitude</term><description>Double</description></item>
	/// <item><term>EndLongitude</term><description>Double</description></item>
	/// <item><term>Calories</term><description>Int16</description></item>
	/// <item><term>TrackIndex</term><description>byte</description></item>
	/// </list>
	/// <para>
	/// The 'TrackIndex' Property values are as follows;
	/// </para>
	/// <list type="table">
	/// <listheader><term>Value</term><description>Description</description></listheader>
	/// <item><term>0 � 252</term><description>The lap is the last in its run. The track index is valid and can be used to lookup the track and associate it with the run.</description></item>
	/// <item><term>253 � 254</term><description>The lap is the last in its run; however, the run has no associated track.</description></item>
	/// <item><term>255</term><description>The lap may or may not be the last in its run. The track for the run is any track not already associated with a run.</description></item>
	/// </list>
    /// <para>
    /// <b>Protocol D1001</b>
    /// </para>
    /// <list type="table">
    /// <listheader><term>Property</term><description>Type</description></listheader>
    /// <item><term>Index</term><description>Int32</description></item>
    /// <item><term>StartTime</term><description>Int32</description></item>
    /// <item><term>TotalTime</term><description>Int32</description></item>
    /// <item><term>TotalDistance</term><description>Single</description></item>
    /// <item><term>MaxSpeed</term><description>Single</description></item>
    /// <item><term>BeginLatitude</term><description>Double</description></item>
    /// <item><term>EndLatitude</term><description>Double</description></item>
    /// <item><term>BeginLongitude</term><description>Double</description></item>
    /// <item><term>EndLongitude</term><description>Double</description></item>
    /// <item><term>Calories</term><description>Int16</description></item>
    /// <item><term>AverageHeartRate</term><description>byte</description></item>
    /// <item><term>MaxHeartRate</term><description>byte</description></item>
    /// <item><term>Intensity</term><description>byte</description></item>
    /// </list>
    /// <para>
    /// <b>Protocol D1011</b>
    /// </para>
    /// <list type="table">
    /// <listheader><term>Property</term><description>Type</description></listheader>
    /// <item><term>Index</term><description>Int32</description></item>
    /// <item><term>StartTime</term><description>Int32</description></item>
    /// <item><term>TotalTime</term><description>Int32</description></item>
    /// <item><term>TotalDistance</term><description>Single</description></item>
    /// <item><term>MaxSpeed</term><description>Single</description></item>
    /// <item><term>BeginLatitude</term><description>Double</description></item>
    /// <item><term>EndLatitude</term><description>Double</description></item>
    /// <item><term>BeginLongitude</term><description>Double</description></item>
    /// <item><term>EndLongitude</term><description>Double</description></item>
    /// <item><term>Calories</term><description>Int16</description></item>
    /// <item><term>AverageHeartRate</term><description>byte</description></item>
    /// <item><term>MaxHeartRate</term><description>byte</description></item>
    /// <item><term>Intensity</term><description>byte</description></item>
    /// <item><term>AverageCadence</term><description>byte</description></item>
    /// <item><term>TriggerMethod</term><description>byte</description></item>
    /// </list>
    /// <para>
    /// <b>Protocol D1015</b>
    /// </para>
    /// <list type="table">
    /// <listheader><term>Property</term><description>Type</description></listheader>
    /// <item><term>Index</term><description>Int32</description></item>
    /// <item><term>StartTime</term><description>Int32</description></item>
    /// <item><term>TotalTime</term><description>Int32</description></item>
    /// <item><term>TotalDistance</term><description>Single</description></item>
    /// <item><term>MaxSpeed</term><description>Single</description></item>
    /// <item><term>BeginLatitude</term><description>Double</description></item>
    /// <item><term>EndLatitude</term><description>Double</description></item>
    /// <item><term>BeginLongitude</term><description>Double</description></item>
    /// <item><term>EndLongitude</term><description>Double</description></item>
    /// <item><term>Calories</term><description>Int16</description></item>
    /// <item><term>AverageHeartRate</term><description>byte</description></item>
    /// <item><term>MaxHeartRate</term><description>byte</description></item>
    /// <item><term>Intensity</term><description>byte</description></item>
    /// <item><term>AverageCadence</term><description>byte</description></item>
    /// <item><term>TriggerMethod</term><description>byte</description></item>
    /// <item><term>FatCalories</term><description>Int16</description></item>
    /// </list>	
    /// </remarks>
	public class Lap : DataObjectBase
	{

        private const string XML_ROOT = "gpslap";
        private const string XML_START_TIME = "start_time";
        private const string XML_TOTAL_TIME = "total_time";
        private const string XML_TOTAL_DISTANCE = "total_distance";
        private const string XML_BEGIN_LATITUDE = "begin_latitude";
        private const string XML_END_LATITUDE = "end_latitude";
        private const string XML_BEGIN_LONGITUDE = "begin_longitude";
        private const string XML_END_LONGITUDE = "end_longitude";
        private const string XML_CALORIES = "calories";
        private const string XML_TRACK_INDEX = "track_index";
        private const string XML_UNUSED_1 = "Unused1";
        private const string XML_INDEX = "index";
        private const string XML_AVERAGE_HEART_RATE = "averageHeartRate";
        private const string XML_MAX_HEART_RATE = "maxHeartRate";
        private const string XML_INTENSITY = "intensity";
        private const string XML_AVERAGE_CADENCE = "averageCadence";
        private const string XML_TRIGGER_METHOD = "triggerMethod";
        private const string XML_MAX_SPEED = "maxSpeed";
        private const string XML_FAT_CALORIES = "fatCalories";

        private int m_hashCode = -1;
        private int m_index = 0;
        private int m_startTime = 0;
        private int m_totalTime = 0;
        private float m_totalDistance = 0;
        private double m_beginLatitude = 0;
        private double m_endLatitude = 0;
        private double m_beginLongitude = 0;
        private double m_endLongitude = 0;
        private short m_calories = 0;
        private byte m_trackIndex = 0;
        private byte m_unused1;
        private byte m_unused2;
        private int m_unused3;
        private byte m_maxHeartRate = 0;
        private byte m_averageHeartRate = 0;
        private byte m_intensity = 0;
        private byte m_averageCadence = 0;
        private byte m_triggerMethod = 0;
        private float m_maxSpeed = 0;
        private short m_fatCalories;
       
        /// <summary>
        /// Returds/Sets the unique lap index.
        /// </summary>
        public int Index
        {
            get { return m_index; }
            set { m_index = value; }
        }
        /// <summary>
        /// Returds/Sets the maximum speed.
        /// </summary>
        public float MaxSpeed
        {
            get { return m_maxSpeed; }
            set { m_maxSpeed = value; }
        }
        /// <summary>
        /// Returns/Sets the average heart rate.
        /// </summary>
        public byte AverageHeartRate
        {
            get { return m_averageHeartRate; }
            set { m_averageHeartRate = value; }
        }
        /// <summary>
        /// Returns/Sets the maximum heart rate.
        /// </summary>
        public byte MaxHeartRate
        {
            get { return m_maxHeartRate; }
            set { m_maxHeartRate = value; }
        }
        /// <summary>
        /// Returns/Sets the intensity.
        /// </summary>
        public byte Intensity
        {
            get { return m_intensity; }
            set { m_intensity = value; }
        }
        /// <summary>
        /// Returns/Sets the average cadence.
        /// </summary>
        public byte AverageCadence
        {
            get { return m_averageCadence; }
            set { m_averageCadence = value; }
        }
        /// <summary>
        /// Returns/Sets the trigger method.
        /// </summary>
        public byte TriggerMethod
        {
            get { return m_triggerMethod; }
            set { m_triggerMethod = value; }
        }
        /// <summary>
		/// Returns/Sets the start tme of the lap.
		/// </summary>
		public int StartTime
        {
            get
            {
                return m_startTime;
            }
            set
            {
                m_startTime = value;
            }
        }

		/// <summary>
		/// Returns/Sets the total time for the lap.
		/// </summary>
		public int TotalTime
        {
            get
            {
                return m_totalTime;
            }
            set
            {
                m_totalTime = value;
            }
        }

		/// <summary>
		/// Returns/Sets the total distance of the lap.
		/// </summary>
		public float TotalDistance
        {
            get
            {
                return m_totalDistance;
            }
            set
            {
                m_totalDistance = value;
            }
        }

		/// <summary>
		/// Returns/Sets the Latitude of the start of the lap.
		/// </summary>
		public double BeginLatitude
        {
            get
            {
                return m_beginLatitude;
            }
            set
            {
                m_beginLatitude = value;
            }
        }

		/// <summary>
		/// Returns/Sets the Latitude of the end of the lap.
		/// </summary>
		public double EndLatitude
        {
            get
            {
                return m_endLatitude;
            }
            set
            {
                m_endLatitude = value;
            }
        }

		/// <summary>
		/// Returns/Sets the Logitude of the start of the lap.
		/// </summary>
		public double BeginLongitude
        {
            get
            {
                return m_beginLongitude;
            }
            set
            {
                m_beginLongitude = value;
            }
        }

		/// <summary>
		/// Returns/Sets the Logitude of the end of the lap.
		/// </summary>
		public double EndLongitude
        {
            get
            {
                return m_endLongitude;
            }
            set
            {
                m_endLongitude = value;
            }
        }

		/// <summary>
		/// Returns/Sets the calories used.
		/// </summary>
		public short Calories
        {
            get
            {
                return m_calories;
            }
            set
            {
                m_calories = value;
            }
        }

		/// <summary>
		/// Returns/Sets the track index.
		/// </summary>
		public byte TrackIndex
        {
            get
            {
                return m_trackIndex;
            }
            set
            {
                m_trackIndex = value;
            }
        }
        /// <summary>
        /// Fat Calories burned in this lap, 0xFFFF is returned if the value is invalid.
        /// </summary>
        public short FatCalories
        {
            get
            {
                return m_fatCalories;
            }
            set
            {
                m_fatCalories = value;
            }
        }
		/// <summary>
		/// Unused.
		/// </summary>
		internal byte Unused1
        {
            get
            {
                return m_unused1;
            }
            set
            {
                m_unused1 = value;
            }
        }
        /// <summary>
        /// Unused.
        /// </summary>
        internal byte Unused2
        {
            get
            {
                return m_unused2;
            }
            set
            {
                m_unused2 = value;
            }
        }
        /// <summary>
        /// Unused.
        /// </summary>
        internal int Unused3
        {
            get
            {
                return m_unused3;
            }
            set
            {
                m_unused3 = value;
            }
        }


				
		/// <summary>
		/// Returns an XML representation of the object.
		/// </summary>
		public string ToXml()
		{
			XmlDocument objXMLDOM = new XmlDocument();

			XmlElement objRootNode = null;
			
			try
			{
				//create the root node
				objRootNode = objXMLDOM.CreateElement(XML_ROOT);
			
				//append the root node to the document
				objXMLDOM.AppendChild(objRootNode);

				//create the child nodes
				AddXMLNode(objRootNode, XML_START_TIME, StartTime.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_TOTAL_TIME,TotalTime.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_TOTAL_DISTANCE, TotalDistance.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_BEGIN_LATITUDE, BeginLatitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_END_LATITUDE, EndLatitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_BEGIN_LONGITUDE, BeginLongitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_END_LONGITUDE, EndLongitude.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_CALORIES, Calories.ToString(CultureInfo.InvariantCulture), false);
				AddXMLNode(objRootNode, XML_TRACK_INDEX, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
				//AddXMLNode(objRootNode, XML_UNUSED_1, Unused1.ToString(CultureInfo.InvariantCulture), false);

                AddXMLNode(objRootNode, XML_INDEX, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_AVERAGE_HEART_RATE, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_MAX_HEART_RATE, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_INTENSITY, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_AVERAGE_CADENCE, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_TRIGGER_METHOD, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_MAX_SPEED, TrackIndex.ToString(CultureInfo.InvariantCulture), false);
                AddXMLNode(objRootNode, XML_FAT_CALORIES, FatCalories.ToString(CultureInfo.InvariantCulture), false);

			}

			catch(Exception e)
			{
				throw new XmlException(e.Message, e);
			}

			return objXMLDOM.OuterXml;
		}

		/// <summary>
		/// This method populates the object from XML.
		/// </summary>
        public void XmlLoad(string xml)
		{
			string strXPathRoot = "";
			XmlDocument objDOM = new XmlDocument();

			try
			{
                objDOM.LoadXml(xml);

				if(objDOM.FirstChild.Name == XML_ROOT)
				{
					strXPathRoot = string.Concat("//" , XML_ROOT , "/");

					//need to ignore type conversion errors and missing xml elements
					StartTime = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_START_TIME));
					TotalTime = ReadXMLNodeAsInteger(objDOM, string.Concat(strXPathRoot, XML_TOTAL_TIME));
					TotalDistance = ReadXMLNodeAsSingle(objDOM, string.Concat(strXPathRoot, XML_TOTAL_DISTANCE));
					BeginLatitude = ReadXMLNodeAsDouble(objDOM, string.Concat(strXPathRoot, XML_BEGIN_LATITUDE));
					EndLatitude = ReadXMLNodeAsDouble(objDOM, string.Concat(strXPathRoot, XML_END_LATITUDE));
					BeginLongitude = ReadXMLNodeAsDouble(objDOM, string.Concat(strXPathRoot, XML_BEGIN_LONGITUDE));
					EndLongitude = ReadXMLNodeAsDouble(objDOM, string.Concat(strXPathRoot, XML_END_LONGITUDE));
					Calories = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_CALORIES));
					TrackIndex = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_TRACK_INDEX));
					//Unused1 = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_UNUSED_1));

                    Index = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_INDEX));
                    AverageHeartRate = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_AVERAGE_HEART_RATE));
                    MaxHeartRate = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_MAX_HEART_RATE));
                    Intensity = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_INTENSITY));
                    AverageCadence = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_AVERAGE_CADENCE));
                    TriggerMethod = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_TRIGGER_METHOD));
                    MaxSpeed = ReadXMLNodeAsByte(objDOM, string.Concat(strXPathRoot, XML_MAX_SPEED));
                    FatCalories = ReadXMLNodeAsShort(objDOM, string.Concat(strXPathRoot, XML_FAT_CALORIES));
                
                }
			}
            catch (NullReferenceException e)
            {
				throw new XmlException(e.Message ,e);
			}

		}
        /// <summary>
        /// Overridden method. Returns true of the values of each
        /// of the properties are equal in value.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>boolean</returns>
        public override bool Equals(object obj)
		{
			try
			{
				//check the type first
				if(obj.GetType() != this.GetType() )
					return false;

				//cast it
				Lap objLap = (Lap)obj;

				if( objLap.BeginLatitude != BeginLatitude ) return false; 
				if( objLap.BeginLongitude  != BeginLongitude ) return false;
				if( objLap.Calories != Calories ) return false;
				if( objLap.EndLatitude != EndLatitude ) return false;
				if( objLap.EndLongitude != EndLongitude ) return false;
				if( objLap.StartTime != StartTime ) return false;
				if( objLap.TotalDistance != TotalDistance ) return false;
				if( objLap.TotalTime != TotalTime ) return false;
				if( objLap.TrackIndex != TrackIndex ) return false;
                if (objLap.Index != Index) return false;
                if (objLap.StartTime != StartTime) return false;
                if (objLap.AverageHeartRate != AverageHeartRate) return false;
                if (objLap.MaxHeartRate != MaxHeartRate) return false;
                if (objLap.Intensity != Intensity) return false;
                if (objLap.AverageCadence != AverageCadence) return false;
                if (objLap.TriggerMethod != TriggerMethod) return false;
                if (objLap.MaxSpeed != MaxSpeed) return false;

			}
			catch
			{
				throw;
			}
			finally
			{
			}
			//if we get here all must be the same.
			return true;
		}
        /// <summary>
        /// Overridden function. Retrieves a value that indicates the hash code value for the object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{

			//this value will be used for the overriden GetHashCode function and should
			//ensure that two object that are the same return the same Hash Code.
			//the hash code has to be imutable to is stored in a member variable.
			if( m_hashCode <= 0 )
				m_hashCode = StartTime.GetHashCode() ^ TotalTime.GetHashCode();

			return m_hashCode;
		}
	}
}
