using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Waymex.Resources
{
    internal class ResourceManager
    {
        /// <summary>
        /// Returns an embedded resource as a stream. 
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        public static Stream GetEmbeddedResource(String resourceName)
        {

            Assembly assembly = Assembly.GetExecutingAssembly();
            //string embeddedPath = string.Format("{0}.{1}", assembly.GetName().Name, resourceName);
            string embeddedPath = resourceName;

            string[] names = assembly.GetManifestResourceNames();
            Stream resource = assembly.GetManifestResourceStream(embeddedPath);
            return resource;

        }
    }
}
